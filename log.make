latexmk main
Latexmk: This is Latexmk, John Collins, 17 Jan. 2018, version: 4.55.
Rule 'xelatex': Rules & subrules not known to be previously run:
   xelatex
Rule 'xelatex': The following rules & subrules became out-of-date:
      'xelatex'
------------
Run number 1 of rule 'xelatex'
------------
------------
Running 'xelatex -shell-escape -file-line-error -halt-on-error -interaction=nonstopmode -no-pdf -synctex=1  -recorder  "main.tex"'
------------
Latexmk: applying rule 'xelatex'...
This is XeTeX, Version 3.14159265-2.6-0.99999 (TeX Live 2018) (preloaded format=xelatex)
 \write18 enabled.
entering extended mode
(./main.tex
LaTeX2e <2018-04-01> patch level 2
Babel <3.18> and hyphenation patterns for 84 language(s) loaded.
(./thuthesis.cls
Document Class: thuthesis 2021/05/31 7.2.3 Tsinghua University Thesis Template
(/usr/local/texlive/2018/texmf-dist/tex/generic/iftex/iftex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvdefinekeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ltxcmds.sty))
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvsetkeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/infwarerr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/etexcmds.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifluatex.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/kvoptions.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/keyval.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexbook.cls
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3-code.tex)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/l3xdvipdfmx.def))
Document Class: ctexbook 2018/01/28 v2.4.12 Chinese adapter for class book (CTE
X)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xparse/xparse.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/l3keys2e/l3keys2e.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexpatch.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fix-cm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ts1enc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ms/everysel.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctexopts.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/book.cls
Document Class: book 2014/09/29 v1.4h Standard LaTeX document class
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/bk12.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/engine/ctex-engine-xetex.def
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xtemplate/xtemplate.st
y) (/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec-xetex.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fontenc.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/tuenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJKfntef.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/ulem/ulem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/environ/environ.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/trimspaces/trimspaces.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/cjk/texinput/CJKfntef.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber-utf8.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/scheme/ctex-scheme-plain-boo
k.def) (/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctex-cs4size.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctex.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/etoolbox/etoolbox.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/filehook/filehook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/geometry/geometry.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifpdf.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifvtex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/ifxetex/ifxetex.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fancyhdr/fancyhdr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/titlesec/titletoc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/notoccite/notoccite.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsmath.sty
For additional information on amsmath, use the `?' option.
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amstext.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsgen.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsbsy.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsopn.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphicx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphics.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/trig.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/graphics.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-def/xetex.def)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/subcaption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption3.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/pdfpages.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ifthen.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/calc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/eso-pic/eso-pic.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/atbegshi.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/xcolor/xcolor.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/color.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/ppxetex.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/enumitem/enumitem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/footmisc/footmisc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/soul/soul.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/array.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/booktabs/booktabs.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/url/url.sty)) (./thusetup.tex
(/usr/local/texlive/2018/texmf-dist/tex/latex/amscls/amsthm.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-xetex.s
ty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-table.t
ex)))kpathsea:make_tex: Invalid filename `[XITSMath-Regular', contains '['

(/usr/local/texlive/2018/texmf-dist/tex/latex/threeparttable/threeparttable.sty
) (/usr/local/texlive/2018/texmf-dist/tex/latex/multirow/multirow.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/longtable.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/float/float.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithmic.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/natbib/natbib.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/bibunits/bibunits.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-generic.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/auxhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/pd1enc.def)
(/usr/local/texlive/2018/texmf-dist/tex/latex/latexconfig/hyperref.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/puenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hxetex.def
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/stringenc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/rerunfilecheck.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/psdextra.def)
(./main.aux) ABD: EverySelectfont initializing macros
*geometry* driver: auto-detecting
*geometry* detected driver: xetex
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/ltcaption.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/pdflscape.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/lscape.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator-basic-dicti
onary-English.dict)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx-abbreviations.cfg
) (/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/nameref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/gettitlestring.sty))

Package hyperref Warning: Rerun to get /PageLabels entry.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.

[1] [2] (./data/abstract.tex [1]) [2] [3] (./data/chap01.tex
第1章
[1]) (./data/chap02.tex [2] [3]
第2章

LaTeX Warning: Reference `fig:example' on page 4 undefined on input line 7.


LaTeX Warning: Reference `fig:subfig-a' on page 4 undefined on input line 25.


LaTeX Warning: Reference `fig:subfig-b' on page 4 undefined on input line 25.


LaTeX Warning: Reference `tab:three-line' on page 4 undefined on input line 41.


[4]

Package longtable Warning: Column widths have changed
(longtable)                in table 2.3 on input line 110.

[5]) (./data/chap03.tex [6]
第3章
[7]

LaTeX Warning: Reference `eq:example' on page 8 undefined on input line 69.

) (./data/chap04.tex [8]
第4章

Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 19.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 20.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 21.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 22.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 23.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 23.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 34.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 35.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 36.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 37.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 38.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 38.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 51.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 52.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 53.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 54.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 55.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 55.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 73.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 73.


Package natbib Warning: Citation `dupont1974bone' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `zhengkaiqing1987' on page 9 undefined on inpu
t line 73.


Package natbib Warning: Citation `jiangxizhou1980' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `jianduju1994' on page 9 undefined on input li
ne 73.


Package natbib Warning: Citation `merkt1995rotational' on page 9 undefined on i
nput line 73.


Package natbib Warning: Citation `mellinger1996laser' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `bixon1996dynamics' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `mahui1995' on page 9 undefined on input line 
73.


Package natbib Warning: Citation `carlson1981two' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `taylor1983scanning' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `taylor1981study' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `shimizu1983laser' on page 9 undefined on inpu
t line 73.


Package natbib Warning: Citation `atkinson1982experimental' on page 9 undefined
 on input line 73.


Package natbib Warning: Citation `kusch1975perturbations' on page 9 undefined o
n input line 73.


Package natbib Warning: Citation `guangxi1993' on page 9 undefined on input lin
e 73.


Package natbib Warning: Citation `huosini1989guwu' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `wangfuzhi1865songlun' on page 9 undefined on 
input line 73.


Package natbib Warning: Citation `zhaoyaodong1998xinshidai' on page 9 undefined
 on input line 73.


Package natbib Warning: Citation `biaozhunhua2002tushu' on page 9 undefined on 
input line 73.


Package natbib Warning: Citation `chubanzhuanye2004' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `who1970factors' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `peebles2001probability' on page 9 undefined o
n input line 73.


Package natbib Warning: Citation `baishunong1998zhiwu' on page 9 undefined on i
nput line 73.


Package natbib Warning: Citation `weinstein1974pathogenic' on page 9 undefined 
on input line 73.


Package natbib Warning: Citation `hanjiren1985lun' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `dizhi1936dizhi' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `tushuguan1957tushuguanxue' on page 9 undefine
d on input line 73.


Package natbib Warning: Citation `aaas1883science' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `fugang2000fengsha' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `xiaoyu2001chubanye' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `oclc2000about' on page 9 undefined on input l
ine 73.


Package natbib Warning: Citation `scitor2000project' on page 9 undefined on inp
ut line 73.

)
Underfull \hbox (badness 10000) in paragraph at lines 62--56
[]\TU/SongtiSC(0)/m/n/12.045 注 意，|  引 文 参 考 文 献 的 每 条 都 要 在 正 文 中 标
[9]
No file main.bbl.

Package natbib Warning: There were undefined citations.


Package longtable Warning: Table widths have changed. Rerun LaTeX.

(./main.aux (./bu.aux))

Package rerunfilecheck Warning: File `main.out' has changed.
(rerunfilecheck)                Rerun to get outlines right
(rerunfilecheck)                or use package `bookmark'.


LaTeX Warning: There were undefined references.


LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right.

 )
(see the transcript file for additional information)
Output written on main.xdv (14 pages, 120028 bytes).
SyncTeX written on main.synctex.gz.
Transcript written on main.log.
Latexmk: References changed.
Latexmk: Non-existent bbl file 'main.bbl'
 No file main.bbl.
Latexmk: References changed.
Latexmk: References changed.
Latexmk: Log file says output to 'main.xdv'
Latexmk: List of undefined refs and citations:
  Citation `aaas1883science' on page 9 undefined on input line 73
  Citation `atkinson1982experimental' on page 9 undefined on input line 73
  Citation `baishunong1998zhiwu' on page 9 undefined on input line 73
  Citation `biaozhunhua2002tushu' on page 9 undefined on input line 73
  Citation `bixon1996dynamics' on page 9 undefined on input line 73
  Citation `carlson1981two' on page 9 undefined on input line 73
  Citation `chubanzhuanye2004' on page 9 undefined on input line 73
  Citation `dizhi1936dizhi' on page 9 undefined on input line 73
  Citation `dupont1974bone' on page 9 undefined on input line 73
  Citation `fugang2000fengsha' on page 9 undefined on input line 73
  Citation `guangxi1993' on page 9 undefined on input line 73
  Citation `hanjiren1985lun' on page 9 undefined on input line 73
  Citation `huosini1989guwu' on page 9 undefined on input line 73
  Citation `jianduju1994' on page 9 undefined on input line 73
  Citation `jiangxizhou1980' on page 9 undefined on input line 73
  Citation `kusch1975perturbations' on page 9 undefined on input line 73
  Citation `mahui1995' on page 9 undefined on input line 73
  Citation `mellinger1996laser' on page 9 undefined on input line 73
  Citation `merkt1995rotational' on page 9 undefined on input line 73
  Citation `oclc2000about' on page 9 undefined on input line 73
  Citation `peebles2001probability' on page 9 undefined on input line 73
  Citation `scitor2000project' on page 9 undefined on input line 73
  Citation `shimizu1983laser' on page 9 undefined on input line 73
  Citation `taylor1981study' on page 9 undefined on input line 73
  Citation `taylor1983scanning' on page 9 undefined on input line 73
  Citation `tushuguan1957tushuguanxue' on page 9 undefined on input line 73
  Citation `wangfuzhi1865songlun' on page 9 undefined on input line 73
  Citation `weinstein1974pathogenic' on page 9 undefined on input line 73
  Citation `who1970factors' on page 9 undefined on input line 73
  Citation `xiaoyu2001chubanye' on page 9 undefined on input line 73
  Citation `zhangkun1994' on page 9 undefined on input line 19
  Citation `zhangkun1994' on page 9 undefined on input line 20
  Citation `zhangkun1994' on page 9 undefined on input line 21
  Citation `zhangkun1994' on page 9 undefined on input line 22
  Citation `zhangkun1994' on page 9 undefined on input line 23
  Citation `zhangkun1994' on page 9 undefined on input line 34
  Citation `zhangkun1994' on page 9 undefined on input line 35
  Citation `zhangkun1994' on page 9 undefined on input line 36
  Citation `zhangkun1994' on page 9 undefined on input line 37
  Citation `zhangkun1994' on page 9 undefined on input line 38
  Citation `zhangkun1994' on page 9 undefined on input line 51
  Citation `zhangkun1994' on page 9 undefined on input line 52
  Citation `zhangkun1994' on page 9 undefined on input line 53
  Citation `zhangkun1994' on page 9 undefined on input line 54
  Citation `zhangkun1994' on page 9 undefined on input line 55
  Citation `zhangkun1994' on page 9 undefined on input line 73
  Citation `zhaoyaodong1998xinshidai' on page 9 undefined on input line 73
  Citation `zhengkaiqing1987' on page 9 undefined on input line 73
  Citation `zhukezhen1973' on page 9 undefined on input line 23
  Citation `zhukezhen1973' on page 9 undefined on input line 38
  Citation `zhukezhen1973' on page 9 undefined on input line 55
  Citation `zhukezhen1973' on page 9 undefined on input line 73
  Reference `eq:example' on page 8 undefined on input line 69
  Reference `fig:example' on page 4 undefined on input line 7
  Reference `fig:subfig-a' on page 4 undefined on input line 25
  Reference `fig:subfig-b' on page 4 undefined on input line 25
  Reference `tab:three-line' on page 4 undefined on input line 41
=== TeX engine is 'XeTeX'
Latexmk: Found bibliography file(s) [ref/refs.bib]
Latexmk: Summary of warnings:
  Latex failed to resolve 6 reference(s)
  Latex failed to resolve 52 citation(s)
Rule 'bibtex main': File changes, etc:
   Changed files, or newly in use since previous run(s):
      'main.aux'
   Non-existent destination files:
      'main.bbl'
------------
Run number 1 of rule 'bibtex main'
------------
------------
Running 'bibtex  "main"'
------------
Latexmk: applying rule 'bibtex main'...
For rule 'bibtex main', running '&run_bibtex(  )' ...
This is BibTeX, Version 0.99d (TeX Live 2018)
The top-level auxiliary file: main.aux
The style file: thuthesis-numeric.bst
A level-1 auxiliary file: bu.aux
Database file #1: ref/refs.bib
Warning--empty year in oclc2000about
Warning--empty year in oclc2000about
(There were 2 warnings)
Rule 'xelatex': File changes, etc:
   Changed files, or newly in use since previous run(s):
      'bu.aux'
      'main.aux'
      'main.bbl'
      'main.out'
------------
Run number 2 of rule 'xelatex'
------------
------------
Running 'xelatex -shell-escape -file-line-error -halt-on-error -interaction=nonstopmode -no-pdf -synctex=1  -recorder  "main.tex"'
------------
Latexmk: applying rule 'xelatex'...
This is XeTeX, Version 3.14159265-2.6-0.99999 (TeX Live 2018) (preloaded format=xelatex)
 \write18 enabled.
entering extended mode
(./main.tex
LaTeX2e <2018-04-01> patch level 2
Babel <3.18> and hyphenation patterns for 84 language(s) loaded.
(./thuthesis.cls
Document Class: thuthesis 2021/05/31 7.2.3 Tsinghua University Thesis Template
(/usr/local/texlive/2018/texmf-dist/tex/generic/iftex/iftex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvdefinekeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ltxcmds.sty))
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvsetkeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/infwarerr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/etexcmds.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifluatex.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/kvoptions.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/keyval.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexbook.cls
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3-code.tex)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/l3xdvipdfmx.def))
Document Class: ctexbook 2018/01/28 v2.4.12 Chinese adapter for class book (CTE
X)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xparse/xparse.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/l3keys2e/l3keys2e.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexpatch.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fix-cm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ts1enc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ms/everysel.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctexopts.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/book.cls
Document Class: book 2014/09/29 v1.4h Standard LaTeX document class
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/bk12.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/engine/ctex-engine-xetex.def
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xtemplate/xtemplate.st
y) (/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec-xetex.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fontenc.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/tuenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJKfntef.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/ulem/ulem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/environ/environ.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/trimspaces/trimspaces.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/cjk/texinput/CJKfntef.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber-utf8.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/scheme/ctex-scheme-plain-boo
k.def) (/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctex-cs4size.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctex.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/etoolbox/etoolbox.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/filehook/filehook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/geometry/geometry.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifpdf.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifvtex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/ifxetex/ifxetex.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fancyhdr/fancyhdr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/titlesec/titletoc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/notoccite/notoccite.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsmath.sty
For additional information on amsmath, use the `?' option.
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amstext.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsgen.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsbsy.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsopn.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphicx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphics.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/trig.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/graphics.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-def/xetex.def)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/subcaption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption3.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/pdfpages.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ifthen.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/calc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/eso-pic/eso-pic.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/atbegshi.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/xcolor/xcolor.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/color.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/ppxetex.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/enumitem/enumitem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/footmisc/footmisc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/soul/soul.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/array.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/booktabs/booktabs.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/url/url.sty)) (./thusetup.tex
(/usr/local/texlive/2018/texmf-dist/tex/latex/amscls/amsthm.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-xetex.s
ty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-table.t
ex)))kpathsea:make_tex: Invalid filename `[XITSMath-Regular', contains '['

(/usr/local/texlive/2018/texmf-dist/tex/latex/threeparttable/threeparttable.sty
) (/usr/local/texlive/2018/texmf-dist/tex/latex/multirow/multirow.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/longtable.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/float/float.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithmic.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/natbib/natbib.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/bibunits/bibunits.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-generic.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/auxhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/pd1enc.def)
(/usr/local/texlive/2018/texmf-dist/tex/latex/latexconfig/hyperref.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/puenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hxetex.def
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/stringenc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/rerunfilecheck.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/psdextra.def)
(./main.aux (./bu.aux)) ABD: EverySelectfont initializing macros
*geometry* driver: auto-detecting
*geometry* detected driver: xetex
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/ltcaption.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/pdflscape.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/lscape.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator-basic-dicti
onary-English.dict)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx-abbreviations.cfg
) (/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/nameref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/gettitlestring.sty))
(./main.out) (./main.out)

Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.

[1] [2] (./data/abstract.tex [1]) [2] (./main.toc) [3] (./data/chap01.tex
第1章
[1]) (./data/chap02.tex [2] [3]
第2章
[4] [5]) (./data/chap03.tex [6]
第3章
[7]) (./data/chap04.tex [8]
第4章

Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 19.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 20.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 21.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 22.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 23.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 23.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 34.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 35.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 36.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 37.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 38.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 38.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 51.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 52.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 53.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 54.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 55.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 55.


Package natbib Warning: Citation `zhangkun1994' on page 9 undefined on input li
ne 73.


Package natbib Warning: Citation `zhukezhen1973' on page 9 undefined on input l
ine 73.


Package natbib Warning: Citation `dupont1974bone' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `zhengkaiqing1987' on page 9 undefined on inpu
t line 73.


Package natbib Warning: Citation `jiangxizhou1980' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `jianduju1994' on page 9 undefined on input li
ne 73.


Package natbib Warning: Citation `merkt1995rotational' on page 9 undefined on i
nput line 73.


Package natbib Warning: Citation `mellinger1996laser' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `bixon1996dynamics' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `mahui1995' on page 9 undefined on input line 
73.


Package natbib Warning: Citation `carlson1981two' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `taylor1983scanning' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `taylor1981study' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `shimizu1983laser' on page 9 undefined on inpu
t line 73.


Package natbib Warning: Citation `atkinson1982experimental' on page 9 undefined
 on input line 73.


Package natbib Warning: Citation `kusch1975perturbations' on page 9 undefined o
n input line 73.


Package natbib Warning: Citation `guangxi1993' on page 9 undefined on input lin
e 73.


Package natbib Warning: Citation `huosini1989guwu' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `wangfuzhi1865songlun' on page 9 undefined on 
input line 73.


Package natbib Warning: Citation `zhaoyaodong1998xinshidai' on page 9 undefined
 on input line 73.


Package natbib Warning: Citation `biaozhunhua2002tushu' on page 9 undefined on 
input line 73.


Package natbib Warning: Citation `chubanzhuanye2004' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `who1970factors' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `peebles2001probability' on page 9 undefined o
n input line 73.


Package natbib Warning: Citation `baishunong1998zhiwu' on page 9 undefined on i
nput line 73.


Package natbib Warning: Citation `weinstein1974pathogenic' on page 9 undefined 
on input line 73.


Package natbib Warning: Citation `hanjiren1985lun' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `dizhi1936dizhi' on page 9 undefined on input 
line 73.


Package natbib Warning: Citation `tushuguan1957tushuguanxue' on page 9 undefine
d on input line 73.


Package natbib Warning: Citation `aaas1883science' on page 9 undefined on input
 line 73.


Package natbib Warning: Citation `fugang2000fengsha' on page 9 undefined on inp
ut line 73.


Package natbib Warning: Citation `xiaoyu2001chubanye' on page 9 undefined on in
put line 73.


Package natbib Warning: Citation `oclc2000about' on page 9 undefined on input l
ine 73.


Package natbib Warning: Citation `scitor2000project' on page 9 undefined on inp
ut line 73.

)
Underfull \hbox (badness 10000) in paragraph at lines 62--56
[]\TU/SongtiSC(0)/m/n/12.045 注 意，|  引 文 参 考 文 献 的 每 条 都 要 在 正 文 中 标
[9] (./main.bbl [10]
Underfull \hbox (badness 4144) in paragraph at lines 168--174
[]\TU/TimesNewRoman(0)/m/n/10.53937 Weinstein L, Swertz M N.  Pathogenic proper
ties of invading microorganism[M]//
)

Package natbib Warning: There were undefined citations.

[11] (./main.aux (./bu.aux

Package natbib Warning: Citation(s) may have changed.
(natbib)                Rerun to get citations correct.

))

Package rerunfilecheck Warning: File `main.out' has changed.
(rerunfilecheck)                Rerun to get outlines right
(rerunfilecheck)                or use package `bookmark'.

 )
(see the transcript file for additional information)
Output written on main.xdv (16 pages, 271356 bytes).
SyncTeX written on main.synctex.gz.
Transcript written on main.log.
Latexmk: Found input bbl file 'main.bbl'
Latexmk: References changed.
Latexmk: References changed.
Latexmk: Log file says output to 'main.xdv'
Latexmk: List of undefined refs and citations:
  Citation `aaas1883science' on page 9 undefined on input line 73
  Citation `atkinson1982experimental' on page 9 undefined on input line 73
  Citation `baishunong1998zhiwu' on page 9 undefined on input line 73
  Citation `biaozhunhua2002tushu' on page 9 undefined on input line 73
  Citation `bixon1996dynamics' on page 9 undefined on input line 73
  Citation `carlson1981two' on page 9 undefined on input line 73
  Citation `chubanzhuanye2004' on page 9 undefined on input line 73
  Citation `dizhi1936dizhi' on page 9 undefined on input line 73
  Citation `dupont1974bone' on page 9 undefined on input line 73
  Citation `fugang2000fengsha' on page 9 undefined on input line 73
  Citation `guangxi1993' on page 9 undefined on input line 73
  Citation `hanjiren1985lun' on page 9 undefined on input line 73
  Citation `huosini1989guwu' on page 9 undefined on input line 73
  Citation `jianduju1994' on page 9 undefined on input line 73
  Citation `jiangxizhou1980' on page 9 undefined on input line 73
  Citation `kusch1975perturbations' on page 9 undefined on input line 73
  Citation `mahui1995' on page 9 undefined on input line 73
  Citation `mellinger1996laser' on page 9 undefined on input line 73
  Citation `merkt1995rotational' on page 9 undefined on input line 73
  Citation `oclc2000about' on page 9 undefined on input line 73
  Citation `peebles2001probability' on page 9 undefined on input line 73
  Citation `scitor2000project' on page 9 undefined on input line 73
  Citation `shimizu1983laser' on page 9 undefined on input line 73
  Citation `taylor1981study' on page 9 undefined on input line 73
  Citation `taylor1983scanning' on page 9 undefined on input line 73
  Citation `tushuguan1957tushuguanxue' on page 9 undefined on input line 73
  Citation `wangfuzhi1865songlun' on page 9 undefined on input line 73
  Citation `weinstein1974pathogenic' on page 9 undefined on input line 73
  Citation `who1970factors' on page 9 undefined on input line 73
  Citation `xiaoyu2001chubanye' on page 9 undefined on input line 73
  Citation `zhangkun1994' on page 9 undefined on input line 19
  Citation `zhangkun1994' on page 9 undefined on input line 20
  Citation `zhangkun1994' on page 9 undefined on input line 21
  Citation `zhangkun1994' on page 9 undefined on input line 22
  Citation `zhangkun1994' on page 9 undefined on input line 23
  Citation `zhangkun1994' on page 9 undefined on input line 34
  Citation `zhangkun1994' on page 9 undefined on input line 35
  Citation `zhangkun1994' on page 9 undefined on input line 36
  Citation `zhangkun1994' on page 9 undefined on input line 37
  Citation `zhangkun1994' on page 9 undefined on input line 38
  Citation `zhangkun1994' on page 9 undefined on input line 51
  Citation `zhangkun1994' on page 9 undefined on input line 52
  Citation `zhangkun1994' on page 9 undefined on input line 53
  Citation `zhangkun1994' on page 9 undefined on input line 54
  Citation `zhangkun1994' on page 9 undefined on input line 55
  Citation `zhangkun1994' on page 9 undefined on input line 73
  Citation `zhaoyaodong1998xinshidai' on page 9 undefined on input line 73
  Citation `zhengkaiqing1987' on page 9 undefined on input line 73
  Citation `zhukezhen1973' on page 9 undefined on input line 23
  Citation `zhukezhen1973' on page 9 undefined on input line 38
  Citation `zhukezhen1973' on page 9 undefined on input line 55
  Citation `zhukezhen1973' on page 9 undefined on input line 73
=== TeX engine is 'XeTeX'
Latexmk: Found bibliography file(s) [ref/refs.bib]
Latexmk: Summary of warnings:
  Latex failed to resolve 52 citation(s)
Rule 'bibtex main': File changes, etc:
   Changed files, or newly in use since previous run(s):
      'bu.aux'
      'main.aux'
------------
Run number 2 of rule 'bibtex main'
------------
------------
Running 'bibtex  "main"'
------------
Latexmk: applying rule 'bibtex main'...
For rule 'bibtex main', running '&run_bibtex(  )' ...
This is BibTeX, Version 0.99d (TeX Live 2018)
The top-level auxiliary file: main.aux
The style file: thuthesis-numeric.bst
A level-1 auxiliary file: bu.aux
Database file #1: ref/refs.bib
Warning--empty year in oclc2000about
Warning--empty year in oclc2000about
(There were 2 warnings)
Rule 'xelatex': File changes, etc:
   Changed files, or newly in use since previous run(s):
      'bu.aux'
      'main.aux'
      'main.out'
      'main.toc'
------------
Run number 3 of rule 'xelatex'
------------
------------
Running 'xelatex -shell-escape -file-line-error -halt-on-error -interaction=nonstopmode -no-pdf -synctex=1  -recorder  "main.tex"'
------------
Latexmk: applying rule 'xelatex'...
This is XeTeX, Version 3.14159265-2.6-0.99999 (TeX Live 2018) (preloaded format=xelatex)
 \write18 enabled.
entering extended mode
(./main.tex
LaTeX2e <2018-04-01> patch level 2
Babel <3.18> and hyphenation patterns for 84 language(s) loaded.
(./thuthesis.cls
Document Class: thuthesis 2021/05/31 7.2.3 Tsinghua University Thesis Template
(/usr/local/texlive/2018/texmf-dist/tex/generic/iftex/iftex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvdefinekeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ltxcmds.sty))
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/kvsetkeys.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/infwarerr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/etexcmds.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifluatex.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/kvoptions.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/keyval.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexbook.cls
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/expl3-code.tex)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3kernel/l3xdvipdfmx.def))
Document Class: ctexbook 2018/01/28 v2.4.12 Chinese adapter for class book (CTE
X)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xparse/xparse.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/l3keys2e/l3keys2e.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctexpatch.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fix-cm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ts1enc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ms/everysel.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctexopts.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/book.cls
Document Class: book 2014/09/29 v1.4h Standard LaTeX document class
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/bk12.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/engine/ctex-engine-xetex.def
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/l3packages/xtemplate/xtemplate.st
y) (/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec-xetex.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/fontenc.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/tuenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fontspec/fontspec.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJK.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/xelatex/xecjk/xeCJKfntef.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/ulem/ulem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/environ/environ.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/trimspaces/trimspaces.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/cjk/texinput/CJKfntef.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/zhnumber/zhnumber-utf8.cfg))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/scheme/ctex-scheme-plain-boo
k.def) (/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/ctex-cs4size.clo))
(/usr/local/texlive/2018/texmf-dist/tex/latex/ctex/config/ctex.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/etoolbox/etoolbox.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/filehook/filehook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/geometry/geometry.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifpdf.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/ifvtex.sty)
(/usr/local/texlive/2018/texmf-dist/tex/generic/ifxetex/ifxetex.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/fancyhdr/fancyhdr.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/titlesec/titletoc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/notoccite/notoccite.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsmath.sty
For additional information on amsmath, use the `?' option.
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amstext.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsgen.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsbsy.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/amsmath/amsopn.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphicx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/graphics.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/trig.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/graphics.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-def/xetex.def)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/subcaption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/caption3.sty)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/pdfpages.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/base/ifthen.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/calc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/eso-pic/eso-pic.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/atbegshi.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/xcolor/xcolor.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics-cfg/color.cfg)))
(/usr/local/texlive/2018/texmf-dist/tex/latex/pdfpages/ppxetex.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/enumitem/enumitem.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/footmisc/footmisc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/soul/soul.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/array.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/booktabs/booktabs.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/url/url.sty)) (./thusetup.tex
(/usr/local/texlive/2018/texmf-dist/tex/latex/amscls/amsthm.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-xetex.s
ty
(/usr/local/texlive/2018/texmf-dist/tex/latex/unicode-math/unicode-math-table.t
ex)))kpathsea:make_tex: Invalid filename `[XITSMath-Regular', contains '['

(/usr/local/texlive/2018/texmf-dist/tex/latex/threeparttable/threeparttable.sty
) (/usr/local/texlive/2018/texmf-dist/tex/latex/multirow/multirow.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/tools/longtable.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithm.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/float/float.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/algorithms/algorithmic.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/natbib/natbib.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/bibunits/bibunits.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-hyperref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/hobsub-generic.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/auxhook.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/pd1enc.def)
(/usr/local/texlive/2018/texmf-dist/tex/latex/latexconfig/hyperref.cfg)
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/puenc.def))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/hxetex.def
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/stringenc.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/rerunfilecheck.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/psdextra.def)
(./main.aux (./bu.aux)) ABD: EverySelectfont initializing macros
*geometry* driver: auto-detecting
*geometry* detected driver: xetex
(/usr/local/texlive/2018/texmf-dist/tex/latex/caption/ltcaption.sty)
(/usr/local/texlive/2018/texmf-dist/tex/latex/oberdiek/pdflscape.sty
(/usr/local/texlive/2018/texmf-dist/tex/latex/graphics/lscape.sty))
(/usr/local/texlive/2018/texmf-dist/tex/latex/translator/translator-basic-dicti
onary-English.dict)
(/usr/local/texlive/2018/texmf-dist/tex/latex/siunitx/siunitx-abbreviations.cfg
) (/usr/local/texlive/2018/texmf-dist/tex/latex/hyperref/nameref.sty
(/usr/local/texlive/2018/texmf-dist/tex/generic/oberdiek/gettitlestring.sty))
(./main.out) (./main.out)

Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 21.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.


Package hyperref Warning: Token not allowed in a PDF string (Unicode):
(hyperref)                removing `math shift' on input line 25.

[1] [2] (./data/abstract.tex [1]) [2] (./main.toc) [3] (./data/chap01.tex
第1章
[1]) (./data/chap02.tex [2] [3]
第2章
[4] [5]) (./data/chap03.tex [6]
第3章
[7]) (./data/chap04.tex [8]
第4章
) [9] (./main.bbl [10]
Underfull \hbox (badness 4144) in paragraph at lines 168--174
[]\TU/TimesNewRoman(0)/m/n/10.53937 Weinstein L, Swertz M N.  Pathogenic proper
ties of invading microorganism[M]//
) [11] (./main.aux (./bu.aux)) )
(see the transcript file for additional information)
Output written on main.xdv (16 pages, 280436 bytes).
SyncTeX written on main.synctex.gz.
Transcript written on main.log.
Latexmk: Found input bbl file 'main.bbl'
Latexmk: Log file says output to 'main.xdv'
=== TeX engine is 'XeTeX'
Latexmk: Found bibliography file(s) [ref/refs.bib]
Rule 'xdvipdfmx': File changes, etc:
   Changed files, or newly in use since previous run(s):
      'main.xdv'
------------
Run number 1 of rule 'xdvipdfmx'
------------
------------
Running 'xdvipdfmx -q -E -o "main.pdf"  "main.xdv"'
------------
Latexmk: applying rule 'xdvipdfmx'...
For rule 'xdvipdfmx', running '&do_viewfile(  )' ...
Latexmk: All targets (main.pdf main.xdv) are up-to-date

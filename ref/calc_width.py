def width(x):
    """Input: branching fraction
    output: width in 10^-14 GeV
    """
    hbar = 6.582 * 10**(-22) * 10**6 * 10**(-9)
    #tau = 256*10**(-15) # Xicc++
    #tau = 45*10**(-15) # Xicc+
    #tau = 128*10**(-15) # Omega_cc+
    #tau = 256*10**(-15) # Xicc++
    #tau = 140*10**(-15) # Xicc+
    #tau = 180*10**(-15) # Xicc+
    #tau = 256*10**(-15) # Xicc++
    tau = 45*10**(-15) # Xicc+
    return x*hbar/tau*10**(14)

if __name__=="__main__":
    import sys
    br = float(sys.argv[1])
    print(width(br))

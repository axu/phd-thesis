% mass measurement

In this section, 
the procedure of measuring the invariant mass of \Xiccp 
with $\Xiccp\to\Lc\Km\pip$ decay is established, 
following the strategy used in the $\Xiccpp\to\Lc\Km\pip\pip$ 
analysis~\inlinecite{LHCb-PAPER-2017-018}.
The numbers given below are from the studies performed using 2016 data and simulation 
if not otherwise specified.

The $\Xiccp$ invariant mass is measured by fitting to the invariant-mass distribution 
of $\Lc\Km\pip$ final state.
The mass difference corrected by the known $\Lc$ mass
\begin{equation}
  \label{eq:mXicc}
  m(\Lc\Km\pip) \equiv M([\proton\Km\pip]_{\Lc}\Km\pip)
                       - M([\proton\Km\pip]_{\Lc})
                       + M_{\mathrm{PDG}}(\Lc)
\end{equation}
is used in the study, 
where $M([\proton\Km\pip]_{\Lc}\Km\pip)$ is
the reconstructed mass of the \Xiccp candidate,
$M([\proton\Km\pip]_{\Lc})$ is the reconstructed mass of the \Lc candidate
with kinematic and geometric refit (DecayTreeFitter)
to constrain $\Xiccp$ candidate pointing to its associated primary vertex,
and $M_{\mathrm{PDG}}(\Lc)$ is the known value of the \Lc mass~\inlinecite{PDG2018}.
The kinematic refitting helps to improve the mass resolution and perform
the momentum scaling correction.

The shape of the signal component is studied with fully simulated $\Xiccp$ sample.
The $m(\Lc\Km\pip)$ distribution can be well described 
by a Gaussian distribution plus double-sided crystal ball function (DSCB) 
sharing the mean value $\mu$
\begin{equation}
  \mathcal{P}(x) = 
  f\times\mathcal{P}_{\mathrm{Gaussian}}(x) + 
  (1-f)\times\mathcal{P}_{\mathrm{DSCB}}(x)
\end{equation}
as shown in Fig.~\ref{fig:mXicc_MC}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/mass/Xiccshape_f35.pdf}
  \caption{Distribution of $m(\Xiccp)$ in the MC sample, 
  along with the fit with a double-sided crystal ball function plus a Gaussian function.}
  \label{fig:mXicc_MC}
\end{figure}
The width of the DSCB function is scaled according to the width of the Gaussian function.
We define the effective resolution as 
$\sigma=\sqrt{f \times \sigma_1^2+(1-f) \times \sigma_2^2}$, 
where $\sigma_1$ ($\sigma_2$) is the width of Gaussian (DSCB) function
and $f$ is the fraction of the Gaussian function. 
Due to the strong correlation of $f$ with other parameters, 
it is fixed to $f=0.35$ according to simulation.
Resultant parameters are given in Table~\ref{tab:MCFitParameters:DSCB}.
\begin{table}[tb]
  \caption{Resultant parameters from the fit to the $\Xiccp$ invariant-mass distribution 
  in simulation, with a Gaussian function plus a DSCB function.}
  \centering
  \begin{tabular}{c l}
    \toprule
    Parameter & Value\\
    \midrule
    $\mu$       & $3621.555 \pm 0.081 \mev$ \\
    $\sigma$    & $5.09 \pm 0.09 \mev$ \\
    $R\equiv\frac{\sigma(\mathrm{DSCB})}{\sigma(\mathrm{Gaussian})}$ &$1.46 \pm 0.14$ \\
    $f$         & $0.35 \,(\mathrm{fixed})$ \\
    $a_L$       & $1.59 \pm 0.09$ \\
    $n_L$       & $4.04 \pm 0.82$ \\
    $a_R$       & $2.13 \pm 0.12$  \\
    $n_R$       & $2.83 \pm 0.57$ \\
    \bottomrule
  \end{tabular}
  \label{tab:MCFitParameters:DSCB}
\end{table}
Due to imperfect modelling of the detector resolution in simulation,
the expected mass resolution in data is different from that in simulation. 
When we fit to the RS sample, 
parameter $\mu$ and $\sigma$ of the signal component are free to vary.
Other parameters are fixed to values obtained in the fit to $\Xiccp$ simulation 
as shown in Table~\ref{tab:MCFitParameters:DSCB}.
The effects of the fraction of the Gaussian will be checked after
unblinding, by using the fit models obtained with different fraction of Gaussian.
%The results of the fits to the $\Xiccp$ simulation with different
%fraction of Gaussian (0.05-0.5) are summarized
%in App.~\ref{app:fit_model_checks}.

The mass resolution is a function of the energy release
$Q\equiv M(\Xiccp)-M(\Lc)-M(K^-)-M(\pip)$,
and therefore varies as a function of the \Xiccp invariant-mass.
To take into account this effect, the simulated signals generated
with $m(\Xiccp)=3518.7\mev$ and $m(\Xiccp)=3738.0\mev$ mass hypotheses
are used to determine the resolution at other $Q$ values.
Assuming that the resolution parameter of the signal lineshape scales 
linearly with the value of $m(\Xiccp)$,
the resolution can be parametrised as a linear function of $m(\Xiccp)$,
as is shown in Fig.~\ref{fig:MassResolution}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/yield/resolution.pdf}
  \caption{Mass resolution of the reconstructed $\Xiccp$ candidates at
    different $\Xiccp$ mass hypotheses.}
  \label{fig:MassResolution}
\end{figure}

The background component can be well described by the second
order Chebychev polynomial, as shown in Fig.~\ref{fig:mXicc_WS}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/yield/WS95_16.pdf}
  \caption{Distribution of $m(\Xiccp)$ in the WS sample, 
  along with the fit with a second-order Chebychev polynomial.}
  \label{fig:mXicc_WS}
\end{figure}
A mass window of $\pm150\mev$ around the signal peak will be used 
to measure the invariant mass, 
which is large enough compared with the mass resolution, 
and sufficient to constrain the background distribution.


\subsection{Systematic uncertainties in the mass measurement}%
\label{sub:systematic_uncertainties_in_the_mass_measurement}

Sources of systematic effects on the mass measurements, 
including the fit model, uncertainty on the momentum scaling calibration, 
the bias of reconstructed $\Lcp$ invariant mass,
the final state radiation, and 
the uncertainty on the known $\Lcp$ mass.
Contributions of the systematic uncertainties 
from the above sources are summarized in Table~\ref{tab:totalsys_mass}.
\begin{table}[tb]
  \centering
  \caption{Systematic uncertainties on the \Xiccp mass. Values with XXX needs
  to be determined with the observed signal peak.}
  \label{tab:totalsys_mass}
  \begin{tabular}{c c}
    \toprule
    Source & Systematics [\mev] \\
    \midrule
    Fit model                  & XXX \\
    Momentum scale calibration & 0.26\\
    Reconstructed \Lc mass     & 0.05\\
    Event selection            & 0.08\\
    Unknown \Xiccp lifetime    & 2.06\\
    FSR                        & XXX\\
    Uncertainty of \Lc mass    & 0.14 \\
    \midrule
    Total                      & XXX \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Fit model.}
\label{subsec:mass_fitModel}
The effects due to choice of the fit model will be checked after unblinding.
For the signal line shape, 
the difference between the default model 
(Gaussian plus double-sided Crystal ball function) and 
and an alternative one (the Iptia2 function),
will be taken as a systematic uncertainty. 
The difference of two models are 0.11$\mev$
according to simulation,
as shown in Fig.~\ref{fig:hypatiafit}.
As the simulation is known to describe well the final state radiation
and to describe the reconstructed mass resolution to a 10\% precision, 
the correlation between the mean and the resolution in the default fit model,
$-0.135$,
is considered to have negligible effects ($-0.135 \times 10\%$)
on the mass measurement in this analysis.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xiccpp_hypatiafit16.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xiccp_fitmodelcomparison.pdf}
  \caption{Distribution of (left) $m(\Xiccp)$ in simulation, along with the fit with Iptia2
  function, and (right) resultant line shapes of the default and alternative signal model.}
  \label{fig:hypatiafit}
\end{figure}

\paragraph{Momentum scale calibration.}
\label{subsubsec:momentum_scale}
Measured track momenta need to be calibrated to correct for possible biases,
due to the imperfect alignment of the tracking system, 
imprecise material map, and the uncertainty on the magnetic field. 
For Run~2 data,
the momentum scale is calibrated with large samples of 
detached \jpsi in $\jpsi \to \mumu$ decay and $\Bp \to \jpsi \Kp$
decays~\inlinecite{CERN-LHCb-INT-2017-008,LHCb-PAPER-2011-035,LHCb-PAPER-2013-011}.
The momentum correction factor $\xi$ is determined by scaling the track momenta
by $1-\xi$ so that the measured \jpsi and \Bp masses are
consistent with their world averages. 
The momentum correction is performed for each data taking period,
magnetic field polarity and track charge. 
The overall correction factor is about $3\times10^{-4}$.
The correction shifts the measured mass of a given particle, 
reconstructed through a certain decay,
by an amount of $\Delta \mu \approx Q \times \xi$, 
where $Q$ is the energy release of the decay channel.
The momentum scale calibration is cross-checked 
with masses of many other resonance decays including $b$-hadrons and quarkonia. 
The measured masses after the calibration agree with their known values
within $\pm3\times 10^{-4}$, 
which is quoted as the systematic uncertainty on the momentum correction factor.

The uncertainty on the momenta of the final-state tracks 
leads to the uncertainty of the reconstructed invariant mass of \Xiccp candidates. 
The effect is studied with simulated sample by varying the momentum correction factor $\xi$ 
by its uncertainty, $3\times10^{-4}$, 
and measuring $\Delta m$, the per-candidate mass shift of \Xiccp baryon.
A fit to the distribution of the shift 
with a Gaussian function is performed to extract the mean value, 
as is shown in Fig.~\ref{fig:ms}.
The mean mass shift is found to be $0.26\mev$,
which needs to be corrected by the ratio of the observed $Q$ value in data and 
the hypothetical $Q$ value in MC,
\begin{equation}
  r_{Q} \equiv Q_{\mathrm{data}}(\Xiccp)/Q_{\mathrm{MC}}(\Xiccp),
\end{equation}
where the $Q(\Xiccp) \equiv M(\Xiccp) - M_{\mathrm{PDG}}(\Lc) - M(\Km) - M(\pip)$.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/mass/MS.pdf}
  \caption{Gaussian fit to the distribution of per-candidate mass
  shift, $\Delta m$, for the variation of momentum correction factor by $1\sigma$.}
  \label{fig:ms}
\end{figure}

\paragraph{Bias due to the \Lcp reconstructed mass.}
\label{subsubsec:bias_lc_mass}
It is found in simulation that the reconstructed \Xiccp mass is
biased by a similar amount to the bias of \Lcp mass.
Therefore, the corrected mass
$m(\Lc K^-\pip) \equiv M(\Xiccp)-M(\Lcp)+M_{\mbox{PDG}}(\Lcp)$ 
can be used as an approximately unbiased estimator of \Xiccp invariant mass. 
To check whether the effect of \Lcp mass bias is removed or reduced by
using the mass difference variable, 
we calculate the mass of \Xiccp candidate (denoted as $M_{\mathrm{REC}}$) 
using \Lcp kinematics in combination with the true
kinematics of the accompanying $K^-$, $\pi^+$ tracks without resolution effect, 
subtracted by the reconstructed \Lcp mass. 
%The value is subtracted by MC input \Xiccp
%mass (denote $M_{TRUE}$) calculated using truth kinematics of $K^-$,
%$\pi^+$ and \Lcp, on an candidate by candidate basis. 
By using the true kinematics for accompanying $K^-$, $\pi^+$ tracks, 
the effect of \Lcp mass bias is isolated from other effects 
(momentum scaling of the accompanying tracks etc.). 
The difference of $M_{\mathrm{REC}}$ and the input value in simulation
is shown in the left plot in Fig~\ref{fig:lcrecbias},
from which the mean value of bias is determined to be $0.04\mev$.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/lcrec_bias.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/lc_masscon_bias.pdf}
  \caption{Distribution of (left) \Xiccp mass bias 
  (measured value subtracted by true value) 
  when the mass difference is used as estimator of
  the \Xiccp mass, along with the fit with a Double Gaussian function,
  and (right) \Xiccp mass bias (measured value subtracted by true value) 
  when the measured \Lcp mass is constrained to its known value, 
  along with the fit with a DSCB function.}
  \label{fig:lcrecbias}
\end{figure}

An alternative way to reduce the bias due to \Lcp reconstructed mass 
is to recalculate \Lcp momentum, constraining \Lcp mass to its known value 
using the kinematic refit. 
In this case, the \Xiccp mass is calculated using the refitted
\Lcp momentum combined with those of the other two tracks. 
This is studied in a similar way as discussed above, 
in which the refitted \Lcp momentum is combined the 
true momenta of accompanying $K^-$, $\pi^+$ tracks 
to calculate the \Xiccp mass.
The difference between the reconstructed and input \Xiccp mass is
shown in the right plot in Fig~\ref{fig:lcrecbias}, with an average bias of 0.05\mev.

\paragraph{Bias due to event selection.}
\label{subsubsec:bias_xicc_mass}
The measured \Xiccp mass could be biased due to 
the lifetime-based selections~\inlinecite{LHCb-PAPER-2017-018}.
In trigger selections, 
no further lifetime related variables are used 
when reconstructing \Xiccp candidate from $\Lc$, kaon, and pion candidates, 
except for $\mathrm{DIRA}>0$, 
while further offline selections explores the lifetime information.
These selections are expected to bias the \Xiccp mass 
similar to the effect discussed above for \Lcp mass. 
The effect is studied with the \Xiccp simulation, 
in which we intentionally remove all the decay time related requirements.
The distribution of the difference between the reconstructed mass and the input one 
for this ``unbiased'' sample is shown in the left plot of Fig~\ref{fig:xiccbias}, 
The distribution for events surviving offline selections including MVA is shown
in the right plot in Fig~\ref{fig:xiccbias}.
Comparing the fitted results in these two plots, 
it can be seen that there is an additional bias of
$\delta\mu=0.29\mev$ (with an input lifetime of $\tau=0.33$\ps).
%where the uncertainty, coming from limited
%statistics in the simulation sample, is calculates as
%$\sqrt{\sigma_{1}^2-\sigma_{2}^2}$. The $\sigma_1$ and $\sigma_2$ are the
%uncertainties of the mean mass before and after the offline selections.
The same comparison for 2012 data is shown in Figure~\ref{fig:xiccbias16Vs12}.
%The distribution of the difference between the reconstructed mass
%and the input one after all selections 
%is shown in Figure~\ref{fig:xiccbias16Vs12} for 2012.
It has very similar behavior to 2016 data.
%, and one can conclude that
%such bias has no visible dependence on the data-taking configuration
%within the current precision.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xicc_unbias_16_3pdf.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xicc_bias.pdf}
  \caption{Distribution of difference between the reconstructed mass and the input one
  (left) without applying cuts relating to decay time of \Xiccp, and
  (right) after all offline selections, including the MVA variable, 
  along with fits with DSCB function.}
  \label{fig:xiccbias}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xicc_bias.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/xicc_bias_12.pdf}
  \caption{Distribution of difference between the reconstructed mass
    and the input one in (left) 2016 and (right) 2012 simulated signal samples, 
    after all offline selections, including the MVA selection, 
    along with the fit with DSCB function.}
  \label{fig:xiccbias16Vs12}
\end{figure}

With a lifetime of 80\fs in simulation, the observed bias is 1.12 $\mev$ 
with MVA selection applied.
The bias depends on the lifetime of the $\Xiccp$ baryon. 
The effect of unknown lifetime is studied using other lifetime hypotheses
by reweighting the \Xiccp simulation sample to match other lifetime
value $\tau'$ according to
\begin{equation}
  w = \frac{1/\tau'exp^{-t/\tau'}}{1/\tau_{0}exp^{-t/\tau_{0}}},
\end{equation}
where $\tau'$ ($\tau_0$) is the target (input) lifetime 
and $t$ is the reconstructed decay time.
For each target lifetime, the mean mass bias is studied in the same
way as in the default case. 
Using the default MVA selections, the bias as a function of lifetime 
is shown in Fig~\ref{fig:lifetimebias}. 
As expected, the bias is getting smaller when the lifetime becomes larger.  
The largest variation of measured mass bias for lifetimes in the range $40<\tau<333\fs$,
which covers most theoretical predictions,
is 2.06 $\mev$.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/mass/bias_lifetime.pdf}
  \caption{Bias of fitted mass at different \Xiccp lifetime
    hypotheses for 2016 and 2012 simulated signal samples 
    with the same 2016 trigger selection.}
  \label{fig:lifetimebias}
\end{figure}

Concerning the selection-related bias and its dependence on \Xiccp lifetime, 
$1.12\mevcc$ is used to correct the measured \Xiccp mass 
with an uncertainty of $0.11\mev$ due to limited statistics in simulation for this study, 
and an uncertainty of $2.06\mev$ due to the lifetime dependence.

\paragraph{Final state radiation.}
\label{subsubsec:fsr}
In the $\XiccpDecay$ decay, 
soft photons are radiated by the final states. 
Since only charged tracks are used to reconstructed \Xiccp candidates, 
the observed mass is biased to smaller value due to 
loss of energy carried out by the photons. 
The effect is studied using simulated \Xiccp events with a smear procedure.
The $\Xiccp$ mass calculated with the true momenta of the final states 
in the simulated signal sample is smeared by a Gaussian 
with a mass resolution in the interested range 
(covering the expected mass resolution in data).
The mass constraints of the intermediate states 
are also considered when calculating the $\Xiccp$ mass. 
Then the mass difference defined previously is fitted 
to obtain the \Xiccp mean mass, 
with the same fit model used to fit the signal in data.

An example of the fit is shown in the left plot of Fig~\ref{fig:fsrbias}. 
In the right plot of Fig~\ref{fig:fsrbias}, 
the bias as a function of resolution $\sigma$ is shown, 
suggesting that the effect is quite small.
The distribution is fitted with a linear function, 
and the bias is XXX for the resolution observed in data.
\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/fsr_smear.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/deltaM_sigma.pdf}
  \caption{Distribution of (left) \Xiccp mass
  calculated from final-state track momenta smeared with a Gaussian function for resolution, 
  corrected by the reconstructed \Lcp mass calculated in the same way,
  and (right) mean \Xiccp mass as a function of the resolution.}
  \label{fig:fsrbias}
\end{figure}

\paragraph{Uncertainty due to \Lcp mass.}
\label{subsubsec:sys_lc_mass}
The \Lcp mass is known with an uncertainty of 0.14$\mevcc$~\inlinecite{PDG2020}, 
which should be propagated to measurement of \Xiccp mass. 
The study is performed using kinematic fit, 
where we modify the known \Lcp mass slightly and 
study the corresponding change of \Xiccp mass candidate by candidate. 
Since the kinematic for the \Xiccpp and \Xiccp are quite similar, 
we quote the results of the \Xiccpp variation as a function of \Lcp mass 
from the \Xiccpp mass measurement.%~\inlinecite{LHCb-ANA-2017-025}, 
For the uncertainty of $0.14\mev$ on \Lcp mass, 
the corresponding \Xiccp mass uncertainty is estimated to be $0.14\mev$, 
which is quoted as the systematic uncertainty.


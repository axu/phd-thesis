% efficiency

To calculate upper limits,
the efficiency of the reconstruction and selection of exclusive decays 
of \Lc and \Xiccpp baryons is estimated.

The total efficiency can be factorised into several components as
\begin{equation}
  \label{equ:eff}
  \varepsilon = \varepsilon^{\text{Acc}} \times \varepsilon^{\text{Sel$\mid$Acc}} \times \varepsilon^{\text{PID$\mid$Sel}} \times \varepsilon^{\text{MVA1$\mid$PID}} \times \varepsilon^{\text{MVA2$\mid$MVA1}} \times \varepsilon^{\text{Trigger$\mid$MVA2}} , 
\end{equation}
where the pieces are acceptance (Acc),
HLT2 and cut-based preselection (Sel),
PID requirements (PID), 
the first (MVA1) and second (MVA2) MVA, 
and the L0 and HLT2 trigger (Trigger).
The symbol ``$\mid$'' means conditional 
(\eg $\varepsilon^{\text{Sel$\mid$Acc}}$ is the efficiency 
given that the candidate has passed the acceptance requirement) 
and all selections are cumulative.
It should be noted that this factorisation dose not have to be the same
as the sequence in which these selections are applied.

\paragraph{Correction of \Xiccp kinematics.}%
\label{par:correction_of_xiccp_kinematics_}
Distributions of \pt of \Xiccp and the number of SPD hits (nSPDHits) 
in the signal simulation are reweighted 
according to the data-simulation discrepancy observed in
the $\Xiccpp\to\Lc\Km\pip\pip$ mode.
It is with a good reason to assume that
the production mechanism is the same for \Xiccp and \Xiccpp baryons,
given that these are isospin partners.
Distributions of \pt of \Xiccpp and nSPDHits with
background-subtracted data,
and simulation samples before and after the reweighting,
are shown in Fig.~\ref{fig:data-MC_pt}.
It is found that the discrepancy of the distribution of rapidity $y$ of \Xiccpp 
between data and simulation samples is small after the \pt and nSPDHits reweighting,
as illustrated in Fig.~\ref{fig:data-MC_y}.
Given the small number of signals in data,
no further reweighting of rapidity is applied.
The \pt of the mother particles and nSPDHits of \Lc and \Xiccpp control modes
are reweighted with the same procedure. 
This reweighting procedure removes the dependence on the \Xiccp kinematic distribution
in the event generator \genxicc.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/PT_Xicc++.pdf}
  \includegraphics[width=0.43\linewidth]{ana_xicc/efficiency/SPD_Xicc++.pdf}
  \includegraphics[width=0.46\linewidth]{ana_xicc/efficiency/PT_Xicc++_12.pdf}
  \includegraphics[width=0.46\linewidth]{ana_xicc/efficiency/SPD_Xicc++_12.pdf}
  \caption{
  Distributions of (left) \Xiccpp \pt and 
  (right) nSPDHits of the $\Xiccpp\to\Lc\Km\pip\pip$ decay
  for (top) 2016 and (bottom) 2012 data, respectively.
  Blue stands for the background-subtracted data,
  and magenta (black) for the simulation (MC) sample after (before) reweighting.}
  \label{fig:data-MC_pt}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/y_Xicc++.pdf}
  \includegraphics[width=0.50\linewidth]{ana_xicc/efficiency/y_Xicc++_12.pdf}
  \caption{
    Distributions of \Xiccpp $y$ of the $\Xiccpp\to\Lc\Km\pip\pip$ decay 
    for (right) 2016 and (left) 2012 data, respectively.
    Blue stands for the background-subtracted data,
    and magenta (black) for the simulation (MC) sample after (before) reweighting.}
  \label{fig:data-MC_y}
\end{figure}

\paragraph{Correction of intermediate \Lc kinematics.}%
\label{par:correction_of_intermediate_lc_kinematics_}
Simulation used in this analysis does not
well model the resonance structure of the \Lc decays.
Simulation samples of the \Xiccp signal and \Lc control modes
implement phase-space models in the \Lc decay,
while the simulation samples of the \Xiccpp control mode 
utilize a pseudo-resonant model, 
taking into account the \Kstarz, $\Deltares^{++}$ and $\Lz(1520)^{0}$ resonances.
In order to illustrate the discrepancy,
distributions in 2016 data and simulation of the $\Lc\to\proton\Km\pip$ decay in bins of 
the \Lc Dalitz-plot variables $m(\proton\Km)$ and $m(\Km\pip)$ 
(the invariant mass of the proton-Kaon and Kaon-Pion system)
are shown in Fig.~\ref{fig:dalitz_data_mc}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Dalitz_Data.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Dalitz_MC.pdf}
  \caption{Comparison of the Dalitz-plot distributions of 
  (left) 2016 data and (right) simulation for the $\Lc\to\proton\Km\pip$ decay.}
  \label{fig:dalitz_data_mc}
\end{figure}
The selection efficiency shows strong dependence on the \Lc Dalitz-plot variables.
This is illustrated in Fig.~\ref{fig:acc_dalitz} 
by the efficiency of the HLT2 selection as a function of $m(\proton\Km)$ and $m(\Km\pip)$
for the \Lc control sample.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/TRUEM_pK.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/TRUEM_KPi.pdf}
  \caption{Efficiency of the HLT2 selection 
  for the \Lc control mode in bins of (left) $m(\proton\Km)$ and (right) $m(\Km\pip)$.}
  \label{fig:acc_dalitz}
\end{figure}
Therefore,
$m(\proton\Km)$ and $m(\Km\pip)$ distributions are reweighted 
when estimating the selection efficiencies.
Simulation samples of both signal and control modes 
are reweighted in two-dimensional bins of $m(\proton\Km)$ and $m(\Km\pip)$
to the inclusive \Lc data,
with background subtracted through sWeights 
from the mass fit shown in Fig.~\ref{fig:LcMass_control}.
Tables of weights are summarised in Fig.~\ref{fig:dalitz_weights}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Dalitz_weight_Lc.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Dalitz_weight_Xiccpp.pdf}
  \caption{Weights in bins of Dalitz-plot variables for the
  (left) \Lc and (right) \Xiccpp control modes.}
  \label{fig:dalitz_weights}
\end{figure}
Good agreements of Dalitz-plot distributions 
are achieved after reweighing, 
as shown in the projections to $m(\proton\Km)$ and $m(\Km\pip)$ 
distributions in Fig.~\ref{fig:projection_Lc} and Fig.~\ref{fig:projection_Xiccpp}, 
for the \Lc and \Xiccpp control mode, respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/M_pK_weighted_Lc.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/M_KPi_weighted_Lc.pdf}
  \caption{Comparison of projections to (left) $m(\proton\Km)$ and (right) $m(\Km\pip)$ 
    for background-subtracted inclusive \Lc data and 
    the \Lc simulation sample before and after reweighting.}
  \label{fig:projection_Lc}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/M_pK_weighted_Xiccpp.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/M_KPi_weighted_Xiccpp.pdf}
  \caption{Comparison of projections to (left) $m(\proton\Km)$ and (right) $m(\Km\pip)$ 
    for background-subtracted inclusive \Lc data and 
    the \Xiccpp simulation sample before and after reweighting.}
  \label{fig:projection_Xiccpp}
\end{figure}
We compare the efficiency estimated with and without 
reweighting the Dalitz-plot distributions. 
It turns out that in spite of the change of efficiency in individual modes,
the difference of efficiency ratio of signal to control modes is small.
The difference is less than 1\% to the \Lc control mode,
and 4\% to the \Xiccpp control mode.

In the following sub-sections, 
we present the results of different efficiency components for 2012 and 2016 data. 
Results of 2017 and 2018 data are evaluated with the same procedure
and summarised in Appendix~\ref{app:run2_efficiency}.


\paragraph{Ratio of acceptance efficiency.}%
\label{ssub:ratio}
The acceptance efficiency $\varepsilon^{\text{Acc}}$ 
in this context is defined as the efficiency for the decay products 
of the signal candidates in the fiducial region 
(defined as $2.0<y<4.5, \; 4<\pt<15 \gev$)
to be in the \lhcb acceptance ($10<\theta<400\mrad$). 
It is determined with generator-level simulation samples,
\ie only signal kinematic and decay are simulated,
but not the interaction with detector materials.
Results are listed in Table~\ref{tab:Acc}, 
which includes ratios of the control mode to the signal mode.
\begin{table}[tb]
  \centering
  \caption{Acceptance efficiencies and 
  the ratios of the control to the signal mode.}
  \label{tab:Acc}
  \input{table/Ratio_Table_Acc_dalitz}
\end{table}


\paragraph{Ratio of HLT2 and preselection efficiency.}%
\label{ssub:ratio_of_turbo_and_preselection_efficiencies}
The HLT2 (or stripping for the 2012 dataset) and preselection
efficiencies $\varepsilon^{\text{Sel$\mid$Acc}}$
are determined with simulation samples.
PID requirements are excluded and will be calculated separately.
Track reconstruction efficiency is included 
implicitly in the HLT2 efficiency.
There is known data-simulation discrepancy 
to be corrected in the track reconstruction efficiency.
This is taken into account 
using correction tables
obtained with the tag-and-probe method.
%The correction tables for 2016 and 2012 data
%are shown in Fig.~\ref{fig:tracking_correction}.
The correction value for each final-state track 
is multiplied to get the total correction of the candidate. 
Systematic uncertainties associated with this method 
are discussed in Sec.~\ref{subsub:systematics_tracking}.
The efficiencies are listed in Table~\ref{tab:turbo_eff}.
%\begin{figure}[tb]
%  \centering
%  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/TrackingTable_16.pdf}
%  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/TrackingTable_12.pdf}
%  \caption{Tracking efficiency correction table for 2016 (left) and 2012 (right) MC samples.}
%  \label{fig:tracking_correction}
%\end{figure}
\begin{table}[tb]
  \centering
  \caption{HLT2 (or stripping for 2012 data set) and preselection efficiency
   and efficiency ratios of the control modes and the signal mode.}
  \label{tab:turbo_eff}
  \input{table/Ratio_Table_Sel_dalitz}
\end{table}

\paragraph{Ratio of PID efficiency.}%
\label{ssub:ratio_of_pid_efficiencies}
The PIDCalib package is used to determine the efficiency of PID
requirements $\varepsilon^{\text{Pid$\mid$Sel}}$.
For 2016 data, the calibration sample used for kaons and pions 
is the $\Dstarp \to \Dz (\to \Km \pip) \pip $ sample,
and that used for protons is the $\Lz \to \proton \pion$ sample.
For 2012 data, additional samples of $\Lc\to\proton\Km\pip$ decay are
also included for protons.
It is found that the proton PID efficiency 
given by $\Lz \to \proton \pion$ and $\Lc\to\proton\Km\pip$ 
can differ by a few percents.
However such difference would mostly cancel in the ratio between
$\Xiccp(\to \Lc K^-\pip)/\Lc$ or 
$\Xiccp(\to \Lc K^-\pip)/\Xiccp(\to \Lc K^-\pip\pip)$.
The efficiency for PID requirements on each final-state track 
is determined in each $p$ and $\eta$ bin.
The binning scheme for each type of final-state tracks 
is listed in Table~\ref{tab:binning}.
The same binning schemes are used 
for the signal and control modes.
The total PID efficiency of the candidate 
is the product of efficiencies of each final track. 
The PID efficiency for the signal and control modes 
is summarised in Table~\ref{tab:PID_eff}.
\begin{table}[tb]
  \centering
  \caption{Binning scheme used for \kaon, \pion and \proton for PID calibration.}
  \label{tab:binning}
  \begin{tabular}{c l}
    \toprule
    Variable & Binning \\
    \midrule
    $p$ [\gev] & \makecell[cl]{[ 2.0, 5.6, 9.2, 12.8, 16.4, 20.0, 26.0, 32.0, 38.0, 44.0, 50.0, \\
    56.0, 62.0, 68.0, 74.0, 80.0, 90.0, 100.0, 110.0, 120.0, 150.0 ]} \\
    $\eta$     & [ 1.5, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.33, 5.0 ] \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{PID efficiencies and the ratios of the control modes to the signal mode.}
  \label{tab:PID_eff}
  \input{table/Ratio_Table_PID_dalitz}
\end{table}

\paragraph{Ratio of MVA efficiency.}%
\label{ssub:the_ratio_of_mva_efficiencies}
In the case of setting upper limits,
the MVA selection consisting of two steps is developed,
as discussed in Sec.~\ref{ssub:multivariate_selection_2step}.
The MVA efficiency and their ratios of the control modes to the signal mode
at the optimal working points $\text{MVA1}>0.00$ and $\text{MVA2}>0.70$ 
are summarised in Tables~\ref{tab:MVA1_eff} and \ref{tab:MVA2_eff},
for MVA1 and MVA2, respectively. 
\begin{table}[tb]
  \centering
  \caption{MVA1 efficiencies at the working point $\text{MVA1}>0.00$
    and their ratios of the control modes to the signal mode.} 
  \label{tab:MVA1_eff}
  \input{table/Ratio_Table_MVA1_dalitz}
\end{table}
\begin{table}[tb]
  \centering
  \caption{MVA2 efficiencies at the working points $\text{MVA1}>0.00$ 
  and $\text{MVA2}>0.70$
  and their ratios of the control modes to the signal mode.}
  \label{tab:MVA2_eff}
  \input{table/Ratio_Table_MVA2_dalitz}
\end{table}

\paragraph{Ratio of trigger efficiency.}%
\label{ssub:the_ratio_of_trigger_efficiencies}
The efficiencies of L0 and HLT1 trigger requirements 
are evaluated with simulation samples.
The results are summarised in Table~\ref{tab:trigger}.
\begin{table}[tb]
  \centering
  \caption{L0 and HLT1 trigger efficiencies 
  and their ratios of the control modes to the signal mode.}
  \label{tab:trigger}
  \input{table/Ratio_Table_Trigger_dalitz}
\end{table}

\paragraph{Ratio of MC-Match efficiency.}%
\label{ssub:ratio_of_MC-Match_efficiencies}
The MC truth-matching is defined as follows.
A reconstructed particle is matched to a simulated particle
if there is an overlap of at least 70\% between 
the hits created by the reconstructed particle and 
those associated to the simulated particle.
The choice of the percentage threshold 
will lead to an inefficiency in the truth matching.
Therefore, the selection efficiency 
estimated with simulation samples needs to be corrected as
$\varepsilon^{\text{Sel}}_{\text{corr}} = \varepsilon^{\text{Sel}}/\varepsilon^{\text{MC-Match}}$,
where the $\varepsilon^{\text{MC-Match}}$ is the MC-Match efficiency.

The MC-Match efficiency is calculated as the ratio of
the number of MC-matched signals to the number of signals 
obtained by fitting to the invariant-mass distribution without MC-matching. 
Both numbers are obtained with samples after full event selection.
The results are summarized at Table~\ref{tab:Match}.
\begin{table}[tb]
  \centering
  \caption{MC-Match efficiencies and the ratios of the control to the signal modes.}
  \label{tab:Match}
  \input{table/Ratio_MC-Match}
\end{table}

\paragraph{Summary of efficiency.}%
\label{ssub:summary}
Each of the efficiency ratios in Eq.~\ref{equ:eff} has been determined. 
The total efficiencies of the control modes and the signal mode 
as well as their ratios are summarised in Table~\ref{tab:eff_summary}.

The $\Lc$ efficiency is much higher 
as it decays to only three final-state particles.  
The $\Xiccp\to\Lc K^- \pip$ efficiency is lower than that of the
$\Xiccpp \to \Lc K^-\pip\pip$ because the $\Xiccp$ lifetime
is lower by around a factor of three than that of 
$\Xiccpp$.
The inefficiency is compensated partially by one less track.

According to the isospin symmetry, 
the $\Xiccp$ production cross-section is expected to be very similar, 
if not identical, to that of the $\Xiccpp$. 
Some theoretical calculations predict that 
the $\mathcal{B}(\Xiccp \to \Lc \Km \pip)$ 
is a factor of five smaller than 
$\mathcal{B}(\Xiccpp \to \Lc \Km \pip \pip)$~\inlinecite{Yu:2017zst}.
Therefore the expected yield 
can be estimated through the efficiency comparison of 
\Xiccp with \Xiccpp ($r_\Xiccpp)$),
which is about 27 for the 2016 case and 4 for the 2012 case.
\begin{table}[tb]
  \centering
  \caption{Total efficiencies and their ratios of the control modes to the signal mode.}
  \label{tab:eff_summary}
  \input{table/Ratio_Table_Total_dalitz_mcmatch}
\end{table}


\paragraph{Variation of the efficiency with lifetime.}%
\label{sub:variation_of_the_efficiency_with_lifetime}
The efficiency depends strongly on the \Xiccp lifetime,
due to variables that explore displaced vertices in the selection.
Unfortunately, the prediction of \Xiccp lifetime 
has a large theoretical uncertainty, as discussed in Sec.~\ref{sub:lifetime_th}.
Therefore, upper limits vary under different lifetime hypotheses.
To take this effect into account, 
the efficiency is recalculated under different lifetime hypotheses.
%(a discrete set of $0\fs$, $40\fs$, $80\fs$, $120\fs$, and $160\fs$ is considered).
A discrete set of $40\fs$, $80\fs$, $120\fs$, and $160\fs$ is considered.

For non-zero lifetime hypotheses,
the simulation sample generated with an input lifetime of $\tau_0=333\fs$ 
is reweighted according to the TRUE decay time $t$ 
with the weight $w(t)$ defined as
\begin{equation}
  w(t) = \frac{\frac{1}{\tau}\text{exp}(-\frac{t}{\tau})}{\frac{1}{\tau_0}\text{exp}(-\frac{t}{\tau_0})},
\end{equation}
where $\tau$ is the target lifetime hypothesis to be considered.
Hence, the efficiency for a given selection is
\begin{equation}
  \varepsilon = \frac{\sum_{\text{pass}} w_i}{\sum_{\text{before}} w_j},
\end{equation}
where the sum $i$ runs over the events that pass the selection and
$j$ runs over all the events before the selection.
For zero lifetime hypothesis,
a dedicated simulation sample generated with $\tau=0\fs$ 
is used to calculate the efficiency.
The results are summarised in Tables~\ref{tab:eff_vary_tau} and \ref{tab:eff_vary_tau_12},
and are visualised in Fig.~\ref{fig:eff_vary_tau}.
A roughly linear dependence of the efficiency on lifetime hypotheses is observed.
The variation of the efficiency with lifetime hypotheses for 2017 and 2018 data
is studied with the same approach and summarised in Appendix~\ref{app:run2_efficiency}.
\begin{table}[tb]
  \centering
  \caption{Efficiencies at different lifetime hypotheses for 2016 data.}
  \label{tab:eff_vary_tau}
  \input{table/Efficiency_VaryTau_16_dalitz}
  %\input{table/Efficiency_VaryTau_dalitz}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Efficiencies at different lifetime hypotheses for 2012 data.}
  \label{tab:eff_vary_tau_12}
  \input{table/Efficiency_VaryTau_12_dalitz}
\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Efficiency_VaryTau.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/efficiency/Efficiency_VaryTau_12.pdf}
  \caption{Efficiencies at different lifetime hypotheses for 
  (left) 2016 and (right) 2012 data.}
  \label{fig:eff_vary_tau}
\end{figure}

\paragraph{Variation of the efficiency with the \Xiccp mass.}%
\label{sub:variation_of_the_efficiency_with_mass}
Kinematic distributions of final-state tracks depend on 
the invariant mass of \Xiccp baryons.
Therefore, the efficiency may vary as a function of the mass.

To study this effect, a weighting procedure is used.
Firstly, generator-level simulation samples is produced 
with different \Xiccp mass hypotheses of
$3518.7\mev$ and $3700.0\mev$.
Secondly, the full-simulated sample, which is generated with $m_0 = 3621.4\mev$,
is weighted according to the generator-level \pt distributions of 
\Xiccp decay products, \Lc, \Km and \pip track.
The efficiency is then recalculated for each mass hypothesis
with weighted samples.
The weights are obtained with the \texttt{GBReweightor} 
from the \texttt{hep\_ml} python module,
a reweighting algorithm based on ensemble of regression trees~\inlinecite{gbreweight:1}.

The results are shown in Table~\ref{tab:eff_vary_mass} and 
visualized in Fig.~\ref{fig:eff_vary_mass}.
The dependence of the total efficiency on the \Xiccp mass hypotheses 
is weak in a wide range of mass hypothesis.
Therefore, we do not include this effect 
when calculating the upper limits at different mass hypotheses.
\begin{table}[tb]
  \centering
  \caption{Efficiencies at different mass hypotheses.}
  \label{tab:eff_vary_mass}
  \input{table/eff_varymass_dalitz}
\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/efficiency/Efficiency_VaryMass.pdf}
  \caption{Efficiency as a function of mass hypotheses.}
  \label{fig:eff_vary_mass}
\end{figure}

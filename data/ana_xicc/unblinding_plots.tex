% unblinded results

With the green light to unblind the total Run~1 and Run~2 data in the signal window,
we apply the event selection elaborated in Sec.~\ref{sec:xicc_event_selection} to the RS data.
Figure~\ref{fig:unblind_Xiccp} shows the distributions of 
$M([\proton\Km\pip]_{\Lc})$ and $m(\Lc\Km\pip)$ in the \Lc mass range of 2270-2306\mev
in the full data sample.
As a comparison, the $m(\Lc\Km\pip)$ distribution of the WS control sample
is also shown in the right plot of Fig.~\ref{fig:unblind_Xiccp}.
The dotted red line indicates the mass of the $\Xiccp(3520)$ baryon reported 
by SELEX~\inlinecite{Mattson:2002vu}
and the dashed blue line refers to the mass of 
the \Xiccpp baryon~\inlinecite{LHCb-PAPER-2017-018,LHCb-PAPER-2018-026}.
A small enhancement is seen near a mass of 3620\mev.
There is no excess near a mass of 3520\mev.
The small enhancement below 3500\mev in the RS sample,
compared with the WS sample, 
is due to partially reconstructed $\Xiccpp$ decays.
Distributions of $m(\Lc\Km\pip)$ defined in Eq.~\ref{eq:mXicc} are also shown in
\ref{fig:unblinding_Run1}, \ref{fig:unblinding_Run2}, and \ref{fig:unblinding_Run12}
for Run~1, Run~2 and the combined data sets, respectively.
Results with MVA optimised for both default and zero lifetime hypotheses
are shown in each figure.
To determine the statistical significance of this enhancement,
an extended unbinned maximum-likelihood fit is performed.
The signal component is described with the sum of
a Gaussian function and a doubly-sided crystal ball function.
The parameters of the signal model are fixed from simulation
except for the common peak position $\mu$, of the two functions 
that is allowed to vary freely in the fit.
The background component is described by
a second-order Chebyshev polynomial with all parameters free to vary in the fit.
%Parameter $\mu$ has an initial value of 3620\mev
%and is float in the mass region of [3400, 3800]\mev.
%Other parameters are fixed to those obtained with the simulated
%samples as described in Sec.~\ref{subsec:mass_fitModel}.
It is found that the fit result of $\mu$ 
is not stable against the choice of initial values,
which is guided by the likelihood scan of $\mu$ discussed below.
For example, the fit result in the left plot of Fig.~\ref{fig:unblinding_Run12}
can only be obtained with initial values in [3600,3640]\mev,
as indicated by the likelihood scan of $\mu$ in Fig.~\ref{fig:likelihood_scan}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.49\linewidth]{ana_xicc/paper/Fig1a.pdf}
  \includegraphics[width=0.49\linewidth]{ana_xicc/paper/Fig1b.pdf}
  \caption{Mass distributions of the (left) intermediate \Lc and 
  (right) \Xiccp candidates for the full data sample.
  All selection is applied, including the \Lc mass requirement,
  indicated by the cross-hatched region in the left plot,
  of $2270\mev<M([\proton\Km\pip]_{\Lc})<2306\mev$.
  The right-sign (RS) $m(\Lc\Km\pip)$ distribution is shown in the right plot,
  along with the wrong-sign (WS) $m(\Lc\Km\pim)$ distribution
  normalised to have the same area.
  The dotted red line at 3518.7\mev 
  indicates the mass of the \Xiccp baryon reported by SELEX~\inlinecite{Mattson:2002vu}
  and the dashed blue line at 3621.2\mev 
  indicates the mass of the \Xiccpp baryon~\inlinecite{LHCb-PAPER-2018-026}.}
  \label{fig:unblind_Xiccp}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run1_1step.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run1_1step_zero.pdf}
  \caption{Distribution of $m(\Xiccp)$ in Run~1 data with the MVA selection
  optimised for (left) default and (right) zero lifetime hypothesis.}
  \label{fig:unblinding_Run1}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run2_1step.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run2_1step_zero.pdf}
  \caption{Distribution of $m(\Xiccp)$ in Run~2 data with the MVA selection
  optimised for (left) default and (right) zero lifetime hypothesis.}
  \label{fig:unblinding_Run2}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run12_1step.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/RS_unbin_Run12_1step_zero.pdf}
  \caption{Distribution of $m(\Xiccp)$ in Run~1 and 2 data with the MVA selection
  optimised for (left) default and (right) zero lifetime hypothesis.}
  \label{fig:unblinding_Run12}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/unblinding/Likelihood_meanXicc.pdf}
  \caption{Likelihood scan of $\mu$ for the 
  combined Run~1 and Run~2 data with the MVA selection
  optimised for the default lifetime hypothesis.}
  \label{fig:likelihood_scan}
\end{figure}

The $\sqrt{2\Delta\ln L}$,
corresponding to the local significance according to Wilk's theorem,
%(i.e., without the correction of the look-elsewhere effect), 
is reported in each plot.
The largest $\sqrt{2\Delta\ln L}$ is $3.1\sigma$
for the total data set with MVA selection optimised for the default lifetime hypothesis. 
The systematic uncertainty is incorporated 
by convolving the negative log likelihood versus the signal yield curve 
with a Gaussian distribution with a width equal to the systematic uncertainty. 
The systematic is about 10\%, which is dominated by the difference of resolution 
in data and in simulation.
This uncertainty is studied with the \Xiccpp control sample~\inlinecite{LHCb-PAPER-2017-018}. 
The largest local significance is determined to be $2.7\sigma$,
taking into account the systematic uncertainty.
The local $p$-value is calculated using the one-sided Gaussian tail convention as
\begin{equation}
  % change from one-sided convention to two-sided
  %\texttt{TMath::Prob(}2\Delta\ln L,\texttt{1)}
  \texttt{0.5*TMath::Prob(}2\Delta\ln L,\texttt{1)}
\end{equation}
as a function of $m(\Xiccp)$ for Run~1, Run~2, and total data sets,
as shown in Fig.~\ref{fig:localsig_massscan_RS_unbin_Run12_1step}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/LocalP_80fsCaseA.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/LocalP_0fsMass.pdf}
  \caption{Local $p$-value as a function of $m(\Xiccp)$ 
  for the combined Run~1 and Run~2 data sets
  with the MVA selection optimised for (left) default
  and (right) zero lifetime hypothesis.}
  \label{fig:localsig_massscan_RS_unbin_Run12_1step}
\end{figure}

The look elsewhere effect is taken into account according to Ref.~\inlinecite{Gross:2010qma}.
In Ref.~\inlinecite{Gross:2010qma}, an upper limit of the global $p$-value is given by
\begin{equation}
  \label{eq:bound}
  P(\chi^2_s > c) + \langle N(c_0) \rangle \left( \frac{c}{c_0} \right) ^{(s-1)/2} e ^{-(c-c_0)/2},
\end{equation}
where $c$ is the observed profile likelihood ratio $2\Delta\ln L$, 
$s=1$ the degree of freedom of the \chisq distribution,
reference level $c_0$ chosen to be 0.5,
and $\langle N(c_0) \rangle$ the so-called expected number of 
``upcrossing'' at reference level $c_0$.
The number of ``upcrossing'' $N(c_0)$ is the number of times that 
the profile likelihood ratio curve goes from below to above a certain threshold $c_0$.
The $\langle N(c_0) \rangle$ is evaluated 
with a small number (one thousand) of background-only toy experiments.
The mass region of interest is chosen to be [3500,3700]\mev,
which covers most theoretical predictions and
the mass of the structure reported by the SELEX experiment.
As an illustration,
the profile likelihood ratio distribution of one of the toy experiments
is shown in Fig.~\ref{fig:profile_likelihood}.
The $N(c_0)$ corresponding to this plot is 5.
In the case of the total data set 
with MVA selection optimised for the default lifetime hypothesis,
$\langle N(c_0) \rangle$ is found to be 3.729, 
and hence the global $p$-value is 0.042.
Converting the $p$-value to significance according to 
\begin{equation}
  \sqrt{2}\texttt{*TMath::ErfcInverse(2*p-value)},
\end{equation}
we can get the global significance of 1.7 for the largest local significance.
No excess above $3.0\sigma$ is observed after taking into account
the correction of the look elsewhere effect.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/unblinding/profile_likelihood_495.pdf}
  \caption{Profile likelihood ratio distribution 
  of one of the toy experiments.
  The $N(c_0)$ corresponding to this plot is 5.}
  \label{fig:profile_likelihood}
\end{figure}

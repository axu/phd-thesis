% upper limit

With the knowledge of the ratios of luminosity, efficiency, and
the yields of the control modes,
the upper limit on $R$, calculated according to Eq.~\ref{eq:alpha},
can be evaluated with different mass and lifetime hypotheses.
The values of single event sensitivity $\alpha$, defined in Eq.~\ref{eq:alpha},
for \Lc and \Xiccpp control modes are summarised in
Table~\ref{tab:alpha_Lc} and Table~\ref{tab:alpha_Xicc}, respectively.
Only statistical uncertainties are included in these Tables.
One can see that the sensitivity has been improved by more than a
factor of ten for 2016 data compared with 2012 data
when normalised to $\Lc$, 
and by four when normalised to $\Xiccpp$, 
thanks to the improved Run~2 trigger selection.
Upper limits will be set in the fiducial region of \Xiccp baryons
and will be quoted for two different center of mass energy: $\sqrt{s}=8\tev$, 
corresponding to the 2012 data, and $\sqrt{s}=13\tev$,
corresponding to 2016-2018 data.
\begin{table}[tb]
  \centering
  \caption{Single event sensitivity $\alpha(\Lc)$ ($\times 10^{-5}$) of the \Lc control mode 
   for different datasets and lifetime hypotheses.}
  \label{tab:alpha_Lc}
  \input{table/Alpha_dalitz_mcmatch_Lc}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Single event sensitivity $\alpha(\Xiccpp)$ ($\times 10^{-2}$)
  of the \Xiccpp control mode for different datasets and lifetime hypotheses.}
  \label{tab:alpha_Xicc}
  \input{table/Alpha_dalitz_mcmatch_Xiccpp}
\end{table}

The procedure of setting the upper limits is as follows:
\begin{itemize}
  \item The mass parameter $\mu$ in the signal lineshape 
    is fixed to a given value in the range of $[3400,3800]\mev$
    with a step size of $2.5\mev$, 
    which corresponds to roughly half of the mass resolution.
  \item An extended unbinned likelihood fit is performed 
    to the $m(\Lc\Km\pip)$ distribution of data sample in
    the range of $[3400,3800]\mev$.
  \item The negative log likelihood (NLL) as a function of $R$ 
    is obtained from the fit and is converted to a likelihood 
    (offset is added to have the minimum of the likelihood at 0).
  \item The likelihood profile is obtained for 
    100 values of $R$ in fine steps of equal size.
  \item The likelihood profile is used to compute the numerical integral 
    and determine the value of $R$
    corresponding to 95\% of the total likelihood integral.
    This value corresponds to the upper limit of $R$ at 95\% confidence level.
\end{itemize}
%The fixed mass value is varied to get the upper limits in the mass scan region. 
The resolution parameter $\sigma$ of the signal lineshape
is varied at different mass hypotheses,
while other parameters remain the same.
The upper limits at different lifetime hypotheses are 
determined by considering the efficiency variation
described in Sec.\ref{sub:variation_of_the_efficiency_with_lifetime}.
Due to the different $\alpha$ values for 2016-2018 data sets,
simultaneous fit to the $m(\Lc\Km\pip)$ distribution with shared $R$ is
performed to calculate the upper limit at $\sqrt{s}=13\tev$.

The above procedure is tested with a toy experiment 
and the fits are robust and returns expected results across the mass scan region.
%More details are included in 
%Appendix~\ref{sec:test_of_the_procedure_of_setting_upper_limits_with_toy_mc}.
As a cross check, the upper limit at $m(\Lc\Km\pip)=3621.4\mevcc$ is evaluated 
with the 2011 dataset and is found to be in good agreement of the previous LHCb search
using 2011 data~\inlinecite{LHCb-PAPER-2013-049}.
%The details are discussed in Appendix~\ref{sec:validatiion_with_toy_mc}.

Systematic uncertainties are considered 
by convolving the normalised likelihood curve 
with a Gaussian function 
$G(\mu,\sigma)=G(0,\sigma_{\mathrm{syst}})$
with $\sigma_{\mathrm{syst}}=\mu\times\sigma_{\mathrm{rel}}$,
where $\sigma_{\mathrm{rel}}$ is the relative systematic uncertainty described 
in Sec.~\ref{sub:xicc_systematic_uncertainties}.
The convolution can be described in terms of
\begin{equation}
\label{equ:ul_sys}
  L(N') = \int_{0}^{\infty}
   L(N) \times \frac{1}{\sqrt{2\pi}\sigma_{\mathrm{syst}}} \times 
   \exp\left[ \frac{-(N'-N)^2}{2\sigma_{\mathrm{syst}}^2} \right] \mathrm{d} N,
\end{equation}
where $L(N)$ is the normalised likelihood distribution obtained 
from fitting the curve of normalised likelihood value 
versus the cross-section ratio and 
parameterised as a Gaussian function.
As an illustration,
the likelihood distribution before and after the convolution
at the mass point with the largest local significance is shown
in Fig.~\ref{fig:ul_sys} for 2012 and 2016 data, respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/ul_systematic_rlc_run1.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/ul_systematic_rxicc_run1.pdf}\\
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/ul_systematic_rlc.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/ul_systematic_rxicc.pdf}
  \caption{Normalized likelihood distribution before (red curve) 
  and after (blue curve) convolution
  for (left) $R_\Lc$ and (right) $R_\Xiccp$
  and for (top) 2012 and (bottom) 2016 data
  at the best mass point.
  The arrow shows the 95\% upper limit 
  using only statistical uncertainty (red) and 
  using both statistical and systematic uncertainties (blue).}
  \label{fig:ul_sys}
\end{figure}

Figures~\ref{fig:mass_scan_tau_run1} and~\ref{fig:mass_scan_tau_run2}
show the 95\% credibility level upper limits
at centre-of-mass energies of $\sqrt{s}=8\tev$
and $\sqrt{s}=13\tev$, respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/paper/Fig5a.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/paper/Fig5b.pdf}
  \caption{Upper limits on (left) $\mathcal{R}(\Lc)$
  and (right) $\mathcal{R}(\Xiccpp)$
  at 95\% credibility level as a function of $m(\Lc\Km\pip)$
  at $\sqrt{s}=8\tev$ for four $\Xiccp$ lifetime hypotheses.}
  \label{fig:mass_scan_tau_run1}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/paper/Fig6a.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/paper/Fig6b.pdf}
  \caption{Upper limits on (left) $\mathcal{R}(\Lc)$
  (right) $\mathcal{R}(\Xiccpp)$
  at 95\% credibility level as a function of $m(\Lc\Km\pip)$ at
  $\sqrt{s}=13\tev$,
  for four $\Xiccp$ lifetime hypotheses.}
  \label{fig:mass_scan_tau_run2}
\end{figure}

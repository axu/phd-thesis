% selection for upper limits

Two control modes are chosen as the control mode:
the prompt \Lc baryon reconstructed through $\proton\Km\pip$ final state,
and the \Xiccpp baryon reconstructed through $\Lc\Km\pip\pip$ final state,
with $\Lc\to\proton\Km\pip$.
To calculate upper limits,
exclusive decays of \Lc and \Xiccpp baryons are reconstructed and selected,
as detailed in Appendix~\ref{cha:event_selection_for_lc_and_xiccpp_control_modes}.
To reduce systematic uncertainties in the estimation of efficiency,
fiducial cuts are applied to the signal mode.
Tighter trigger requirements are applied.
In addition, the MVA classifier to select the signal decay
is factorised into two: one for the selection of intermediate \Lc candidates,
the other for the selection of \Xiccp candidates.
The former is applied to both the signal mode and the \Lc control mode.

\paragraph{Fiducial region.}%
\label{sub:fidutial_region}
The fiducial region is defined to better estimate the selection efficiency.
For the signal mode, 
the \Xiccp candidate is required to lie in the kinematic region of
\begin{equation}
  \label{eq:fidutial}
  2.0<y<4.5, \; 4<\pt<15 \gev.
\end{equation}

\paragraph{Trigger requirements.}%
\label{sub:turbo_selection_xicc}
For L0 and HLT1 trigger, a \Lc TOS chain is used.
For 2011-2012 data, these are
\begin{itemize}
  \item L0: \verb|L0Hadron| TOS on \Lc,
  \item HLT1: \verb|Hlt1TrackAllL0| TOS on \Lc,
\end{itemize}
For 2015-2018 data, these are
\begin{itemize}
  \item L0: \verb|L0Hadron| TOS on \Lc,
  \item HLT1: \texttt{Hlt1TrackMVA||Hlt1TwoTrackMVA} on \Lc.
\end{itemize}
These trigger requirements are applied to both the signal and control modes.

In terms of HLT2 trigger, 
the selection of the signal mode and the \Xiccpp control mode
is tightened by the requirements
marked with a dagger ($\dagger$) in Table~\ref{tab:stripping_Lc_12},
in order to be consistent with the \Lc control mode.

\paragraph{Multivariate analysis of $\Xiccp\to\Lc\Km\pip$ decay.}%
\label{ssub:multivariate_selection_2step}
The multivariate classifier previously developed 
is factorised into two steps,
with the first step applies also to the \Lc control mode.
The motivation of breaking one MVA down into two steps 
is to minimise the systematic uncertainties due to the \Lc selections. 
In the first step, 
an MVA classifier (MVA1) is trained to remove fake \Lc candidates 
and will be applied to both the signal and the \Lc control mode. 
In the second step, another MVA classifier (MVA2) is trained 
to remove fake \Xiccp candidates 
and will be applied only to the signal mode.
In the remaining discussion,
the result for the default lifetime hypothesis is shown.
The same approach is used to train the two-step MVA
assuming that the lifetime of \Xiccp baryon is negligible.
%The result for zero lifetime hypothesis is discussed in Appendix~\ref{sec:mva2tep0tau}.

\paragraph{Training.}%
\label{par:first_mva_}
For the first step, the training sample for the signal is the 2016 simulation sample 
filtered by the trigger selection and preselection.
The training sample for the background is the 2016 WS sample filtered 
by the same event selection.
Due to the large sample size, 
only 5\% of the total filtered WS sample is used in the training.
Candidates of the signal sample are required to 
lie in the \Xiccp and the \Lc mass windows, 
while candidates of the background sample are required 
to be in the \Xiccp mass window and the \Lc mass \emph{sideband},
which is defined to be $(2223,2258)\cup(2318,2353)\mev$.
For the second step, candidates of both the signal and background samples 
are required to lie in the \Xiccp and the \Lc mass window, 
and to pass the requirement on the response of MVA1 classifier.

Training variables are determined with the same strategy 
as discussed in Sec.~\ref{sec:xicc_event_selection}.
In the first step, 
variables that significantly bias the source of \Lc candidate are avoided
such that they are applicable to both \Lc signals from \Xiccp decays and from PV.
The final set of training variables under the default lifetime hypothesis includes:
\begin{itemize}
  \item \chisqvtxndf of the \Lc vertex fit, 
  \item Maximum distance of the closest approach of \Lc,
  \item \pt of \Lc and its secondaries,
  \item Log sized flight distance \chisq of \Lc to PV,
  \item Log sized \chisqip of \Lc secondaries to PV 
\end{itemize}
for the first MVA classifier and
\begin{itemize}
  \item \chisqvtxndf of \Xiccp (without DTF),
  \item DTF \chisqvtxndf of \Xiccp with PV constraint,
  \item Maximum distance of the closest approach of \Xiccp,
  \item \pt of \Xiccp, and \pip and \Km from \Xiccp,
  \item Log sized \chisqip of \Xiccp to PV,
  \item $\cos^{-1}(\text{DIRA})$ of \Xiccp to PV,
  \item Log sized flight distance \chisq of \Xiccp to PV,
  \item Log sized \chisqip of \Lc, and \pip and \Km from \Xiccp,
  \item $\cos^{-1}(\text{DIRA})$ of \Lc to PV
\end{itemize}
for the second MVA classifier. 
The correlation matrices of the training variables 
under the default lifetime hypothesis are
shown in Figs~\ref{fig:first_nominal} and 
\ref{fig:second_nominal} for input variables in the first and second steps,
respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixS_1st_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixB_1st_nominal.pdf}
  \caption{Correlation matrices of training variables of 
  (left) the signal and (right) the background samples for
  the first MVA classifier with the default lifetime hypothesis.}
  \label{fig:first_nominal}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixS_2nd_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixB_2nd_nominal.pdf}
  \caption{Correlation matrices of the training variables of 
  (left) the signal and (right) the background samples for
  the second MVA classifier with the default lifetime hypothesis.}
  \label{fig:second_nominal}
\end{figure}

Based on the performance of different MVA algorithms in Sec.~\ref{sec:xicc_event_selection}, 
the BDTG algorithm is used in the two-step MVA. 
The ROC curves for the first and second MVA classifiers 
are shown in Fig.~\ref{fig:ROC_twostep_nominal},
while the response curves are illustrated in Fig.~\ref{fig:response_2step_nominal}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/rejBvsS_1st_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/rejBvsS_2nd_nominal.pdf}
  \caption{ROC curves for the (left) first and (right) second 
  MVA classifiers under the default lifetime hypothesis.}
  \label{fig:ROC_twostep_nominal}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_BDTG_1st_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_BDTG_2nd_nominal.pdf}
  \caption{Response curves for the (left) first and (right) second 
  MVA classifiers under the default lifetime hypothesis.
  For the second step, the requirement of $t_1>0.0$ for MVA1 is applied.}
  \label{fig:response_2step_nominal}
\end{figure}

\paragraph{Determination of the working point.}%
\label{par:determination_of_the_working_point_}
To determine the optimal working points of the two MVA classifiers, 
we maximize the Punzi FoM defined as
\begin{equation}
  F(t_1,t_2) = \frac{ \varepsilon(t_1,t_2) }{ \frac{a}{2} + \sqrt{B(t_1,t_2)} },
\end{equation}
where $t_1$ and $t_2$ are selection criteria 
on the first and the second MVA responses, respectively;
$\varepsilon$ and $B$ are defined and evaluated in the same manner 
as in Eq.~\ref{eq:fom_casea}.
The scan of the MVA responses is shown in Fig.~\ref{fig:FoM_twostep_nominal}.
The optimal working points are found to be $t_1>0.0$ and $t_2>0.70$. 
The trained MVA algorithms and the optimal working points 
are applied also to 2012 datasets.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/FoM_Tau80_BDTG_2step_nominal.pdf}
  \caption{Punzi figure of merit $F(t_1,t_2)$ 
  for the two-step MVA under the default lifetime hypothesis.}
  \label{fig:FoM_twostep_nominal}
\end{figure}

\paragraph{Invariant-mass distribution after event selection.}%
The invariant-mass distribution of $m(\Lc\Km\pip)$
after additional event selection for upper-limit setting is shown 
in Fig.~\ref{fig:unblind_Xiccp_caseb},
with candidates in the \Lc mass range from 2270\mev to 2306\mev.
As a comparison, the $m(\Lc\Km\pip)$ distribution of the WS control sample
is also shown in Figure~\ref{fig:unblind_Xiccp_caseb}.
The dashed red line indicates the mass of the \Xiccp baryon reported
by SELEX~\inlinecite{Mattson:2002vu},
and the dashed blue line refers to the mass of the \Xiccpp baryon~\inlinecite{
  LHCb-PAPER-2017-018,
  LHCb-PAPER-2018-026}.
The local $p$-value is also calculated in this case
as a function of $m(\Xiccp)$ for 2012, 2016--2018, and total data sets,
as shown in the right plot in Fig.~\ref{fig:unblind_Xiccp_caseb}.
Taking into account the look elsewhere effect
in the mass range of 3500\mev to 3700\mev according to Ref.~\inlinecite{Gross:2010qma},
the global $p$-value increases to $1.3\times10^{-3}$,
corresponding to a global significance of $3.0\sigma$.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/Compare_XiccM_upper_limit_2step.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/unblinding/LocalP_80fsCaseB.pdf}
  \caption{The (left) invariant-mass distributions of the \Xiccp candidates
  for the signal data sample collected in 2012 and 2016-2018
  and (right) local $p$-value as a function of $m(\Lc\Km\pip)$.
  The event selection for upper-limit setting is applied, 
  including the \Lc mass requirement of $2270\mev<M([\proton\Km\pip]_{\Lc})<2306\mev$.
  The right-sign (RS) $m(\Lc\Km\pip)$ distribution is shown in the right plot,
  along with the wrong-sign (WS) $m(\Lc\Km\pim)$ distribution
  normalized to have the same area as the RS sample.
  The dashed red line of 3518.7\mev indicates the mass of the \Xiccp baryon
  reported by SELEX
  and the dashed blue line of 3621.2\mev refers to the mass of the \Xiccpp baryon.}
  \label{fig:unblind_Xiccp_caseb}
\end{figure}

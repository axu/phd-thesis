Sources of systematic uncertainties are studied when calculating the upper limits,
including those in the efficiency estimation and in the yield determination.
Systematic uncertainties are summarized in Table~\ref{tab:ratio_sys_summary}.
The total systematic uncertainty is calculated 
as the quadratic sum of individual uncertainties,
assuming all sources to be independent.
The systematic uncertainty is dominated by the uncertainty of
the L0 trigger efficiency.
\begin{table}[tb]
  \centering
  \caption{Summary of the systematic uncertainties 
  in the upper limit setting.}
  \label{tab:ratio_sys_summary}
  \begin{tabular}{c c c c c}
    \toprule
    \multirow{2}*{Source} & \multicolumn{2}{c}{2016}&  \multicolumn{2}{c}{2012}\\
    %\cline{2-5}
    ~ & $R_\Lc$ & $R_\Xiccpp$ & $R_\Lc$ & $R_\Xiccpp$ \\
    \midrule
    Tracking          & 4.4\% & 3.1\%  &4.3\%  & 2.6\% \\
    PID               & 0.9\% & 0.8\%  &2.5\%  & 4.6\% \\
    L0 trigger        & 4.9\% & 11.2\% &11.7\% & 17.7\% \\
    Fit model         & 0.6\% & 0.4\%  &5.8\%  & 8.9\% \\
    \hline
    Total             & 6.7 &11.4&14.0&20.5 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Tracking efficiency uncertainty.}
\label{subsub:systematics_tracking}
As mentioned in Sec.~\ref{ssub:ratio_of_turbo_and_preselection_efficiencies}, 
the ratio of the tracking efficiencies is 
corrected for the discrepancy between data and simulated samples.
Three sources of systematic uncertainties 
related to this correction are considered.

The first source is the statistical uncertainty of the tracking correction table.
The effect is estimated using \num{1000} pseudoexperiments. 
In each pseudoexperiment,
a new correction table is created.
The correction factor in each bin is obtained
by sampling from the Gaussian distribution 
with the mean value of the default table 
and the width equal to its statistical uncertainty.
Then the reconstruction and selection efficiency is reevaluated with the new table,
using the same procedure as in the estimation of the default efficiency.
The Gaussian width of the distribution of the efficiency ratios
is assigned as the systematic uncertainty,
which, divided by the central value, is estimated to be 
$0.64\%$ and $1.33\%$ for the \Lc control mode in 2016 and 2012 datasets, respectively,
and $0.63\%$ and $0.16\%$ for the \Xiccpp control mode in 2016 and 2012 datasets, respectively.

The second source is the method used when generating the correction tables.
It is found to be $0.8\%$ and $0.4\%$ per track for 2016 and 2012 datasets
without the dependence on the kinematics of the track.
Assuming the systematic uncertainties for different type of particles 
in the signal and control mode are fully correlated,
the uncertainties due to the same type of particles 
in both the signal and control mode
(e.g., $p, K, \pi$ from the $\Lc$ decay) cancel out 
for the \Lc and \Xiccpp control modes.
The uncertainties are found to be $1.6\%$ and $0.8\%$ for 2016 datasets, 
and $0.8\%$ and $0.4\%$ for 2012 datasets.

The third source is related to the hadronic interaction of the final-state hadrons.
To study this effect, the full-simulated sample of \Xiccpp baryons is used
to estimate the fractions of kaons and pions %from \Xiccpp 
that end up with a hadronic interaction before the last T-station.
It is found to be $14.9\%$ and $25.4\%$ for \kaon and \pion, respectively.
Assuming, conservatively, that the overall uncertainty of 
detector material budget to be $10\%$
as discussed in Section 3.4.1 of \inlinecite{LHCb-PUB-2011-025},
the uncertainties for tracking efficiency due to hadronic interaction is thus 
$1.49\%$ and $2.54\%$ for \kaon and \pion, respectively.
Assuming the systematic uncertainties for different type of particles 
in the signal and control mode are fully correlated,
the uncertainties due to the same type of particles 
in both the signal and control mode
(e.g., $p, K, \pi$ from the $\Lc$ decay) cancel out.
The relative uncertainties for the \Lc and \Xiccpp control modes 
are found to be $4.0\%$ and $2.5\%$, respectively.

The systematic uncertainties on tracking efficiency 
are obtained by combining the above uncertainties as a sum in quadrature,
and are summarized in Table~\ref{tab:ratio_sys_trackeff}.
\begin{table}[tb]
  \centering
  \caption{Systematic uncertainties on ratios of the tracking efficiency.}
  \label{tab:ratio_sys_trackeff}
  \begin{tabular}{ccccc}
    \toprule
    \multirow{2}*{Source} & \multicolumn{2}{c}{2016}&  \multicolumn{2}{c}{2012}\\
    \cline{2-5}
    ~ & $r(\Lc)$ & $r(\Xiccpp)$ & $r(\Lc)$ & $r(\Xiccpp)$ \\
    \midrule
    Correction table uncertainty & 0.64\% & 0.63\% & 1.33\% & 0.16\% \\
    Tracking method              & 1.6\%  & 0.8\%  & 0.8\%  & 0.4\% \\
    Hadronic interaction         & 4.0\%  & 2.5\%  & 4.0\%  & 2.5\% \\
    \midrule
    Total                        & 4.4\%  & 3.1\%  & 4.3\%  & 2.6\% \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{PID efficiency.}
\label{subsub:systematics_PID}
The PID efficiency is evaluated with the PIDCalib package.
The systematic uncertainties due to the statistical uncertainty of correction tables 
are evaluated with simulation and are found to be negligible for 
both \Lc and \Xiccpp control modes.
The systematic uncertainty due to the binning scheme of the kinematic variables
is estimated by varying the binning schemes, 
in this case we double the momentum bins,
of the calibration sample. 
The discrepancy of the efficiency ratios between different binning schemes 
are taken as a systematic uncertainty. 
Results are summarised in Table~\ref{tab:ratio_sys_pideff}.
Due to the limited statistics, 
the systematics for 2012 data is larger.
\begin{table}[tb]
  \centering
  \caption{Systematic uncertainties on the ratios of the PID efficiencies.}
  \label{tab:ratio_sys_pideff}
  \begin{tabular}{lccc}
    \toprule
    PID efficiency Syst. [\%] & 2016 & 2012 \\
    \midrule
    $r(\Lc)$    &  0.9 & 2.5 \\
    $r(\Xiccpp)$&  0.8 & 4.6 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{L0 trigger efficiency.}
\label{subsub:systematics_trigger}
The default value of the trigger efficiency 
is estimated with the full-simulated samples. 
We treat the HLT triggers as well modelled in simulation, 
with appropriate corrections for tracking efficiency. 
However, it is known that simulation does not describe the L0 trigger well. 
To estimate the systematic uncertainty of the L0 trigger efficiencies, 
two extra control samples of
$\Lambda_{b}^{0}\rightarrow\Lc \pi^- \pi^+ \pi^-$ 
and $\Lambda_{b}^{0}\rightarrow\Lc \pi^-$ are used. 
These two channels have a similar decay topology 
compared with that of \Xiccp decays.
The systematic uncertainties of the L0 efficiency are thus calculated with
\begin{equation}
  \begin{aligned}
    &S_{\textrm{L0}}^{\textrm{TOS}}(\Lc)= \frac{\mid R_{\textrm{Data}}^{\textrm{TOS}}(\Lc)-R_{\textrm{MC}}^{\textrm{TOS}}(\Lc)\mid}{R_{\textrm{MC}}^{\textrm{TOS}}(\Lc)}  \\
    &S_{\textrm{L0}}^{\textrm{TOS}}(\Xiccpp)= \frac{\mid R_{\textrm{Data}}^{\textrm{TOS}}(\Xiccpp)-R_{\textrm{MC}}^{\textrm{TOS}}(\Xiccpp)\mid}{R_{\textrm{MC}}^{\textrm{TOS}}(\Xiccpp)}, \\
  \end{aligned}
\end{equation}
where the $R^{\textrm{TOS}}(\Lc)$ and $R^{\textrm{TOS}}(\Xiccpp)$ are defined as
\begin{equation}
  \begin{aligned}
    &R^{\textrm{TOS}}(\Lc) = \frac{\varepsilon^{\textrm{TOS}}(\LcDecay)}{\varepsilon^{\textrm{TOS}}(\Lambda_{b}^{0}\rightarrow\Lc \pi^- \pi^+ \pi^-)} \\
    &R^{\textrm{TOS}}(\Xiccpp) = \frac{\varepsilon^{\textrm{TOS}}(\Lambda_{b}^{0}\rightarrow\Lc \pi^- \pi^+ \pi^-)}{\varepsilon^{\textrm{TOS}}(\Lambda_{b}^{0}\rightarrow\Lc \pi^- )}.
  \end{aligned}
\end{equation}
The subscripts indicate whether the efficiency is evaluated 
with real data or simulation.
The L0 efficiency $\varepsilon^{\textrm{TOS}}$ is determined with the TISTOS method by
\begin{equation}
  \begin{aligned}
    &\varepsilon^{\textrm{TOS}}=\frac{N^{\textrm{TIS}\&\textrm{TOS}}}{N^{\textrm{TIS}}}
  \end{aligned}
\end{equation}
where $N^{\textrm{TIS}\&\textrm{TOS}}$ is the number of events 
that pass both L0 TIS and \texttt{Lc\_L0Hadron} TOS trigger requirements, 
and $N^{\textrm{TIS}}$ is the number of events that pass L0 TIS requirements. 
To be specific, 
the L0 TIS events are required to pass either of the following L0 TIS triggers: 
\texttt{L0Photon}, \texttt{L0Electron}, \texttt{L0Muon}, and \texttt{L0DiMuon}. 
The discrepancies of the measured L0 TOS efficiencies 
between simulation and data are assigned as the L0 efficiency systematics.
The measured efficiencies and corresponding systematic uncertainties are summarized in 
Table~\ref{tab:L0_eff_control_sample} and Table~\ref{tab:ratio_sys_l0eff}, respectively.
\begin{table}[tb]
  \centering
  \caption{Summary of the L0 efficiencies of the control channels.}
  \label{tab:L0_eff_control_sample}
  \begin{tabular}{l c c c c}
    %\begin{tabular}{c|c|c|c|c}
    \toprule
    \multirow{2}*{$R^{\textrm{TOS}}$ } &
    \multicolumn{2}{c}{2016} & \multicolumn{2}{c}{2012}\\
    %\multirow{2}*{Source} & \multicolumn{2}{c}{2016}&  \multicolumn{2}{|c}{2012}\\
    ~&Data & MC & Data & MC \\
    \midrule
    $R^{\textrm{TOS}}(\Lc)$      & 1.17$\pm$0.02 & 1.23$\pm$0.12 & 1.62$\pm$0.04 & 1.45$\pm$0.07 \\
    $R^{\textrm{TOS}}(\Xiccpp)$  & 0.99$\pm$0.02 & 0.89$\pm$0.04&1.13$\pm$0.03&0.96$\pm$0.04\\
    \bottomrule
    \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Systematic uncertainties on the L0 efficiency.}
  \label{tab:ratio_sys_l0eff}
  \begin{tabular}{cccc}
    \toprule
    L0 efficiency Syst. [\%] & 2016 & 2012 \\
    \midrule
    $r(\Lc)$ & 4.9& 11.7 \\
    $r(\Xiccpp)$ &  11.2& 17.7 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Yield determination.}
\label{subsub:systematics_fit}
For the \Lc control mode, 
the yield of the prompt $\Lc$ is extracted by the fit to the \logip distribution. 
The systematic effects is mainly due to the signal and background modelling. 
For the signal modelling, 
an alternative model with the ARGUS function is used. 
The difference of the obtained yields are assigned as the systematic uncertainty.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/yield/LcIPfit16_sigmodel_sys.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/yield/LcIPfit12_sigmodel_sys.pdf}
  \caption{Distribution of $\log{\chisqip}$ in (left) the 2016 data sample
  and (right) 2012 data sample, along with the fit results.}
  \label{fig:toy_lcfit}
\end{figure}

In the default result,
the shape of the combinatorial background is taken from the mass-sideband samples.
To evaluate the uncertainty in the background shape,
we utilise a toy approach. 
We take the default histogram and fluctuate the populations in each bin. 
The combinatorial yield in each bin 
is the sum of background sWeights.
The uncertainty on the yield is the sum of these weights squared. 
We resample the yield of each bin according to a Gaussian distribution, 
with the default bin yield as the mean and the uncertainty on that yield as the width. 
We repeat the fit \num{1000} times and 
perform a Gaussian fit to the distribution of the yield.
The width of the distribution is assigned as the systematic uncertainty 
due to background modelling. 
The fit result is shown in Fig.~\ref{fig:toy_lcfit_bkg} 
and the systematic uncertainties are summarized at Table~\ref{tab:fitmodelsys_ratio}.

The systematics are also studied using Transverse Impact Parameter
(TIP, the minimum distance between the track projection 
on a plane transverse to the beam and the PV)
as the discriminating variable.
The corresponding uncertainties are determined to be 4.2\% for 2016 sample 
and 5.5\% for 2012 sample. 
%The details are shown in Appendix~\ref{app:tip_fit_check}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/upperlimit/toy_lcbgfit16.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/upperlimit/toy_lcbgfit12.pdf}
  \caption{Distribution of the extracted prompt yields of toy experiments, 
  for the (left) 2016 data sample and (right) 2012 data sample,
  along with the fit results.}
  \label{fig:toy_lcfit_bkg}
\end{figure}

For the \Xiccpp control mode,
the systematics due to the signal modelling are studied 
by changing the signal shape to a two-Gaussian function.
The resultant changes in the yields are taken as systematics. 
To estimate the impact of the background model, 
we alter the background shape to a second order Polynomial function.
The relative systematic uncertainties are summarized in Table~\ref{tab:fitmodelsys_ratio}.
\begin{table}[tb]
  \centering 
  \caption{Systematic uncertainties due to the fit model.}
  \label{tab:fitmodelsys_ratio}
  \begin{tabular}{ccccc}
    \toprule
    \multirow{2}*{Source} & \multicolumn{2}{c}{2016}&  \multicolumn{2}{c}{2012}\\
    \cline{2-5}
    ~ & $r(\Lc)$ & $r(\Xiccpp)$ & $r(\Lc)$ & $r(\Xiccpp)$ \\
    \midrule
    signal model     & 0.6\% & 0.3\% & 5.8\% & 5.7\% \\
    background model & 0.2\%  & 0.3\%  & 0.4\%  & 6.8\% \\
    \midrule
    Total                        & 0.6\%  & 0.4\%  & 5.8\%  & 8.9\% \\
    \bottomrule
  \end{tabular}
\end{table}    


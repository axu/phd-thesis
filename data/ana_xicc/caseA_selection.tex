% event selection

In this section, the selection criteria are shown for the $\Xiccp\to\Lc\Km\pip$ signal mode.
The whole reconstruction and selection sequence 
is similar to that of the singly charmed baryon,
except that there are two intermediate states (\Xicc and \Lc baryon),
and hence two decay vertices instead of one in the decay chain.
The selection is developed with the 2016 data and simulation samples,
which is representative of the major data sets,
and are applied to the datasets of other data-taking years without reoptimisation. 
%The difference due to different center-of-mass energy is reduced with the 
%pre-selections applied in the trigger and offline. 

In this analysis, the \Lc mass window is defined to be [2270,2306]\mev, 
corresponding roughly to $\pm 3\sigma_m$ around the known \Lc mass~\inlinecite{PDG2020},
where $\sigma_m$ refers to the invariant mass resolution of \Lc baryon.
The \Xiccp mass window is defined to be [3300,3800]\mev,
which covers most of the theoretical predictions of the \Xiccp baryon mass.

\subsection{Trigger selection}%
\label{ssub:turbo_selection}

\paragraph{L0 trigger.}%
\label{par:l0_trigger_}
To retain maximal statistic power,
no specific L0 trigger lines are required,
which means every event that is L0-triggered will be considered in following steps.

\paragraph{HLT1 trigger.}%
\label{par:hlt1_trigger_}
The software trigger has evolved between Run~1 and Run~2
and between different data-taking years during Run~2,
to reflect the improvement of algorithm and increase of computational resources,
as discussed in Sec.~\ref{sub:the_lhcb_detector}.

For data samples taken in 2011-2012,
the \texttt{Hlt1TrackAllL0} TOS is required on \Lc candidates.
For 2016 data, 
the HLT1 TOS requirement \texttt{Hlt1TrackMVA||Hlt1TwoTrackMVA}
is embedded in the HLT2 line to \Lc candidates.
Details of these two HLT1 lines
are discussed in Sec.~\ref{sub:performance_of_the_lhcb_detector}.
For 2017-2018 data, 
the HLT1 TOS requirement \texttt{Hlt1TrackMVA||Hlt1TwoTrackMVA}
is \emph{removed} in the HLT2 line to increase the trigger efficiency.

\paragraph{HLT2 trigger.}%
\label{par:hlt2_trigger_}

For 2011-2015 data, the offline exclusive Stripping line is the counterpart 
of the exclusive HLT2 line in 2016-2018. 
Therefore, they are introduced here instead of as a part of the offline selection.
The selection requirements are summarised in Table~\ref{tab:turbo_stripping}
and are introduced below:
\begin{itemize}
  \item Selection of \Lc decay products.
    The \Lc candidates are reconstructed in the $\proton\Km\pip$ final state.
    Three tracks used to reconstruct the \Lc candidate are required to
    have a large transverse momentum, a good track quality,
    and not to originate from any PV.
    PID requirements are imposed on all three tracks 
    to suppress combinatorial background and misidentified charmed meson decays.
  \item Selection of the \Lc candidate.
    The \Lc vertex is required to be displaced from its PV.
    The \Lc candidate is required to have a mass in the range of 2211-2362\mev,
    and to be consistent with originating from its PV.
  \item Selection of \Xiccp decay products.
    The \Xiccp candidates are reconstructed by combining a \Lc candidate with two tracks,
    one identified as a \Km and one as a \pip.
    The kaon and pion tracks are required to
    have a large transverse momentum and a good track quality.
    These three tracks are required to form a common vertex.
    To suppress duplicate tracks,
    the angle between each pair of final-state tracks with the same charge is
    required to be larger than 0.5\mrad.
  \item Selection of the \Xiccp candidate.
    The \Xiccp candidate is required to have $\pt>4\gevc$ and to originate from its PV.
    Similar requirements are imposed to reconstruct the \Xiccpp candidates
    in the \Xiccpp normalization mode, with an additional \pip in the final state.
\end{itemize}
For 2017-2018 data, the DIRA cut on \Lc candidates is looser compared to that
in 2016 data, which reflects the improved knowledge of the \Xiccp lifetime
after the observation of the \Xiccpp baryon.
For 2012 data, an additional HLT2 requirement of
\verb|Hlt2CharmHadLambdaC2KPPi| TOS is applied to \Lcp candidates,
which is dedicated to charm baryon studies in Run~1 and is shown in Table~\ref{tab:HLT2}.
\begin{table}[tb]
  \centering
  \caption{HLT2 selection requirements for \Xiccp candidate.}
  \label{tab:turbo_stripping}
  \scriptsize
  \begin{tabular}{cllll}
    \toprule
    \multirow{2}*{Particle} & \multirow{2}*{Variable} & \multicolumn{3}{c}{Requirements} \\
    ~ & ~ & 2011--2015 & 2016 & 2017--2018 \\
    \midrule
    &Track $\chisqndf$                                          & $<5$                  & $<3$                   & $<3$                   \\
    &Kaon and Pion Momentum                                     & $>2\gev$             & $>1\gev$              & $>1\gev$              \\
    &Proton momentum                                            & $>2\gev$             & $>10\gev$             & $>10\gev$             \\
    &Transverse momentum                                        & $>0.25\gev$          & $>0.2\gev$            & $>0.2\gev$            \\
    &Arithmetic sum of daughter $\pt$                           & --                    & $>3\gev$              & $>3\gev$              \\
    &Maximum of daughter $\pt$                                  & --                    & $>1\gev$              & $>1\gev$              \\
    &Second maximum of daughter $\pt$                           & --                    & $>0.4\gev$            & $>0.4\gev$            \\
    $\Lc$ daughters &$\chisqip$ to PV                           & $>4$                  & $>6$                   & $>6$                   \\
    &Maximum of daughter $\chisqip$                             & $>4$                  & $>16$                  & $>16$                  \\
    &Second maximum of daughter $\chisqip$                      & --                    & $>9$                   & $>9$                   \\
    & HASRICH                                                   & 1                     &                        &                        \\
    &Proton particle ID $\dllppi$                               & $>5$                  & $>5$                   & $>5$                   \\
    &Proton particle ID $\mathrm{DLL}_{\proton\kaon}$           & $>0$                  & $>5$                   & $>5$                   \\
    &Kaon particle ID $\dllkpi$                                 & $>5$                  & $>5$                   & $>5$                   \\
    &Pion particle ID $\dllkpi$                                 & $<0$                  & $<5$                   & $<5$                   \\
    \midrule                                                                                                                             
    &Transverse momentum                                        & $>1\gev$             &                        &                        \\
    &Vertex $\chisqvtxndf$                                      & $<10$                 & $<10$                  & $<10$                  \\
    &Maximum DOCA                                               &$<0.5\mm$              &                        &                        \\
    %&Maximum distance of closest approach      & - &$<0.5$ \\                                                                           
    $\Lc$     &Cosine of decay angle (DIRA)                     & $>0.99$               & $>0.99995$             & $>0$                   \\
    &Vertex distance \chisq                                     &$>16.0$                &                        &                        \\
    &Decay time                                                 &                       &$>0.15\ps$              &$>0.15\ps$              \\
    &Invariant mass [\mev]                                    & $(2211,2362)$         &$(2211,2362)$           &$(2211,2362)$           \\
    \midrule                                                                                                                             
    &Track $\chisqndf$                                          & $<5$                  & $<3$                   & $<3$                   \\
    &Transverse momentum                                        & $>0.25\gev$          & $>0.5\gev$            & $>0.5\gev$            \\
    $\Xiccp$ daughters &Momentum                                & $>2\gev$             &$>1\gev$               &$>1\gev$               \\
    &HASRICH                                                    & 1                     &                        &                        \\
    &Kaon particle ID $\dllkpi$                                 & $>5$                  &$>10$                   &$>10$                   \\
    &Pion particle ID $\dllkpi$                                 & $<0$                  &$<0$                    &$<0$                    \\
    \midrule                                                                                                                             
    &Vector sum of daughter $\pt$                               & $>2\gev$             &$>2\gev$               &$>2\gev$               \\
    &Vertex $\chisqvtxndf$                                      & $<10$                 &$<30$                   &$<30$                   \\
    &Maximum DOCA                                               &                       &$<0.5$\mm               &$<0.5$\mm               \\
    %&Maximum distance of closest approach     & - &$<0.5$\mm\\                                                                          
    &DOCA between $(\Lc,\pip)$, $(\Km,\pip)$                    &                       &$<10$\mm                &$<10$\mm                \\
    $\Xiccp$ &$\Lc$ vertex $z$ displacement w.r.t. $\Xiccp$     & $>0.01\mm$            & $>0.01\mm$             & $>0.01\mm$             \\
    &DIRA                                                       &$>0$                   & $>0$                   & $>0$                   \\
    &Vertex distance \chisq                                     &$>-1$                  &                        &                        \\
    &Invariant mass [\mev]                                    & $<4000$               &$(3100,4000)$           &$(3100,4000)$           \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{HLT2 trigger requirements of \texttt{Hlt2CharmHadLambdaC2KPPi} line.}
  \label{tab:HLT2}
  \begin{tabular}{cll}
    \toprule
    Particle & Variable & Cut \\
    \midrule
    &Track $\chisqndf$   & $<3$\\
    &Transverse momentum & $>0.5\gev$\\
    Daughters of $\Lc$   &$\chisqip$ to PV    & $>9$\\
    &Proton transverse momentum & $>1.5\gev$\\
    &Proton Momentum     & $>10\gev$\\
    %                           &Proton particle ID $\mathrm{DLL}_{\proton\kaon}$ & $>0$\\
    &Proton particle ID $\dllppi$      & $>0$ \\
    & Arithmetic sum of daughter \pt   & $>2.5$ \gev \\
    \midrule
    &Transverse momentum & $>2.5\gev$\\
    &Vertex $\chisqvtxndf$           &$<15$\\
    $\Lc$                 &Cosine of decay angle (DIRA)    &$>0.99985$\\
    &$\rho$ distance between the end vertex and PV                          &$<4.0$ mm\\
    %&BPVVDR                          &$<4.0$\\
    &Vertex distance \chisq          &$>49$ \\
    \bottomrule
  \end{tabular}
\end{table}

\subsection{Offline preselection}%
\label{ssub:cut_based_offline_selection}

After the trigger selection, 
the \Xiccp candidates are refitted with DecayTreeFitter (DTF)
and additional preselection is applied.
The DTF is performed to the whole decay chain, 
and the \Xiccp candidate is constrained to originate from its PV. 
Details of the preselection is shown in Table~\ref{tab:preselection_xicc}.
The purpose is to have high overall signal efficiency 
while cleaning up most of the obvious 
combinatorial or misidentification backgrounds. 
\begin{table}[tb]
  \centering
  \caption{Preselection requirements.}
  \label{tab:preselection_xicc}
  \begin{tabular}{c  l  l}
    \toprule
    Particle & Variable & Cut \\
    \midrule
    & Momentum & in $(2,150)\gev$\\
    All final tracks & Pseudorapidity & in $(1.5,5.0)$\\
    & ProbNNghost & $ < 0.9$\\
    \midrule
    Proton    & ProbNNp & $ > 0.1$\\
    Kaon      & ProbNNk & $ > 0.1$\\
    Pion      & ProbNNpi& $ > 0.1$\\
    \midrule
    & $\log(\chisqip)$ &  $< 4$\\
    \Xiccp    & $\chisqvtxndf$ & $ < 10$\\
    & $\pt$   & $ > 4\gev$\\
    & $\chi^{2}_{\text{DTF}}$ & $ < 50$\\
    \bottomrule
  \end{tabular}
\end{table}

\subsection{Multivariate analysis}%
\label{sec:multivariate_analysis}

After the preselection, 
a multivariate analysis (MVA) is performed to further improve the sensitivity.
The MVA selectors are trained with the lifetime hypotheses of
$\tau(\Xiccp)=80\fs$ (default) and $\tau(\Xiccp)=0\fs$ (zero), respectively. 

\paragraph{Training samples.}%
\label{par:training_samples_}
The training samples for signal are 2016 MC samples 
filtered by trigger selection and preselection.
The MC sample reweighted to $\tau(\Xiccp)=80\fs$ is used 
for the $\tau(\Xiccp)=80\fs$ lifetime hypothesis.
The MC sample generated with $\tau(\Xiccp)=0\fs$ is used 
for the $\tau(\Xiccp)=0\fs$ lifetime hypothesis.
The training sample for the background is the 2016 
WS sample filtered by the same trigger selection and preselection.
Due to the large statistics, 
only $5\%$ of the total filtered WS sample is used in the training.
Both the signal and background samples are required to 
lie in the \Lc and \Xiccp mass windows.
Following the standard TMVA approach, 
both the signal and background samples
are randomly split into two equally-sized disjoint subsamples 
for the purpose of training and test, respectively.

\paragraph{Training variables.}%
\label{par:training_variables_}
The training variables are selected from a long list of candidate variables.
Those that do not significantly contribute to the overall performance
and/or are highly correlated with other variables are removed. 
For zero lifetime hypothesis, variables strongly correlated 
to the lifetime of the \Xiccp baryon are removed.
The final training variables for default lifetime hypothesis 
are summarised in Table~\ref{tab:training_variable}.
%\begin{itemize}
%  \item \chisqvtxndf of the \Lc vertex fit;
%  \item \chisqvtxndf of the \Xiccp vertex fit (without DTF);
%  \item DTF \chisqvtxndf of the \Xiccp candidate with PV constraint;
%  \item Maximum distance of the closest approach of \Xiccp;
%  \item Log sized \pt of \Xiccp, \Lc and of their daughters;
%  \item Log sized \chisqip of \Xiccp to PV;
%  \item $\cos^{-1}(\text{DIRA})$ of \Xiccp to PV;
%  \item Log sized flight distance \chisq of \Lc to PV;
%  \item Log sized flight distance \chisq of \Xiccp to PV;
%  \item Log sized \chisqip of daughters of \Xiccp to PV.
%\end{itemize}
Their distributions for signal and background samples 
are shown in Fig.~\ref{fig:input_variable_nominal}.
%The rank of input variables (BDTG method specific) 
%for default lifetime hypothesis is shown on Table~\ref{tab:rank_nominal}.
The final training variables for zero lifetime hypothesis 
are also summarised in Table~\ref{tab:training_variable}.
%\begin{itemize}
%  \item \chisqvtxndf of the \Lc vertex fit;
%  \item \chisqvtxndf of the \Xiccp vertex fit (without DTF);
%  \item DTF \chisqvtxndf of the \Xiccp candidate with PV constraint;
%  \item Maximum distance of the closest approach of \Xiccp;
%  \item Maximum distance of the closest approach of \Lc;
%  \item Minimum \pt of daughters of \Xiccp;
%  \item Minimum \pt of daughters of \Lc;
%  \item Scalar sum of \pt of daughters of \Xiccp.
%  \item Log sized flight distance \chisq of \Lc to PV;
%  \item Log sized flight distance \chisq of \Lc to its original vertex;
%  \item Minimum log sized \chisqip of daughters of \Xiccp to PV;
%\end{itemize}
Their distributions for signal and background samples 
are shown in Fig.~\ref{fig:input_variable_zero}.
%The rank of input variables (MLPBNN method specific) 
%for zero lifetime hypothesis is shown on Table~\ref{tab:rank_zero}.
The correlation matrices of the training variables
for default and zero lifetime hypotheses 
are shown in Fig.~\ref{fig:correlation_nominal} 
and Fig.~\ref{fig:correlation_zero}, respectively.
\begin{table}[tb]
  \centering
  \caption{MVA training variables for default and zero lifetime hypotheses.}
  \label{tab:training_variable}
  \scriptsize
  \begin{tabular}{lcc}
    \toprule
    Variable & Nominal lifetime & Zero lifetime \\
    \midrule
    \chisqvtxndf of the \Lc vertex fit & $\surd$ & $\surd$ \\
    \chisqvtxndf of the \Xiccp vertex fit (without DTF) & $\surd$ & $\surd$ \\
    DTF \chisqvtxndf of the \Xiccp candidate with PV constraint & $\surd$ & $\surd$ \\
    Maximum distance of the closest approach of \Xiccp & $\surd$ & $\surd$ \\
    Maximum distance of the closest approach of \Lc & & $\surd$ \\
    Log sized \pt of \Xiccp, \Lc and of their daughters & $\surd$ & \\
    Minimum \pt of daughters of \Xiccp & & $\surd$ \\
    Minimum \pt of daughters of \Lc & & $\surd$ \\
    Scalar sum of \pt of daughters of \Xiccp & & $\surd$ \\
    Log sized \chisqip of \Xiccp to PV & $\surd$ & \\
    $\cos^{-1}(\text{DIRA})$ of \Xiccp to PV & $\surd$ & \\
    Log sized flight distance \chisq of \Lc to PV & $\surd$ & $\surd$ \\
    Log sized flight distance \chisq of \Lc to its original vertex & & $\surd$ \\
    Log sized flight distance \chisq of \Xiccp to PV & $\surd$ & \\
    Log sized \chisqip of daughters of \Xiccp to PV & $\surd$ & \\
    Minimum log sized \chisqip of daughters of \Xiccp to PV & & $\surd$ \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/tmva_1step/variables_id_c1_nominal.pdf}
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/tmva_1step/variables_id_c2_nominal.pdf}
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/tmva_1step/variables_id_c3_nominal.pdf}
  \caption{Distributions of input variables for signal and background samples 
  under default lifetime hypothesis.}
  \label{fig:input_variable_nominal}
\end{figure}
%\begin{table}[tb]
%  \centering
%  \caption{Rank of input variables (BDTG method specific) for default lifetime hypothesis.}
%  \label{tab:rank_nominal}
%  \scriptsize
%  \begin{tabular}{rll}
%    \toprule
%    Rank & Variable                & Variable Importance \\
%    \midrule
%    1 & \verb|log_XiccK_PT            | & 1.137e-01 \\
%    2 & \verb|log_XiccPi_PT           | & 1.126e-01 \\
%    3 & \verb|log(C_FDCHI2_OWNPV)     | & 8.532e-02 \\
%    4 & \verb|C_PVFit1_chi2_nDOF      | & 7.420e-02 \\
%    5 & \verb|log_C_IPCHI2_OWNPV      | & 6.780e-02 \\
%    6 & \verb|acos(C_DIRA_OWNPV)      | & 6.345e-02 \\
%    7 & \verb|log_C_PT                | & 6.077e-02 \\
%    8 & \verb|log(Lc_FDCHI2_OWNPV)    | & 5.188e-02 \\
%    9 & \verb|log_XiccPi_IPCHI2_OWNPV | & 4.807e-02 \\
%    10 & \verb|C_MAXDOCA               | & 4.560e-02 \\
%    11 & \verb|log_XiccK_IPCHI2_OWNPV  | & 4.415e-02 \\
%    12 & \verb|log_LcPi_PT             | & 4.269e-02 \\
%    13 & \verb|C_ENDVERTEX_CHI2_NDOF   | & 3.846e-02 \\
%    14 & \verb|log_LcP_PT              | & 3.776e-02 \\
%    15 & \verb|Lc_ENDVERTEX_CHI2_NDOF  | & 3.278e-02 \\
%    16 & \verb|log_Lc_IPCHI2_OWNPV     | & 3.031e-02 \\
%    17 & \verb|log_LcK_PT              | & 2.750e-02 \\
%    18 & \verb|log_Lc_PT               | & 2.298e-02 \\
%    \bottomrule 
%  \end{tabular}
%\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/tmva_1step/variables_id_c1_0tau.pdf}
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/tmva_1step/variables_id_c2_0tau.pdf}
  \caption{Distributions of input variables for signal and background samples 
  under zero lifetime hypothesis.}
  \label{fig:input_variable_zero}
\end{figure}
%\begin{table}[tb]
%  \centering
%  \caption{Rank of input variables (MLPBNN method specific) for zero lifetime hypothesis.}
%  \label{tab:rank_zero}
%  \scriptsize
%  \begin{tabular}{rll}
%    \toprule
%    Rank & Variable                & Variable Importance \\
%    \midrule
%    1 &\verb| min_Xicc_Daughters_PT         | &  1.931e+01\\ 
%    2 &\verb| min_Lc_Daughters_PT_          | &  1.387e+01\\
%    3 &\verb| Lc_MAXDOCA                    | &  1.307e+01\\
%    4 &\verb| sum_Xicc_Daughters_PT         | &  1.247e+01\\
%    5 &\verb| acos_C_DIRA_OWNPV_            | &  1.141e+01\\
%    6 &\verb| C_MAXDOCA                     | &  1.007e+01\\
%    7 &\verb| Lc_ENDVERTEX_CHI2_NDOF        | &  8.668e+00\\
%    8 &\verb| C_ENDVERTEX_CHI2_NDOF         | &  6.187e+00\\
%    9 &\verb| C_PVFit1_chi2_nDOF            | &  3.714e+00\\
%    10 &\verb| log_Lc_FDCHI2_OWNPV_          | &  3.681e+00\\
%    11 &\verb| log_C_FDCHI2_OWNPV_           | &  6.440e-01\\
%    12 &\verb| log_min_Xicc_Daughters_IPCHI2 | &  2.761e-01\\
%    \bottomrule 
%  \end{tabular}
%\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixS_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixB_nominal.pdf}
  \caption{Correlation matrices of the training variables of
  (left) signal and (right) background samples for default lifetime hypothesis.}
  \label{fig:correlation_nominal}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixS_0tau.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/CorrelationMatrixB_0tau.pdf}
  \caption{Correlation matrices of the training variables of (left) signal 
  and (right) background samples for zero-lifetime hypothesis.}
  \label{fig:correlation_zero}
\end{figure}

\paragraph{MVA algorithms.}%
\label{par:mva_algorithms_}
Several classification algorithms are considered in the training. 
Boosted-decision-tree (BDT)
and multilayer-perceptron (MLP) based algorithms show best performance
and are considered further.
Receiver Operation Characteristics (ROC) curves of BDT and MLP based algorithms
are shown in Fig.~\ref{fig:ROC}. 
The response distributions are shown in Fig.~\ref{fig:Overtraining_nominal} and 
Fig.~\ref{fig:Overtraining_0tau} for default and zero lifetime hypothesis, respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/rejBvsS_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/rejBvsS_0tau.pdf}
  \caption{ROC curves for different algorithms under 
  (left) default and (right) zero-lifetime hypotheses.}
  \label{fig:ROC}
\end{figure}
\begin{figure}[tb]
  \centering
  \begin{center}
    \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_BDT_nominal.pdf}
    \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_BDTG_nominal.pdf}
    \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_MLP_nominal.pdf}
    \includegraphics[width=0.45\linewidth]{ana_xicc/selection/overtrain_MLPBNN_nominal.pdf}
  \end{center}
  \caption{Response distributions for 
  (top left) BDT,  (top right) BDTG, and (bottom) MLP algorithms,
  under default lifetime hypothesis.}
  \label{fig:Overtraining_nominal}
\end{figure}
\begin{figure}[tb]
  \centering
  \begin{center}
    \includegraphics[width=0.32\linewidth]{ana_xicc/selection/overtrain_BDTG_0tau.pdf}
    \includegraphics[width=0.32\linewidth]{ana_xicc/selection/overtrain_MLP_0tau.pdf}
    \includegraphics[width=0.32\linewidth]{ana_xicc/selection/overtrain_MLPBNN_0tau.pdf}
  \end{center}
  \caption{Response curves for
  (left) BDTG, (middle) MLP, and (right) MLPBNN algorithms, 
  under zero lifetime hypothesis.}
  \label{fig:Overtraining_0tau}
\end{figure}

\paragraph{Determination of working point.}%
\label{par:determination_of_working_point_}
Punzi figure of merit (FoM) is used 
to quantify the performance of classification algorithms
and to determine their optimal working point~\inlinecite{Punzi:2003bu}. 
The FoM as a function of the MVA response cut $t$ is defined as 
\begin{equation}
  \label{eq:fom_casea}
  F(t) = \frac{ \varepsilon(t) }{ \frac{a}{2} + \sqrt{B(t)} },
\end{equation}
where $\varepsilon(t)$ is the total signal efficiency, 
$B(t)$ the expected background in the \Xiccp signal window of the RS sample,
and $a=5$ is the desired significance.
The signal efficiency $\varepsilon(t)$ is calculated by 
$$\varepsilon(t) = \frac{N_{\text{pass}}(t)}{N_{\text{gen}}},$$
where $N_{\text{gen}}$ is the number of signals generated 
and $N_{\text{pass}}(t)$ the number of 
signals passing the trigger selection, the preselection 
and with the MVA response larger than $t$. 
The expected background $B(t)$ is evaluated as
$$B(t) = B_{\text{raw}}(t) \times f_{\text{scale}} \times f_{\text{RS}} \times f_{\text{window}},$$
where 
\begin{itemize}
  \item $B_{\text{raw}}(t)$ is the number of events of the test sample passing 
    the turbo selection, the preselection and the MVA cut $t$;
  \item $f_{\text{scale}}=40$ is the factor for the use of partial WS sample 
    (the test background sample, which is $50\%\times5\%$ of the total WS sample);
  \item $f_{\text{RS}}=0.960$ is the factor for the difference of the background level between the RS and the WS samples. 
    It is taken as the ratio of the number of events in the \Xiccp mass sideband of the RS and WS samples;
  \item $f_{\text{window}}=0.056$ is the factor to normalize the number of events from the wide \Xiccp mass window
    to that in the narrow signal window of $\pm2.5\sigma$
    ($\sigma=5~\mevcc$, determined from simulated signal) around the
    nominal $\Xiccp$ mass (chosen to be $3621.4\mevcc$).
    It is evaluated with the WS sample by first fitting the \Xiccp mass spectrum with 
    the second order polynomials and then calculating 
    the fraction of events in the signal window by integral.
\end{itemize}

The variation of $F(t)$ for different algorithms are shown in Fig.~\ref{fig:FoM}. 
The optimal working point and the performance for each algorithm 
are summarised in Tables~\ref{tab:working_point_nominal} and \ref{tab:working_point_zero} 
for default and zero lifetime hypotheses, respectively.
For default lifetime hypothesis, 
it is found that the performance is similar among algorithms. 
The BDTG algorithm at working point $t=0.70$ is chosen 
due to the slightly better performance.
For zero lifetime hypothesis, 
the MLPBNN algorithm at working point $t=0.88$ shows the best performance.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/FoM_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/FoM_0tau.pdf}
  \caption{Punzi figure of merit $F(t)$ as a function of the MVA response $t$, 
  under (left) default and (right) zero lifetime hypotheses.}
  \label{fig:FoM}
\end{figure}
\begin{table}[tb]
  \centering
  \caption{Optimal working point and 
  the performance for different algorithms under default lifetime hypothesis, 
  where $\varepsilon_{\text{MVA}}$ is the efficiency of the MVA cut.}
  \label{tab:working_point_nominal}
  \begin{tabular}{c c c c}
    \toprule
    Algorithm & Optimal cut & $F(t)\,(\times 10^{-6})$ & $\varepsilon_{\text{MVA}}$ \\
    \midrule
    BDT  & 0.04 & 3.74$\pm$0.10 & 0.33$\pm$0.01 \\
    BDTG & 0.70  & 4.01$\pm$0.10 & 0.43$\pm$0.01 \\
    MLP  & 0.92 & 3.85$\pm$0.09 & 0.41$\pm$0.01 \\
    MLPBNN  & 0.92 & 3.98$\pm$0.10 & 0.38$\pm$0.01 \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Optimal working point and 
  the performance for different algorithms under zero lifetime hypotheses, 
  where $\varepsilon_{\text{MVA}}$ is the efficiency of MVA cut.}
  \label{tab:working_point_zero}
  \begin{tabular}{c c c c}
    \toprule
    Algorithm & Optimal cut & $F(t)\,(\times 10^{-6})$ & $\varepsilon_{\text{MVA}}$ \\
    \midrule
    BDTG & 0.66  & 3.07$\pm$0.12 & 0.38$\pm$0.01 \\
    MLP  & 0.92  & 3.12$\pm$0.14 & 0.27$\pm$0.01 \\
    MLPBNN  & 0.88  & 3.13$\pm$0.13 & 0.35$\pm$0.01 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Cross-checks.}%
\label{par:cross_checks_}
Some cross-checks are performed 
of the chosen MVA algorithms and optimal working points.
Firstly, the \Xiccp mass distribution and 
the background retention rate in each mass bin of the WS sample are examined, 
as illustrated in Fig.~\ref{fig:MVA_Bkg}. 
No peaking structure is observed in this background sample. 
Secondly, the performance of the MVA selection is studied at different lifetime hypotheses
by reweighting the decay time.
Two approaches are adopted here: 
1) apply the default MVA algorithm with reoptimised 
working point for different lifetime hypotheses; 
2) apply the default MVA algorithm and the working point 
directly to reweighted samples. 
The results are summarised in Fig.~\ref{fig:FoM_vary_tau}.
The vertical axis shows the Punzi FoM, 
which also reflects the variation of efficiency,
since the denominator (desired significance and the number of background) 
is not affected by reweighting.
As expected, the performance is better at longer lifetimes.
It can be seen that the performance varies roughly linearly with the lifetime hypothesis
in the neighbourhood of default lifetime. 
By comparing the two approaches, 
the default MVA algorithm and the corresponding working point
gives similar although less optimal performance when the genuine
lifetime of the sample is different from that used to train the MVA classifier.
Therefore, the MVA selection works well in a wide range of lifetime hypotheses.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/BDTG_Bkg_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/BDTG_Bkg_Retention_nominal.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/BDT_Bkg_0tau.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/selection/BDT_Bkg_Retention_0tau.pdf}
  \caption{ The (left) \Xiccp invariant-mass distribution 
  and (right) the background retention after 
  the MVA cut in each invariant-mass bin of the WS sample,
  for (top) default and (bottom) zero lifetime hypotheses.}
  \label{fig:MVA_Bkg}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/selection/FoM_compare_tau.pdf}
  \caption{Punzi FoM evaluated with reweighted MC samples 
  for different lifetime hypotheses. 
  The blue line refers to the approach 
  in which reoptimisation is performed, 
  and the red line refers to the approach 
  where the default working point is used.}
  \label{fig:FoM_vary_tau}
\end{figure}


\subsection{Removal of track-clone candidates}%
\label{ssub:removal_of_internal_track_clone_candidates}

As a multi-body decay with five final tracks, 
the reconstructed \Xiccp candidates 
are expected to suffer from the contamination of internal-track-clone candidates.
An internal-track-clone candidate refers to the candidate in which 
at least one track is a clone of another track of the same candidate.
The distribution of the angle between such two clone tracks,
$\delta\theta_{i,j}$, is supposed to peak at zero.

The distributions of the angles for any pair of tracks with the same charge are shown
in Figs~\ref{fig:angle_ws} and \ref{fig:angle_mc} for WS and MC samples, respectively. 
A peak around zero is observed in the WS sample, 
while not in the MC sample. 
A requirement of $\delta\theta_{i,j}>0.5\mrad$ is added to the selection sequence 
to remove the internal-track-clone candidates. 
This will remove around 2.2\% candidates in the WS sample, 
while keeping $>99\%$ of the signals in the MC sample.
The mass distribution of the internal-track-clone candidates of the WS
sample is shown in Fig.~\ref{fig:M_WS_Clone},
in which there is no significant peak.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/Angle_WS.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/Angle_WS_zoom.pdf}
  \caption{Distributions of angles of tracks with the same charge in the WS sample.
  The right plot zooms in on the zero angle.}
  \label{fig:angle_ws}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/Angle_MC.pdf}
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/Angle_MC_zoom.pdf}
  \caption{Distributions of angles of tracks with the same charge in the MC sample.
  The right plot zooms in on the zero angle.}
  \label{fig:angle_mc}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/M_WS_Clone.pdf}
  \caption{Mass distribution of the internal-track-clone candidates of the WS sample.}
  \label{fig:M_WS_Clone}
\end{figure}


\subsection{Duplicate candidates}%
\label{ssub:duplicate_candidates}

Multiple candidates in the same event are expected
since track multiplicity of the final state is high.
In one scenario, two or more candidates in the same event share a \Lc candidate
but have different additional tracks to form the \Xiccp candidate.
It is known that this kind of candidates do not create fake narrow peaks
and confirmed in the mass measurement of \Xiccpp baryon.%~\inlinecite{LHCb-ANA-2017-025}.
We tend to keep them to avoid any possible bias and reduction of the signal significance.

Duplicate candidates are a special kind of multiple candidates,
which may increase the signal significance artificially.
There are three scenarios: 
1) the \Km from the \Lc decay is swapped with the \Km directly from the \Xiccp decay; 
2) the \pip from the \Lc decay is swapped with the \pip directly from the \Xiccp decay; 
3) a combination the above two cases.
The selection requirements such as the \Lc mass window and vertex fit quality
help to suppress duplicate candidates, 
but they may still have a chance to survive.

The mass distribution of the duplicate candidates is studied with the MC sample.
The truth-matching requirements in the MC sample are loosened to allow in 
the duplicate candidates. 
A total of 39 sets of duplicate candidates 
(involving 79 candidates) are found out of \num{3593} entries.
The mass distribution is shown in Fig.~\ref{fig:M_MC_Dup}, 
where a peak around the hypothetical \Xiccp mass is observed. 
In practice, only one of the duplicate candidates of the same set
can be a signal due to the small production cross-section of doubly charmed baryons. 
To avoid artificial increase of the significance, 
the following procedure is adopted. 
Events with multiple candidates are examined to see whether or not the 
final tracks are exactly the same. 
If yes, a single candidate is randomly selected out of the set of duplicate candidates.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_xicc/mass/M_MC_Dup.pdf}
  \caption{Mass distribution of the duplicate candidates of the MC sample.}
  \label{fig:M_MC_Dup}
\end{figure}

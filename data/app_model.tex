\chapter{Statistical models}%
\label{sub:statistical_models}

There are various analytical functions used in the maximum likelihood fit
to describe the probability distribution function 
(different from the parton distribution function in a hadron),
as discussed below.

\paragraph{Gaussian distribution.}%
\label{par:gaussian_distribution_}
Gaussian distribution function is the most important one,
defined as
\begin{equation}
  \mathcal{P}(x;\mu,\sigma) = 
  \exp \left( -\frac{1}{2} {\left( \frac{x-\mu}{\sigma} \right)}^{2} \right).
\end{equation}
It can be used to describe the resolution effect of a measured quantity.
It can also be used to build more involved functions.

\paragraph{Double-sided crystal ball function.}%
\label{par:double_sided_crystal_ball_function_}
The double-sided crystal ball function has a Gaussian core
and power-lay tails on each side, defined as
\begin{equation}
  \mathcal{P}(x;\mu,\sigma,a_L,n_L,a_R,n_R) =
  \begin{cases}
    A_{L} \times \left( B_{L} - \frac{x-\mu}{\sigma} \right)^{-n_{L}}          & \textrm{for $\frac{x-\mu}{\sigma} \leqslant -|a_{L}|$}, \\
    \exp \left( -\frac{1}{2} {\left( \frac{x-\mu}{\sigma} \right)}^{2} \right) & \textrm{for $-|a_{L}| < \frac{x-\mu}{\sigma} \leqslant |a_{R}|$}, \\
    A_{R} \times {\left( B_{R} + \frac{x-\mu}{\sigma} \right)}^{-n_{R}}        & \textrm{for $\frac{x-\mu}{\sigma} > |a_{R}|$},
  \end{cases}
\end{equation}
where $A_L = \left(\frac{n_L}{|a_L|}\right)^{n_L} \times \exp \left(-\frac{a^2_L}{2}\right)$,
$B_L = \frac{n_L}{|a_L|} - |a_L|$,
$A_R = \left(\frac{n_R}{|a_R|}\right)^{n_R} \times \exp \left(-\frac{a^2_R}{2}\right)$,
and $B_R = \frac{n_R}{|a_R|} - |a_R|$.
It can be used to describe the effect due to final-state radiation
and the kinematic refit of the decay tree.

\paragraph{Bukin function.}%
\label{par:bukin_function_}
Bukin function has a Gaussian-like core with exponential tails on both sides, defined as
\begin{equation}
  \mathcal{P}(x;\mu,\sigma,\xi,\rho_1,\rho_2) =
  \begin{cases}
    \exp \left\{ \frac{ (x-x_1)\xi\sqrt{\xi^2+1}\sqrt{2\ln{2}} }{ \sigma\left(\sqrt{\xi^2+1}-\xi\right)^2\ln\left(\sqrt{\xi^2+1}+\xi\right) }
    + \rho_1\left(\frac{x-x_1}{\mu-x_1}\right)^2 - \ln{2} \right\} & x \leq x_1, \\
    \exp \left\{ - \left[ \frac{ \ln\left(1+2\xi\sqrt{\xi^2+1}\frac{x-\mu}{\sigma\sqrt{2\ln{2}}}\right) }
    { \ln\left(1+2\xi^2-2\xi\sqrt{\xi^2+1}\right) } \right ]^2 \times \ln{2} \right\} & x_1 < x < x_2, \\
    \exp \left\{ \frac{ (x-x_2)\xi\sqrt{\xi^2+1}\sqrt{2\ln{2}} }{ \sigma\left(\sqrt{\xi^2+1}-\xi\right)^2\ln\left(\sqrt{\xi^2+1}+\xi\right) }
    + \rho_2 \left( \frac{x-x_2}{\mu-x_2} \right)^2 - \ln{2} \right\} & x \geq x_2,
  \end{cases}
\end{equation}
where $x_1 = \mu + \sigma \sqrt{2 \ln 2} \left( \frac{\xi}{\sqrt{\xi^2+1}} -1 \right)$,
and $x_2 = \mu + \sigma \sqrt{2 \ln 2} \left( \frac{\xi}{\sqrt{\xi^2+1}} +1 \right)$.
Its tail parameters are flexible enough to describe
asymmetric distributions such as a monotonic distribution after a logarithmic transformation.
As an illustration,
a family of Bukin functions with different parameters are shown in Fig.~\ref{fig:Bukin}.
The parameter $\mu$ is the peak position,
$\sigma$ describes the width of the distribution,
$\xi$ indicates the degree of deviation from the Gaussian,
and $\rho_1$ and $\rho_2$ control the shape of the left and right tail, respectively.
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_omegac/Bukin.pdf}
  \caption{A family of Bukin functions with different parameters and
  $\mu=0$, $\sigma=1$, $\rho_1=0$.}
  \label{fig:Bukin}
\end{figure}

\paragraph{ARGUS function.}%
\label{par:argus_function_}
The ARGUS distribution, named after the high-energy physics experiment ARGUS,
is defined as
\begin{equation}
  \mathcal{P}(x ; \chi, c) = \frac{\chi^{3}}{\sqrt{2 \pi} \Psi(\chi)} \times 
  \frac{x}{c^{2}} \sqrt{1-\frac{x^{2}}{c^{2}}} 
  \exp \left\{-\frac{1}{2} \chi^{2}\left(1-\frac{x^{2}}{c^{2}}\right)\right\}
\end{equation}
for $0<x<c$,
where $\Psi(\chi)=\Phi(\chi)-\chi \phi(\chi)-\frac{1}{2}$.
It is usually used to describe the invariant-mass distribution 
of the partially reconstructed decay.
A family of ARGUS function with different parameters is shown in Fig.~\ref{fig:argus}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{introduction/ArgusPDF.png}
  \caption{A family of ARGUS function with different parameters.}
  \label{fig:argus}
\end{figure}

\paragraph{Chebychev polynomial.}%
\label{par:chebychev_polynomial_}
The Chebyshev polynomials of the first kind are obtained from the recurrence relation
\begin{equation}
  \begin{aligned}
    \label{eq:cheby}
    T_0(x) &= 1, \\
    T_1(x) &= x, \\
    T_{n+1}(x) &= 2x T_n(x) + T_{n-1}(x). \\
  \end{aligned}
\end{equation}
The first five Chebychev polynomials are shown in Fig.~\ref{fig:chebychev}.
They are widely used to describe smooth background distributions.
They are favored over a geometric series thanks to the numerical stability of the coefficients
in the minimisation process.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{introduction/Chebyshev_Polynomials_of_the_First_Kind.png}
  \caption{Plot of the first five Chebychev polynomials.}
  \label{fig:chebychev}
\end{figure}



To fully exploit the opportunities provided by the upgraded LHC,
the LHCb detector is currently undergoing a major upgrade~\inlinecite{LHCb:2011dta,LHCb:2012doh}
and will be further upgraded during Long Shutdown 3~\inlinecite{Aaij:2244311,LHCb:2018roe},
which is discussed in some detail below.
Thereafter, other current and future experimental programs will be discussed briefly
in this section.

\paragraph{LHCb upgrade.}%
\label{par:lhcb_upgrade_}
Two phases of upgrade are scheduled for the LHCb detector.
The data taking plan and operation conditions are summarised in Table~\ref{tab:lhcb_upgrade}.
\begin{table}[tb]
  \centering
  \caption{Operation conditions for LHCb Upgrades.}
  \label{tab:lhcb_upgrade}
  \begin{tabular}{c c c c c c}
    \toprule
    Run & Period & \makecell{$\sqrt{s}$ \\\relax [\!\tev]}
    & \makecell{Instantaneous lumi. \\\relax [$\!\cm^{−2}\sec^{−1}$]}
    & \makecell{Integrated lumi. \\\relax [\!\invfb]}
    & Average pile-up \\
    \midrule
    Upgrade I    & 2019-2021 & & & &\\
    \makecell{Run~3\\Run~4} & \makecell{2022-2024 \\ 2027-2030} 
    & 14 & $2\times10^{33}$ & 50 & 5 \\
    Upgrade II   & 2031 & & & & \\
    Run~5        & $2032\to$ & 14 & $2\times10^{34}$ & 300 & 50\\
    \bottomrule
  \end{tabular}
\end{table}

The key feature of Upgrade I is the trigger-less readout and 
fully flexible software trigger.
The maximum readout rate of the LHCb detector in Run~2
is determined by front-end (FE) electronics to be 1\,MHz.
A hardware trigger is used to reduce the LHC clock rate of 40\,MHz,
based on the deposit of several\gev 
of transverse energy in the Calorimeter and Muon systems
by charged hadrons, muons, electrons or photons, 
as discussed in Sec.~\ref{sub:the_lhcb_detector}.
The trigger yield therefore saturates for hadronic channels with increasing luminosity.
The detector in Upgrade I will employ a trigger-less readout
by upgrading all the front-end electronics 
using modern technologies adapted for high energy physics.
This will allow to perform data acquisition 
and event building at the full rate of 40 MHz.
The upgrade trigger diagram is shown in Fig.~\ref{fig:trigger_upgrade}~\inlinecite{LHCB-TDR-016}.
The software HLT shares the basic feature with Run~2 trigger,
with an significantly increased output bandwidth due to 
the increase of computational resources.
The discriminating power of the trigger is enhanced
with the full event information, 
including whether tracks originate from the displaced vertex 
that is characteristic of heavy flavour decays.
An evolved reduced event model is extensively used,
in which additional reconstructed objects in the event are selected and persisted.
This is implemented with sufficient flexibility,
thus allowing for fin-grained tuning between trigger output bandwidth 
and event utility offline.

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{introduction/LHCb_Trigger_RunIII.pdf}
  \caption{LHCb upgrade trigger diagram.}
  \label{fig:trigger_upgrade}
\end{figure}

In addition, subdetectors have to be replaced to cope with
high occupancy conditions of the upgrade with $2\times10^{33}\cm^{−2}\sec^{−1}$ luminosity.
In particular, a pixel Vertex Locator (VELO), 
a silicon tracking station before the magnet (UT),
and a large-scale downstream Scintillating Fibre (SciFi) tracker system will be installed.
The impact parameter resolution of the upgrade detector
is studied using simulation in the upgrade condition,
as shown in Fig.~\ref{fig:ip_reso_upgrade}~\inlinecite{LHCb-TDR-013}.
The impact parameter resolution is improved relative to the Run~2 detector,
leading to an improvement of 5\fs in decay-time resolution for the $\Bs\to\jpsi\phi$ mode.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{introduction/upgradeI_IPx.png}
  \includegraphics[width=0.32\linewidth]{introduction/upgradeI_IP3D.png}
  \includegraphics[width=0.32\linewidth]{introduction/upgradeII.png}
  \caption{Impact parameter resolution (left) in the $x$ projection for
  Upgrade I, (middle) 3D resolution for Upgrade I, 
  and (right) 3D resolution for Upgrade II.
  In the left and middle plots,
  the VELO in Run~2 is shown with black circles and the Upgrade I VELO with red squares.
  In the right plot, different circles correspond to different design scenarios.}
  \label{fig:ip_reso_upgrade}
\end{figure}

Further data taking with the Upgrade I detector 
will not be attractive beyond Run~4
due to the excessive ``data-doubling'' time and 
that many of its components will have reached the end of their natural life span 
due to radiation exposure.
During Upgrade II,
the detector will collect data with ten times larger 
luminosity of $2\times10^{34}\cm^{−2}\sec^{−1}$.
The increased particle multiplicity and rates 
will present even more significant problems for all subdetectors.
The key features of the Upgrade II detector include:
\begin{itemize}
  \item Precise timing in the VELO detector 
    and also downstream of the magnet for both charged tracks and neutrals.
    This information will allow charged tracks and photons to be associated 
    to the correct interaction vertex and therefore to suppress combinatoric background.
  \item A high granularity tungsten sampling electromagnetic calorimeter 
    will extend the experiment’s capabilities in final states involving photons,
    \piz mesons and electrons.
  \item The tracking acceptance will be significantly increased for soft tracks 
    by instrumenting the side walls of the dipole,
    improving the experiment’s efficiency for high multiplicity decays.
\end{itemize}
A similar performance of vertex resolution to Upgrade I is achieved in much hash conditions,
as illustrated in Fig.~\ref{fig:ip_reso_upgrade}.


\paragraph{Other programs.}%
\label{par:other_future_programs_}
As discussed in Sec.~\ref{sec:dcb_th},
there are potentially a large number of doubly charmed baryons
produced in different environments other than $pp$ collision, including 
the super $B$ factory experiment Belle II at SuperKEKB~\inlinecite{Belle-II:2018jsg},
the Circular Electron Positron Collider (CEPC)
as a Super $Z$ factory~\inlinecite{CEPCStudyGroup:2018rmc},
the Future Circular Collider (FCC),
as a super $Z$ factory~\inlinecite{FCC:2018byv},
the International Linear Collider (ILC) at 1st stage~\inlinecite{Behnke:2013xla},
the Compact Linear Collider (CLIC)~\inlinecite{Aicheler:2012bya},
the Large Hadron-Electron Collider (LHeC) at the HL-LHC~\inlinecite{LHeC:2020van},
and A Large Ion Collider Experiment (ALICE)~\inlinecite{ALICE:2012dtf}.
The operation conditions are summarised in Table~\ref{tab:future_exp}.
\begin{table}[tb]
  \centering
  \caption{Current and future experimental programs that are candidate players of doubly charmed baryon studies.}
  \label{tab:future_exp}
  \begin{tabular}{c c c c c}
    \toprule
    Programs & Type & Energy & Luminosity [$\!\cm^{-2}\sec^{-1}$] & Comment \\
    \midrule
    Belle II  & $\epem$  & $\sqrt{s} = 10.58\gev$        & $8\times10^{35}$   & $\FourS$\\
    CEPC      & $\epem$  & $\sqrt{s} = 91\gev$           & $32\times10^{34}$  & $Z$-mode \\
    FCC       & $\epem$  & $\sqrt{s} = 91\gev$ & $2\times10^{36}$   & FCC-ee-Z \\
    ILC       & $\epem$  & $\sqrt{s} = 250\gev$          & $7.5\times10^{33}$ & 1st stage\\
    CLIC      & $\epem$  & $\sqrt{s} = 500\gev$          & $2.3\times10^{34}$ & \\
    LHeC      & $ep$     & $E_e = 60\gev,E_p=7\tev$      & $1\times10^{33}$   & LHC Run~5\\
    LHCb Upgrade I  & $pp$ & $\sqrt{s} = 14\tev$         & $2\times10^{33}$   & LHC Run~3-4\\
    LHCb Upgrade II & $pp$ & $\sqrt{s} = 14\tev$         & $2\times10^{34}$   & HL-LHC\\
    ALICE Upgrade   & PbPb & $\sqrt{s_{NN}} = 5.5\tev$   & $6\times10^{27}$   & \\
    \bottomrule
  \end{tabular}
\end{table}


% DCB production

The theoretical calculation of the production cross-section of doubly charmed baryons
in various production environments provides guidance for experimental searches
for these particles.
The observation and production measurement of doubly charmed baryons in turn
test theoretical approaches and constrain values of input parameters.
The interplay between theory and experiment already happens in the study of \Xiccpp baryon,
and is promising in the hunting of remaining doubly charmed baryons.

The fundamental mechanism of the inclusive production of doubly charmed baryons 
can be factorised into three effects:
\begin{enumerate}
  \item The production of two charm quarks, which is a hard process and
    can be calculated perturbatively in QCD.
  \item The coalescence of two charm quarks into a diquark $c + c \to (cc)$,
    which is of nonperturbative nature
    and can be described by matrix elements within the nonrelativistic QCD framework.
    These matrix elements are universal in different production environments
    for each state of diquark.
    Both color $\bar{3}$ state $[^{3}S_{1}]_{\bar{3}}$ and
    color $6$ state $[^{1}S_{0}]_{6}$ are considered~\inlinecite{Ma:2003zk}.
    The production of exinlinecited diquarks is discussed at the end of this section.
  \item The hadronisation of a diquark into a doubly charmed baryon, which is also a
    nonperturbative process.
    The $(cc)[^{3}S_{1}]_{\bar{3}}$ diquark hadronises into a baryon by absorbing a light quark,
    while the $(cc)[^{1}S_{0}]_{6}$ diquark hadronises by capturing a light quark
    and a soft gluon in order to form a colorless bound state.
    The probability of this process is usually assumed to be 
    unit~\inlinecite{Berezhnoy:1998aa,Chang:2006eu,Chang:2006xp} in literature, 
    which implies that the momentum of the baryon
    is roughly the same as the initial diquark.
    This simple assumption proves to be of good accuracy
    when compared with the fragmentation function obtained from phenomenological models (2014).
\end{enumerate}

Each production environment provides unique opportunities 
for the study of doubly charmed baryons.
In this section, the production mechanism in different initial states
is reviewed and the theoretical prediction of the production cross-section is given for each 
environment.
The representative Feynman diagrams in different production environments 
are shown in Fig.~\ref{fig:feynman} and are explained in detail below.
\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/1.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/ee.png}}
    \caption{$\ep\en \to \gamma^* \to c + c + \cquarkbar + \cquarkbar$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/2.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/gammagamma.png}}
    \caption{$\gamma\gamma \to c + c + \cquarkbar + \cquarkbar$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/3.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/ep.png}}
    \caption{$e + g \to e + c + c + \cquarkbar + \cquarkbar$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/4.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/gg.png}}
    \caption{$g+g \to c + c + \cquarkbar + \cquarkbar$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/5.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/gc.png}}
    \caption{$g+c \to c + c + \cquarkbar$.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.3\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/6.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{production/cc.png}}
    \caption{$c+c \to c + c + g$.}
  \end{subfigure}
  \caption{Representative Feynman diagrams in different production environments.
  Reproduced from Ref.~\inlinecite{Ma:2003zk,Chen:2014frw,Sun:2020mvl,Chang:2006eu}.}
  \label{fig:feynman}
\end{figure}

\paragraph{\epem production.}

The study of the production of doubly charmed baryon in \epem collisions
can probe the nonperturbative effect in a cleaner environment than hadroproduction
without the complication of parton distribution functions (PDFs). 
Besides, a measurement of the production asymmetry between forward and backward direction
at $Z$-factories can probe the effective eletro-weak mixing angle
with reduced theoretical and experimental uncertainties compared with traditional
measurements with $\epem\to Z \to f \bar{f}$ production~\inlinecite{Zheng:2018fqv}.

The production of two charm quarks in \epem collisions
includes the \mbox{$\ep + \en \to \gamma^* \to c + c + \cquarkbar + \cquarkbar$} process.
In addition, the contribution from
\mbox{$\ep + \en \to Z^0 \to c + c + \cquarkbar + \cquarkbar$}
needs to be considered for collisions around $\sqrt{s}=m_Z$.
The total production cross-section can be factorised
into perturbative and nonperturbative contributions
and expanded in orders of $v_c$, the velocity of the charm quark in the baryon,
within the NRQCD framework as
\begin{equation}
  \sigma_{ab} = H_{ab \to (cc)[^{3}S_{1}]_{\bar{3}}} \times h_3 +
  H_{ab \to (cc)[^{1}S_{0}]_{6}} \times h_1 + \cdot\cdot\cdot.
\end{equation}
The perturbative coefficient $H$ describes the production of two charm quarks and
is determined by calculating all contributing Feynman diagrams.
The nonperturbative coefficient $h$ corresponds to
the binding of two charm quarks into a diquark
and can be determined using nonperturbative methods such as potential model
or from experimental data.
Contributions in higher orders of $v_c$ are neglected.

Adopting the parameters of $h_1 = h_3 = 0.039 \gev^3$, $m_c = 1.6\gev$,
and $\alpha_s(2m_c) = 0.24$,
the production cross-section of \Xiccp at $B$-factories with $\sqrt{s}=10.6\gev$
is estimated to be $\sigma_{\Xiccp} \approx 230\fb$~\inlinecite{Ma:2003zk}.
The results will be doubled if we take charge conjugate states into account.

Adopting the parameters of $h_1 = h_3 = 0.039 \gev^3$, $m_c = 1.5\gev$,
$m_u = m_d = 0.3\gev$, $m_s = 0.5\gev$,
$\alpha_s(2m_c) = 0.24$, $\alpha=1/129$, $m_Z = 91.1876\gev$, $\Gamma_Z = 2.4952\gev$,
and $\sin^2 \theta_{W}^{eff} = 0.232$,
the production cross-section of \Xiccp at super $Z$-factories with $\sqrt{s}=m_Z$
is estimated to be $\sigma_{\Xiccp} \approx 395\fb$~\inlinecite{Zheng:2018fqv}.
The results will be doubled if we take charge conjugate states into account.
If one further assumes the instantaneous luminosity of $10^{36} \cm^{-2} \sec^{-1}$
and the collider year of $10^7 \sec$,
the number of events produced per year is $\mathcal{O}(10^6)$.


\paragraph{Photoproduction.}%
\label{par:photoproduction}

The production cross-section of doubly charmed baryons through
\mbox{$\ep + \en \to \gamma^*/Z^0 \to c + c + \cquarkbar + \cquarkbar$}
in \epem collisions is highly suppressed 
when the collision energy is much higher than the $Z^0$ boson mass.
The dominant production mechanism in this case is through the
\mbox{$\gamma\gamma \to c + c + \cquarkbar + \cquarkbar$} process.
The high energy $\gamma$ beam can be obtained by 
backward Compton scattering of laser light
which is focused on the electron beams~\inlinecite{Ginzburg:1981vm}.
The theoretical calculation of the production cross-section
is similar to that of \epem collisions,
with the Feynman diagrams replaced by relevant $\gamma\gamma$ scattering processes.
One additional complication is that one needs to take into account the energy distribution
of the nonmonochromatic $\gamma$ beam.

The production cross-section of doubly charmed baryon \Xiccp
through photoproduction is calculated in Ref.~\inlinecite{Chen:2014frw} at 
$\sqrt{s}=250\gev$, $\sqrt{s}=500\gev$, and $\sqrt{s}=1\tev$
to be $400\fb$, $200\fb$, and $67\fb$, respectively.
The collision energies correspond to the condition of ILC.
The results indicate that $\mathcal{O}(10^6)$ number of events can be produced per year at ILC.


\paragraph{Deep inelastic scattering.}%
\label{par:deep_inelastic_scattering}

The production of doubly charmed baryons in deep inelastic $ep$ scattering
is of special interest due to its reduced uncertainty in theoretical calculations
and more experimental observables available.
The dominant mechanism in the $Q^2$ region of $[2,100]\gev^2$
is the gluon partonic process $e + g \to e + c + c + \cquarkbar + \cquarkbar$.
According to the factorisation theorem, the inclusive production cross-section
can be formulated as
\begin{equation}
  \sigma = g(x) \otimes \hat{\sigma}_{eg}(x),
\end{equation}
where $g(x)$ is the gluon distribution function in proton and $\hat{\sigma_{eg}}(x)$
is the partonic cross-section.
The numerical calculation of the partonic cross-section is similar to that in \epem collisions.
In Ref.~\inlinecite{Sun:2020mvl}, 
the production cross-section of \Xiccp in deep inelastic $ep$ scattering
at $E_e=60\gev$ and $E_p = 7\tev$ and in the LHeC fiducial region
is calculated to be $10\pb$ with 30\% uncertainty.
%which is dominated by the choice of renormalisation energy scale.
The result implies $\mathcal{O}(10^6)$ number of event produced per year at LHeC.


\paragraph{Hadroproduction.}

The inclusive production of doubly charmed baryons at hadron colliders
is of the most importance thanks to the large data sets accumulated at LHC.
The dominant mechanisms for the production of two charm quarks include
``gluon-gluon fusion'' \mbox{$g+g \to c + c + \cquarkbar + \cquarkbar$},
``gluon-charm creation'' \mbox{$g+c \to c + c + \cquarkbar$},
and ``charm fusion'' \mbox{$c+c \to c + c + g$}.
It should be noted that NLO charm fusion process \mbox{$c+c \to c + c + g$} is considered
instead of LO process $c+c \to c + c$, because the LO process only contributes
to production with zero transverse momentum which is usually beyond the reach 
of experimental facilities.
The contribution due to light quark annihilation 
\mbox{$q + \quarkbar \to c + c + \cquarkbar + \cquarkbar$} is neglected 
because it is much smaller than gluon-gluon fusion according to the study of \Bc production.

As shown in Ref.~\inlinecite{Chang:2006eu,Chang:2006xp}, based on which a dedicated event generator
\genxicc~\inlinecite{Chang:2007pp,Chang:2009va,Wang:2012vj} 
is developed and used in the LHCb experiment,
the cross-section of the inclusive production of doubly charmed baryons
in hadron collisions $H_1 + H_2 \to \Xicc + X$ is a sum of different contributions
\begin{equation}
  \label{eq:ccq_xsection}
  \sigma = \sigma_{gg} + \sigma_{gc} + \sigma_{cc} + \cdot\cdot\cdot,
\end{equation}
with each contribution can be formulated according to the factorisation theorem as
\begin{equation}
  \begin{aligned}
    \sigma_{gg} &= g_1 (x_1) g_2 (x_2) \otimes \hat{\sigma}_{gg} (x_1,x_2), \\
    \sigma_{gc} &= \sum_{i=1}^{2}\sum_{j=1}^{2} \delta_{ij} g_{i}(x_i)c_{j}(x_j)\otimes\hat{\sigma}_{gc}(x_1,x_2), \\
    \sigma_{cc} &= \sum_{i=1}^{2}\sum_{j=1}^{2} \delta_{ij} c_{i}(x_i)c_{j}(x_j)\otimes\hat{\sigma}_{cc}(x_1,x_2),
  \end{aligned}
\end{equation}
where $gg$, $gc$, and $cc$ refer to different mechanisms discussed in the first item above,
$g_i(x_i)$ and $c_i(x_i)$ are parton distribution functions (PDFs) of gluon and \cquark quark
inside the collision hadron $H_i$, $x_i$ is the fraction of $H_i$'s momentum carried by
the parton $i$, $\delta_{ij}$ is the Kronecker delta function,
and $\hat{\sigma}$ is the partonic cross-section.
The renormalisation scale and constituent charm quark mass is omitted 
in the above expression for simplicity.

According to Eq.~\ref{eq:ccq_xsection}, in order to calculate the cross-section numerically,
one needs to determine the PDFs as well as the hard-scattering cross-sections
corresponding to each subprocess.
The PDFs can be determined by global fitting to various partonic cross-sections.
It should be noticed that the PDF for charm quark needs to be modified properly 
to avoid double-counting between the gluon-gluon fusion and gluon-charm creation mechanism.
The partonic cross-sections of different production mechanisms can be further factorised
into perturbative and nonperturbative contributions
and evaluated in the same approach as for \epem collisions.

Adopting the parameters of $h_1 = h_3 = 0.039 \gev^3$, $m_{\Xicc} = 3.50\gev$,
$m_c = 1.75\gev$, $\Lambda_{\rm{QCD}}^{n_f = 4} = 0.215\gev$,
and the renormalisation scale $\sqrt{m_{\Xicc}^2 + \pt^2}$,
the $cc$ diquark production cross-section 
in $pp$ collisions at $\sqrt{s}=14\tev$
is $62\nb$ in the fiducial region of $\pt>4\gev$ and $|y|<1.5$,
corresponding roughly to the geometric acceptance of \atlas and \cms detectors at LHC,
and is $30\nb$ in the fiducial region of $\pt>4\gev$ and $1.8<\eta<5.0$, 
corresponding to the geometric acceptance of the LHCb detector at LHC.
If one takes the fragmentation fraction to be
\mbox{$f((cc) \to \Xiccpp):f((cc) \to \Xiccp):f((cc) \to \Omegacc) = 10:10:3$}
as shown in \pythia~\inlinecite{Sjostrand:2006za},
the production cross-section at LHCb is estimated to be $\sigma_{\Xiccpp}\approx 13\nb$,
$\sigma_{\Xiccp} \approx 13\nb$, and $\sigma_{\Omegacc} \approx 4\nb$.
The results will be doubled if we take charge conjugate states into account.
The dominant uncertainties of the calculated production cross-section
stem from the choice of renormalisation energy scale and 
the value of constituent charm quark mass,
which are about 30\% relative to the central values.

%\myalert{Comment on the experimental measurements.}


\paragraph{Heavy ion production.}

The study of the doubly charmed baryons in heavy ion collisions
provides unique probe of properties of the quark-gluon plasma (QGP)
as the production mechanism differs significantly from $pp$ collisions
due to nuclear effects~\inlinecite{Yao:2018zze,Chen:2018koh}.
The difference occurs in all aspects compared with the hadroproduction.
First, the rapidity density of charm quarks produced in a single collision is higher.
The nuclear parton distribution function (nPDF) contains a modification of proton PDF 
due to nuclear multibody effects.
Second, the charm quarks can diffuse in the deconfined QGP medium,
resulting in a smaller relative momentum of a charm quark pair and an enhancement
of the coalescence probability of a $cc$ diquark.
Third, the formed $cc$ diquark can diffuse and dissociate in the dynamical evolvement
in the QGP medium.
Fourth, in the hadronisation of the $cc$ diquark, additional light quark to be captured
follows the Fermi-Dirac distribution at certain temperature $T_c$.

In Ref.~\inlinecite{Yao:2018zze}, a set of coupled Boltzmann transport equations
are used to describe the dynamical evolvement of charm quarks and diquarks in QGP medium
and are solved with Monte Carlo simulations.
The predicted yield of \Xiccp in the fiducial region of $0<\pt<5\gev$ and $|y|<1$
in PbPb collisions at $\sqrt{s_{NN}} = 2.76\tev$, 
corresponding to the condition of the \alice experiment at LHC,
is 0.01 per collision assuming a melting temperature of $T_m = 250\mev$.

In Ref.~\inlinecite{Chen:2018koh}, nPDFs are used in the calculation
and the obtained production cross-section is
$\sigma_{p\rm{Pb}} = 1.62\times 10 ^2 \mub$ 
in $p$Pb collisions at $\sqrt{s_{NN}} = 8.16\tev$,
and $\sigma_{\rm{PbPb}} = 1.85\times 10 ^4 \mub$
in PbPb collisions at $\sqrt{s_{NN}} = 5.02\tev$,
corresponding to the condition of the \alice experiment at LHC.
The production is enhanced by 2--4 orders of magnitude compared with $pp$ collisions.


\paragraph{Production with exinlinecited diquark.}%
\label{par:production_with_exinlinecited_diquark_}

In previous discussions, only the production of $cc$ diquark in $S$-wave is considered.
Ref.~\inlinecite{Berezhnoy:2018krl} studies the production with exinlinecited $cc$ diquark at LHC
using the factorisation approach.
It is estimated that the production with radially exinlinecited $2S$ or $3S$ $cc$ diquark
is about 50\% of the total production cross-section,
while the contribution of $P$-wave excitations is about 5\%.
This observation implies a sizable contribution of \Xiccpp events
from the $\Xiccp(nS) \to \Xiccpp\pim$ decay.


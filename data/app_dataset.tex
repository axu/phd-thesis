\chapter{Bookkeeping of data and simulation samples}%
\label{sec:bookkeeping_of_data_and_simulation_samples}

In Run~1, the charmed baryon decays are triggered and reconstructed offline,
using dedicated Stripping lines.
In Run~2, the charmed baryon decays are reconstructed online and
the trigger outputs are used directly for further analysis,
as discussed in Sec.~\ref{par:trigger_system_}.
The luminosity for each data-taking year is shown in Table~\ref{tab:operation}.

For \Omegac and \Xicz baryon,
data sets collected during 2016-2018 are used for the measurement.
The same data-taking period is used for the $\Dz\to\Kp\Km\pip\pim$ control mode.
Earlier data-taking periods are not used due to the lack of dedicated trigger lines.
For \Xiccp baryon,
data sets collected during 2011-2012 and 2015-2018 are used for the search.
The same data-taking period is used for the $\Lc\to p \Km\pip$ 
and $\Xiccpp\to\Lc\Km\pip\pip$ control modes.
Besides signal and control modes,
the wrong-sign (WS) combinations of $\Lc\Kp\pim$ and $\Lc\Km\pim\pip$
are recorded to mimic the distribution of combinatorial background
in \Xiccp and \Xiccpp decay samples
in the study of event selection.

To determine event selection requirements, 
model the detector acceptance effect,
and estimate selection efficiency,
simulated signals are generated for each data-taking year.
The \Omegac and \Xicz baryons are generated using \pythia with
a lifetime hypothesis of 250\fs.
The \Lc baryon and \Dz meson are are generated with 
their known lifetimes~\inlinecite{PDG2020}.
The \Xiccp and \Xiccpp baryon are generated using \genxicc,
with an input lifetime of 80\fs and 256\fs, respectively.
All hadrons decay according to the phase-space model implemented in \evtgen,
except for \Lc baryon.
The \Lc baryon decays according to the pseudo-resonant model,
in which the $K^{*0}$, $\Delta^{++}$, and $\Lambda(1520)^0$ resonances are considered.
Differences of kinematic distributions between data and simulation
are taken into account with a weighting procedure
when we estimate the selection efficiency using simulated samples.

The simulated samples contain combinatorial backgrounds
due to the random combination of tracks in the event reconstruction and selection.
To avoid the bias in efficiency estimation,
these background candidates are rejected by matching the reconstructed signal
to the generated one.
The matching procedure requires both a correctly reconstructed track
and the correct correspondence between final-state tracks and their 
ancestor resonances.

There exists evolution in the reconstruction software,
which has an impact on the lifetime measurement.
This effect and corresponding correction is described below.

\section{Change of VELO hit error parametrization in 2017--2018 data}%
\label{sub:change_of_velo_parametrization_in_2017_2018_data}

It is known that the change of VELO hit error parametrization in 2017-2018 data
makes the data-simulation agreement of some decay-time related variables
worse than that in 2016 data,
%~\footnote{\scriptsize See https://indico.cern.ch/event/942464/contributions/3960514/attachments/2081322/3496321/Run2\_Data\_MC\_Studies.pdf},
including the decay-time resolution and the \chisqip distributions of the final-state tracks.
It is due to the fact that
the change of VELO error parametrization is not implemented in 2017-2018 simulation
as is done for data.
The effect of the discrepancy of decay-time resolution
is discussed in Sec.~\ref{sub:time_resolution}.
The discrepancy of the \chisqip distributions may lead to an inaccurate description of
the decay-time acceptance and need to be taken into account.

To correct for the discrepancy of the \chisqip distributions
due to different VELO error parametrization between data and simulation in 2017-2018,
we scale the \chisqip distributions of the final-state tracks in 2017-2018 simulation
by multiplying a constant scaling factor
before any \chisqip-related selections are applied.
The scaling factor is determined by comparing the background-subtracted \chisqip distributions of the final-state tracks
in 2016 and 2017--2018 data.
The idea is that the difference due to the change of VELO parametrization should be the same in data and in simulation.
For \Dz control mode,
it can be observed that a scaling factor of 1.05 makes a good agreement between 2016 and 2017--2018 data.
As an illustration, Fig.~\ref{fig:smear_ipchi2_distribution} shows a comparison of 2016 and 2018 data,
and Fig.~\ref{fig:smear_ipchi2_data-mc} shows a comparison of data and MC for 2016 and 2018.
Therefore, the \chisqip distributions of all (four in total) final-state tracks in 2017--2018 simulation
for both signal and control modes
are multiplied by 1.05 before the original \chisqip selection criteria are applied.
\begin{figure}[tb]
  \centering
    \includegraphics[width=0.4\linewidth]{ana_omegac/smear_ipchi2/data_Km_IPCHI2.pdf}
    \includegraphics[width=0.4\linewidth]{ana_omegac/smear_ipchi2/data_Pip_IPCHI2.pdf}
  \caption{The comparison of the log-sized \chisqip distributions of the final-state (left) Kaons and (right) Pions
  in 2016 and 2018 data for \Dz control mode. The \chisqip distributions in 2016 data are scaled by a factor of 1.05.}
  \label{fig:smear_ipchi2_distribution}
\end{figure}
\begin{figure}[htb]
  \centering
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_16_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_17_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_18_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_16_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_17_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_18_scale}
  \caption{The comparison of the log-sized \chisqip distributions of the final-state (top) Kaons and (bottom) Pions
  in (left) 2016 data and simulation and (right) 2018 data and simulation for \Dz control mode.
  The \chisqip distributions in 2018 simulation are scaled by a factor of 1.05.}
  \label{fig:smear_ipchi2_data-mc}
\end{figure}

%A more accurate option for this correction is proposed
%but the result w.r.t to a constant scaling factor is negligible,
%as discussed in Appendix~\ref{sec:app_velo_parametrization}.

\section{Offline PV refit}%
\label{sub:offline_pv_refit}

To obtain an unbiased measure of the displacement of a particle w.r.t. a PV,
PVs are supposed to be refitted excluding the signal tracks under consideration.
However, the \texttt{ReFitPVs} option is not enabled
in the \texttt{Xic0ToPpKmKmPip} turbo line.
%A detailed description of this issue can be found in the footnote~\footnote{\scriptsize{https://twiki.cern.ch/twiki/bin/view/LHCb/TurboPVRefittingBug}}.
If one or more tracks from your signal candidate are included in the PV fit,
this will pull the position of the PV towards those tracks and so will bias any displacement variables calculated wrt the PV.

As the Turbo line \texttt{Xic0ToPpKmKmPipTurbo} used to collect the signal decays
enables the \texttt{PersistReco} option,
we are able to re-fit the PVs with the persisted HLT1 VELO tracks
offline in \texttt{DaVinci} jobs.
This is done for 2016--2018 data of the signal mode,
where data are processed with the original P2PV relations killed
and \texttt{ReFitPVs=True} for \texttt{DecayTreeTuple} in the \texttt{DaVinci} option file,
which triggers the offline PV re-fitting
and the re-fitted PVs are used by various TupleTools to calculate physics quantities.

For the Turbo line \texttt{Hlt2CharmHadDstp2D0Pip\_D02KmKpPimPipTurbo} used to collect the
decays of the control mode, the \texttt{PersistReco} option is not enabled.
However, with the available VELO clusters the PV-refit can be recovered
by recalculating the weight used for tracks in the PV fit.
%with the \href{https://gitlab.cern.ch/lhcb/Phys/-/blob/v26r6/Phys/VertexFit/src/RecalcLSAdaptPV3DFitterWeightsAlg.h}{RecalcLSAdaptPV3DFitterWeightsAlg} tool,
%as discussed in \href{https://indico.cern.ch/event/859265/contributions/3623401/attachments/1935324/3206996/20191030-PVRefittingUpdate.pdf}{this Run 1--2 performance meeting}.
This is done for 2017-2018 data of the control mode.
As no VELO clusters are available for 2016 data,
the calculated decay time is smeared by the bias
observed in 2018 data with and without the offline PV refit.

Pseudo-experiments are performed to study the impact of offline PVFit
on the measured lifetime.
%as discussed in Appendix~\ref{sec:app_refitpv_toy}.
A similar approach to the study of decay time resolution is used.
First, pseudo-data and pseudo-mc samples are generated according to
\begin{equation}
  \left( \exp ( -t/\tau ) \times Acc(t) \right) \ast Gaus(t;0,\sigma_{t}) \ast Smear(t),
\end{equation}
for each signal and control modes,
where $Acc(t)$ is obtained from corresponding simulation samples as shown in
Sec.~\ref{sub:decay_time_acceptance},
$\sigma_{t}$ is the decay time resolution obtained from corresponding simulation samples
as discussed in Sec.~\ref{sub:time_resolution}.
The decay time is smeared by an additional value sampled from the $\Delta t$ distribution
shown in Fig.~\ref{fig:refitpv} to account for the effect due to offline PV-refit.
Second, the decay time fit discussed in Sec.~\ref{sec:decaytime_fit} is performed with
the pseudo samples.
Finally, run the above pseudo-experiment about 10K times and
examine the distribution of the difference between the input and fitted lifetime,
as shown in Fig.~\ref{fig:syst_refitpv}.
The distribution is fitted with a Gaussian function, whose mean value
indicates the bias due to the lack of PV refit.
The result is $0.31\fs$ and $0.06\fs$ for \Omegac and \Xicz mode, respectively.
The width of the Gaussian function shows the statistical uncertainty of the decay time fit.
%The deviation from 0 is assigned as systematic uncertainty,
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/Xic0_PVZ_diff.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/Xic0_TAU_diff.pdf}
  \caption{An event-by-event comparison of (left) the $z$ coordinates of the best PV
  and (right) of the decay time with and without the PV refitted offline
  for 2018 data sample.}
  \label{fig:refitpv}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_with_acc_omegac.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_with_acc_xicz.pdf}
  \caption{The distribution of the difference between the input and fitted lifetime
  for (left) \Omegac and (right) \Xicz mode.}
  \label{fig:syst_refitpv}
\end{figure}

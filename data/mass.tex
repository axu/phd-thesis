% review of the mass predictions

A vast amount of theoretical predictions and postdictions
of masses and mass relations of doubly charmed baryons are available.
Theoretical approaches, from phenomenological quark models
to ab initio calculations of LQCD, are utilized.

\paragraph{Ground states.}%

Nonrelativistic quark models are extensively used to calculate the masses
of doubly charmed baryons.
Mass formulas are used to predict baryons containing two heavy quarks
in Ref.~\inlinecite{Bjorken:1985ei,Karliner:2014gca,Karliner:2018hos,Weng:2018mmf}.
While these mass formulas are useful to predict masses of $S$-wave baryons,
they are unable to describe exinlinecited states
and to give further insights into other properties such as magnetic moments and decays.
Predictions using various quark potential models are made in Refs~\inlinecite{DeRujula:1975qlm,
Fleck:1989mb,
Bagan:1994dy,
Itoh:2000um,
%Use the harmonic-oscillator potential for the long-distance iteraction.
%Use a modified quark distance to take into account the effect that
%a stronger attractive force will lead to a smaller distance between quarks.
Kiselev:2001fw,
PhysRevD.66.034030,
Albertus:2006ya,Roberts:2007ni,
%variational method
Kerbikov:1987vx,
%exact solution with MC method
Silvestre-Brac:1996myf,
Vijande:2004at,
Garcilazo:2007eh}. 
%three-body problem with Faddeev equations

Bag model is also widely used.
The picture of the bag model resembles 
a bubble (the hadron) in the medium (vacuum)~\inlinecite{Lee:1981mf}.
A hadron is taken to be a finite region of space containing quark and gluon fields.
The pressure of the field is balanced by a universal pressure $B$.
The dynamics are specified by equations of motion and boundary equations for reach filed.
The first application devoted to the ground-state light hadrons by MIT group
with the approximation of a rigid cavity with spherical shape~\inlinecite{DeGrand:1975cf}.
Later the bag model was adopted to describe heavy quark systems~\inlinecite{Hasenfratz:1977dt}
and open-flavour systems~\inlinecite{Izatt:1981pt}.
One advantage of the bag model is that the mixing of states with the same
total angular momentum but different diquark angular momentum
can be calculated as the creation and annihilation operators for relevant interaction
can be introduced.

The bag model for doubly charmed baryons combine the methods used for quarkonia
and for open-flavour hadrons~\inlinecite{Fleck:1989mb}.
The spherical bag centred at the middle of the two heavy quarks is considered.
The zeroth-order mass is computed as the minimum with respect to the bag radius $R$ of
\begin{equation}
  M(R) = \sum_i \omega_i - \frac{Z_0}{R} + \frac{4\pi}{3} B R^3,
\end{equation}
where $\omega_i = m_c$ for heavy quarks and $\omega_i = \sqrt{\chi_i^2 + (m_i R)^2}/R$
for light quarks with a hyper parameter $\chi_i$.
The finite energy $Z_0 / R$ is associated with zero-point fluctuation of the fields.
The energy $\frac{4\pi}{3} B R^3$ is due to the bag pressure.
It is noticed that there are dramatic differences between different set of model parameters
extracted from different systems.
This implies that different approximations
result in different renormalisation of the parameters.
Unlike the potential model, the bag model involves many parameters
with ambiguous phenomenological determination
and thus is difficult to extrapolate to other hadrons.
The predictions using the bag model are available in Refs~\inlinecite{Jaffe:1975us,
Fleck:1989mb,
He:2004px,
Bernotas:2008bu}.

Quark model predictions with relativistic corrections
are calculated in Ref.~\inlinecite{Martynenko:2007je,Giannuzzi:2009gh}.
Relativistic quark model based on quasipotential is used in Ref.~\inlinecite{Itoh:2000um,Lu:2017meb}.
the effective interaction is the sum of the one-gluon exchange term
with the mixture of the long-distance vector and scalar linear confining potentials.
A form factor in the one-gluon exchange interaction is included to consider
the structure of the heavy diquark.
The expansion in inverse powers of the heavy diquark mass
is employed in solving the quark-diquark equation.
BSE equation is also used to calculate 
the mass spectra of doubly charmed baryons~\inlinecite{Migura:2006ep}.

QCD sum rules are also adopted to study doubly charmed baryons in Ref.~\inlinecite{Kiselev:1999zj,
Kiselev:2000jb,
Zhang:2008rt,
Wang:2010hs,
Aliev:2012ru,
Chen:2017sbg,
Wang:2018lhz,
Wang:2017qvg}.
Calculations based on effective filed theory are conducted
in Ref.~\inlinecite{Brambilla:2005yk,
Hwang:2008dj,
Brodsky:2011zs,
Sun:2014aya,
Sun:2016wzh}.
LQCD calculations of ground-state doubly charmed baryons are performed in Ref.~\inlinecite{Liu:2009jc,Durr:2012dw,Briceno:2012wt,Namekawa:2013vu,Alexandrou:2014sha,Brown:2014ena,Perez-Rubio:2015zqb,Padmanath:2015jea,Alexandrou:2017xwd,Chen:2017kxr,Mathur:2018rwu,Bahtiyar:2020uuj}.

Theoretical predictions of masses of \jph doubly charmed baryons 
are summarised in Fig.~\ref{fig:mass_dcb_1}.
Some comments are appropriate here:
\begin{itemize}
  \item Various quark model predictions are in general consistent with each other,
    and in extremely good agreement with the \Xiccpp mass measured by LHCb.
    However, quark model predictions can not provide reasonable estimation
    of model uncertainties.
  \item Predictions based on QCD sum rules, HQET, NRQCD and CHPT agree with
    experiment within theoretical uncertainties.
    They provide insight into the structure of doubly charmed baryons based on QCD,
    although with large but understood uncertainties.
  \item Calculations with BES equation are relatively new thanks to the technical development
    in this field.
  \item LQCD results are in excellent agreement with experiment,
    with theoretical uncertainties carefully estimated and under control.
  \item The mass of \Omegacc is about 100\mev above that of \Xicc.
  \item It should be noted that some postdictions are also included,
    which agree well with experiment.
\end{itemize}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{mass/ccq_mass.pdf}
  \includegraphics[width=0.45\linewidth]{mass/ccs_mass.pdf}
  \caption{Theoretical predictions of masses of \jph doubly charmed baryon
  (left) \Xicc and (right) \Omegacc.
  The $x$-axis corresponds to the time of the theoretical work.
  The $y$-axis corresponds to the predicted mass subtracted by the known mass of \jpsi meson.
  Isospin splitting is ignored in \Xicc doublets.
  Results are grouped according to the theoretical method used,
  including quark model (denoted by QM),
  effective file theory or related method of QCD sum rules, HQET, NRQCD and CHPT 
  (denoted by EFT),
  Bethe-Salpeter equation (denoted by BSE),
  and LQCD (denoted by LQCD).
  Experimental results of $\Xiccp(3520)$ baryon reported by SELEX in 2002,
  and $\Xiccpp(3620)$ baryon reported by LHCb in 2017 are indicated by
  yellow and pink lines, respectively.}
  \label{fig:mass_dcb_1}
\end{figure}

Theoretical predictions of hyperfine splitting between \jph and \jpth baryons
are summarised in Fig.~\ref{fig:mass_dcb_3}.
The hyperfine mass splittings of doubly charmed baryons 
are related to that of charmed mesons through the mass relations
\begin{equation}
  \begin{aligned}
    m_{\Xi^{*}_{cc}} - m_{\Xi_{cc}}a = \frac{3}{4} (m_{D^*} -m_{D}),
    m_{\Omega^{*}_{cc}} - m_{\Omega_{cc}}a = \frac{3}{4} (m_{D^*_s} -m_{D_s}),
  \end{aligned}
\end{equation}
according to HQET~\inlinecite{Savage:1990di}, NRQCD~\inlinecite{Brambilla:2005yk},
or quark model~\inlinecite{Ebert:2002ig}.
Most calculations obtain a value below the $\Xicc\pi$ or $\Omega_{cc}\pi$ threshold,
which indicates that the transition from \jpth to \jph state
can only proceed radiatively through $\gamma$ emission.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{mass/dccqstar_mass.pdf}
  \includegraphics[width=0.45\linewidth]{mass/dccsstar_mass.pdf}
  \caption{Theoretical predictions of hyperfine splitting between \jph and \jpth baryons
  for (left) \Xicc and (right) \Omegacc.
  The $x$-axis corresponds to the time of the theoretical work.
  The $y$-axis corresponds to the predicted mass difference.
  Results are grouped according to the theoretical method used,
  including quark model (denoted by QM),
  effective file theory or related method of QCD sum rules, HQET, NRQCD and CHPT
  (denoted by EFT),
  and LQCD (denoted by LQCD).}
  \label{fig:mass_dcb_3}
\end{figure}

\paragraph{Isospin splitting.}%
\label{par:isospin_splitting_}

The isospin splitting of doublet \Xiccp and \Xiccpp baryon
stems from the different mass (QCD) and charge (QED) of up and down quarks.
Quark model~\inlinecite{Lichtenberg:1977mv,Tiwari:1985ru,Itoh:2000um,Karliner:2017gml}, 
effective field theory~\inlinecite{Hwang:2008dj,Brodsky:2011zs}, and LQCD~\inlinecite{Borsanyi:2014jba}
are used to calculate the isospin splitting.
The results are summarised in Fig.~\ref{fig:isosplitting}.
All methods predict that \Xiccpp baryon is heavier than \Xiccp baryon by a few \mev.
This is in sharp contradiction with the fact that
the $\Xiccp(3520)$ baryon reported by SELEX is isospin partner
of the $\Xiccpp(3620)$ baryon reported by LHCb.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{mass/dccq_mass.pdf}
  \caption{Theoretical predictions of isospin splitting between \Xiccp and \Xiccpp baryon.
  The $x$-axis corresponds to the time of the theoretical work.
  The $y$-axis corresponds to the predicted mass difference.
  Results are grouped according to the theoretical method used,
  including quark model (denoted by QM),
  effective file theory or related method of QCD sum rules, HQET, NRQCD and CHPT
  (denoted by EFT),
  and LQCD (denoted by LQCD).}
  \label{fig:isosplitting}
\end{figure}

\paragraph{Exinlinecited states.}%
\label{par:exinlinecited_states_}

Similar approaches are also utilized to calculate the spectra of exinlinecited doubly charmed baryons.
The mass spectra of \Xicc and \Omegacc baryons obtained with 
relativistic quark model~\inlinecite{Itoh:2000um} are shown in Fig.~\ref{fig:ccq_spectrum_qm}.
States are organized according to the quantum number of diquarks.
The spectra are first shown in the infinite diquark mass limit.
Then the first order corrections in the inverse of diquark mass are considered.
LQCD predictions of mass spectra of doubly charmed baryons in Ref.~\inlinecite{Padmanath:2015jea}
are shown in Fig.~\ref{fig:ccq_spectrum_lattice}.
State are grouped according to their total angular momentum.
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.45\linewidth]{mass/ccq_spectrum.png}
  \includegraphics[width=0.45\linewidth]{mass/ccs_spectrum.png}
  \caption{Mass spectra of (left) \Xicc baryons and (right) \Omegacc baryons calculated
  in Ref.~\inlinecite{Itoh:2000um}. The horizontal dashed line indicates the $\Lambda_c D$
  and $\Lambda_c D_s$ threshold for \Xicc and \Omegacc baryons, respectively.
  The notation of $(n_D L n_q l) J^P$ is used to describe the quantum numbers of baryons,
  where $n_D$ and $n_q$ are the radial quantum number of the diquark and the light quark,
  $L$ and $l$ are the orbital angular momentum of the diquark and the light quark,
  and $J^P$ is the total angular momentum $J$ and parity $P$ of the baryon.}
  \label{fig:ccq_spectrum_qm}
\end{figure}
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.45\linewidth]{mass/lattice_ccq.png}
  \includegraphics[width=0.45\linewidth]{mass/lattice_ccs.png}
  \caption{Mass spectra of (left) \Xicc and (right) \Omegacc baryons
  with spin up to $\tfrac{7}{2}$ 
  calculated in Ref.~\inlinecite{Padmanath:2015jea}.
  The masses are shown w.r.t. the known mass of $\etac$.}
  \label{fig:ccq_spectrum_lattice}
\end{figure}
Some comments are appropriate here:
\begin{itemize}
  \item The flavour-dependent interaction results in the splitting of degenerate states
    and the mixing of states with different total light quark angular momentum
    but the same total angular momentum and parity.
  \item The spectra for the ground-state diquark
    are very similar to the ones for heavy-light mesons,
    as expected in the light-quark-heavy-diquark picture.
    However, the full spectra are richer thanks to the additional
    degree of freedom of diquark excitation.
  \item The first exinlinecited state above the low-lying states is due to the diquark excitation,
    which can decay strongly to the ground state via pion emission for \Xicc excitations.
  \item The $\Omegaccstar\to\Omegacc \pi$ decay is isospin violating and suppressed.
    $\Omegaccstar\to\Xicc K$ is allowed for radial excitation of \Omegacc.
\end{itemize}

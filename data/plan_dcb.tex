The focus of the experimental study in the doubly charmed sector
is different in the near future for each doubly charmed baryon.
Therefore, we will discuss their prospects separately below.

LHCb will be the primary experiment for studies of 
the physics of doubly charmed baryons for the foreseeable future,
with benefits from increased luminosity,
improved selection efficiency,
and reduced combinatorial background level.
With the data collected in Run~3, 
LHCb is expected to observe the remaining two weakly decaying doubly charmed baryons
and characterise their physical properties. 
Run~4 and 5 will supply precision measurements of doubly differential cross-sections 
that will provide insight into production mechanisms of doubly heavy baryons.
In addition, the spectroscopy of exinlinecited states will be probed.
A projection of the expected yield for LHCb Upgrade 
is shown in Table~\ref{tab:projection_yield_lhcb}.
The projection is based on the observed yield of around 300
for $\Xiccpp\to\Lc\Km\Km\pip$ decay in 2016.
The reconstruction and selection efficiency is assumed to
be improved by a factor of two due to the trigger-less readout and fully software trigger.
The decay branching fraction for dominant channels
and the reconstruction efficiency is assumed to be comparable
between doubly charmed baryons.
A factor of 0.5 is assigned to \Xiccp and \Omegacc baryon to account for the inefficiency
due to lower lifetimes of \Xiccp and \Omegacc baryon.
An additional factor of 0.3 is assigned to \Omegacc baryon
to take into account the lower production cross-section compared with \Xicc baryon.
For completeness, the expected yield for \Xibc is also shown
as given in Ref.~\inlinecite{LHCb:2018roe}.
\begin{table}[tb]
  \centering
  \caption{The expected yield of doubly heavy baryons for LHCb Upgrade.}
  \label{tab:projection_yield_lhcb}
  \begin{tabular}{l c c c}
    \toprule
    Channels & 23\invfb & 50\invfb & 300\invfb \\
    \midrule
    $\Xiccpp\to\Lc\Km\Km\pip$ & \num{7000} & \num{15000} & \num{90000} \\
    $\Xiccp\to\Lc\Km\pip$     & \num{3500} & \num{7500}  & \num{45000} \\
    $\Omegacc\to\Xicp\Km\pip$ & \num{1000} & \num{2200}  & \num{13000} \\
    $\Xibcp\to\jpsi\Xicp$     & \num{50}   & \num{100}   & \num{600}   \\
    \bottomrule
  \end{tabular}
\end{table}

Study of doubly charmed baryons in other production environment
can provide complementary information to $pp$ collisions
due to different production mechanism.
For example, the absolute branching fraction of \Xicc decays can be measured
at the $\epem$ colliders.
Doubly charmed baryons in heavy ion collisions 
provide unique probe of properties of the quark-gluon plasma (QGP).
A projection of the annual number of doubly charmed baryons produced
at future colliders is shown in Table~\ref{tab:projection_yield_other}.
The projection is based on the production cross-section discussed in Sec.~\ref{sec:dcb_th}
and the instantaneous luminosity discussed 
in Sec.~\ref{sec:lhcb_upgrade_and_other_future_programs}.
A conventional detector year of $10^7\sec$ is used to calculate the 
annual integrated luminosity.
It should be noted that decay branching fractions and 
detection efficiency are \emph{not} considered in the projection.
Therefore, care should be taken when 
comparing these numbers with those in Table~\ref{tab:projection_yield_other}.
In addition, the background level varies significantly from low in the $\epem$ machine
to high in the PbPb collisions.
\begin{table}[tb]
  \centering
  \caption{Projection of the number of doubly charmed baryons produced at future colliders.}
  \label{tab:projection_yield_other}
  \begin{tabular}{c c}
    \toprule
    Programs & Production per year \\
    \midrule
    Belle II      & $2\times 10^{6}$ \\ %\num{1800000} \\
    CEPC          & $1\times 10^{6}$ \\ %\num{1300000} \\
    FCC           & $8\times 10^{6}$ \\ %\num{7900000} \\
    ILC           & $3\times 10^{4}$ \\ %\num{30000} \\
    CLIC          & $5\times 10^{4}$ \\ %\num{46000} \\
    LHeC          & $5\times 10^{5}$ \\ %\num{100000} \\
    ALICE Upgrade & $1\times 10^{9}$ \\ %\\
    \bottomrule
  \end{tabular}
\end{table}


\paragraph{Searches for \Xiccp baryon.}%
\label{par:searches_for_xiccp_baryon_}
At present (2022), the highest priority of the \Xiccp baryon study
is still to establish its existence with high significance.
To achieve this purpose, we need to identify the decay channels
with easiest experimental access,
reconstruct and select signal decays with high efficiency and purity.
An easy experimental access means a large number of reconstructed
signal decays with good signal purity.
This requires an optimal combination of 
a) a large branching fraction of doubly charmed baryon decays;
b) a large decay branching fraction of decay products of doubly charmed baryons;
c) no or few neutral final-state particles which can not be detected efficiently.
Theoretical predictions of branching fractions of nonleptonic \Xiccp decays 
are shown in Table~\ref{tab:decay_nl_xiccp}.
Relevant branching fractions of subsequent decays of doubly charmed baryon products
are shown in Table~\ref{tab:br_cascade}.
Semileptonic \Xiccp decays as well as decays with more than two neutral particles
are not considered at the present stage.
We then conclude that the following final states are favoured
for future searches with current and upgraded LHCb data sets:
\begin{itemize}
  \item $\Lc\Km\pip$: access to fully reconstructed $\Xiccp\to\Lc\bar{K}^{*0}$ decay.
  \item $\Xicz\pip$: access to fully reconstructed $\Xiccp\to\Xicz\pip$ decay
    and partially reconstructed $\Xiccp\to\Xicz\rhop$, $\Xiccp\to\Xi^{\prime0}_c\pip$,
    $\Xiccp\to\Xi^{\prime0}_c\rhop$ decays.
    The \Xicz baryon can be reconstructed in both $p\Km\Km\pip$ and $\Lz \Km \pip$ decays.
  \item $\Xicp\pip\pim$: access to fully reconstructed $\Xiccp\to\Xi^{+}_c\rhoz$
    $\Xiccp\to\Xi^{+}_c\eta$, and $\Xiccp\to\Xi^{*0}_c\pip$ decays, 
    and partially reconstructed $\Xiccp\to\Xi^{\prime+}_c\eta$ 
    and $\Xiccp\to\Xi^{*0}_c\rhop$ decays.
    This final state is studied very recently with LHCb Run~1 and 2 
    data sets~\inlinecite{LHCb-PAPER-2021-019}.
    No significant signal is observed in the expected mass region.
    The \Xicp baryon is reconstructed in $p\Km\pip$ decay.
  \item $\Lz\Dz\pip$: access to fully reconstructed $\Xiccp\to\Lz D^{*+}$ decay.
    The \Dz meson is reconstructed in $\Km\pip$ decay.
\end{itemize}
\begin{table}[tb]
  \centering
  \caption{Relevant branching fractions of decay products of doubly charmed baryons.}
  \label{tab:br_cascade}
  \begin{tabular}{l c}
    \toprule
    Channels & Branching fraction \\
    \midrule
    $\Xi^{\prime0}_c \to \Xicz \gamma$        & 1 \\
    $\Xi^{*0}_c \to \Xicp\pim (\Xicz\piz)$    & $2/3(1/3)$ \\
    $\Xi^{\prime+}_c \to \Xicp \gamma$        & 1 \\
    $\Xi^{*+}_c \to \Xicz\pip (\Xicp\piz)$    & $2/3(1/3)$ \\
    $\Sigma_c^{++}\to\Lc\pip$                 & 1 \\
    $\Sigma_c^{+}\to\Lc\piz$                  & 1 \\
    $D^{*+} \to \Dz\pip (\Dp\piz)$            & $2/3(1/3)$ \\
    $\bar{K}^{*0}\to\Km\pip(\bar{K}^{0}\piz)$ & $2/3(1/3)$ \\
    $\rhoz\to\pip\pim$                        & 1 \\
    $\rhop\to\pip\piz$                        & 1 \\
    \midrule
    $\Omegac\to p\Km\Km\pip (\Omega^{-}\pip)$ & N/A \\
    $\Xicp \to p\Km\pip$                      & $4.5\times10^{-3}$ (w.r.t $\Xi^{-}2\pip$)\\
    $\Xicz \to p \Km\Km\pip (\Lz \Km \pip)$   & $4.8\times10^{-3}(1.5\times10^{-2})$ \\
    $\Lc\to p\Km\pip$                         & $6.3\times10^{-2}$ \\
    $\Omega^{-} \to \Lz \Km$                  & 2/3 \\
    $\Xi^{-} \to \Lz \pim$                    & 1 \\
    $\Lz \to p \Km$                           & 2/3 \\
    \bottomrule
  \end{tabular}
\end{table}

On the other hand,
it is possible to implement MVA selection with cut-based quality
in the upgrade software trigger.
This, along with the full software trigger in the upgrade, 
will improve the signal efficiency and purity significantly.

These decay channels, along with Cabbibo-suppressed channels and nonphysical channels
serving as control channels,
are being implemented in the LHCb upgrade software trigger 
to enable the programs in the upcoming Run~3 data-taking.
With the preparations and improvements discussed above,
there stands a good chance to observe \Xiccp baryon at the LHCb experiment
with data collected during the Run~3 of LHC.

What is more, as discussed in Sec.~\ref{sec:dcb_th},
the production cross-section of exinlinecited \Xicc baryons is supposed to be sizeable
compared to the ground state.
With the benefit of the long lifetime of \Xiccpp baryon
and improved signal purity in the \Xiccpp baryon data sample,
it is possible to search for exinlinecited \Xiccp states 
via the $\Xiccp(nS) \to \Xiccpp\pim$ transition.


\paragraph{Measurement of \Xiccpp baryon.}%
\label{par:measurement_of_xiccpp_baryon_}

As discussed in Sec.~\ref{sub:experimental_status},
the mass, lifetime, production cross-section of \Xiccpp baryon have been measured.
Limited information on the branching fractions is also available 
by studying the $\Lc\Km\pip\pip$, $\Xicp\pip$, and $\Dp p\Km\pip$ decay channels.
More decay channels are important to understand the dynamics of the \Xiccpp baryon.
With the same consideration as in the \Xiccp baryon case,
we identify favoured decay channels for future programs:
\begin{itemize}
  \item $\Lc\Km\pip\pip$: access to fully reconstructed $\Xiccpp\to\Sigma^{++}_c\bar{K}^{*0}$
    decay.
  \item $\Xicp\pip$: access to fully reconstructed $\Xiccpp\to\Xicp\pip$ decay,
    and partially reconstructed $\Xiccpp\to\Xicp\rhop$, $\Xiccpp\to\Xi^{\prime+}_c\pip$
    decays. The latter has already been observed in the partial reconstruction
    very recently~\inlinecite{LHCb-PAPER-2021-052}.
  \item $\Xicz\pip\pip$: access to fully reconstructed $\Xiccpp\to\Xi^{*+}_c\pip$ decay
    and partially reconstructed $\Xiccpp\to\Xi^{*+}_c\rhop$ decay.
\end{itemize}
These decay channels, along with Cabbibo-suppressed channels and nonphysical channels
serving as control channels,
are being implemented in the LHCb upgrade software trigger
to enable the programs in the upcoming Run~3 data-taking.


\paragraph{Searches for other doubly heavy baryons.}%
\label{par:searches_for_other_doubly_heavy_baryons_}

As discussed in Sec.~\ref{sec:dcb_th},
the production cross-section of \Omegacc is about 30\% of that of the \Xiccpp baryon.
With a lifetime in between the \Xiccp and \Xiccpp baryon,
the \Omegacc baryon is expected to be within the reach of the upgraded LHCb experiment,
if appropriate decay channels are identified.

The production cross-section of doubly heavy baryon \Xibc is
expected to be comparable, though smaller by a factor of about two,
with that of \Xicc baryons~\inlinecite{Zhang:2011hi}.
It is possible to search for \Xibc baryons with promising exclusive decay channels
in the LHCb upgrade era.
It is also proposed to perform an inclusive search
by searching for displaced \Xiccpp baryons given the large
$\Xibc\to\Xicc X$ branching fraction and nonnegligible \Xibc lifetime~\inlinecite{Qin:2021wyh}.


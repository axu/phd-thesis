% introduction to nonperturbativ methods

Ab initio calculations for observables are possible for perturbative QCD,
where the involved interactions are at high-energy scale
and perturbative expansions in the small coupling constant are possible.
These include, \eg, cross-sections in various high-energy scattering processes.
However, this is not the case for description of properties of hadrons,
including their masses and matrix elements.
Various nonperturbative methods have been developed
with different degrees of success.
In this section, relevant so-called second-generation theoretical technologies are reviewed.
%while the old fellow phenomenological quark model 
%will be presented in Sec.~\ref{sub:the_quark_model}.
The common features of these methods include a factorisation of 
short- and long-distance contributions,
or/and an expansion in a small quantity other than the coupling constant,
which is specific for the given system.
%Relevant methods are reviewed below,
%which are used in combination or separately in
%predicting properties of doubly charmed baryons
%and charmed baryon lifetimes.
Their applications to the prediction of 
charmed baryon lifetimes 
and
properties of doubly charmed baryon
are discussed in 
Sec.~\ref{sec:lifetimes_of_charmed_baryons}
and
Sec.~\ref{sec:doubly_charmed_baryons},
respectively.

\paragraph{Lattice QCD.}%
\label{par:}

Lattice QCD (LQCD) is a tool to determine the low energy properties of QCD
and to carry out ab initio calculations of hadron properties.
LQCD calculations use a discretised version of the QCD Lagrangian as input.
In LQCD Euclidean space-time is discretised on a hypercubic lattice
with lattice spacing $a$.
Quark fields are placed on lattice sites and gauge fields on the links between sites.
The definition of LQCD does not rely on perturbative expansion
and allows for nonperturbative calculations of the path integral numerically.
The number of input parameters in LQCD is the same as for continuum QCD,
including the strong gouge coupling $\alpha_s$ and the quark masses for each flavour.
The gauge coupling is a function of energy scale, which is the inverse lattice spacing
$1/a$ in lattice QCD.
Early LQCD calculations were performed in quenched approximations,
neglecting the effect of sea quarks.
This was due to the lack of computing resources necessary to add virtual quark-antiquark pairs.
Recent calculations,
with advances in algorithms and computing hardware,
include loop effects of light quarks, denoted as $N_f = 2+1$ simulations,
or even loops of charm quarks, referred to as $N_f = 2 + 1 + 1$ simulations.
Lattice results usually come with both statistical and systematic uncertainties,
arising from limited computing resources and inefficiency of algorithms, respectively.
The systematic uncertainties stem from nonzero lattice spacing, unphysical values used
for quark masses, finite lattice volume, and how dynamic quarks are added.

LQCD has been applied successfully to calculate spectra,
electroweak decay constants and form factors of hadrons,
and to determine fundamental parameters of the standard model such as strong gauge coupling
and quark masses.
The major part of lattice spectroscopy deals with light hadrons up to now.
Results are illustrated in Fig.~\ref{fig:lattice_hadron}.
LQCD predictions of low-lying light hadrons and heavy mesons
agree very well with spectroscopic data within uncertainties.
Calculations of ground-state singly charmed baryons from Ref.~\inlinecite{Brown:2014ena}
are shown in Fig.~\ref{fig:single_charm_baryon_mass}.
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{mass/lattice_light_hadron.pdf}
  \caption{Hadron spectrum from lattice QCD in Sec.~15 of \pdg~\inlinecite{PDG2020}.
    Horizontal bars (boxes) denote experimental masses (widths).
    Lattice calculations from different groups are denoted by points with error bars.}
  \label{fig:lattice_hadron}
\end{figure}
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{mass/charm_baryon_spectrum.pdf}
  \caption{Comparison of ground-state charmed baryon spectrum
  between experimental data and 
  calculations from lattice QCD~\inlinecite{Brown:2014ena}
  and quark models~\inlinecite{Roncaglia:1995az,Itoh:2000um}.}
  \label{fig:single_charm_baryon_mass}
\end{figure}

Calculations of exinlinecited states are much more challenging due to the fact that:
a) exinlinecited states are unstable resonances;
b) there are many states with the same quantum numbers.
So far only a few two-body resonances are well studies.

The present frontier considers electromagnetic effects in lattice simulations,
with the inclusion of isospin breaking (use different up and down quark masses) and QED.
This introduces a host of technical challenges,
one of which is that electromagnetic interactions are long range while
lattice volume is finite.


\paragraph{Bethe-Salpeter Equations.}%
\label{par:bethe_salpeter_equations_}
Hadron properties are encoded in QCD's Green functions.
Bound states can appear as poles in $n$-point correlation functions through their 
spectral representation.
In full QCD the Bethe-Salpeter equation (BSE) is an exact equation for 
the fully covariant bound state wave function.
It can be formulated in terms of the Bethe-Salpeter amplitude $\Gamma$ as~\inlinecite{Eichmann:2016yit}
\begin{equation}
  \Gamma = \mathbf{K} \mathbf{G}_0 \Gamma,
\end{equation}
where $\mathbf{G}_0$ is the disconnected product of a dressed quark and antiquark propagator
and $\mathbf{K}$ is the four-quark scattering kernel.
Approximation at different levels of sophistication 
has to be made in actual calculations.


\paragraph{Operator Product Expansion.}%
\label{par:operator_product_expansion_}
Operator Product Expansion (OPE) is a formal framework
that expands the product of quark currents in a series of local operators $\mathcal{O}_d$
with effective coupling constant $C_d$, known as the Wilson coefficient.
The operators are ordered according to their energy dimension $d$.
Wilson coefficients receive dominant contributions from short-distance regions,
while long-distance dynamics is represented by universal operator matrix elements
which are independent of the properties of the quark currents.
The most import feature of the OPE is the factorisation
of short-distance and long-distance contributions.
Low dimension operators include dimension-three operator $\mathcal{O}_3 = \bar{\psi}\psi$,
dimension-four operator $\mathcal{O}_4 = F_{\mu\nu}^A F^{A,\mu\nu}$,
dimension-five operator $\mathcal{O}_5 =  \bar{\psi} \sigma_{\mu\nu} t^A F^{A,\mu\nu} \psi$,
dimension-six operator $\mathcal{O}_6^{\psi} = (\bar{\psi}\Gamma_r\psi)(\bar{\psi}\Gamma_s\psi)$
and $\mathcal{O}_6^{g} = f_{ABC} F_{\mu\nu}^A F_{\sigma}^{B,\nu} F^{C,\sigma\mu}$.
A generalization of OPE in inverse heavy quark mass $1/m_Q$,
named Heavy Quark Expansion (HQE),
is widely used to investigate inclusive weak decays of heavy hadrons 
systematically~\inlinecite{Shifman:1986mx}.


\paragraph{QCD Sum Rules.}%
\label{par:qcd_sum_rules}
QCD sum rules is a widely used tool in hadron phenomenology~\inlinecite{Colangelo:2000dp}.
Hadrons are represented in a model independent way
by their interpolating quark currents taken at large virtualities.
The correlation function of these currents is expanded in the framework of OPE.
The short-distance interactions are calculated using QCD perturbative theory.
The long-distance interactions are parameterised in terms of universal vacuum condensates,
the vacuum expectation value of the operator.
The QCD calculation is matched to a \emph{sum} over hadronic states via dispersion relation.
In this way, the sum rule allows for the calculation of observable properties of hadronic states.
The accuracy of this method is limited by the truncation of the OPE
and the complicated structure of the hadronic dispersion integrals,
and can not be improved beyond certain limits.


\paragraph{Heavy Quark Effective Theory.}%
Heavy Quark Effective Theory (HQET)
provides a systematic expansion of QCD Lagrangian
in terms of inverse powers of the heavy quark mass.
By definition, the notion of HQET can be applied to
charm ($m_c \approx 1.5 \gev$) and beauty ($m_b \approx 4.8 \gev$) quarks in nature.
The leading term in this expansion gives rise to new spin- and flavor-symmetry,
known as Heavy Quark Symmetry (HQS).
For infinitely heavy quarks,
HQS states that beauty hadron is identical to a charmed hadron at equal velocity
regardless of the spin orientation of the heavy quarks.
The picture is similar to hydrogen, deuterium, and tritium atoms
in the context of QED.
The heavy quark sector in the QCD Lagrangian 
is reformulated in the HQET at order $1/m_Q$ as~\inlinecite{Korner:1994nh}
\begin{equation}
  \label{eq:hqet}
  \mathcal{L}_{\mathrm{HQET}} =
    \bar{h}_{v} i v \cdot D h_{v} +
    \frac{1}{2 m_{Q}} \bar{h}_{v}
    \left((i D)^{2}-(i v \cdot D)^{2}-\frac{g}{2} \sigma_{\mu \nu} F^{\mu \nu}\right) h_{v} +
    \cdots,
\end{equation}
where $h_v$ is the projection of the Dirac spinor with velocity $v$ 
to the upper two components,
$D_{\mu}  = \partial_{\mu} + i g_s \mathcal{A}_{\mu}$ is the gauge-covariant derivative,
and $F^{\mu \nu}$ is the gluon field tensor in Eq.~\ref{eq:gluon_field}.
All three terms at order $1/m_Q$ beak the flavor symmetry,
while the last term also breaks the spin symmetry.

\paragraph{Nonrelativistic QCD.}%
\label{par:nonrelativistic_qcd}
Nonrelativistic QCD (NRQCD) is an effective field theory
designed for separating relativistic from nonrelativistic energy scales.
It consists of a nonrelativistic Schr\"odinger field theory for the heavy quark and antiquark
that is coupled to the usual relativistic field theory for light quark and gluons.
A finite ultraviolet cutoff of order $m_Q$ is introduced to excludes relativistic states,
whose effect is incorporated through renormalisation of coupling constants,
know as low-energy constants (LECs).
The NRQCD Lagrangian is~\inlinecite{Caswell:1985ui,Bodwin:1994jh}
\begin{equation}
  \mathcal{L}_{\mathrm{NRQCD}} = \mathcal{L}_{\mathrm{light}} + \mathcal{L}_{\mathrm{heavy}}
    + \delta \mathcal{L}.
\end{equation}
The light degrees of freedom is the same as the full QCD.
The heavy quark part coincides with the leading term of HQET Lagrangian in Eq.~\ref{eq:hqet}.
The relativistic effects of full QCD are reproduced through the correction term
$\delta \mathcal{L}$.
Various operators are organised into a hierarchy using velocity scaling rules.


\paragraph{Chiral Perturbation Theory.}
Chiral Perturbation Theory (CHPT) is the effective theory of the SM
below the scale of spontaneous chiral symmetry breaking.
It is the common language of nuclear and low-energy particle physics.
The QCD Lagrangian exhibits chiral symmetry in the chiral limit of massless light quarks.
CHPT assumes that the chiral limit constitutes a realistic starting point
for a systematic expansion in chiral symmetry breaking interactions.
The CHPT Lagrangian is obtained by extending the full QCD Lagrangian
in the chiral limit by coupling the light quarks to external hermitian matrix field
$v_\mu$, $a_\mu$, $s$, $p$~\inlinecite{Ecker:1994gg}:
\begin{equation}
  \mathcal{L}_{\mathrm{CHPT}} = \mathcal{L}_{\mathrm{QCD}}^0 +
    \bar{q} \gamma^{\mu}\left(v_{\mu}+a_{\mu} \gamma_{5}\right) q -
    \bar{q}\left(s-i p \gamma_{5}\right) q,
\end{equation}
where $\mathcal{L}_{\mathrm{QCD}}^0$ is the full QCD Lagrangian in the chiral limit
and $q$ is the light quark field.

% an introduction to quark model
% with emphasis on charmed baryons

Historically,
a phenomenological model, named \emph{quark model},
was constructed to categorize a number of known hadrons
without knowledge of the underlying dynamics of the strong interaction~\inlinecite{GellMann:1964nj,
Zweig:352337,Zweig:570209}.
It enjoyed great success in the classification of hadrons
and the description of deep inelastic scattering.
At present, the quark model is still important as a standard by which
one defines the ``expected'' and ``unexpected''.
It is helpful when other more advanced tools, such as lattice QCD,
are not available for a property yet.
It can also educate our intuition and help to interpret results obtained from other methods.
In addition, it can be used to evaluate certain matrix elements
arising in nonperturbative calculations.
%HQET, QCD sum rules \etc.

In the quark model, hadrons are classified according to their Fermi statistics
into mesons (with integer spin) and baryons (with half-integer spin).
The quark model assumes that mesons are bound states of a quark and an antiquark,
while baryons are bound states of three quarks or three antiquarks.
We will focus on the properties of baryons in the following discussion.

\paragraph{Quark quantum numbers.}%
\label{par:quark_quantum_numbers}
%Quarks come in six flavours.
%The up ($u$), down ($d$), and strange ($s$) quarks are regarded as light-flavour quarks,
%while charm ($c$), beauty ($b$), and top ($t$) quarks are heavy-flavour quarks due to their 
%large masses.
Baryons containing only light quarks are called light baryons,
while heavy baryons contain one or more heavy quarks~\footnote{Top quarks can not
constitute hadrons as constituent quarks, because
they are so heavy that they decay weakly before hadronise.}.
Hadron physics studying properties of heavy hadrons is
referred to as \emph{heavy flavour physics}, or simply flavour physics.
%The lifetime of top quark is too short to form a bound state.
The addictive quantum numbers of quarks are shown in Table~\ref{tab:quark_qn}.
As bound states of quarks, the quantum numbers of baryons 
are given by those of their constituent quarks or antiquarks.
\begin{table}[!tb]
  \centering
  \caption{Quark quantum numbers. The quantum number from top to bottom corresponds to 
  baryon number, electric charge (in unit of $e$), 
  isospin, isospin $z$-component,
  strangeness, charm, bottomness, and topness.
  The convention is that the quark flavour has the same sign as its charge.}
  \label{tab:quark_qn}
  \begin{tabular}{c r r r r r r}
    \toprule
    & $d$ & $u$ & $s$ & $c$ & $b$ & $t$ \\
    \midrule
    $\mathcal{B}$ & $\tfrac{1}{3}$ & $\tfrac{1}{3}$ & $\tfrac{1}{3}$ & 
                    $\tfrac{1}{3}$ & $\tfrac{1}{3}$ & $\tfrac{1}{3}$ \\
    $Q$ & $-\tfrac{1}{3}$ & $+\tfrac{2}{3}$ & $-\tfrac{1}{3}$ & $+\tfrac{2}{3}$ &
          $-\tfrac{1}{3}$ & $+\tfrac{2}{3}$ \\
    $I$ & $\tfrac{1}{2}$ & $\tfrac{1}{2}$ & 0 & 0 & 0 & 0 \\
    $I_z$ & $-\tfrac{1}{2}$ & $\tfrac{1}{2}$ & 0 & 0 & 0 & 0 \\
    $S$ & 0 & 0 & $-1$ & 0 & 0 & 0 \\
    $C$ & 0 & 0 & 0 & $+1$ & 0 & 0 \\
    $B$ & 0 & 0 & 0 & 0 & $-1$ & 0 \\
    $T$ & 0 & 0 & 0 & 0 & 0 & $+1$ \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Classification scheme.}%
\label{par:classification_scheme}

In the nonrelativistic quark model,
the baryon wave function is a product of four components
\begin{equation}
  \Psi = \Psi_{\mathrm{colour}} \times \Psi_{\mathrm{flavour}} \times 
         \Psi_{\mathrm{spin}} \times \Psi_{\mathrm{space}}.
\end{equation}
According to Fermi statistic
and treating all quarks (regardless of colour and flavour) as 
different states of a single particle,
the baryon wave function $\Psi$ is supposed to be antisymmetric
under the permutation of any pair of the three constituent quarks.
As all observed baryons are color singlets, 
the colour component $\Psi_{\mathrm{colour}}$ is antisymmetric.
For ground-state baryons~\footnote{Throughout this thesis,
we refer to the states of a baryon without orbital excitation (\ie $S$-wave)
as the ground states.},
the spacial component $\Psi_{\mathrm{space}}$ is symmetric.
Therefore, the $\Psi_{\mathrm{flavour}} \times \Psi_{\mathrm{spin}}$ part needs to be
symmetric.
First we consider baryons containing only light quarks.
In the language of group theory,
the direct product of (approximate) flavour symmetry $\suthree_f$
can be decomposed into the direct sum of multiplets with different symmetric properties
according to
\begin{equation}
  \mathbf{3} \otimes \mathbf{3} \otimes \mathbf{3} =
  \mathbf{10}_{S} \oplus \mathbf{8}_{MS} \oplus \mathbf{8}_{MA} \oplus \mathbf{1}_A,
\end{equation}
and the direct product of spin symmetry $\sutwo$
can be decomposed into the direct sum of multiplets with different symmetric properties
according to
\begin{equation}
  \mathbf{2} \otimes \mathbf{2} \otimes \mathbf{2} =
  \mathbf{4}_{S} \oplus \mathbf{2}_{MS} \oplus \mathbf{2}_{MA} \oplus \mathbf{1}_A,
\end{equation}
where the subscripts $S$, $A$, $MS$ and $MA$ denote symmetry, antisymmetry, 
mixed symmetry and mixed antisymmetry, respectively.
The ground-state multiplets can then be obtained with the combined 
$\suthree_f$ and $\sutwo$ symmetry, including the \jph octet and the \jpth decuplet.
%\begin{equation}
%  \mathbf{6} \otimes \mathbf{6} \otimes \mathbf{6} =
%  \mathbf{56}_{S} \oplus \mathbf{70}_{MS} \oplus \mathbf{70}_{MA} \oplus \mathbf{20}_A,
%\end{equation}
%with these multiplets further decomposed into flavour multiplets according to 
%\begin{equation}
%  \begin{aligned}
%    \mathbf{56} &= {}^4\mathbf{10} \oplus {}^2\mathbf{8},\\
%    \mathbf{70} &= {}^2\mathbf{10} \oplus {}^4\mathbf{8} \oplus {}^2\mathbf{8} \oplus {}^2\mathbf{1}, \\
%    \mathbf{20} &= {}^2\mathbf{8} \oplus {}^4\mathbf{1},\\
%  \end{aligned}
%\end{equation}
%where the superscript $(2S+1)$ indicates the total spin of each particle.
For exinlinecited states, we need to include quark orbital angular momentum
with the orthogonal rotation group $\mathrm{O}(3)$.

The existence of charm quark was confirmed in 1974 by the discovery of 
\jpsi particle~\inlinecite{E598:1974sol,SLAC-SP-017:1974ind},
now interpreted as an $S$-wave $\cquark\cquarkbar$ bound state.
Baryons containing one or more charm quarks,
referred to as singly and doubly charmed baryons respectively,
were predicted as a natural extension of the light sector~\inlinecite{DeRujula:1975qlm}.
The decomposition of (approximate) flavour symmetry $\mathrm{SU}(4)_f$,
with the addition of charm flavour, reads
\begin{equation}
  \mathbf{4} \otimes \mathbf{4} \otimes \mathbf{4} =
  \mathbf{20}_{S} \oplus \mathbf{20}_{MS} \oplus \mathbf{20}_{MA} \oplus \bar{\mathbf{4}}_A.
\end{equation}
The ground-state multiplets are extended to 20-plets
when combined with spin symmetry.
The diagram of $\mathrm{SU}(4)_f$ 20-plets are shown in Fig.~\ref{fig:multiplet}.
\begin{figure}[!tb]
  \centering
  \raisebox{-0.5\height}{\includegraphics[width=0.45\linewidth]{introduction/baryon1.pdf}}
  \raisebox{-0.5\height}{\includegraphics[width=0.45\linewidth]{introduction/baryon3.pdf}}
  \caption{$\mathrm{SU}(4)_f$ 20-plets of ground-state (left) \jph and (right) \jpth baryons.
  Right-handed Cartesian coordinates of $(I_z, S, C)$ are adopted.
  Constituent quark contents are indicated at each dot.
  Circle dots indicate locations where tow states are located.
  The naming scheme follows Sec.~7 in \pdg~\inlinecite{PDG2020}.}
  \label{fig:multiplet}
\end{figure}


\paragraph{Dynamical models.}%
\label{par:dynamical_models_}
The most fundamental assumption of the quark model is that
the effective degrees of freedom are the constituent quarks,
whose masses enter as parameters of the theory.
The Hamiltonian consists of a kinetic part describing these constituent quarks
together with a potential for the inter-quark forces.
In nonrelativistic quark model it has the form
\begin{equation}
  H =  \sum_i \left( m_i + \frac{p^2_i}{2m_i} \right) + \sum_{i<j} V (\mathbf{r}_{ij}),
\end{equation}
where $i$ labels different constituent quarks,
$V(r_{ij})$ denotes the two-body potential between constituent quarks.
Some striking regularities in the mass and mass differences of hadrons
can be understood from general properties of the interactions of quarks
without an explicit form of the Hamiltonian~\inlinecite{DeRujula:1975qlm,Roncaglia:1995az},
including the Gell-Mann-Okubo formula for the baryon octet
\begin{equation}
  2N + 2\Xi = 3 \Lambda + \Sigma,
\end{equation}
the equal-spacing rule for the decuplet
\begin{equation}
  \Delta - \Sigma^* = \Sigma^* - \Xi^* = \Xi^* - \Omega,
\end{equation}
and the $\mathrm{SU}(6)$ relation
\begin{equation}
  \Sigma^* - \Sigma = \Xi^* - \Xi.
\end{equation}
The symmetry breaking in multiplets can also be explained.
The gluon-exchange interaction violates the spin \sutwo symmetry,
while differences in quark masses violates the flavour \suthree symmetry.

Many specific models exist with different degrees of sophistication,
but most contain a similar set of dynamical ingredients.
Two models are representative of various models commonly used in literature.
The first one is the ``Coulomb-plus-linear'' potential
\begin{equation}
  V_{i j}^{c}=\frac{1}{2}\left[-\frac{a}{r_{i j}}+b r_{i j}+d\right],
\end{equation}
which is supported by lattice calculations.
The second one is the ``power-law'' potential
\begin{equation}
  V_{i j}^{c}=\tfrac{1}{2}\left(A+B r_{i j}^{\beta}\right).
\end{equation}
The above potentials need to be supplemented by the hyperfine interaction of Fermi-Breit type
\begin{equation}
  V_{i j}^{s s}=\frac{C}{2} \frac{\mathbf{s}_{i} \cdot \mathbf{s}_{j}}{m_{i} m_{j}} \delta\left(\mathbf{r}_{i j}\right),
\end{equation}
which is responsible for hyperfine splitting between \jph and \jpth baryons.
The total potential model is given by $V_{ij} = V_{ij}^c + V_{ij}^{ss}$.

Various approximation methods are adopted to solve the three-body Schr\"odinger equation
$H \psi = E \psi$ to obtain the bound-state energy $E$ (\ie, the baryon mass)
and the baryon wave function $\psi$.
\emph{Variational methods} include harmonic oscillator expansion,
in which the wave function is expanded in terms of the eigenstates of a symmetric oscillator,
and hyperspherical formalism,
in which the wave function is expanded into partial-waves in spherical 
coordinates~\inlinecite{Itoh:2000um}.
The \emph{adiabatic approximation}, also known as Born-Oppenheimer approximation,
exploits the fact that two heavy quarks have much lower velocity than the 
light quark~\inlinecite{Fleck:1989mb}.
The binding energy of the light quark is calculated for each relative coordinate of
two heavy quarks, which is in turn used as the effective potential
governing the relative motion of the two heavy quarks.
The \emph{quark-diquark approximation} decomposes the exact three-body problem
into two successive two-body problems~\inlinecite{Fleck:1988vm}.
First, a diquark $D$ is built out of two quarks $q_2$ and $q_3$
and its mass $m_D$ is computed from two-body Hamiltonian
\begin{equation}
  H_{23}=m_{2}+m_{3}+\frac{p_{2}^{2}}{2 m_{2}}+\frac{p_{3}^{2}}{2 m_{3}}+V\left(\mathbf{r}_{23}\right).
\end{equation}
The diquark is then considered as a point-like object to form the baryon with quark $q_1$.
The approximate baryon mass is obtained from the two-body Hamiltonian
\begin{equation}
  H_{q D}=m_{1}+m_{D}+\frac{p_{1}^{2}}{2 m_{1}}+\frac{p_{D}^{2}}{2 m_{D}}+2 V\left(\mathbf{r}_{1 D}\right).
\end{equation}

Thanks to efforts in nuclear and atomic physics,
powerful tools are also available to solve the three-body problem exactly.
The \emph{Modified Green Function Monte Carlo method}
enables one to obtain exact ground-state masses and wave functions of multiquark states
using numerical algorithm~\inlinecite{Kerbikov:1987vx}.
The \emph{Faddeev formalism} can perform exact numerical solutions
for both ground and exinlinecited states~\inlinecite{Silvestre-Brac:1996myf}.

The nonrelativistic approximation is not adequate for the light quark,
since the estimates of the light quark velocity
in \eg doubly charmed baryons, $v/c \sim 0.7$, 
are highly relativistic.
The light quark can be treated fully relativistically with the quasipotential approach
without employing the expansion in $1/m_q$~\inlinecite{Ebert:2002ig}.
In the light-quark-heavy-diquark picture,
the quasipotential Schr\"odinger equation
for both the diquark bound state and the quark-diquark bound state reads
\begin{equation}
  \left(\frac{b^{2}(M)}{2 \mu_{R}}-\frac{\mathbf{p}^{2}}{2 \mu_{R}}\right) \psi(\mathbf{p})=\int \frac{\mathrm{d}^{3} q}{(2 \pi)^{3}} V(\mathbf{p}, \mathbf{q} ; M) \psi(\mathbf{q}),
\end{equation}
where $\mu_R$ is the relativistic reduced mass,
$b(M)$ is the relative momentum on mass shell,
$\mathbf{p}$ is the relative momentum,
and the kernel $V(\mathbf{p}, \mathbf{q} ; M)$ is the quasipotential operator
of the quark-quark or quark-diquark interaction.

To appreciate the predictive power of the quark model,
calculations of ground-state charmed baryons are compared with
lattice and experimental and data in Fig.~\ref{fig:single_charm_baryon_mass}.
An agreement within $10\mev$ is achieved.
In principle, a nonrelativistic treatment is fully justified
only for baryons containing three heavy quarks.
While in practice, the nonrelativistic potential model
has been applied with great success to systems in which its validity may be questioned.
The answer is far from definite and one possibility is that
the relativistic effect can be effectively simulated in the nonrelativistic scheme
with renormalised model parameters.

Despite the huge success beyond expectation,
the quark model has its limitations.
The number of exinlinecited states predicted by the quark model is far more than
the number of observed states, which is the so-called problem of missing resonances.
Besides, the ordering of resonances contradicts with data in many cases.
Moreover, ever-emerging new states with ``exotic'' quantum numbers,
referred to as exotic hadrons,
can not be understood in a systematic way.
From the theoretical side, its connection to full QCD and
the exact Bethe-Salpeter equation needs to be further elucidated.
In addition,
no systematic improvement on the theoretical uncertainty of quark model prediction 
is possible.

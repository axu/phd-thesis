% overview of doubly charmed baryons

The motivation for the study of doubly charmed baryons has been 
introduced in a broad context in Sec.~\ref{sec:the_standard_model_of_particle_physics}.
The existence of doubly charmed baryons was predicted~\inlinecite{DeRujula:1975qlm}
in $\mathrm{SU}(4)_f$ 20-plets soon after the discovery of \jpsi particle in 1974.
Since then, theoretical investigations of properties of doubly charmed baryons
have been extensively performed with experimental stimulations from time to time,
and eventually reached a climax in 2017, when the observation of 
the doubly charmed baryon \Xiccpp was reported by the LHCb experiment.
The citation summary with the key word \emph{doubly charmed baryons}
at INSPIRE HEP is shown in Fig.~\ref{fig:citation}.
The interplay between theory and experiment is
driven by the former in the study of doubly charmed baryons.
It is because that despite the observation in 2017, 
the experimental measurements are still very limited compared to
the huge amount of theoretical predictions.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\linewidth]{introduction/dhb_literature_per_year.png}
  \caption{The citation summary in March 2022 with the key word \emph{doubly charmed baryons}
      at INSPIRE HEP. The peaking record corresponds to 40 papers in 2018.
      The steep rises correspond to the observation of \Omegac in 1995,
      the claim of $\Xiccp(3520)$ baryon by SELEX in 2002,
      and the observation of \Xiccpp baryon by LHCb in 2017.}
  \label{fig:citation}
\end{figure}

The doubly charmed baryons refer to baryons containing two charm quarks and a light quark.
Ground-state doubly charmed baryons constitute a \jph triplet and a \jpth triplet,
as shown in Fig.~\ref{fig:multiplet}.
Doubly charmed baryons are expected to combine the dynamics found in the $D$ meson,
a relativistic motion of a light quark orbitting around 
a heavy static color $\bar{\mathbf{3}}$ source
at a distance ($\sim 1/m_q$) much larger than the source size ($\sim 1/m_c$),
and the dynamics of Charmonium.
A light-quark-heavy-diquark picture is often used as an approximation
in calculations of doubly charm baryons,
and is supported by nonrelativistic quark model calculations~\inlinecite{Fleck:1988vm}
at least for ground states.

There are several important energy scales in doubly charmed baryons
that play import roles in dynamics,
including the mass of the charm quark $m_c$,
its typical three-momentum $m_c v_c$,
its typical kinetic energy $m_c v_c^2$,
and the scale associated with nonperturbative effects \qcdscale
involving gluons and light quarks.
The typical velocity $v$ of the heavy quark decreases as the mass increases.
If the mass is sufficiently large,
the heavy quark is nonrelativistic,
with typical velocity $v \ll 1$.
The average value of $v_c^2$ is about 0.3 for charmonium
according to quark potential model calculations~\inlinecite{Bodwin:1994jh}.
The nonperturbative scale can be estimated with the
coefficient of the linear potential between quarks
and is about $\qcdscale \approx 450\mev$~\inlinecite{Bodwin:1994jh}.
Thus, we have well separated energies scales
in doubly charmed baryons:
\begin{equation}
  \Lambda^2_{\mathrm{QCD}} \sim \left(m_c v_c^2\right)^2 \ll \left(m_c v_c\right)^2 \ll m_c^2,
\end{equation}
which can be employed as expansion parameters in the evaluation of their properties.

Theoretical calculations and interpretations of the properties of 
the doubly charmed baryons are discussed in Sec.~\ref{sec:dcb_th},
followed by a review of the experimental status in Sec.~\ref{sub:experimental_status}.

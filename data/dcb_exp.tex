% review of experiment

Many experimental programs have been launched to search for the doubly charmed baryons,
since the observation of \Bc meson has demonstrated the accessibility
of hadrons with open double heavy flavours in modern facilities.
These experimental expeditions are summarised in Fig.~\ref{fig:dhb_exp}
and will be discussed in details below.
For completeness,
searches for doubly heavy baryons such as \Xibc are also included in the plot.
The configuration of experiments involved is shown in Table~\ref{tab:dcb_exp}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\linewidth]{introduction/dhb_experiments.pdf}
  \caption{Experimental searches for doubly heavy baryons.
    Null results are labelled with squares.
    Claims of observation are labelled with states.}
  \label{fig:dhb_exp}
\end{figure}
\begin{table}[tb]
  \centering
  \caption{Experiments involved in searches for doubly charmed baryons.}
  \label{tab:dcb_exp}
  \begin{tabular}{c c c c}
    \toprule
    Experiments & Collision & Energy & Statistics \\
    \midrule
    SELEX  & \makecell{$\Sigma$, $\pi$, or $p$ beam \\ Cu or diamond target} 
           & 600\gev beam & \makecell{$15\times10^9$ \\ inelastic interactions} \\
    FOCUS  & $\gamma p$ collision & $\sqrt{s} = 200\gev$ & $10^6$ charmed baryons \\
    \babar & \epem collision & $\sqrt{s} = 10.58\gev$ & 232\invfb \\
    \belle & \epem collision & $\sqrt{s} = 10.59\gev$ & 980\invfb \\
    \lhcb  & $pp$ collision  & $\sqrt{s} = 7,8,13\tev$ & 9\invfb \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{SELEX experiment.}%
\label{par:selex_experiment_}

SELEX experiment, a charm hadroproduction experiment at Fermilab 
with configuration shown in Table~\ref{tab:dcb_exp},
claimed an observation of doubly charmed baryon \Xiccp
in the $\Lc\Km\pip$ decay mode~\inlinecite{Mattson:2002vu}.
An excess of 15.9 events are observed with a local statistical significance of $6.3\sigma$.
The observed mass of the state is $3519\pm1\mev$ with a mass resolution of $3\mev$.
The lifetime of the state is less than 33\fs at 90\% confidence level.
About 20\% of the \Lc baryon in the sample are produced by \Xiccp decay.
SELEX reported confirmation of this state in the $pD^+\Km$ decay mode~\inlinecite{SELEX:2004lln},
with 5.6 events of a local statistical significance of $4.8\sigma$.
In conference proceedings~\inlinecite{Russ:2002bw,SELEX:2002xhn},
SELEX also reported a family of doubly charmed baryon \Xiccpp in the
$\Lc\Km\pip\pip$ decay mode,
$\Xiccpp(3460)$, $\Xiccpp(3452)$, $\Xiccpp(3541)$, and $\Xiccpp(3780)$.

These claims receive critical comments from the theoretical community~\inlinecite{Kiselev:2002an}.
The arguments are summarised below:
\begin{itemize}
  \item The state $\Xiccp(3520)$ has extremely exotic characteristics
    if interpreted as the doubly charmed baryon \Xiccp.
    The measured lifetime is too short and comparable with detector time resolution.
    The estimated production cross-section is too large in a fixed target experiment.
  \item The interpretation of associate charm production can not be ruled out
    with sufficient evidence.
  \item The reported \Xiccpp and \Xiccp states can not be treated as isospin partners.
\end{itemize}
It is also noticed that the evaluation of signal significance
by SELEX dose not take into account the Look Elsewhere Effect~\inlinecite{Gross:2010qma},
which states that the statistical significance of an observation increases
due to the large parameter space searched.
After LEE correction, the signal significance reduced below $5\sigma$.


\paragraph{FOCUS experiment.}%
\label{par:focus_experiement_}
FOCUS experiment,
a photoproduction experiment with configuration shown in Table~\ref{tab:dcb_exp},
searched for low lying doubly charmed baryon states
in 21 possible decay modes~\inlinecite{Ratti:2003ez}.
No evidence was observed in the range of 3.4 to 4.0\gev,
including the region explored by the SELEX experiment.


\paragraph{$B$ factories.}%
\label{par:_b_factories_}
$B$ factory experiments \babar and \belle also searched for
doubly charmed baryons \Xiccp in the $\Lc\Km\pip$ and $\Xicz\pip$ decay modes,
and doubly charmed baryons \Xiccpp in the $\Lc\Km\pip\pip$ and $\Xicz\pip\pip$ 
decay modes~\inlinecite{Aubert:2006qw,Chistov:2006zj}.
No significant signals were observed in all decay modes.


\paragraph{LHCb experiment.}%
\label{par:lhcb_experiemnt_}
In 2013, LHCb experiment reported a search for the $\Xiccp\to\Lc\Km\pip$ decay
with $pp$ collision data at center-of-mass energy of 7\tev,
corresponding to an integrated luminosity of 0.65\invfb~\inlinecite{LHCb-PAPER-2013-049}.
No significant signal was found in the mass range 3300--3800\mev.
the ratio of the \Xiccp production cross-section times branching fraction to that of the \Lc
was found to be less than $1.5\times10^{-2}$ for a lifetime of 100\fs
at 95\% confidence level.

Later in 2017, LHCb experiment reported the observation of 
a highly significant structure, identified as doubly charmed baryon \Xiccpp,
in the $\Lc\Km\pip\pip$ decay mode,
with $pp$ collision data center-of-mass energy of 13\tev,
corresponding to an integrated luminosity of 1.7\invfb~\inlinecite{LHCb-PAPER-2017-018}.
The \Xiccpp mass is determined to be 
$3621.40 \pm 0.72 \pm 0.27 \pm 0.14\mev$, 
where the uncertainty is due to statistical, systematic,
and the limited knowledge of the \Lc mass.
The structure is confirmed in the data collected at center-of-mass energy of 8\tev.
This observation is soon confirmed in the $\Xicp\pip$ decay mode~\inlinecite{LHCb-PAPER-2018-026},
while no signifiant signal was observed in the $\Dp p\Km\pip$
decay mode~\inlinecite{LHCb-PAPER-2019-011}.
The \Xiccpp state reported by LHCb experiment can not be the isospin partner
of the SELEX $\Xiccp(3520)$ state due the large mass difference.

The properties of \Xiccpp baryons were measured soon after its discovery.
The $\Xiccpp$ lifetime has been measured to be
$\ensuremath{0.256\,^{+0.024}_{-0.022}\,{\rm(stat)\,}\pm
0.014\,{\rm(syst)}\,\ps}$~\inlinecite{LHCb-PAPER-2018-019},
which establishes the weakly decaying nature of the $\Xiccpp$ baryon.
The production cross-section times the branching fraction of $\Xiccpp\to\Lc\Km\pip\pip$
decay is measured with regard to prompt \Lc production cross-section
to be $(2.22 \pm 0.27 \pm 0.29)\times10^{-4}$ in the fiducial region of
$4<\pt<15\gev$ and $2.0<y<4.5$ in $pp$ collisions at center-of-mass energy of 13\tev,
where the uncertainty is due to statistical and systematic.

In summary, the \Xiccpp baryon is still the only well-established one of the three
doubly charmed baryons.
Its properties have been measured extensively with good precision.
Based on the known properties of \Xiccpp baryon
and the large data sets collected by the LHCb experiment during Run~1 and Run~2 of the LHC,
it is desirable to launch search programs for other doubly charmed baryons, 
especially the \Xiccp baryon.

This thesis reports
the measurement of lifetimes of \Omegac and \Xicz baryons~\inlinecite{LHCb-PAPER-2021-021}
and
the search for doubly charmed baryon \Xiccp~\inlinecite{LHCb-PAPER-2019-029}.
The theoretical motivations and experimental status are reviewed in this Chapter.
The experimental facilities and techniques are introduced in Chapter~\ref{chap:detector}.
Data analysis of these measurements are discussed in detail
in Chapter~\ref{chap:lifetime} and \ref{chap:xicc},
with dedicated discussions of the implications of the results.
The summary and prospects are presented in Chapter~\ref{chap:summary}.

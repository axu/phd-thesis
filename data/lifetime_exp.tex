% review of experiment

Many measurements of lifetimes of heavy flavour hadrons have been performed 
in the last thirty years.
Precisions of experimental measurements have been continuously improved
due to the increase of statistics and development of detector techniques.
Measurements of charmed hadron lifetimes are summarised in Fig.~\ref{fig:lifetime_charm}.
Lifetimes of charmed mesons are measured with precision of about 1\%
and the central values of various measurements agree well with each other.
Lifetimes of charmed baryons are measured less precisely,
with precision in the range of 3--17\% before the recent LHCb measurements.
As a comparison,
measurements of beauty hadron lifetimes are shown in Fig.~\ref{fig:lifetime_beauty}.
Lifetimes of beauty hadrons are known with good precision, except for that of the \Omegab baryon.
Fig.~\ref{fig:lifetime_beauty} illustrates that 
all beauty hadrons have similar lifetimes,
indicating that the spectator effects
are significantly suppressed with increasing heavy quark mass.
It is interesting to notice that there was a change in the central values
of \Bz meson over the years.
Although successive measurements agree with each other within uncertainty,
the central values vary by a factor of about two over a span of twenty years.
Historically, this lead to a discrepancy between HQE predictions and the measured
ratio of $\tau_{\Lb}/\tau_{\Bz}$ for some time~\inlinecite{Cheng:2015iom}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{introduction/charm_meson_lifetime.pdf}
  \includegraphics[width=0.45\linewidth]{introduction/charm_baryon_lifetime.pdf}
  \caption{Measurements of lifetimes of (left) charmed mesons and (right) charmed baryons.}
  \label{fig:lifetime_charm}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{introduction/beauty_meson_lifetime.pdf}
  \includegraphics[width=0.45\linewidth]{introduction/beauty_baryon_lifetime.pdf}
  \caption{Measurements of lifetimes of (left) beauty mesons and (right) beauty baryons.}
  \label{fig:lifetime_beauty}
\end{figure}

A converse story occurs in surprise in the sector of charmed baryons 
when the LHCb experiments recently reported a systematic update of 
charmed baryon lifetimes~\inlinecite{LHCb-PAPER-2018-028,LHCb-PAPER-2019-008}.
These measurements violated the harmonic agreement between theoretical predictions
and experimental measurements by changing the lifetime hierarchy from 
\begin{equation}
  \tau_{\Omegac}<\tau_{\Xicz}<\tau_{\Lc}<\tau_{\Xicp}
\end{equation}
into
\begin{equation}
  \tau_{\Xicz}<\tau_{\Lc}<\tau_{\Omegac}<\tau_{\Xicp}.
\end{equation}
To be more specific,
the measured \Omegac lifetime, $\tau_{\Omegac}$, 
is nearly four times larger than the previous world average~\inlinecite{PDG2018},
which is inconsistent at a level of seven standard deviations.
The measured \Xicz lifetime, $\tau_{\Xicz}$, 
is larger than the previous world average by three standard deviations,
as can be seen from Table~\ref{tab:lifetime_exp}.
\begin{table}[tb]
  \centering
  \caption{Measurements of lifetimes of singly charmed baryons.}
  \label{tab:lifetime_exp}
  \begin{tabular}{l c c c c c}
    \toprule
    & \Omegac & \Xicz & \Lc & \Xicp \\
    \midrule
    PDG18 [fs]    & $69\pm12$ & $112^{+13}_{-10}$ & $200\pm6$ & $442\pm26$ \\
    LHCb SL [fs]  & $268 \pm 26$ & $154.5 \pm 2.6$ & $203.5 \pm 2.2$ & $456.8 \pm 5.5$ \\
    \bottomrule
  \end{tabular}
\end{table}
These measurements use charmed baryons produced in semileptonic beauty baryon decays,
collected in $pp$ collisions at center-of-mass energies of 7, 8\tev
by the LHCb experiment during 2011-2012,
corresponding to an integrated luminosity of 3\invfb.
The \Omegac lifetime is measured using around 
\num{1000} $\Omegab \to \Omegac \mun \neumb X$ decays,
while the \Xicz lifetime is measured using around
$2\times10^4$ $\Xibm\to\Xicz\mun \neumb X$ decays.
Both \Omegac and \Xicz baryon are reconstructed in the $p\Km\Km\pip$ final state.
The $\Dp\to\Km\pip\pip$ decay is used as the normalisation channel to
reduce the uncertainties associated with systematic effects.
The lifetime of \Omegac baryon is still limited by the signal sample size.

Resolving the emergent discrepancy is essential for 
our understanding of weak decays of heavy flavour hadrons.
Independent measurements are crucial to establish a sound reference
for further theoretical development.

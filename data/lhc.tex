% introduction to LHC

The Large Hadron Collider beauty (LHCb) experiment detects collisions of particle bunches
provided by the Large Hadron Collider (LHC),
the most powerful tool for Particle Physics research in the world.
The LHC also hosts large-scale experiments
A Toroidal LHC Apparatus (ATLAS),
the Compact Muon Solenoid (CMS),
and A Large Ion Collider Experiment (ALICE).
The ATLAS and CMS are general-purposed detectors
focusing on measurements of the Higgs boson and
searches for new physics beyond the Standard Model 
in a direct approach~\inlinecite{ATLAS:2008xda,CMS:2008xjf}.
The ALICE detector studies quark-gluon plasma by
measuring lead-ion collision~\inlinecite{ALICE:2008ngc}.
The description in this section is based on the 
LHC Design Report~\inlinecite{Bruning:2004ej,Buning:2004wk,Benedikt:2004wm}
and its abridged version in Ref.~\inlinecite{Evans:2008zzb} unless otherwise stated.

The Large Hadron Collider (LHC)
is a two-ring-superconducting-hadron accelerator and collider 
installed in the 26.7\km tunnel at CERN near Geneva.
%The exploration of rare events in the LHC collisions 
%requires both high beam energies and high beam intensities.
It is designed to collide proton beams with a centre-of-mass energy of 14\tev 
and an unprecedented luminosity of $10^{34}\cm^{−2}\sec^{−1}$.
It can also collide heavy (Pb) ions with an energy of 2.8\tev per nucleon 
and a peak luminosity of $10^{27}\cm^{−2}\sec^{−1}$.
The LHC has two rings with counter-rotating beams because
it is a particle-particle collider,
unlike particle-antiparticle colliders that
can have both beams sharing the same phase space in a single ring.
The peak beam energy depends on the integrated dipole field around the storage ring, 
which implies the use of superconducting magnet technology.
The LHC beam parameters at collision are shown in Table~\ref{tab:lhc_beam}.
\begin{table}[tb]
  \centering
  \caption{LHC beam parameters at collision.}
  \label{tab:lhc_beam}
  \begin{tabular}{l c c}
    \toprule
    Parameters & $pp$ collision & PbPb collision \\
    \midrule
    Energy per nucleon [$\!\tev$]    & 7                   & 2.76            \\
    Luminosity [$\cm^{−2}\sec^{−1}$] & $10^{34}$           & $10^{27}$       \\
    Number of bunches                & 2808                & 592             \\
    Bunch spacing [ns]               & 24.95               & \\
    Intensity per bunch              & $1.15\times10^{11}$ & $7.0\times10^7$ \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Machine layout.}%
\label{par:machine_layout_}

The basic layout of the LHC is illustrated in Figure~\ref{fig:lhc_schematic}. 
The LHC has eight arcs and eight straight sections.
The arcs contain the dipole bending magnets,
while each straight section can serve as an experimental or utility insertion. 
There four straight sections that have collision points.
The two high luminosity experimental insertions are located 
at diametrically opposite straight sections: 
the ATLAS experiment is located at Point 1 and the CMS experiment at Point 5. 
Two more experimental insertions are located at Point 2 and Point 8:
the ALICE experiment is located at Point 2 and the LHCb experiment at Point 8.
Points 2 and 8 also include the injection systems for Beam 1 and Beam 2, respectively. 
The remaining four straight sections do not have beam crossings. 
Insertions at Points 3 and 7 each contain two collimation systems. 
The insertion at Point 4 contains two RF systems. 
The straight section at Point 6 contains the beam dump insertion.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\linewidth]{introduction/lhc_layout.pdf}
  \caption{Schematic layout of the LHC.}
  \label{fig:lhc_schematic}
\end{figure}

\paragraph{Magnets.}%
\label{par:magnets_}
There is a large variety of magnets in the LHC,
including dipoles, which bending particles in the arcs,
quadrupoles, which focus the beam size at the collision points,
and other correction magnets such as sextupoles, octupoles, decapoles \etc.
LHC magnet system makes use of the well-proven technology 
based on NbTi Rutherford cables, 
cools the magnets to a temperature below $2\,K$ using superfluid helium, 
and operates at fields above $8\,\mathrm{T}$.

\paragraph{Vacuum systems.}%
\label{par:vaccum_systems_}
The LHC has three vacuum systems:
the insulation vacuum for cryomagnets,
the insulation vacuum for the helium distribution line,
and the beam vacuum (ultrahigh vacuum) to avoid beam collisions with gas molecules.

\paragraph{Cavities.}%
\label{par:cavities_}
The LHC cavities keep particle bunches tightly bunched
to ensure high luminosity at collision points
and deliver radiofrequency (RF) power to the beam.

\paragraph{Injection chain.}%
\label{par:injection_chain_}
The accelerator complex at CERN is a succession of machines
with increasing higher energies, as illustrated in Fig.~\ref{fig:accelerator_complex}.
The LHC is the last element of this chain.
The injection chain of proton is 
\begin{equation}
  \text{proton} \xrightarrow{\text{LINAC 2}} 50\mev 
  \xrightarrow{\text{BOOSTER}} 1.4\gev
  \xrightarrow{\text{PS}} 25\gev
  \xrightarrow{\text{SPS}} 450\gev
  \xrightarrow{\text{LHC}} 6.5\tev.
\end{equation}
The injection chain of lead ion is
\begin{equation}
  \text{lead ion} \xrightarrow{\text{LEIR}} 72\mev/u
  \xrightarrow{\text{PS}} 5.9\gev/u
  \xrightarrow{\text{SPS}} 177\gev/u
  \xrightarrow{\text{LHC}} 2.56\tev/u.
\end{equation}
Here the energy achieved in Run~2 is quoted,
which is slightly lower than the nominal value in design.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\linewidth]{introduction/cern_complex.pdf}
  \caption{Accelerator complex at CERN.}
  \label{fig:accelerator_complex}
\end{figure}

\paragraph{Luminosity.}%
\label{par:luminosity_}
The number of events generated in the LHC collisions per second is given by
\begin{equation}
  N_{\text{event}} = \mathcal{L} \times \sigma_{\text{event}},
\end{equation}
where $\sigma_{\text{event}}$ is the cross-section for the event under consideration
and $\mathcal{L}$ is the instantaneous luminosity.
The machine luminosity depends on beam parameters via
\begin{equation}
  \mathcal{L} = \frac{N^2_{p} N_b f_{\text{rev}} \gamma_{r}}{4\pi\varepsilon_n\beta^{*}} 
                \times F,
\end{equation}
where $N_p$ is the number of particles per bunch,
$N_b$ is the number of bunches per beam,
$f_{\text{rev}}$ is the revolution frequency,
$\gamma_{r}$ is the relativistic gamma factor,
$\varepsilon_n$ is the normalised transverse beam emittance,
$\beta^{*}$ is the beta function at the collision point,
and $F$ is the geometric luminosity reduction factor
due to nonzero crossing angle and bunch length.
The luminosity in the LHC is not constant over a physics run, 
but decays due to the degradation of intensities and emittances of the circulating beams. 
The luminosity half-life is estimated to be $\tau_L \approx 15\,h$.
The turnaround time,
the time between the end of a luminosity run and a new beam at top energy, 
is approximately 7 hours.
The integrated luminosity over one run yields
\begin{equation}
  \mathcal{L}_{\text{int}} = \mathcal{L}_0 \times \tau_L \times 
                             \left[ 1 - \exp(-T_{\text{run}}/\tau_L) \right],
\end{equation}
where $T_{\text{run}}$ is the total length of the luminosity run.
The overall collider efficiency depends on 
the ratio of the length of the run to the average turnaround time,
and is optimised to be 80\invfb 
with a run time of 12 hours and a turnaround time of 7 hours.

\paragraph{Operation and plan.}%
\label{par:hl_lhc_}
The present LHC baseline programme is shown schematically in Table~\ref{tab:lhc_plan}. 
During Run~1 the LHC was operated with 50\ns bunch spacing. 
After the consolidation measures in Long Shutdown 1 (LS1),
the LHC was operated in Run~2 at 13\tev centre-of-mass energy. 
The bunch spacing was reduced to 25\ns, the design value, 
and the luminosity was progressively increased, 
attaining the nominal design luminosity of $10^{34}\cm^{−2}\sec^{−1}$ on 26 June 2016. 
A peak luminosity of $2\times10^{34}\cm^{−2}\sec^{−1}$ was achieved in 2018 
thanks to the small emittances of the beam delivered by the injectors 
and to a smaller than design $\beta^{*}$ value.
At present (2022),
the LHC is in Long Shutdown 2 (LS2) during which further consolidation measures
are being pursued and should enable the LHC to reach its nominal design beam energy
of 7\tev~\inlinecite{ZurbanoFernandez:2020cco}.
In the Run~3 period from 2022 to 2024,
the LHC aims to further increase the integrated luminosity total: 
the present goal is to reach $350\invfb$  by the end of Run~3, 
well above the initial LHC goal of about $300\invfb$.
The LHC will need a major upgrade in the 2020s to
a) extend its operability by another decade or more;
b) increase its collision rate and thus the delivered 
integrated luminosity~\inlinecite{ZurbanoFernandez:2020cco}.
The machine configuration of the upgrade is referred to as the high-luminosity LHC (HL-LHC),
with the following targets:
\begin{itemize}
  \item A peak luminosity of $5\times10^{34}\cm^{−2}\sec^{−1}$ with levelling operation;
  \item An integrated luminosity of 250\invfb per year, 
    with the goal of 3000\invfb in the 12 years or so after the upgrade.
\end{itemize}
\begin{table}[tb]
  \centering
  \caption{LHC baseline plan for the next decade and beyond.}
  \label{tab:lhc_plan}
  \begin{tabular}{c c c c}
    \toprule
    Run & Periods & Energy [TeV] & Luminosity (ATLAS, CMS) [$\!\invfb$] \\
    \midrule
    Run~1   & 2011-2012 & 7, 8  & 30   \\
    LS1     & 2013-2014 &       &      \\
    Run~2   & 2015-2018 & 13    & 190  \\
    LS2     & 2019-2021 &       &      \\
    Run~3   & 2022-2024 & 13-14 & 350  \\
    LS3     & 2025-2026 &       &      \\
    Run~4-5 & 2027-2040 & 14    & 3000 \\
    \bottomrule
  \end{tabular}
\end{table}

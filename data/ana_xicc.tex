This chapter reports a search for the doubly charmed baryon \Xiccp 
through the $\Lc\Km\pip$ decay channel~\inlinecite{LHCb-PAPER-2019-029}.
This search is an update of the previous LHCb search,
with a more than ten times larger data set and the improved analysis method.
An overview of the data analysis strategy and method is presented
in Sec.~\ref{sec:xicc_analysis_overview}.
Event selection, 
which is the key to observation of rare signals,
is discussed extensively in Sec.~\ref{sec:xicc_event_selection}.
The strategy for measuring the mass of \Xiccp baryon 
is presented in Sec.~\ref{sec:strategy_for_measuring_the_mass}.
Distributions of the invariant mass of \Xiccp baryon 
is shown in Sec.~\ref{sec:invariant_mass_distribution_after_unblinding}.
With the lack of significant signals,
upper limits are set relative to two control modes in Sec.~\ref{sec:upper_limits}.
To conclude, result and its interpretation are presented 
in Sec.~\ref{sec:xicc_result_and_discussion}.

\section{Analysis overview}%
\label{sec:xicc_analysis_overview}

In this analysis,
a search for the $\Xiccp$ baryon through $\Lcp K^- \pip$ final state was reported.
The contributing tree-level Feynman diagram with internal $W$-emission
is shown in Fig.~\ref{fig:feynman_xicc}.
This final state is chosen given
a) a large predicted branching fraction of the doubly charmed baryon decay;
b) a large decay branching fraction of subsequent \Lc decay;
c) decay products are all charged particles that are easy to detect.
The data samples collected by the LHCb detector during Run~1 and Run~2 of the LHC
are used corresponding to an integrated luminosity of 9\invfb, 
as detailed in Appendix~\ref{sec:bookkeeping_of_data_and_simulation_samples}.
The \Lc baryon is reconstructed through the $p\Km\pip$ final state.
The searching mass window is chosen to be 3.4-3.8\gev,
which centered at the \Xiccpp mass 
measured by the LHCb experiment~\inlinecite{LHCb-PAPER-2017-018},
and covers most theoretical predictions of the mass of ground-state \Xiccp baryon,
as well as the $\Xiccp(3520)$ state reported by the SELEX experiment~\inlinecite{Mattson:2002vu}.
It is interesting to notice that this window also covers the mass of \Omegacc
baryon decaying to the same final state.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_xicc/feynman.pdf}
  \caption{The contributing tree-level Feynman diagram with internal $W$-emission
  for the decay of $\Xiccp\to\Lcp\Km\pip$.}
  \label{fig:feynman_xicc}
\end{figure}

Since this analysis is a search for nonestablished particles
and in order to avoid experimentalists' subjective bias,
the signal window was not examined nor used in the development of the analysis
until the whole analysis procedure was reviewed and finalised.
When the green light to unblind is given,
we may find ourselves in two different situations,
well-defined by the significance of the signal structure in Sec.~\ref{sub:strategy}.
If any significant peak structure consistent with the \Xiccp baryon is observed, 
as we hope, the $\Xiccp$ mass will be measured.
If not, upper limits on the ratio of production cross-section times branching fraction
between the signal and the control mode will be set.
Two control modes are chosen as the reference: 
the prompt \Lc baryon reconstructed through $\proton\Km\pip$ final state
and the \Xiccpp baryon reconstructed through $\Lc\Km\pip\pip$ final state,
with $\Lc\to\proton\Km\pip$.
The ratios of the production cross-section times branching fraction are defined as
\begin{equation}
  \begin{aligned}
    \label{eq:RLc}
    R(\Lc)     &\equiv \frac{\sigma(\Xiccp)\times\BF(\Xiccp \to \Lc\Km\pip)}{\sigma(\Lc)}, \\
    R(\Xiccpp) &\equiv \frac{\sigma(\Xiccp)\times\BF(\Xiccp \to
    \Lc\Km\pip)}{\sigma(\Xiccpp)\times\BF(\Xiccpp \to \Lc\Km\pip\pip)}.
  \end{aligned}
\end{equation}
The $R(\Lc)$ gives us an idea on the ratio between the production
cross-sections of the doubly charm and the singly charm.
It can be compared with the previous LHCb search, as well as the SELEX search.
The $R(\Xiccpp)$ tells us the combined information of relationship of the 
branching fractions and lifetimes between $\Xiccp \to \Lc\Km\pip$ and 
$\Xiccpp \to \Lc\Km\pip\pip$ decays,
as the production cross-sections of the $\Xiccp$ baryon is expected, with great reason, 
to be similar to that of the $\Xiccpp$ baryon.

The displace decay vertices of charmed baryons serve as a signature in both
the trigger and offline selection.
Consequently, the lifetime of the weakly decaying particle
has an significant effect on the selection efficiency.
According to the theoretical predictions and the measurement of \Xiccpp lifetime,
the lifetime value of $\tau_{\Xiccp} = 80\fs$ is used as the default value
for the development of event selection,
corresponding to around one third of that of the \Xiccpp baryon.
A dedicated multivariate classifier is also trained,
assuming the lifetime of \Xiccp is negligible.

\subsection{Strategy for evaluating significance and setting limits}%
\label{sub:strategy}

To be quantitative, we describe the criteria of choosing different paths.
In the discussion below, we will refer to local and global significances.
By a local significance, 
we mean the significance of a signal evaluated for one
particular mass hypothesis. 
By a global significance,
we mean the significance of a signal in the whole mass range
taking into account the look-elsewhere effect (LEE).
That is, the global significance reflects the probability 
under the null hypothesis for such a signal to pop up anywhere in the mass range scanned.

In the previous $\Xiccp \to \Lc \Km \pip$ search,
carried out on the 2011 data, 
we evaluated local and global significances for a search range of 500\mev.
It is found that there was a factor of roughly 30 between the local and
global $p$-values.
(This makes intuitive sense to within a factor of two or so:
the resolution was roughly 5\mev, so if you imagine dividing
the data into disjoint $\pm 2\sigma$ chunks you have 25 such chunks.)
To put this into context, here is how the integral of a Gaussian
distribution for various intervals (\ie the relation between significance and $p$-value):
\begin{table*}[!h]
  \centering
  \begin{tabular}{cc}
    \toprule
    Range around peak & Integral outside range \\
    \midrule
    $\pm 1\sigma$ & $3.2 \times 10^{-1}$ \\
    $\pm 2\sigma$ & $4.6 \times 10^{-2}$ \\
    $\pm 3\sigma$ & $2.7 \times 10^{-3}$ \\
    $\pm 4\sigma$ & $6.3 \times 10^{-5}$ \\
    $\pm 5\sigma$ & $5.7 \times 10^{-7}$ \\
    $\pm 6\sigma$ & $2.0 \times 10^{-9}$ \\
    $\pm 7\sigma$ & $2.6 \times 10^{-12}$ \\
    \bottomrule
  \end{tabular}
\end{table*}
\\ The key point is that this relation is highly nonlinear.
For significances of 2--3$\sigma$, the LEE is an important
correction. (If the local/global factor is about 30, it will turn
a local significance of $3\sigma$ into a global
significance of well under $2\sigma$, for example.)
But for high significances it is much less important:
the difference in $p$-value between $5\sigma$ and $6\sigma$
is a factor of around 300, for example. As a consequence,
the LEE is only important for results that are marginally significant.

Depending on what we see in data, we will need to choose one of
a few different paths:
\begin{itemize}
  \item If there is a peak whose local significance is above $6\sigma$,
    we will quote that local significance (along with how it was evaluated)
    but will not fuss with a LEE correction.
    We will measure its mass and yield in a fit to the invariant mass distribution.
  \item If there is a peak whose local significance is moderate (3--6$\sigma$),
    we will compute an LEE correction and quote both local and global significances.
    If the global significance remains above $3\sigma$, we will measure the
    mass and yield as above. If the global significance is below $3\sigma$, we will
    quote upper limits on the production rate (see below) as a function of
    the reconstructed mass of \Xiccp candidates for different lifetime hypotheses.
  \item If there is no peak at even $3\sigma$ local, we will quote upper limits as above.
\end{itemize}

\section{Event selection}%
\label{sec:xicc_event_selection}
\input{data/ana_xicc/caseA_selection}
\input{data/ana_xicc/caseA_background}

\section{Strategy for measuring the mass}%
\label{sec:strategy_for_measuring_the_mass}
\input{data/ana_xicc/caseA_mass}

\section{Invariant-mass distribution after unblinding}%
\label{sec:invariant_mass_distribution_after_unblinding}
\input{data/ana_xicc/unblinding_plots}

\section{Upper limits}%
\label{sec:upper_limits}
Since no excess above three standard deviations is observed,
upper limits on the production ratios are set using the
data recorded at $\sqrt{s}=8\tev$ in 2012
and at $\sqrt{s}=13\tev$ in 2016-2018,
which is discussed in detail below.

The ratio of the production cross-sections,
defined in Eq.~\ref{eq:RLc}, can be evaluated as
\begin{equation}
  \label{eq:alpha}
  R      = \frac{\lum_{\text{con}}}{\lum_{\text{sig}}} \frac{\varepsilon_{\text{con}}}{\varepsilon_{\text{sig}}}
  \frac{N_{\text{sig}}}{N_{\text{con}}}
  \equiv \alpha N_{\text{sig}},
\end{equation}
where $\lum_{\text{con}}$ and $\lum_{\text{sig}}$ are
the luminosity of the control mode and signal mode,
$\varepsilon_{\text{con}}$ and $\varepsilon_{\text{sig}}$
are the corresponding efficiency.
It can be seen from Eq.~\ref{eq:alpha}
that the single event sensitivity, $\alpha$,
needs to be measured
to evaluate the ratio of the production cross-sections $R_\Lc$ and $R_\Xiccpp$.
This implies that yields of control modes,
as well as the luminosity and selection efficiency of signal and control modes
need to be evaluated.

For 2012 and 2016-2018 data, the \Lc control mode, \Xiccpp control mode,  and the signal mode 
are collected at the same time,
which leads to the ratio of luminosities in Eq.~\ref{eq:alpha} to be 1.

\subsection{Event selection for setting upper limits}%
\label{sub:event_selection_for_setting_upper_limits}
\input{data/ana_xicc/caseB_selection}
\subsection{Yield of control modes}%
\label{sub:yield_of_control_modes}
\input{data/ana_xicc/caseB_yield}
\subsection{Estimation of efficiency}%
\label{sub:xicc_estimation_of_efficiency}
\input{data/ana_xicc/caseB_efficiency}
\subsection{Systematic uncertainties}%
\label{sub:xicc_systematic_uncertainties}
\input{data/ana_xicc/caseB_systematics}
\subsection{Determination of upper limits}%
\label{sub:determination_of_upper_limits}
\input{data/ana_xicc/caseB_upper_limit}

\section{Result and discussion}%
\label{sec:xicc_result_and_discussion}
\input{data/ana_xicc/summary}


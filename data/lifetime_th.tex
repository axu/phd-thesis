% review for theory

There are four singly charmed baryons and three doubly charmed baryons that decay weakly,
\ie the \jph baryon $\Lc(cud)$, $\Xicp(cus)$, $\Xicz(cds)$, $\Omegac(css)$,
$\Xiccpp(ccu)$, $\Xiccp(ccd)$, and $\Omegacc(ccs)$
in the $\mathrm{SU}(4)_f$ 20-multiplets shown in Fig.~\ref{fig:multiplet}.

\paragraph{Order-of-magnitude estimation.}%
\label{par:order_of_magnitude_estimation_}

At the quark level, there is only a single lifetime for a given flavour.
In the spectator picture,
the order-of-magnitude of the weakly decaying charmed hadrons
can be estimated by relating the lifetime of the charm quark to that of a muon.
Feynman diagrams of a muon and a charm quark decay are shown 
in Fig.~\ref{fig:feynman_cquark_decay}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.43\linewidth]{introduction/muon.pdf}
  \includegraphics[width=0.50\linewidth]{introduction/cquark.pdf}
  \caption{Tree-level Feynman diagram of (left) muon and (right) charm quark decay.}
  \label{fig:feynman_cquark_decay}
\end{figure}
The decay width of a muon up to leading order reads
\begin{equation}
  \Gamma_{\mu}^{(0)} = \frac{G^2_F m^5_\mu}{192 \pi^3},
\end{equation}
where $G_F$ is the Fermi constant and $m_\mu$ is the muon mass.
The lifetime of a charm quark is therefore
\begin{equation}
  \tau_c \approx \tau_\mu \times \frac{\Gamma_\mu}{\Gamma_c} 
  \approx \tau_\mu \times \left( \frac{m_\mu}{m_c} \right)^5
  \times \frac{1}{N_{\mathrm{channel}}} \times \frac{1}{|V_{cs}|^2} \approx 800 \fs,
\end{equation}
where $m_c \approx 1.5\gev$ is the charm quark mass,
$N_{\mathrm{channel}} = 2+3$ takes into account the effect that
there are 2 lepton channels (electron and muon) and 3 quark channels (red, blue, and green)
for a charm quark decay while only 1 lepton channel (muon) for a muon decay,
and $V_{cs} \approx 1$ is one of the CKM matrix element.
Similarly, the lifetime of a double-charm system can be estimated as
\begin{equation}
  \tau_{cc} \approx \frac{1}{2 \Gamma_c} \approx 400 \fs.
\end{equation}

In the real world,
lifetimes of charmed hadrons span an order of magnitude,
from the shortest-lived \Xicz baryon of about 150\fs,
to the longest-lived \Dp meson of about 1050\fs.
This indicates that spectators quarks play an important role 
in the weak decay of charmed hadrons.
The weak decay of charmed hadrons constitute an intriguing and novel laboratory
for studying strong dynamics through the interplay with weak interactions.


\paragraph{Phenomenological model.}%
\label{par:phenomenological_model_}
Two mechanisms have been identified
to explain differences in the lifetimes of charmed hadrons~\inlinecite{Bellini:1996ra}:
\begin{itemize}
  \item The $W$-scattering (WS) of heavy quark $Q$ with the valence diquark system of baryons.
    For example, the process of $cd \to su$ contributes to the \Lc, \Xicz and \Xiccp decay.
  \item The interference between different quark diagrams due to 
    the presence of identical quarks in the final state, 
    referred to as Pauli interference (PI).
    For example, the $u$ ($s$) quark produced in the charm quark decay $c\to su \bar{d}$
    is identical to the spectator quark $u$ ($s$) in \Lc, \Xicp and \Xiccpp
    (\Xicp, \Xicz, and \Omegacc) baryon.
\end{itemize}
It can be noticed that these mechanisms make implicit assumptions that
constituent quarks are the only effective degree of freedom in charmed hadrons.

Despite a clean picture, these treatments are not systematic
and is easy to overlook certain contributions.


\paragraph{Heavy quark expansion.}%
\label{par:heavy_quark_expansion_}
Heavy Quark Expansion (HQE) is
a systematic approach widely used to
express nonperturbative corrections to heavy-flavour decays~\inlinecite{Shifman:1986mx}.
In this approach, the decay width is calculated through an expansion
in inverse powers of heavy quark mass $m_Q$.
The notion of quark-hadron duality is employed implicitly:
inclusive transition rates between hadronic systems can be calculated
in terms of quarks and gluons~\inlinecite{Bellini:1996ra}.

The weak decay of the heavy quark $Q$ in the heavy-flavour hadron $H_Q$
proceeds in a cloud of light degrees of freedom
with which heavy quark $Q$ and its decay products interacting strongly.
The imaginary part of a forward scattering operator $\hat{T}$
can be used to describe the transition rate 
to an inclusive final state $f$~\inlinecite{Bellini:1996ra}
\begin{equation}
  \label{eq:ope}
  \hat{T}(Q \to f \to Q) = i \Im \int \mathrm{d}^4x 
  \mathrm{T}\left[\mathcal{L}_W(x)\mathcal{L}_W^{\dagger}(0)\right]
  \xlongequal{\mathrm{OPE}} G^2_F |V_{CKM}|^2 \sum_d C_d^f \times O_d,
\end{equation}
where $T[\cdot]$ denotes the time-ordered product,
$\mathcal{L}_W$ is the relevant effective weak Lagrangian.
The forward scattering operator is expanded 
as an infinite sum of local operators $\mathcal{O}_d$
with Wilson coefficients $C_d$ as shown in the second equation of Eq.~\ref{eq:ope}.
The master equation of the decay width corresponding to final state $f$
can then be obtained by taking the expectation value of $\hat{T}$ 
for hadron state $\ket{H_Q}$, up to order $1/m_Q^3$, as
\begin{equation}
  \begin{aligned}
    \label{eq:hqe}
    \Gamma(H_Q \to f) &= \bra{H_Q} \hat{T}(Q \to f \to Q) \ket{H_Q} \\
    &= \frac{G^2_F m^5_c}{192 \pi^3} \times \Big[
      C_{3}^{f}\left\langle H_{Q}|\bar{Q} Q| H_{Q}\right\rangle+
      C_{5}^{f} \frac{\left\langle H_{Q}|\bar{Q} i \sigma \cdot G Q| H_{Q}\right\rangle}{m_{Q}^{2}} \\
      & + \sum_{d} C_{6,d}^{f} \frac{\left\langle H_{Q}\left|\left(\bar{Q} \Gamma_{d} q\right)\left(\bar{q} \Gamma_{d} Q\right)\right| H_{Q}\right\rangle}{m_{Q}^{3}}+O\left(1 / m_{Q}^{4}\right)
      \Big],\\
  \end{aligned}
\end{equation}
where $Q$ and $q$ are the heavy and light quark field,
$G$ is the gluon field, $\sigma$ is the Pauli matrix,
$\Gamma$ is a combination of Dirac and Gell-Mann matrices.
Feynman diagrams that correspond to operators in Eq.~\ref{eq:hqe}
are shown in Fig.~\ref{fig:hqe}.
\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/2-1.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/hqe_d3.png}}
    \caption{$\bar{Q}Q$ operator.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/2-2.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/hqe_d5.png}}
    \caption{$\bar{Q} i \sigma \cdot G Q$ operator.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/2-3.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/hqe_d6_ws.png}}
    \caption{Dimension-six operator that generates WS effect.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/2-4.pdf}}
    %\raisebox{-0.5\height}{\includegraphics[width=\linewidth]{introduction/hqe_d6_pi.png}}
    \caption{Dimension-six operator that generates PI effect.}
  \end{subfigure}
  \caption{Feynman diagrams corresponding to local operators in Eq.~\ref{eq:hqe}.
  Reproduced from Ref.~\inlinecite{Bellini:1996ra}.}
  \label{fig:hqe}
\end{figure}
For baryons containing two charm quarks,
a global factor of two is multiplied,
which indicates that decays of two heavy quarks are treated independently.
To be more specific,
the decay width of charmed baryons receive contributions in the following manner
up to $O(1/m_Q^3)$
\begin{equation}
  \begin{aligned}
    \label{eq:}
    \Gamma^{(3)}(\Lc)      &= \Gamma^{(2)}(\Lc)      + \Gamma_{\mathrm{WS}}(\Lc) - \Gamma_{\mathrm{PI}-}(\Lc), \\
    \Gamma^{(3)}(\Xicz)    &= \Gamma^{(2)}(\Xicz)    + \Gamma_{\mathrm{WS}}(\Xicz) + \Gamma_{\mathrm{PI}+}(\Xicz), \\
    \Gamma^{(3)}(\Xicp)    &= \Gamma^{(2)}(\Xicp)    - \Gamma_{\mathrm{PI}-}(\Xicp) + \Gamma_{\mathrm{PI}+}(\Xicp), \\
    \Gamma^{(3)}(\Omegac)  &= \Gamma^{(2)}(\Omegac)  + \Gamma_{\mathrm{PI}+}(\Omegac), \\
    \Gamma^{(3)}(\Xiccp)   &= \Gamma^{(2)}(\Xiccp)   + \Gamma_{\mathrm{WS}}(\Xiccp), \\
    \Gamma^{(3)}(\Xiccpp)  &= \Gamma^{(2)}(\Xiccpp)  - \Gamma_{\mathrm{PI}-}(\Xiccpp), \\
    \Gamma^{(3)}(\Omegacc) &= \Gamma^{(2)}(\Omegacc) + \Gamma_{\mathrm{PI}+}(\Omegacc),\\
  \end{aligned}
\end{equation}
where $\mathrm{PI}-$ denotes the destructive Pauli interference,
$\mathrm{PI}+$ denotes the constructive Pauli interference,
and $\Gamma$ is the positive decay width.
Here only Cabbibo-favoured contributions are listed.

Coefficients $C_d$ encode short-distance interactions and phase-space factors
and can be calculated perturbatively.
Therefore, they are known with 
good precision~\inlinecite{Bigi:1992su,Blok:1992hw,Blok:1992he,Blok:1991st}.
The hadronic matrix elements can be determined by a) relating the matrix element
to other observables, or b) using other nonperturbative methods, such as
LQCD, QCD sum rules, HQET, and phenomenological models such as nonrelativistic quark model.
The matrix elements for dimension-six operators are still poorly known
in the charmed baryon sector.
While predictions for absolute lifetimes have relatively large uncertainties,
ratios of lifetimes have smaller theoretical uncertainties~\inlinecite{Lenz:2014jha}.

Within the HQE framework,
the ratio of charmed baryon lifetimes are calculated numerically 
up to $O(1/m_Q^3)$ and, as summarised in Table~\ref{tab:lifetime_th}.
In some references the absolute values are also available.
It can be seen in Table~\ref{tab:lifetime_th} that the expected lifetime hierarchy
of singly charmed baryons agrees with experimental measurements up to 2018.
It is also clear that although the qualitative feature is reproduced,
the quantitative estimates are still far from satisfactory.
\begin{table}[tb]
  \centering
  \caption{Theoretical calculations of lifetimes of singly charmed baryons.
  As a comparison, the lifetime ratio in each prediction is also shown.
  The reference lifetime, $\tau_0$, is different between predictions.}
  \label{tab:lifetime_th}
  \begin{tabular}{l c c c c}
    \toprule
     & \Omegac & \Xicz & \Lc & \Xicp \\
    \midrule
    Ref.~\inlinecite{Blok:1991st,Bellini:1996ra,Bianco:2003vb} 
    & $\tau_{0}$ & $1.5 \tau_{0}$ & $3.2 \tau_{0}$ & $4.0 \tau_{0}$ \\
    Ref.~\inlinecite{Cheng:1997xba} 
    & $\tau_{0}$ & $1.2 \tau_{0}$ & $1.8 \tau_{0}$ & $2.0 \tau_{0}$ \\
    Ref.~\inlinecite{Cheng:2015iom}
    & $\tau_{0}$ & $1.1 \tau_{0}$ & $1.5 \tau_{0}$ & $2.2 \tau_{0}$ \\
    PDG18~\inlinecite{PDG2018} 
    & $\tau_{0}$ & $(1.6\pm0.3)\tau_{0}$ & $(2.9\pm0.5)\tau_{0}$ & $(6.4\pm1.2)\tau_{0}$ \\
    \midrule
    Ref.~\inlinecite{Cheng:1997xba} [fs]
    & 204 & 249 & 360 & 402 \\
    Ref.~\inlinecite{Cheng:2015iom} [fs]
    & 171 & 193 & 264 & 368 \\
    PDG18~\inlinecite{PDG2018} [fs]
    & $69\pm12$ & $112\pm11$ & $200\pm6$ & $442\pm26$ \\
    \bottomrule
  \end{tabular}
\end{table}
The lifetimes of doubly charmed baryons are calculated and 
summarised in Table~\ref{tab:lifetime_th_dcb}.
While predictions of absolute values of lifetimes vary significantly,
the lifetime hierarchy seems to be in a good agreement.
\begin{table}[tb]
  \centering
  \caption{Theoretical calculations of lifetimes of doubly charmed baryons.
  As a comparison, the lifetime ratio in each prediction is also shown.
  The reference lifetime, $\tau_0$, is different between predictions.}
  \label{tab:lifetime_th_dcb}
  \begin{tabular}{l c c c}
    \toprule
    & \Xiccp & \Omegacc & \Xiccpp \\
    \midrule
    Ref.~\inlinecite{Kiselev:1998sy}      & $\tau_0$ &  & $3.9\tau_0$ \\ 
    Ref.~\inlinecite{Kiselev:2001fw}      & $\tau_0$ & $1.7\tau_0$ & $2.9\tau_0$ \\
    Ref.~\inlinecite{Guberina:1999mx}     & $\tau_0$ & $1.1\tau_0$ & $7.0\tau_0$ \\
    Ref.~\inlinecite{Onishchenko:2000yp}  & $\tau_0$ & $1.4\tau_0$ & $2.3\tau_0$ \\
    Ref.~\inlinecite{Chang:2007xa}        & $\tau_0$ & $0.8\tau_0$ & $2.7\tau_0$ \\
    Ref.~\inlinecite{Karliner:2014gca}    & $\tau_0$ & & $3.5\tau_0$ \\
    Ref.~\inlinecite{Cheng:2018mwu}       & $\tau_0$ & $1.4\tau_0$ & $9.0\tau_0$ \\
    Ref.~\inlinecite{Likhoded:2018tfc}    & $\tau_0$ & $1.4\tau_0$ & $2.2\tau_0$ \\
    \midrule
    Ref.~\inlinecite{Kiselev:1998sy} [fs]     & 110 & & 430 \\
    Ref.~\inlinecite{Kiselev:2001fw} [fs]     & 160 & 270 & 460 \\
    Ref.~\inlinecite{Guberina:1999mx} [fs]    & 220 & 250 & 1550 \\
    Ref.~\inlinecite{Onishchenko:2000yp} [fs] & 200 & 270 & 450 \\
    Ref.~\inlinecite{Chang:2007xa} [fs]       & 250 & 210 & 670 \\
    Ref.~\inlinecite{Karliner:2014gca} [fs]   & 530 & & 1850 \\
    Ref.~\inlinecite{Cheng:2018mwu} [fs]      & 57 & 78 & 520 \\
    Ref.~\inlinecite{Likhoded:2018tfc} [fs]   & 200 & 270 & 440 \\
    \bottomrule
  \end{tabular}
\end{table}

In summary, large theoretical uncertainties persist
in the prediction of charmed baryon lifetimes,
due to both the poorly known matrix elements of dimension-six operators
and the slow convergence of HQE in the charm baryon sector.
Accurate measurements of lifetimes of singly charmed baryons
and experimental input to lifetimes of doubly charmed baryons
are crucial to further improve our understanding of their decay dynamics.











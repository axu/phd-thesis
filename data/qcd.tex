% An introduction to QCD

Quantum Chromodynamics (QCD) is the theory of strong interaction in the SM.
Naively speaking, the physical content of the theory is that
quarks interact with gluons which also interact among themselves.
The QCD Lagrangian in Eq.~\ref{eq:sm} is given by
\begin{equation}
  \mathcal{L}_{\mathrm{QCD}} = \sum_{f} \bar{\psi}_{f, a}
    \left(i \gamma^{\mu} \partial_{\mu} \delta_{a b} -
    g_{s} \gamma^{\mu} t_{a b}^{C} \mathcal{A}_{\mu}^{C}-m_{f} \delta_{a b}\right) 
    \psi_{f, b} -
    \tfrac{1}{4} F_{\mu \nu}^{A} F^{A, \mu \nu},
\end{equation}
where repeated indices are summed over.
Fermion field $\bar{\psi}_{f, a}$ is the Dirac spinor of the quark field
for a quark of flavour $f$ and mass $m_f$,
with a color index $a$ which runs from 1 to $N_c = 3$.
Quarks are in the fundamental representation of $\suthree_C$ group,
denoted by $\mathbf{3}$.
The $\bar{\psi}$ is a shorthand notation for $\psi^{\dagger} \gamma^0$.

Boson field $\mathcal{A}_{\mu}^{C}$ is the gluon field,
where $C$ runs from 1 to $N_c^2 - 1 = 8$.
Matrix $t_{a b}^{C}$ is one of the eight generators of $\suthree_C$ group.
It can thus be seen that a gluon's interaction with a quark
rotates the quark's color in $\suthree_C$ space.
Dimensionless quantity $g_s$, or $\as \equiv \tfrac{g_s^2}{4\pi}$,
is the strong coupling constant
and the only fundamental parameter of QCD.
Quark masses also enter the QCD Lagrangian as free parameters.
However, they have an electroweak origin and are flavour-dependent,
and therefore are not counted as parameters of QCD.

Field tensor $F_{\mu \nu}^{A}$ is defined as
\begin{equation}
  \label{eq:gluon_field}
  F_{\mu \nu}^{A}=\partial_{\mu} \mathcal{A}_{\nu}^{A}-\partial_{\nu} \mathcal{A}_{\mu}^{A}-g_{s} f_{A B C} \mathcal{A}_{\mu}^{B} \mathcal{A}_{\nu}^{C},
\end{equation}
where $f_{A B C}$ is the structure constant of the $\suthree_C$ group.
From the QCD Lagrangian, one can immediately see that
there exist quark-gluon vertex, three-gluon vertex, and four-gluon vertex
in QCD interactions.

QCD is believed to be the underlying theory governing the properties of hadrons.
It displays several features that are important for the understanding of hadron structures.
We illustrate these features below.

\paragraph{Running coupling constant.}%
\label{par:running_coupling_constant_and_quark_mass}
QCD is a renormalisable theory, in the sense that only a finite number of Feynman diagrams,
such as quark self-energy and vacuum polarization, are divergent.
The process of renormalization, under certain renormalisation scheme,
removes these divergences and obtains finite amplitudes and consequently finite predictions
for the observables.
The renormalisation process introduces scale-dependence $\mu$ to
the renormalized coupling constant and quark masses,
which is governed by the renormalisation group equations.
In the modified Minimal Subtraction scheme \mms,
the solution to the renormalisation group equation of coupling constant 
up to leading order is~\inlinecite{PDG2020}
\begin{equation}
  \as^{(f)}(\mu) = \frac{4\pi}{\beta_0^{(f)} \ln \left( \mu^2/\Lambda^{(f)2}_{\mathrm{QCD}}\right)},
\end{equation}
where $\beta_0^{(f)} = \frac{11 N_c - 2 f}{3}$ and $f$ is the number of effective flavours
defined by
\begin{equation}
  f = 
  \begin{cases}
    3  & \mu \leq m_c, \\
    4  & m_c \leq \mu \leq m_b, \\
    5  & m_b \leq \mu \leq m_t, \\
    6  & m_t \leq \mu. \\
  \end{cases}
\end{equation}
The continuity of \as gives the boundary conditions of the piecewise function.
It should be noticed that the renormalisation group equation
is only valid in the perturbative QCD regime and breaks down at low-energy scale around \lqcd.
The numerical values of \as, measured at various values of $\mu$ through different processes
are shown in Fig.~\ref{fig:as}~\inlinecite{PDG2020}.
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{introduction/alphas-v-Q-2021}
  \caption{Measurements of \as as a function of energy scale $Q$~\inlinecite{PDG2020}.}
  \label{fig:as}
\end{figure}

\paragraph{Asymptotic freedom and confinement.}%
\label{par:asymptotic_freedom_and_confinement}
The dependence of \as on the energy scale exhibits the feature of \emph{asymptotic freedom}.
At high energy scale, the coupling constant is small and decrease with energy.
It also displays \emph{confinement}, the converse notion that
the coupling becomes strong at low-energy scales.
This property explains qualitatively 
why quarks are confined in hadrons and colored states are not observed.
At this regime, 
nonpertaburtive methods or phenomenological models 
are needed to make quantitative predictions.

\paragraph{Running quark mass.}%
\label{par:running_quark_mass}
The solution to the renormalisation group equation of quark mass
up to leading order is
\begin{equation}
  m(\mu) = m(\mu_0) \left[ \frac{\as(\mu)}{\as(\mu_0)} \right] ^{\frac{\gamma_m^0}{2\beta_0}},
\end{equation}
where $\gamma_m^0 = 6 C_F$ and $C_F = \frac{N_c^2 - 1}{2 N_c}$.
The dependence of quark mass on the momentum is illustrated 
in Fig.~\ref{fig:qmass}~\inlinecite{Eichmann:2016yit}~\footnote{Natural
units with $\hbar = c = 1$ are used throughout this thesis.}.
%dimension and unit in Appendix~\ref{sec:app_dimension}.
Quark masses generated at low momentum transfers are obtained from nonpertaburtive methods
which will be discussed in next section.
The continuous transition of quark masses from perturbative to nonperturbative regime
provides insight into the relationship between ``current quark'' in the QCD Lagrangian
and the ``constituent quark'' in the quark model.
\begin{figure}[!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{introduction/quark_mass.png}
  \caption{Quark mass function with solutions of Dyson-Schwinger equations
    in infrared and logarithmic running 
    in the ultraviolet from perturbation theory~\inlinecite{Eichmann:2016yit}.}
  \label{fig:qmass}
\end{figure}


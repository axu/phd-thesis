% review of exclusive weak decay

Inclusive weak decays, or equivalently lifetimes, of doubly charmed baryons are discussed
within the framework of Heavy Quark Expansion in Sec.~\ref{sub:lifetime_th}.
In summary, the lifetime hierarchy of weakly decaying doubly charmed baryons is
predicted in many theoretical calculations to be
\begin{equation}
  \tau_{\Xiccp} < \tau_{\Omegacc} < \tau_{\Xiccpp},
\end{equation}
while predictions of absolute values carry large uncertainties.

In this section, we discuss exclusive decays of doubly charmed baryons
and focus on Cabbibo-favoured (CF) decay modes,
which can provide practical guidance for experimental searches
at present and in the near future.
Cabbibo-suppressed and flavour-changing-neutral-current processes
are studied in the literature~\inlinecite{Xing:2018lre},
although far beyond the reach of experiments currently.
Quantitative results are presented in terms of partial widths,
due to limited knowledge of lifetimes of doubly charmed baryons.
It is straightforward to translate these partial widths into branching fractions
once solid information on lifetimes is available.

Back in 80s Bjorken has anticipated the decay modes and branching fractions
of doubly charmed baryons
via an ``unsophisticated, common-sense'' approach~\inlinecite{Bjorken:1986kfa}.
The general patterns and order of magnitudes are still valid 
when compared with predictions made with more involved methods in recent years.
It is still challenging to perform a solid analysis based on QCD
since one has to address a three-body problem with two charm quarks involved.
Therefore, variants of quark models are utilized in quantitative calculations.
Efforts are also made to calculate form factors 
with QCD sum rules~\inlinecite{Onishchenko:2000yp,Hu:2019bqj,Shi:2019hbf}
and to build up the HQET for weak decays of doubly charmed baryons~\inlinecite{Shi:2020qde}
very recently.
For a solid analysis based on QCD, 
one has to take into account all three quarks
and nonperturbative contributions, which is very complicated and far beyond our capability now.


\paragraph{Semileptonic decays.}%
\label{par:semileptonic_decays_}

The semileptonic decays of doubly charmed baryons are induced by
the quark-level transition $c \to s l^+ \nu_l$.
The lepton pair can be $\ep\neue$ and $\mup\neum$.
The hadronic decay product can be either \jph and \jpth singly charmed baryons.
Theoretical predictions of semileptonic partial decay widths 
are shown in Table~\ref{tab:decay_sl}.
Several comments are appropriate here:
\begin{itemize}
  \item Predictions made with different models are in general consistent.
  \item The partial decay widths of semileptonic decays for $\half\to\half$ transition
    are of order $10^{-13}\gev$.
  \item The partial decay widths of semileptonic decays for $\half\to\threehalf$ transition
    is an order of magnitude smaller than that of the $\half\to\threehalf$ transition.
  \item The partial decay width 
    $\Gamma(\Xiccpp\to\Xi^{(\prime)+}_c l^+ \neu_l) \approx \Gamma(\Xiccp\to\Xi^{(\prime)0}_c l^+ \neu_l)$
    due to \suthree flavour symmetry.
  \item The above equation dose not imply equal branching fractions,
    since the total widths of \Xiccp and \Xiccpp are quite different.
\end{itemize}
\begin{table}[tb]
  \centering
  \caption{Partial decay widths of semileptonic decays in unit of $10^{-14}\gev$.}
  \label{tab:decay_sl}
  \begin{tabular}{l r r r}
    \toprule
    Channels & Ref.~\inlinecite{Wang:2017mqp} & Ref.~\inlinecite{Gutsche:2019iac} 
    & Ref.~\inlinecite{Hu:2020mxk} \\
    \midrule
    $\Xiccpp\to\Xicp l^+ \neu_l$      & 11.5 & 7.0  & 8.7 \\
    $\Xiccpp\to\PXi^{\prime+}_c l^+ \neu_l$ & 12.8 & 9.7  & 14.3 \\
    $\Xiccpp\to\PXi^{*+}_c l^+ \neu_l$ & 1.6  & 2.2 & 1.7 \\
    \midrule
    $\Xiccp\to\Xicz l^+ \neu_l$       & 11.4 & 6.9  & 8.6 \\
    $\Xiccp\to\PXi^{\prime0}_c l^+ \neu_l$  & 12.7 & 9.7  & 14.1 \\
    $\Xiccp\to\PXi^{*}_c l^+ \neu_l$   & 1.6  & 2.2 & 1.7 \\
    \midrule
    $\Omegacc\to\Omegac l^+ \neu_l$         & 25.5 & 18.2 & 28.0 \\
    $\Omegacc\to\POmega_c^{*0} l^+ \neu_l$   & 3.1  & 4.0 & 3.5 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Nonleptonic decays.}%
\label{par:nonleptonic_decays_}

We concentrate on the discussion of two-body transitions
of doubly charmed baryons.
Genuine multibody decays are studied with \suthree flavour symmetry 
in Ref.~\inlinecite{Shi:2017dto,Li:2021rfj}.
According to the spin-parity of the decay products,
two-body nonleptonic decays can be classified into four classes:
\begin{equation}
  \begin{aligned}
    \half \to \half + 0^-, \\
    \half \to \half + 1^-, \\
    \half \to \threehalf + 0^-, \\
    \half \to \threehalf + 1^-,
  \end{aligned}
\end{equation}
where $0^-$ refers to pseudoscalar mesons, and $1^-$ denotes vector mesons.

Nonleptonic two-body transitions are also categorised 
according to their color-flavour topologies,
as shown in Fig.~\ref{fig:decay_topo}~\inlinecite{Gutsche:2019iac}.
Type Ia and Ib refer to diagrams due to the external and internal $W$-emission, respectively.
Type II and III refer to different $W$-exchange diagrams.
Contributions from the external $W$-emission are factorizable
and have been studied intensively in the literature,
with consistent theoretical predictions.
Nonfactorizable contributions from internal $W$-emission and $W$-exchange diagrams 
play an essential role and cannot be neglected.
There exist three different approaches for tackling the nonfactorizable contributions
in doubly charmed baryon decays: 
the covariant confined quark model~\inlinecite{Gutsche:2019iac,Ivanov:2020xmw},
final-state interactions~\inlinecite{Jiang:2018oak,Han:2021azw,Li:2020qrh}, 
and the pole model in conjunction with current algebra~\inlinecite{Cheng:2020wmk}.
Table~\ref{tab:decay_topo} summarises all CF nonleptonic two-body decays
of doubly charmed baryons, along with the topological diagrams 
that contribute to these decays.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.9\linewidth]{introduction/decay_topology.png}
  \caption{Color-flavour topologies of nonleptonic weak decays~\inlinecite{Gutsche:2019iac}.}
  \label{fig:decay_topo}
\end{figure}
\begin{table}[tb]
  \centering
  \caption{CF nonleptonic two-body decays of doubly charmed baryons, 
  along with the topological diagrams that 
  contribute to these decays~\inlinecite{Gutsche:2019iac}.}
  \label{tab:decay_topo}
  \begin{tabular}{l c c c c c}
    \toprule
    Channels & Ia & Ib & IIa & IIb & III \\
    \midrule
    $\Xiccpp\to\PXi^{(\prime,*)+}_c + \pip (\rhop)$                 & $\checkmark$ & & & $\checkmark$ & \\
    $\Xiccpp\to\PSigma^{(*)++}_c + \bar{K}^{(*)0}$                  & & $\checkmark$ & & & \\
    $\Xiccpp\to\PSigma^{(*)+} + D^{(*)+}$                           & & & & $\checkmark$ & \\
    \midrule
    $\Xiccp\to\PXi^{(\prime,*)0}_c + \pip (\rhop)$                  & $\checkmark$ & & $\checkmark$ & & \\
    $\Xiccp\to\Lc (\Sigma^{(*)+}_c) + \bar{K}^{(*)0}$              & & $\checkmark$ & $\checkmark$ & & \\
    $\Xiccp\to\PSigma^{(*)++}_c + K^{(*)-}$                         & & & $\checkmark$ & & \\
    $\Xiccp\to\PXi^{(\prime,*)+}_c + \piz (\rhoz)$                  & & & $\checkmark$ & $\checkmark$ & \\
    $\Xiccp\to\PXi^{(\prime,*)+}_c + \eta (\eta^{\prime})$          & & & $\checkmark$ & $\checkmark$ & \\
    $\Xiccp\to\POmega^{(*)0}_c + K^{(*)+}$                          & & & $\checkmark$ & & \\
    $\Xiccp\to\Lz (\Sigma^{(*)0}) + D^{(*)+}$                      & & & & $\checkmark$ & $\checkmark$ \\
    $\Xiccp\to\PSigma^{(*)+} + D^{(*)0}$                            & & & & & $\checkmark$ \\
    $\Xiccp\to\PXi^{(*)0} + D_s^{(*)+}$                             & & & & & $\checkmark$ \\
    \midrule
    $\Omegacc\to\POmega^{(*)+}_c + \pip (\rhop)$                    & $\checkmark$ & & & & \\
    $\Omegacc\to\PXi^{(\prime,*)+}_c + \bar{K}^{(*)0}$              & & $\checkmark$ & & $\checkmark$ & \\
    $\Omegacc\to\PXi^{(*)0} + D^{(*)+}$                           & & & & $\checkmark$ & \\
    \midrule
  \end{tabular}
\end{table}

Numerical predictions of nonleptonic partial decay widths 
are shown in Table~\ref{tab:decay_nl_xiccpp},
Table~\ref{tab:decay_nl_xiccp},
and Table~\ref{tab:decay_nl_omegacc} for
\Xiccpp, \Xiccp, and \Omegacc baryon, respectively.
Differential widths and angular distributions are also discussed in Ref.~\inlinecite{Gutsche:2019iac}.
Several comments are appropriate here:
\begin{itemize}
  \item Decay modes that are dominated by external $W$-emission diagrams
    have large partial widths. Predictions of these modes agree well between different methods.
  \item Decay modes that receive contributions from type II and III diagrams
    can have sizable partial widths in many cases.
  \item Unstable two-body decay products can decay strongly or radiatively,
    which leads to multiple final states for experimental detection.
  \item The observed $\Xiccpp\to\Lc\Km\pip\pip$ decay 
    by LHCb experiment~\inlinecite{LHCb-PAPER-2017-018}
    is predicted and interpreted as two-body $\Xiccpp\to\Sigma^{++}_c\bar{K}^{*0}$,
    followed by $\Sigma^{++}\to\Lc\pip$ and $\bar{K}^{*0}\to\Km\pip$ 
    decay~\inlinecite{Yu:2017zst,Gutsche:2017hux}.
    The observed $\Xiccpp\to\Xicp\pip$ and partially reconstructed 
    $\Xiccpp\to\Xi^{\prime+}_c\pip$ and $\Xiccpp\to\Xicp\rhop$ 
    decay~\inlinecite{LHCb-PAPER-2018-026,LHCb-PAPER-2019-037}
    are consistent with theoretical predictions.
\end{itemize}
\begin{table}[tb]
  \centering
  \caption{Partial decay widths of nonleptonic decays of \Xiccpp baryon in unit of $10^{-14}\gev$.}
  \label{tab:decay_nl_xiccpp}
  \begin{tabular}{l r r r r r r r}
    \toprule
    Channels & Ref.~\inlinecite{Wang:2017mqp} & Ref.~\inlinecite{Jiang:2018oak} 
    & Ref.~\inlinecite{Gutsche:2019iac} & Ref.~\inlinecite{Ivanov:2020xmw} 
    & Ref.~\inlinecite{Cheng:2020wmk} & Ref.~\inlinecite{Han:2021azw} 
    & Ref.~\inlinecite{Li:2020qrh} \\
    \midrule
    $\Xiccpp\to\Xicp\pip$               & 15.7 &       &      & 1.8  & 1.8  & 18.3 & \\
    $\Xiccpp\to\Xicp\rhop$              & 30.3 & 41.1  &      & 6.3  & 12.0 &      & \\
    $\Xiccpp\to\PXi^{\prime+}_c\pip$     & 11.0 &       & 7.8  & 8.2  &      & 12.1 & \\
    $\Xiccpp\to\PXi^{\prime+}_c\rhop$    & 41.2 & 42.5  & 41.4 & 42.7 &      &      & \\
    $\Xiccpp\to\PSigma^{++}_c\bar{K}^{0}$&      &       & 3.2  &      & 3.5  & 0.5  & \\
    $\Xiccpp\to\PSigma^{++}_c\bar{K}^{*0}$ &    & 13.9  & 14.4 &      &      &      & \\
    $\Xiccpp\to\PXi^{*+}_c\pip$          & 2.2  &       & 1.6  &      &      &      & \\
    $\Xiccpp\to\PXi^{*+}_c\rhop$         & 4.7  &       & 11.5 &      &      &      & \\
    $\Xiccpp\to\PSigma^{*++}_c\bar{K}^{0}$&     &       & 0.6  &      &      &      & \\
    $\Xiccpp\to\PSigma^{*++}_c\bar{K}^{*0}$&    &       & 4.2  &      &      &      & \\
    $\Xiccpp\to\PSigma^{+} \Dp$          &      &       &      &      &      &      & 0.8 \\
    $\Xiccpp\to\PSigma^{+} \D^{*+}$      &      &       &      &      &      &      & 4.1 \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Partial decay widths of nonleptonic decays of \Xiccp baryon in unit of $10^{-14}\gev$.}
  \label{tab:decay_nl_xiccp}
  \begin{tabular}{l r r r r r}
    \toprule
    Channels & Ref.~\inlinecite{Wang:2017mqp} & Ref.~\inlinecite{Jiang:2018oak}
    %& Ref.~\inlinecite{Gutsche:2019iac} & Ref.~\inlinecite{Ivanov:2020xmw}
    & Ref.~\inlinecite{Cheng:2020wmk} & Ref.~\inlinecite{Han:2021azw}
    & Ref.~\inlinecite{Li:2020qrh} \\
    \midrule
    $\Xiccp\to\Xicz\pip$                & 15.6 &       & 56.2 & 20.4 & \\
    $\Xiccp\to\Xicz\rhop$               & 29.9 & 38.3  &      &      & \\
    $\Xiccp\to\PXi^{\prime0}_c\pip$      & 10.9 &       & 22.7 & 13.4 & \\
    $\Xiccp\to\PXi^{\prime0}_c\rhop$     & 41.0 & 47.7  &      &      & \\
    $\Xiccp\to\PXi^{+}_c\piz$            &      &       & 34.8 & 4.8  & \\
    $\Xiccp\to\PXi^{+}_c\rhoz$           &      & 18.2  &      &      & \\
    $\Xiccp\to\PXi^{+}_c\eta$            &      &       & 61.1 &      & \\
    $\Xiccp\to\PXi^{\prime+}_c\piz$      &      &       & 2.5  & 0.5  & \\
    $\Xiccp\to\PXi^{\prime+}_c\rhoz$     &      & 6.1   &      &      & \\
    $\Xiccp\to\PXi^{\prime+}_c\eta$      &      &       & 0.7  & 0.8  & \\
    $\Xiccp\to\Lc\bar{K}^{0}$           &      &       & 4.5  & 0.5  & \\
    $\Xiccp\to\Lc\bar{K}^{*0}$          &      & 7.1   &      &      & \\
    $\Xiccp\to\PSigma_c^{+}\bar{K}^{0}$  &      &       & 5.6  & 2.2  & \\
    $\Xiccp\to\PSigma_c^{+}\bar{K}^{*0}$ &      & 8.4   &      &      & \\
    $\Xiccp\to\PXi^{*0}_c\pip$           & 2.2  &       &      &      & \\
    $\Xiccp\to\PXi^{*0}_c\rhop$          & 4.7  &       &      &      & \\
    $\Xiccp\to\Lz\Dp$                   &      &       &      &      & 0.6 \\
    $\Xiccp\to\Lz D^{*+}$               &      &       &      &      & 18.2 \\
    $\Xiccp\to\PSigma^0\Dp$              &      &       &      &      & 0.6 \\
    $\Xiccp\to\PSigma^0 D^{*+}$          &      &       &      &      & 21.7 \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Partial decay widths of nonleptonic decays of \Omegacc baryon in unit of $10^{-14}\gev$.}
  \label{tab:decay_nl_omegacc}
  \begin{tabular}{l r r r r r r r}
    \toprule
    Channels & Ref.~\inlinecite{Wang:2017mqp} & Ref.~\inlinecite{Jiang:2018oak}
    & Ref.~\inlinecite{Gutsche:2019iac} & Ref.~\inlinecite{Ivanov:2020xmw}
    & Ref.~\inlinecite{Cheng:2020wmk} & Ref.~\inlinecite{Han:2021azw}
    & Ref.~\inlinecite{Li:2020qrh} \\
    \midrule
    $\Omegacc\to\Omegac\pip$            & 21.8 &       & 15.8 &      & 20.4 & 22.3 & \\
    $\Omegacc\to\Omegac\rhop$           & 82.7 & 87.5  & 82.9 &      &      &      & \\
    $\Omegacc\to\Xicp\bar{K}^{0}$       &      &       &      & 9.5  & 5.9  & 0.8  & \\
    $\Omegacc\to\Xicp\bar{K}^{*0}$      &      & 13.8  &      & 6.2  &      &      & \\
    $\Omegacc\to\PXi_c^{\prime+}\bar{K}^{0}$ &  &       & 1.7  & 1.5  & 1.5  & 1.2  & \\
    $\Omegacc\to\PXi_c^{\prime+}\bar{K}^{*0}$&  & 26.4  & 7.5  & 7.4  &      &      & \\
    $\Omegacc\to\POmega_c^{*0}\pip$      & 4.3  &       & 3.1  &      &      &      & \\
    $\Omegacc\to\POmega_c^{*0}\rhop$     & 9.5  &       & 22.3 &      &      &      & \\
    $\Omegacc\to\PXi_c^{*+}\bar{K}^{0}$  &      &       & 0.3  &      &      &      & \\
    $\Omegacc\to\PXi_c^{*+}\bar{K}^{*0}$ &      &       & 2.1  &      &      &      & \\
    $\Omegacc\to\PXi^0 \Dp$              &      &       &      &      &      &      & 1.9 \\
    $\Omegacc\to\PXi^0 \D^{*+}$          &      &       &      &      &      &      & 5.0 \\
    \bottomrule
  \end{tabular}
\end{table}

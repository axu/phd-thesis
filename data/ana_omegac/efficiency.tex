\section{Estimation of efficiency}%
\label{sec:efficiency_estimation}

The efficiency of event selection is defined as the ratio
of the number of events after event selection to that before event selection.
The selection efficiency is decay-time dependent due to
requirements of displaced tracks in the event selection.
Consequently, the exponential decay-time distribution is distorted
and the variation of efficiency with decay-time, referred to as the decay-time acceptance,
has to be considered in the decay-time fit.

Simulated samples are used to estimate the reconstruction and selection efficiency.
There exist known mismodellings of simulation.
Therefore, several corrections are applied to simulated samples,
including those to correct for tracking efficiency,
PID efficiency, and
$h$TOS efficiency~\ref{sub:l0_efficiency_correction}.
The phase-space distributions and the kinematic distributions
are weighted to match those in data as they are correlated to efficiency mentioned above.
These corrections are implemented as per-event weight
\begin{equation}
  w = \prod_{i} w_i ,
\end{equation}
where $w$ is the total per-event weight and
$w_i$ is the weight from each individual contribution.

\subsection{Tracking efficiency correction}%
\label{sub:tracking_efficiency_correction}

There is known small discrepancy between data and simulation 
in the tracking reconstruction efficiency.
This is taken into account using the efficiency ratio of simulation to data
in kinematic bins, which is obtained with the tag-and-probe method.
The per-event correction factor is calculated as
\begin{equation}
  w_{\mathrm{trk}} = \prod_{i=1}^4 r_{h_i}(p_i,\eta_i)
\end{equation}
for each candidate, 
where $r_{h_i}(p_i,\eta_i)$ is the correction factor for each final-state track.
This correction is at the order of 1\% or smaller.
%The correction table for 2016 data-taking year
%is shown in Fig.~\ref{fig:tracking_correction_omegac} and used for 2016--2018 simulations.
%\begin{figure}[htb]
%  \centering
%  \includegraphics[width=0.60\linewidth]{ana_omegac/calibration/tracking/map.pdf}
%  \caption{The 2016 tracking correction table.}
%  \label{fig:tracking_correction_omegac}
%\end{figure}

\subsection{PID efficiency correction}%
\label{sub:pid_efficiency_correction}

The PIDCalib package is used to determine the efficiency of PID requirements,
as discussed in Sec.~\ref{sub:performance_of_the_lhcb_detector}.
The per-event efficiency is calculated as
\begin{equation}
  w_{\mathrm{PID}} = \prod_{i}^4 \varepsilon_{h_i}(p_i,\eta_i),
\end{equation}
where $\varepsilon_{h_i}(p_i,\eta_i)$ is the PID efficiency for each final-state track,
as a function of the momentum and rapidity.
The calibration sample used for kaons and pions is 
the $\Dstarp \to \Dz (\to \Km \pip) \pip $ sample,
and that used for protons is the $\Lz \to \proton \pion$ sample.
Efficiency of PID requirements on each final-state track is determined in $p$ and $\eta$ bins.
the binning scheme as well as the efficiency in each bin for MagDown polarity
is shown in Fig.~\ref{fig:pid} as an illustration.
The total PID efficiency of a candidate 
is the product of efficiencies of each final-state track.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/P_16_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/K_16_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/Pi_16_Down.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/P_17_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/K_17_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/Pi_17_Down.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/P_18_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/K_18_Down.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/PID/Pi_18_Down.pdf}\\
  \caption{PID efficiency as a function of momentum $p$ and rapidity $\eta$
  for (left) proton, (middle) kaon and (right) pion
  and in (top) 2016, (middle) 2017 and (bottom) 2018 data of the MagDown polarity.}
  \label{fig:pid}
\end{figure}

\subsection{L0 efficiency correction}%
\label{sub:l0_efficiency_correction}

It is known from other LHCb analyses that
simulated samples dose not to model the L0 efficiency precisely
due to the imprecise description of processes in the ECAL subdetector.
Instead of calculating the $h$TOS efficiency directly from simulation,
we parametrise $h$TOS efficiency 
as a function of the transverse energy deposited in the HCAL \texttt{HCAL\_realET} 
of the final-state tracks.
The efficiency of each fina-state track at different transverse energy $E_T$
is then determined from calibration data with the TISTOS method,
as described in~\inlinecite{MartinSanchez:1407893}.
Efficiency tables for 2016-2018 data
are obtained with $\Lb\to\Lc\mu\nu X$ decays
for different particles, HCAL regions, and charges,
and are visualized in Fig~\ref{fig:l0_eff_table}.
%~\footnote
%{\tiny https://indico.cern.ch/event/861589/contributions/3629567/attachments/1943458/3223600/L0HadronAsym\_12Nov2019\_Ferrari.pdf},
%as suggested in the The Calorimeter Objects Tools Group twiki page~\footnote
%{\tiny https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/CalorimeterObjectsToolsGroupDOC\#L0\_Hadron\_trigger\_efficiencies}.
The per-event correction factor is calculated as
\begin{equation}
  w_{h\mathrm{TOS}} = 1- \prod_{i}^4 (1-\varepsilon_{h_i}(\texttt{HCAL\_realET})),
\end{equation}
where $\varepsilon_{h_i}(\texttt{HCAL\_realET})$ is the $h$TOS efficiency
for each final-state track,
determined as a function of \texttt{HCAL\_realET}
according to the particle ID and the region in the HCAL.
%The L0 $h$TOS efficiency is determined as a function of \texttt{HCAL\_realET} for each track.
%The L0 $h$TOS efficiency for a candidate is calculated as
%\begin{equation}
%  \varepsilon_{h\mathrm{TOS}} = 1 -
%  (1-\varepsilon_{h_1})\cdot(1-\varepsilon_{h_2})\cdot
%  (1-\varepsilon_{h_3})\cdot(1-\varepsilon_{h_4}),
%\end{equation}
%where $\varepsilon_{h_i}$ is the L0 $h$TOS efficiency for the $i$th final state track.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/L0HadronTOS/p.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/L0HadronTOS/K.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/L0HadronTOS/pi.pdf}
  \caption{L0 hadron TOS efficiency for (left) \proton, (middle) \kaon and (right) \pion.}
  \label{fig:l0_eff_table}
\end{figure}

The higher order effect on the calculation of transverse energy
due to the overlapping of clusters of final-state tracks is also estimated.
The transverse energy deposited in the HCAL \texttt{HCAL\_realET}
is corrected as follows before it is used to get the $h$TOS efficiency per track.
For the closest cluster pair $(E_{T1},E_{T2})$ of each decay,
the corrected transverse energy $(E_{T1}^{\mathrm{corr}},E_{T2}^{\mathrm{corr}})$
is calculated as
\begin{equation}
  \begin{cases}
    (E_{T1}+E_{T2},E_{T1}+E_{T2}), 
     \;\; \mathrm{for} \;\; |\Delta x|<0.5d,\,|\Delta y|<0.5d \\
    (E_{T1}+0.5E_{T2},0.5E_{T1}+E_{T2}),
     \;\; \mathrm{for} \;\; |\Delta x|<0.5d,\,0.5d<|\Delta y|<1.0d \\
    (E_{T1}+0.5E_{T2},0.5E_{T1}+E_{T2}),
     \;\; \mathrm{for} \;\; 0.5d<|\Delta x|<1.0d,\,|\Delta y|<0.5d \\
    (E_{T1}+0.25E_{T2},0.25E_{T1}+E_{T2}),
     \;\; \mathrm{for} \;\; 0.5d<|\Delta x|<1.0d,\,0.5d<|\Delta y|<1.0d \\
  \end{cases}
\end{equation}
where $\Delta x$ ($\Delta y$) is the distance in the $x$ ($y$) direction
of the closest cluster pair in the HCAL plane, 
$d$ is the tile size of the HCAL.
The fitted lifetime with or without considering the effect 
due to the overlapping of final-state tracks 
is found to be at sub-fs level and negligible.
Therefore, we do not include this correction in the default procedure.


\subsection{Phasespace correction}%
\label{sub:phasespace_correction}

Simulation samples are generated with the phasespace decay model
and resonant structures in the four-body decay are not considered.
This may lead to a mismodelling of the distributions of kinematic variables 
of final-state tracks,
and hence the efficiency as a function of decay time.
To correct the phasespace distributions,
five Cabibbo-Maksymowicz (CM) variables are used.
For \Dz control mode,
they are calculated as
\begin{itemize}
  \item $M(\Kp\Km)$, the invariant mass of the $\Kp\Km$ system; 
  \item $M(\pip\pim)$, the invariant mass of the $\pip\pim$ system;
  \item $\cos(\theta^{\Kp\Km}_{\Kp})$,
    the cosine of the angle between the direction of the \Dz
    and that of one of the kaons in the rest frame of the two kaons;
  \item $\cos(\theta^{\pip\pim}_{\pip})$,
    the cosine of the angle between the direction of the \Dz
    and that of one of the pions in the rest frame of the two pions;
  \item $\phi^{\Kp\Km}_{\pip\pim}$,
    the angle in the \Dz rest frame between the plane
    defined by the directions of the two kaons and
    the plane defined by the directions of the two pions
\end{itemize}
which are sufficient to describe the whole decay structure.
CM variables for signal modes are defined in a similar way.
Background subtracted data distributions of CM variables
are obtained from the invariant-mass fit
and are used to reweight the simulation.
This indicates that the signal distributions in data include contributions
from both prompt and secondary signals.
This approximation is checked by comparing the phasespace distributions
of prompt and secondary signals in simulation, as shown in Fig.~\ref{fig:omegac_phasespace_mc}
for the \Omegac mode.
%, \ref{fig:xicz_phasespace_mc},
%and \ref{fig:dz_phasespace_mc} for signal and control modes, respectively.
The same degree of agreement is observed for \Xicz and \Dz modes.
The comparison indicates that it is a good approximation not to
distinguish prompt and secondary signals in data distributions
in the reweighting procedure.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_KmPip_Omegac_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_PpKm_Omegac_dalitz_16.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_Pp_Omegac_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_Pi_Omegac_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_phi_Omegac_dalitz_16.pdf}
  \caption{Comparison of phasespace variables for prompt and secondary signals 
  in 2016 \Omegac MC samples.}
  \label{fig:omegac_phasespace_mc}
\end{figure}
%\begin{figure}[tb]
%  \centering
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_KmPip_Xic0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_PpKm_Xic0_dalitz_16.pdf}\\
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_Pp_Xic0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_Pi_Xic0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_phi_Xic0_dalitz_16.pdf}
%  \caption{Comparison of phasespace variables for prompt and secondary signals 
%  in 2016 \Xicz MC samples.}
%  \label{fig:xicz_phasespace_mc}
%\end{figure}
%\begin{figure}[tb]
%  \centering
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_KpKm_D0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_M_PipPim_D0_dalitz_16.pdf}\\
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_K_D0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_theta_Pi_D0_dalitz_16.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_MC_cos_phi_D0_dalitz_16.pdf}
%  \caption{Comparison of phasespace variables for prompt and secondary signals 
%  in 2016 \Dz MC samples.}
%  \label{fig:dz_phasespace_mc}
%\end{figure}

Five-dimensional weight is calculated using 
GBReweighter from the \texttt{hep\_ml} package~\inlinecite{Rogozhnikov:2016bdp}.
The comparison of 2016 data and simulation 
before and after the phasespace weight
is shown in Fig.~\ref{fig:phasespace_omegac}, \ref{fig:phasespace_xicz}, 
and \ref{fig:phasespace_d0} for \Omegac, \Xicz, and \Dz mode, respectively.
To check that the correlations of different phasespace variables are handled properly,
comparisons of additional phasespace variables are shown in Fig.~\ref{fig:phasespace_add},
in which good agreement is observed after the five-dimensional weighting.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KmPip_Omegac_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PpKm_Omegac_L0_dalitz_16.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_Pp_Omegac_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_Pi_Omegac_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_phi_Omegac_L0_dalitz_16.pdf}
  \caption{Comparison of 2016 data and simulation
  before and after the phasespace weight for \Omegac mode,
  where the simulation before (after) weighting is shown in gray (blue),
  and the sWeighted data is shown in red.}
  \label{fig:phasespace_omegac}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KmPip_Xic0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PpKm_Xic0_L0_dalitz_16.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_Pp_Xic0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_Pi_Xic0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_phi_Xic0_L0_dalitz_16.pdf}
  \caption{Comparison of 2016 data and simulation
  before and after the phasespace weight for \Xicz mode.}
  \label{fig:phasespace_xicz}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KpKm_D0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PipPim_D0_L0_dalitz_16.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_K_D0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_cos_theta_Pi_D0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_phi_D0_L0_dalitz_16.pdf}
  \caption{Comparison of 2016 data and simulation
  before and after the phasespace weight for \Dz mode.}
  \label{fig:phasespace_d0}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KmKm_Omegac_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KmKm_Xic0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_KmKpPip_D0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PpPip_Omegac_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PpPip_Xic0_L0_dalitz_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/phasespace/compare_M_PipPimKm_D0_L0_dalitz_16.pdf}
  \caption{Comparison of additional phasespace variables for (left) \Omegac,
  (middle) \Xicz, and (right) \Dz mode.}
  \label{fig:phasespace_add}
\end{figure}

\subsection{Kinematic corrections}%
\label{sub:kinematic}

Kinematic distributions,
including the \pt and $y$ of the charmed hadron 
and the \pt of the final-state tracks,
between prompt signals in data and simulation are found to be different
after the corrections described above.
Further kinematic weights are applied to correct for this discrepancy.

The kinematic weights are obtained as follows.
First, binned one-dimensional distributions in data of 
\pt and $y$ of the charmed hadron
and \pt of its final-state tracks for prompt signals
are extracted with the two-dimensional invariant-mass and \logip fit
described in Sec.~\ref{sec:prompt_yield_extraction}
(in this case the sample is split in bins of kinematic variables
under consideration instead of the decay time).
Second, the weight is calculated 
according to the observed data-simulation discrepancy
for each kinematic variable (six in total) sequentially.
Third, the second step is repeated until (it turns out that two iterations are sufficient)
the kinematic distributions between prompt signals in data and simulation 
are in good agreement.
The procedure is performed for both signal and control modes
and for each data-taking period.
The fact that only one-dimensional distributions are used in the reweighting
is because the sample size of the signal mode is limited.
%when extracting the multi-dimensional kinematic distributions
%of prompt signals in the presence of the secondary contamination.

The comparison of the kinematic distributions 
between prompt signals in data and simulation after the kinematic correction
is shown Fig.~\ref{fig:compare_kine_omegac_twod_18}, 
Fig.~\ref{fig:compare_kine_xicz_twod_18} and 
Fig.~\ref{fig:compare_kine_dz_twod_18} for 2018 data.
The same degree of agreement is observed in other data-taking years.
%The comparison of other variables of interest and for 2016 data 
%are shown in Appendix~\ref{sec:compare_data_mc_twod}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_logXic0PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_Xic0Y_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_logPpPT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_logKm1PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_logKm2PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Omegac_logPipPT_18.pdf}
  \caption{Comparison of prompt distributions between data and simulation for \Omegac mode.
  The prompt data distribution is extracted with two-dimensional fits of 
  invariant-mass and \logip described in Sec.~\ref{sec:prompt_yield_extraction},
  in bins of variables under study.
  The ``log'' indicates a 10-based logarithmic transformation,
  and final-state particle \proton, \Km, and \pip of \Omegac are labeled as 
  Pp, Km1, Km2 and Pip, respectively.}
  \label{fig:compare_kine_omegac_twod_18}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_logXic0PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_Xic0Y_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_logPipPT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_logKm1PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_logKm2PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_Xic0_logPipPT_18.pdf}
  \caption{Comparison of various distributions between data and simulation for \Xicz mode.
  The prompt data distribution is extracted with two-dimensional fits of 
  invariant-mass and \logip described in Sec.~\ref{sec:prompt_yield_extraction},
  in bins of variables under study.
  The ``log'' indicates a 10-based logarithmic transformation,
  and final-state particle \proton, \Km, and \pip of \Xicz are labeled as 
  Pp, Km1, Km2 and Pip, respectively.}
  \label{fig:compare_kine_xicz_twod_18}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_logD0PT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_D0Y_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_logKmPT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_logKpPT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_logPimPT_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/compare/twod/compare_kine_D0_logPipPT_18.pdf}
  \caption{Comparison of various distributions between 2018 data and simulation for \Dz mode.
  The prompt data distribution is extracted with two-dimensional fits of 
  invariant-mass and \logip described in Sec.~\ref{sec:prompt_yield_extraction},
  in bins of variables under study.
  The ``log'' indicates a 10-based logarithmic transformation,
  and final-state particle \Km, \Kp, \pim, \pip of \Dz are labeled as 
  Km, Kp, Pim and Pip, respectively.}
  \label{fig:compare_kine_dz_twod_18}
\end{figure}

\subsection{Decay-time acceptance}%
\label{sub:decay_time_acceptance}

To understand how various corrections affect 
the decay-time distributions in simulation,
the normalised decay-time distributions after each correction stage
are shown in Fig.~\ref{fig:decaytime_mc},
where the most significant correction is due to the L0 efficiency correction.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/decaytime_Omegac250_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/decaytime_Xic0250_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/calibration/decaytime_D0_16.pdf}
  \caption{Normalised decay-time distributions after each correction stage
  for 2016 (left) \Omegac, (middle) \Xicz and (right) \Dz simulation.
  The input lifetime of \Omegac and \Xicz simulation sample is 250\fs.
  The input lifetime of \Dz simulation sample is 410\fs.}
  \label{fig:decaytime_mc}
\end{figure}

The decay-time acceptance is defined as the selection efficiency
as a function of decay time.
It is estimated by taking the ratio of the selected and generated number of events
in each decay-time bin,
where the selected number of events are obtained from the corrected simulation samples.
%The decay-time acceptances of 2016 sample in bins of decay time 
%are shown in Fig.~\ref{fig:acc} for all modes, respectively.
The decay-time acceptances of 2016 sample in bins of decay time 
are shown in Fig.~\ref{fig:acc_all} for all modes overlaid.
The ratio of the decay-time acceptance between signal and control mode 
is shown in Fig.~\ref{fig:acc_ratio},
with 2016 simulation samples 
(input lifetimes of $\tau(\Omegac)=250\fs$,$\tau(\Xicz)=250\fs$, and $\tau(\Dz)=410\fs$).
In the decay-time region of [0.45,2.0]\ps,
which is chosen to perform the lifetime measurement,
the deviation from a flat acceptance ratio is about 10--20\% 
(mainly at low decay-time region).
The fluctuation at 1.3\ps for \Dz mode is due to
some weights in the phasespace correction
and is checked to have negligible effect on the decay-time fit.
%\begin{figure}[tb]
%  \centering
%  %\includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_Omegac250_true_16_tot.pdf}
%  %\includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_Xic0250_true_16_tot.pdf}
%  %\includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_D0_true_16_tot.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_Omegac250_reco_16_tot.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_Xic0250_reco_16_tot.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/acc/acc_D0_reco_16_tot.pdf}
%  \caption{Decay-time acceptance in 2016 sample for 
%  (left) \Omegac, (middle) \Xicz, and (right) \Dz modes
%  and in bins of reconstructed decay time.
%  %and in bins of (top) true decay time and (bottom) reconstructed decay time.
%  The input lifetime of \Omegac and \Xicz simulation sample is 250\fs.}
%  \label{fig:acc}
%\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_true_16_tot.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_reco_16_tot.pdf}
  \caption{Decay-time acceptance in 2016 sample for all modes 
  in bins of (left) true decay time and (right) reconstructed decay time.}
  \label{fig:acc_all}
\end{figure}
\begin{figure}[tb]
  \centering
  %\includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_ratio_true_Omegac_16.pdf}
  %\includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_ratio_true_Xic0_16.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_ratio_reco_Omegac_16.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/acc/acc_ratio_reco_Xic0_16.pdf}
  \caption{Decay-time acceptance ratio of signal to control mode versus 
  the reconstructed decay time.}
  \label{fig:acc_ratio}
\end{figure}


\subsection{Validation with \Dz control mode}%
\label{sub:validation_of_efficiency_correction_with_dz_control_mode}

As a stringent validation,
the normalised decay-time distributions of data and simulation samples
for \Dz control mode are compared, 
as shown in Fig.~\ref{fig:eff_dz_validation}.
Figure~\ref{fig:eff_dz_validation} also 
shows the decay-time ratio of data to simulation.
The data distribution is obtained with the two-dimensional 
invariant-mass and \logip fit described in Sec.~\ref{sec:prompt_yield_extraction}.
The simulated distribution has been corrected as discussed above in this section.
The input lifetime for \Dz simulation is its PDG value of 410\fs.
Good agreement between data and simulation is observed
in the decay-time region of [0.45,2.50]\ps.
Defining $\chisq \equiv \frac{(N_{data}-N_{mc})^2}{\sigma_{N_{data}}^2+\sigma_{N_{mc}}^2}$,
the $\chisq$/ndof for 2016-2018 distributions are 
26/21, 27/21, and 15/21, respectively.
The ratio of decay time is described 
with a linear function and the fit results are shown in Figure~\ref{fig:eff_dz_validation}.
The results indicate that the slope is consistent with zero
within $3\sigma$ for 2016-2018 data.
Since this is a comparison of data and simulation 
for only one decay mode,
the residual discrepancy is expected to be further reduced 
when the ratio is taken between the signal and control mode.
%The agreement for 2017 data is worse compared to 2016 and 2018 data.
%We discuss the remaining mis-modelling effect of the decay-time acceptance,
%espetially in low decay time bins,
%in the systematic uncertainty study Sec.~\ref{sub:decay_time_acceptance}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_offset2_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_ratio_linear_offset2_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/D0_16_norm.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_offset2_17.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_ratio_linear_offset2_17.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/D0_17_norm.pdf}\\
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_offset2_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/decaytime_ratio_linear_offset2_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/validation/D0_18_norm.pdf}
  \caption{Decay-time distributions of (left) data and simulation samples,
  (middle) decay-time ratios of data to simulation samples,
  and (right) the decay-time fit results to the \Dz data
  for (top) 2016, (middle) 2017, and (bottom) 2018 \Dz control mode.}
  \label{fig:eff_dz_validation}
\end{figure}

Quantitatively,
a test is performed by measuring the \Dz lifetime in an absolute manner
with a chi-squared fit to the data decay-time distribution
with the simulated sample as template.
The method used here is very similar to 
what is to be used in the default decay-time fit discussed in Sec.~\ref{sec:decaytime_fit},
except for that there is no control mode to be used to construct a ``double ratio''
in this case.
For each data-taking year, we define
the ratio of the measured prompt yield in data, $N_i$, 
and the effective yield in simulation,
which includes all the corrections applied to simulation, $M_i$,
in the $i$-th decay-time bin as
\begin{equation}
  r^{\mathrm{con}}_i = \frac{N^{\mathrm{con}}_i}{M^{\mathrm{con}}_i}
\end{equation}
for the \Dz control (denoted as ``con'') mode.
We also define the expected prompt yield ratio of data to simulation as
\begin{equation}
  f^{\mathrm{con}}_i = \frac{\int_i \exp(-t/\tau^{\mathrm{con}}) dt}{\int_i \exp(-t/\tau^{\mathrm{con}}_{\mathrm{sim}}) dt}
\end{equation}
for the control mode, where
$\tau_{\mathrm{con}}$ is the \Dz lifetime to be measured and
$\tau_{\mathrm{sim}}^{\mathrm{con}}=410\fs$ is the input lifetime in simulation.
The integral runs from the lower bin edge to the upper bin edge 
of the $i$-th decay-time bin.
Then the \chisq for each year is defined 
(to avoid confusion with the default fit in next section, 
a bar is added in the notation)
and evaluated as
\begin{equation}
  \begin{aligned}
    \bar{\chi}^2_{\mathrm{year}}(\tau^{\mathrm{con}},C_{\mathrm{year}}) & \equiv \sum_i \frac{(N^{\mathrm{con}}_i - C_{\mathrm{year}} \times f^{\mathrm{con}}_i \times M^{\mathrm{con}}_i)^2}
    { \sigma^2_{N^{\mathrm{con}}_i} + C^2_{\mathrm{year}} \times f_i^{\mathrm{con}2} \times \sigma^2_{M^{\mathrm{con}}_i} } \\
    & = \sum_i \frac{ (r^{\mathrm{con}}_i - C_{\mathrm{year}} \times f^{\mathrm{con}}_i)^2 }
    { r_i^{\mathrm{con}2} \times \left( \frac{ \sigma_{N^{\mathrm{con}}_i} }{N^{\mathrm{con}}_i} \right)^2
    + C^2_{\mathrm{year}} \times f^{\mathrm{con2}}_i \times \left( \frac{\sigma_{M^{\mathrm{con}}_i}}{M^{\mathrm{con}}_i} \right)^2 },
  \end{aligned}
  \label{eq:chi2_dz_year}
\end{equation}
where $\sigma_Q$ denotes the variance of the relevant quantity $Q$,
$C_{\mathrm{year}}$ is the normalisation factor 
introduced to account for the different sample size of data and simulation,
and the sum runs through all decay-time bins.
The modelling of decay-time acceptance with simulation 
is included implicitly in $M^{\mathrm{con}}_i$.
The fit results are shown in Fig.~\ref{fig:eff_dz_validation},
where the bin content of the ``Data'' curve is proportional to $N^{\mathrm{con}}_i$,
the bin content of the ``Fit'' curve is proportional to 
$C_{\mathrm{year}} \times f^{\mathrm{con}}_i \times M^{\mathrm{con}}_i$,
and the pull is evaluated as
$(N^{\mathrm{con}}_i - C_{\mathrm{year}} \times f^{\mathrm{con}}_i \times M^{\mathrm{con}}_i)/\sqrt{\sigma^2_{N^{\mathrm{con}}_i}+C^2_{\mathrm{year}} \times f_i^{\mathrm{con}2} \times \sigma^2_{M^{\mathrm{con}}_i}}$.
The \Dz lifetime can also be extracted with a simultaneous fit to 2016-2018 data.
In this case the total chisquare is constructed as
\begin{equation}
  \bar{\chi}^2(\tau^{\mathrm{con}},\vec{C}) = \sum_{\mathrm{year}} \bar{\chi}^2_{\mathrm{year}}(\tau^{\mathrm{con}},C_{\mathrm{year}}),
  \label{eq:chi2_dz_simul}
\end{equation}
where $\tau^{\mathrm{con}}$ is the \Dz lifetime to be measured,
$\vec{C} = (C_{16},C_{17},C_{18})$ is the normalisation vector,
$\bar{\chi}^2_{\mathrm{year}}$ is defined as in Eq.~\ref{eq:chi2_dz_year}
and the sum runs from 2016-2018 data.

The fit results for each data-taking year and 
for 2016-2018 data simultaneously are shown in Table~\ref{tab:dz-decaytime-fit},
which look good given that an absolute measurement is performed.
The largest discrepancy with the PDG value of \Dz lifetime ($410.1\pm1.5\fs$) 
is 2.4$\sigma$ in 2017 data.
The largest discrepancy between years is 3.4$\sigma$ between 2016 and 2017 data.
No systematic uncertainties are considered at this stage.
\begin{table}[tb]
  \centering
  \caption{Decay-time fit results for \Dz control mode.}
  \label{tab:dz-decaytime-fit}
  \begin{tabular}{c c c c c}
    \toprule
    & 2016 & 2017 & 2018 & Simultaneous \\
    \midrule
    $\tau[\fs]$  & $420.9 \pm 4.6$& $401.5 \pm 3.1$& $406.8 \pm 3.6$ & $407.9\pm2.1$\\
    $\chisq$/NDF & $19/19$& $19/19$& $13/19$ & $63/59$ \\
    %$\tau[\fs]$  & $421.5 \pm 4.8$& $402.0 \pm 3.2$& $407.0 \pm 3.7$ & $408.2\pm2.2$ \\
    %$\chisq$/NDF & $19/19$& $21/19$& $14/19$ & $66/59$ \\
    \bottomrule
  \end{tabular}
\end{table}
%For 2017--2018 good agreement is archived
%and the resulting lifetime is consistent with the PDG value.
%%The residual discrepancy is expected to be further reduced 
%%in the fit to the decay-time ratio discussed in Sec.~\ref{sec:decaytime_fit}.
%For 2016 the agreement is not good 
%and the resulting lifetime is about $5\sigma$ above the PDG value.
%% results below are for D0 without HLT1 and without kinematic corrections
%\begin{table}[htpb]
%  \centering
%  \caption{The decay-time fit results for \Dz control mode.}
%  \label{tab:dz-decaytime-fit}
%  \begin{tabular}{c c c c c}
%    \toprule
%    & 2016 & 2017 & 2018 & Simultaneous 2016--2018 \\
%    \midrule
%    $\tau[\fs]$ & $432.5\pm3.9$ & $411.0\pm2.5$ & $406.0\pm2.3$ & $412.9\pm1.6$ \\
%    $\chisq$/NDF & 56/19 & 19/19 & 15/19 & 126/59 \\
%    \bottomrule
%  \end{tabular}
%\end{table}

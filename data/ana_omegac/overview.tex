This chapter reports a measurement of the lifetimes of 
$\Omegac(css)$ and $\Xicz(csd)$ baryon~\inlinecite{LHCb-PAPER-2021-021}.
This measurement makes use of signals produced directly from $pp$ collisions,
different from the previous LHCb measurement 
using signals from semileptonic beauty baryon decays.
An overview of the data analysis strategy and method is presented 
in Sec.~\ref{sec:analysis_overview}.
Event selection is discussed in Sec.~\ref{sec:event_selection}.
Determination of the yield and estimation of the efficiency
are described in Sec.~\ref{sec:prompt_yield_extraction} and 
Sec.~\ref{sec:efficiency_estimation}, respectively.
Extraction of lifetimes is shown in Sec.~\ref{sec:decaytime_fit},
followed by a discussion of systematic uncertainties in Sec.~\ref{sec:systematic_uncertainties}.
To conclude, the result and its interpretation are presented in Sec.~\ref{sec:conclusion_omegac}.

\section{Analysis overview}%
\label{sec:analysis_overview}

In this analysis,
the lifetimes of \Omegac and \Xicz baryon are measured,
using signals produced directly from $pp$ collisions,
referred to as the ``prompt'' signals.
This sample is different from signals from semileptonic beauty baryon decays,
referred to as the ``secondary'' signals.
A schematic view of the decay topology is shown in Fig.~\ref{fig:cartoon_omegac}.
Both \Omegac and \Xicz baryon are reconstructed through the $p\Km\Km\pip$ final state.
The data sample was collected by the LHCb detector during 2016-2018,
corresponding to an integrated luminosity of 5.4\invfb,
as detailed in Appendix~\ref{sec:bookkeeping_of_data_and_simulation_samples}.
The four-body \Dz decay $\Dstarp\to\Dz(\to\Km\Kp\pim\pip)\pip$
is used as the control mode in order to 
a) reduce systematic uncertainties, 
b) estimate sources of systematic uncertainties,
and c) validate the analysis procedure.

The advantage of using prompt signals include
the large production cross-section relative to that of the secondary production,
and the better primary vertex (PV) resolution relative to that of the secondary vertex (SV).
Each advantage comes with its challenge.
The benefit of a large signal yield is reduced by the high combinatorial background level
due to random combination of tracks from PV.
In addition, we need to discriminate between signals from PV and those from SV.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/cartoon_p.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/cartoon_s.pdf}
  \caption{Schematic view of (left) prompt signal and (right) secondary signal.}
  \label{fig:cartoon_omegac}
\end{figure}

In general, the analysis procedure includes:
\begin{itemize}
  \item Selection of signal decays. A multivariate analysis is used to suppress background
    besides the rectangular requirements.
  \item Determination of the prompt signal yield.
    A dedicated model is developed to obtain the prompt signal yields in the selected sample
    out of the contamination of combinatorial background and secondary signals.
    The invariant mass and the $\logip$ distributions are used 
    to differentiate various components statistically.
  \item Estimation of selection efficiency.
    The selection requirement applied to suppress the background
    leads to a distortion of the decay-time distribution from an ideal exponential function.
    This deviation is modelled with simulation in order to obtain an unbiased estimation
    of the lifetime.
  \item Extraction of the lifetime with the least square fit.
\end{itemize}

Since we are measuring controversial quantities
and in order to avoid experimentalists' subjective bias,
the measured central values of \Omegac and \Xicz lifetimes 
were blinded with different random numbers uniformly distributed in $[-100,100]\fs$
before the whole analysis procedure was carefully reviewed and finalised.
The unblinding of the results was achieved by disabling the random numbers
in the least square fit.

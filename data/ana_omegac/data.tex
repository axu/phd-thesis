\section{Data and simulation samples}%
\label{sec:data_and_simulation}

The 2016--2018 data samples are used in this analysis,
corresponding to an integrated luminosity of 5.5\invfb.
The \texttt{Hlt2CharmHadXic0ToPpKmKmPipTurbo} turbo line is used to reconstruct and select signal decays,
while the \texttt{Hlt2CharmHadDstp2D0Pip\_D02KmKpPimPipTurbo} turbo line is used for the control mode.

The 2016--2018 simulation samples are generated 
to study the invariant mass and \logip modelling 
and to estimate the selection efficiency.
The phasespace model is used as the decay model.
The ReDecay fast simulation option is used to speed up the MC production.
Simulated samples are generated with different input lifetime hypotheses
to populate both the small and large decay-time bins.
The configurations of simulated samples are summarised in Table~\ref{tab:simulations}.
By default, the simulation samples with input lifetime of $250\fs$
is used in studies of \Omegac and \Xicz signal modes.
Alternative input lifetimes are used where stated explicitly.

The truth-matched inclusive signal decays are acquired with true particle ID information of the 
charmed hadron and its decay products.
The truth-matched prompt signal decays are acquired by further requiring the true mother ID of the
charmed hadron to be the charm quark from the hard process,
while the truth-matched secondary decays are acquired by further requiring the true mother particle ID of the
charmed hadron to be $b$-hadron.

\begin{table}
  \caption{The configuration of simulated samples.}
  \label{tab:simulations}
  \centering
  \begin{tabular}{l c c}
    \toprule
    Modes & EventType & Input $\tau$ \\
    \midrule
    $\Omegac \to \proton \Km \Km \pip$   & 26104086 & $250\fs$           \\
    $\Omegac \to \proton \Km \Km \pip$   & 26104087 & $500\fs$           \\
    $\Xicz \to \proton \Km \Km \pip$     & 26104089 & $112\fs$           \\
    $\Xicz \to \proton \Km \Km \pip$     & 26104880 & $250\fs$           \\
    $\Dstarp \to \pip \Dz (\to \Km \Kp \pim \pip)$ & 27165003 & $410\fs$ \\
    \bottomrule
  \end{tabular}
\end{table}

The bookkeeping path for data samples are:
\begin{flushleft}
  \scriptsize
  \texttt{/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo03a/94000000/CHARMMULTIBODY.MDST}
  \texttt{/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo03a/94000000/CHARMMULTIBODY.MDST}
  \texttt{/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/CHARMMULTIBODY.MDST}
  \texttt{/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo04/94000000/CHARMMULTIBODY.MDST}
  \texttt{/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo05/94000000/CHARMMULTIBODY.MDST}
  \texttt{/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/CHARMMULTIBODY.MDST}
\end{flushleft}

The bookkeeping path for 2016 simulation samples are
(prepended by \texttt{/MC/2016/Beam6500GeV-2016-Mag\{Down|Up\}-Nu1.6-25ns-Pythia8/}):
\begin{flushleft}
  \scriptsize
  \texttt{Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/26104089/ALLSTREAMS.MDST}
  \texttt{Sim09f-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/26104880/ALLSTREAMS.MDST}
  \texttt{Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/26104086/ALLSTREAMS.MDST}
  \texttt{Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/26104087/ALLSTREAMS.MDST}
  \texttt{Sim09d-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/27165003/ALLSTREAMS.MDST}
\end{flushleft}

The bookkeeping path for 2017 simulation samples are
(prepended by \texttt{/MC/2017/Beam6500GeV-2017-Mag\{Down|Up\}-Nu1.6-25ns-Pythia8/}):
\begin{flushleft}
  \scriptsize
  \texttt{Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/26104089/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/26104880/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/26104086/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/26104087/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/27165003/ALLSTREAMS.MDST}
\end{flushleft}

The bookkeeping path for 2018 simulation samples are
(prepended by \texttt{/MC/2018/Beam6500GeV-2018-Mag\{Down|Up\}-Nu1.6-25ns-Pythia8/}):
\begin{flushleft}
  \scriptsize
  \texttt{Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/26104089/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/26104880/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/26104086/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/26104087/ALLSTREAMS.MDST}
  \texttt{Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/27165003/ALLSTREAMS.MDST}
\end{flushleft}


\subsection{Change of VELO hit error parametrization in 2017--2018 data}%
\label{sub:change_of_velo_parametrization_in_2017_2018_data}

It is reported that the change of VELO hit error parametrization in 2017--2018 data
makes the data-simulation agreement of some decay-time related variables
worse than that in 2016 data~\footnote{\scriptsize See https://indico.cern.ch/event/942464/contributions/3960514/attachments/2081322/3496321/Run2\_Data\_MC\_Studies.pdf},
including the decay-time resolution and the \chisqip distributions of the final-state tracks.
The reason of worse data-simulation discrepancy is that
the change of VELO error parametrization is not implemented in 2017--2018 simulation
as is done for data.
The effect of the discrepancy of decay-time resolution 
is discussed in Sec.~\ref{sub:time_resolution}.
The discrepancy of the \chisqip distributions may lead to an inaccurate description of
the decay-time acceptance and need to be taken into account.

To correct for the discrepancy of the \chisqip distributions
due to different VELO error parametrization between data and simulation in 2017--2018,
we scale the \chisqip distributions of the final-state tracks in 2017--2018 simulation
by multiplying a constant scaling factor
before any \chisqip-related selections are applied.
The scaling factor is determined by comparing the background-subtracted \chisqip distributions of the final-state tracks
in 2016 and 2017--2018 data.
The idea is that the difference due to the change of VELO parametrization should be the same in data and in simulation.
For \Dz control mode,
it can be observed that a scaling factor of 1.05 makes a good agreement between 2016 and 2017--2018 data.
As an illustration, Fig.~\ref{fig:smear_ipchi2_distribution} shows a comparison of 2016 and 2018 data,
and Fig.~\ref{fig:smear_ipchi2_data-mc} shows a comparison of data and MC for 2016 and 2018.
Therefore, the \chisqip distributions of all (four in total) final-state tracks in 2017--2018 simulation
for both signal and control modes
are multiplied by 1.05 before the original \chisqip selection criteria are applied.
\begin{figure}[tb]
  \centering
    \includegraphics[width=0.4\linewidth]{ana_omegac/smear_ipchi2/data_Km_IPCHI2.pdf}
    \includegraphics[width=0.4\linewidth]{ana_omegac/smear_ipchi2/data_Pip_IPCHI2.pdf}
  \caption{The comparison of the log-sized \chisqip distributions of the final-state (left) Kaons and (right) Pions
  in 2016 and 2018 data for \Dz control mode. The \chisqip distributions in 2016 data are scaled by a factor of 1.05.}
  \label{fig:smear_ipchi2_distribution}
\end{figure}
\begin{figure}[htb]
  \centering
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_16_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_17_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Km_IPCHI2_OWNPV_18_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_16_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_17_scale}
    \includegraphics[width=0.32\linewidth]{ana_omegac/smear_ipchi2/Pip_IPCHI2_OWNPV_18_scale}
  \caption{The comparison of the log-sized \chisqip distributions of the final-state (top) Kaons and (bottom) Pions
  in (left) 2016 data and simulation and (right) 2018 data and simulation for \Dz control mode.
  The \chisqip distributions in 2018 simulation are scaled by a factor of 1.05.}
  \label{fig:smear_ipchi2_data-mc}
\end{figure}

A more accurate option for this correction is proposed
but the result w.r.t to a constant scaling factor is negligible,
as discussed in Appendix~\ref{sec:app_velo_parametrization}.


\subsection{Offline PV refit}%
\label{sub:offline_pv_refit}

To obtain an unbiased measure of the displacement of a particle w.r.t. a PV,
PVs are supposed to be refitted excluding the signal tracks under consideration.
However, the \texttt{ReFitPVs} option is not enabled
in the \texttt{Xic0ToPpKmKmPip} turbo line.
A detailed description of this issue can be found in the footnote~\footnote{\scriptsize{https://twiki.cern.ch/twiki/bin/view/LHCb/TurboPVRefittingBug}}.
If one or more tracks from your signal candidate are included in the PV fit,
this will pull the position of the PV towards those tracks and so will bias any displacement variables calculated wrt the PV.

As the Turbo line \texttt{Xic0ToPpKmKmPipTurbo} used to collect the signal decays
enables the \texttt{PersistReco} option,
we are able to re-fit the PVs with the persisted HLT1 VELO tracks
offline in \texttt{DaVinci} jobs.
This is done for 2016--2018 data of the signal mode,
where data are processed with the original P2PV relations killed
and \texttt{ReFitPVs=True} for \texttt{DecayTreeTuple} in the \texttt{DaVinci} option file,
which triggers the offline PV re-fitting
and the re-fitted PVs are used by various TupleTools to calculate physics quantities.

For the Turbo line \texttt{Hlt2CharmHadDstp2D0Pip\_D02KmKpPimPipTurbo} used to collect the 
decays of the control mode, the \texttt{PersistReco} option is not enbled.
However, with the available VELO clusters the PV-refit can be recovred
by recalculating the weight used for tracks in the PV fit
with the \href{https://gitlab.cern.ch/lhcb/Phys/-/blob/v26r6/Phys/VertexFit/src/RecalcLSAdaptPV3DFitterWeightsAlg.h}{RecalcLSAdaptPV3DFitterWeightsAlg} tool,
as discussed in \href{https://indico.cern.ch/event/859265/contributions/3623401/attachments/1935324/3206996/20191030-PVRefittingUpdate.pdf}{this Run 1--2 performance meeting}.
This is done for 2017--2018 data of the control mode.
As no VELO clusters are available for 2016 data,
the calculated decay time is smeared by the bias 
observed in 2018 data with and without the offline PV refit.

Pseudo-experiments are performed to study the impact of offline PVFit
on the measured lifetime, as discussed in Appendix~\ref{sec:app_refitpv_toy}.

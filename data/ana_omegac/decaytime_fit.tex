\section{Extraction of lifetimes}
\label{sec:decaytime_fit}

The lifetime of \Omegac and \Xicz baryons
are determined from a binned \chisq fit 
comparing the signal yields in data with those from the simulation, 
where the input lifetime is known.
The total \chisq is constructed as
\begin{equation}
\begin{aligned}
  \label{eq:decaytime_chisq}
  \chisq(\tau,\vec{C}) &=
  \sum_{j}
  \sum_i \frac{\left(N^{\mathrm{sig}}_{i,j} - C_{j}
               \times F_i(\tau) \times
               R_{i,j} \right)^2}
    { \sigma^2_{N^{\mathrm{sig}}_{i,j}} +
      C^2_{j} \times F_i^2(\tau) \times
      \sigma^2_{R_{i,j}} },
      \qquad
      R_{i,j} &= \frac{N^{\mathrm{con}}_{i,j}}{M^{\mathrm{con}}_{i,j}}
      \times M^{\mathrm{sig}}_{i,j},
\end{aligned}
\end{equation}
where $N^{\mathrm{sig}}_{i,j}$ ($N^{\mathrm{con}}_{i,j}$)
is the signal yield in data for the signal (control) mode
in decay-time interval $i$ and for the data-taking period $j$;
$M_{i,j}$ is the effective yield predicted from simulation;
$C_j$ is a normalisation factor to account for the difference in size between
the data and the simulated samples;
and $\sigma$ is the uncertainty of the relevant quantity.
The difference in lifetime between data and simulated samples is accounted for by
\begin{equation}
  \label{eq:exp_ratio}
F_i(\tau) = \frac{\int_i \exp(-t/\tau) \mathrm{d}t}
                  {\int_i \exp(-t/\tau_{\mathrm{sim}}) \mathrm{d}t} \times
             \frac{\int_i \exp(-t/\tau^{\mathrm{con}}_{\mathrm{sim}}) \mathrm{d}t}
                  {\int_i \exp(-t/\tau^{\mathrm{con}}) \mathrm{d}t},
\end{equation}
where $\tau_{\mathrm{sim}} = 250\fs$ is the signal mode lifetime in simulation
and $\tau^{\mathrm{con}} = \tau^{\mathrm{con}}_{\mathrm{sim}}$ is
the known \Dz lifetime~\inlinecite{PDG2020}, 
but is allowed to vary for estimating the systematic uncertainty.

The resulting lifetimes are
\begin{equation}
  \begin{aligned}
    \tau(\Omegac) &= \omegacval  \pm \omegacstat \fs, \\
    \tau(\Xicz)   &= \xiczval    \pm \xiczstat   \fs,
  \end{aligned}
\end{equation}
with $\chisqndf = 22/23$ ($p\mhyphen\mathrm{value}=0.52$) and 
$\chisqndf = 30/20$ ($p\mhyphen\mathrm{value}=0.06$),
where the uncertainties are statistical only.
The blinded central values are $177\fs$ for \Omegac and $78.9\fs$ for \Xicz,
which serve as the reference to the cross-checks.
The resulting values of the normalisation vectors are shown in Table~\ref{tab:norm}.
The fit results of \Omegac and \Xicz modes are shown in Fig.~\ref{fig:nominal_decay_time_fit}.
The ``Data'' distribution is the raw data yield in 2016-2018 samples combined,
divided by the bin width $\Delta t_i$ of each decay-time bin.
The ``Fit'' distribution is $C \times F_i \times M^{\mathrm{sig}}_i \times r^{\mathrm{con}}_i$
with free parameters taking fitted values,
divided by the bin width $\Delta t_i$ of each decay-time bin.
\begin{table}[tb]
  \centering
  \caption{Fit results of the normalisation vectors.}
  \label{tab:norm}
  \begin{tabular}{c c c c}
    \toprule
    & $C_{16}$ & $C_{17}$ & $C_{18}$ \\
    \midrule
    \Omegac & $(2.078\pm0.274)\times10^{-2}$ & $(2.390\pm0.308)\times10^{-2}$ & $(1.743\pm0.229)\times10^{-2}$ \\
    \Xicz   & $0.769\pm0.059$ & $0.654\pm0.047$ & $0.696\pm0.051$ \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/paper/Fig4a.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/paper/Fig4b.pdf}
  \caption{Decay-time distributions for
  the (left) \Omegac mode and the (right) \Xicz mode with the \chisq fit superimposed.
  The uncertainty on the data distribution is statistical only.}
  \label{fig:nominal_decay_time_fit}
\end{figure}
%\begin{figure}[tb]
%  \centering
%  \includegraphics[width=.45\linewidth]{ana_omegac/fit_data/Omegac_run2_simul_norm.pdf}
%  \includegraphics[width=.45\linewidth]{ana_omegac/fit_data/Xic0_run2_simul_norm.pdf}\\
%  \caption{The decay time fit results of \Omegac and \Xicz modes.}
%  %Only pulls are shown to follow the blind procedure.}
%  \label{fig:nominal_decay_time_fit}
%\end{figure}
%\begin{figure}[tb]
%  \centering
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_16_simul_norm.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_17_simul_norm.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_18_simul_norm.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_16_simul_norm.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_17_simul_norm.pdf}
%  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_18_simul_norm.pdf}
%  \caption{The raw prompt yields in data along with the fit result for each data-taking period.}
%  \label{fig:nominal_decaytime_fit_year}
%\end{figure}


\subsection{Consistency checks}%
\label{sub:consistency_checks}

To check that the measured lifetimes are robust 
against variation of data-taking conditions
and the choice of binning scheme,
several consistency checks are performed,
in which the total data sample is split into different 
subsamples and the lifetimes are measured and compared.
A summary of results of these checks are shown in Fig.~\ref{fig:combine}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/cross_check_omegac.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/cross_check_xicz.pdf}
  \caption{Results of consistency checks for (left) \Omegac and (right) \Xicz modes.}
  \label{fig:combine}
\end{figure}

\paragraph{Data-taking period.}%
\label{par:data_taking_period_}
To check that results of each data-taking period are consistent with each other,
the decay-time fit is performed in each year 
with independent $\tau^{\mathrm{sig}}$ parameters.
The results are summarised in Table~\ref{tab:decaytime_year}
and illustrated in Fig.~\ref{fig:combine}.
The last column shows the weighted average of the resulting lifetime
and \chisqndf of the combination with least squares method.
It can be seen that results of different data-taking period
are compatible with each other within $2\sigma$ statistical uncertainty.
\begin{table}[tb]
  \centering
  \caption{Decay-time fit in each data-taking period.
  The last column shows the weighted average of the resulting lifetime 
  and \chisqndf of the combination with least squares method.}
  \label{tab:decaytime_year}
  \begin{tabular}{c c c c c  c}
    \toprule
    &  & 16 & 17 & 18 & Average \\
    \midrule
    \Omegac
    & $\tau[\fs]$ & $  203 \pm  28$& $  177 \pm  23$& $  160 \pm  20$ & $176\pm13$\\
    & \chisqndf   & $ 9/ 7$& $11/ 7$& $ 2/ 7$ & \\
    \midrule
    \Xicz
    & $\tau[\fs]$ & $73.3 \pm 4.7$& $84.3 \pm 3.9$& $77.3 \pm 3.6$ & $78.7\pm2.3$\\
    & \chisqndf   & $ 6/ 6$& $12/ 6$& $ 9/ 6$  \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Magnetic polarity.}%
\label{par:magnetic_polarity_}
To check that results of each magnetic polarity are consistent with each other,
the data and simulation samples are split by year and magnetic polarity.
The prompt yield and decay-time fits are performed for each sub-sample, respectively.
The results are summarised in Table~\ref{tab:mag}
and illustrated in Fig.~\ref{fig:combine}.
It can be seen that results of different polarities
are compatible with each other within $1.5\sigma$ statistical uncertainty for \Omegac mode
and within $2.5\sigma$ statistical uncertainty for \Xicz mode.
\begin{table}[tb]
  \centering
  \caption{Decay-time fit in each data-taking period and magnetic polarity.}
  \label{tab:mag}
  \begin{tabular}{l c c c c}
    \toprule
    &  & 16 & 17 & 18 \\
    \midrule
    \Omegac MagDown & $\tau[\fs]$ & $  223 \pm  48$& $  164 \pm  25$& $  149 \pm  24$ \\
    \Omegac MagUp   & $\tau[\fs]$ & $  200 \pm  29$& $  199 \pm  22$& $  178 \pm  31$ \\
    \midrule
    \Xicz MagDown   & $\tau[\fs]$ & $77.9 \pm 7.0$& $75.5 \pm 5.5$& $80.4 \pm 5.4$ \\
    \Xicz MagUp     & $\tau[\fs]$ & $67.0 \pm 6.5$& $89.4 \pm 6.1$& $73.7 \pm 5.0$ \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Reduced decay-time bins.}%
\label{par:reduced_decay_time_bins_}
To check that the measured lifetime dose not vary significantly 
with the choice of the lower edge of the decay-time region,
the decay-time fit is performed with reduced decay-time bins,
in which the first two bins in the default binning scheme are removed.
The measured lifetime is $176\pm16\fs$ for \Omegac and $73.8\pm3.4\fs$ for \Xicz,
both in good agreement with default results within statistical uncertainties.

\paragraph{Alternative binning scheme.}%
\label{par:alternative_binning_scheme_}
To check that the measured lifetime is stable w.r.t the choice of binning scheme,
an alternative binning scheme,
\begin{equation}
  [0.45,0.49,0.54,0.59,0.65,0.71,0.77,0.84,0.94,1.15,2.00]\ps,
\end{equation}
is used to perform the lifetime measurement.
Fig.~\ref{fig:binning} shows a comparison of the default (in blue)
and alternative (in red) binning scheme.
The data and simulated samples are split according to
the alternative decay-time bins and
the procedure to measure lifetimes are repeated.
The resulting difference in the measured lifetime
is 0.4\fs for \Omegac and 2.5\fs for \Xicz,
both in good agreement with the nominal results.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.8\linewidth]{ana_omegac/syst/binning.pdf}
  \caption{Default (in blue) and alternative (in red) binning scheme in \ps.}
  \label{fig:binning}
\end{figure}

\subsection{Additional checks}%
\label{sub:additional_checkes}

Several additional checks are performed to further validate the results.

\paragraph{Fit with shared secondary fractions.}%
\label{par:fit_with_shared_secondary_fractions_}
The secondary fraction between data-taking periods
is found to be different for \Dz control mode.
To check that there is no mismodelling effect,
we perform the fit to the invariant-mass and \logip distribution
with secondary fraction $f_{s,i}$ in each decay time bin
shared in 2016-2018 data-taking year.
The resultant secondary fractions are shown in Fig.~\ref{fig:sfraction_share}.
The relative difference of the \Dz prompt yield in each decay-time bin
is shown in Table~\ref{tab:yield_diff}.

This effect is propagated to the measured signal lifetimes with pseudoexperiments.
In each pseudoexperiment,
the prompt data yield $N^{\mathrm{sig}}_i$ and $N^{\mathrm{con}}_i$
in each decay-time bin are sampled from Gaussian distributions
whose means are the default prompt yields and
widths are the default prompt yield times the 
relative difference in Table~\ref{tab:yield_diff}.
Then the \chisq fit to the decay time is repeated
with the sampled prompt data yields.
The resulting lifetime distribution from pseudo-experiments
is fitted with a Gaussian function 
whose width is taken as the uncertainty on the measured lifetime.
The resultant value is $0.9\fs$ for \Omegac lifetime 
and $0.4\fs$ for \Xicz lifetime,
which indicates that the effect is under control 
with the constraint of secondary fraction.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_omegac/share_sfraction/sfraction_D0_run2_share.pdf}
  \caption{Secondary fraction and its uncertainty with shared fractions in 2016-2018.}
  \label{fig:sfraction_share}
\end{figure}
\begin{table}[tb]
  \centering
  \caption{Relative difference (in \%) of the \Dz prompt yield
  with different fit configuration.}
  \label{tab:yield_diff}
  \begin{tabular}{c c c c c c c c c c}
    \toprule
    Year & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\
    \midrule
    16 &   3.3 &   1.1 &   2.8 &   2.2 &   1.9 &   0.8 &   0.7 &   0.9 &   1.0 \\
    17 &   0.7 &   0.8 &   0.3 &   0.2 &   0.8 &   0.5 &   0.6 &   0.2 &   0.3 \\
    18 &   0.7 &   1.1 &   0.9 &   0.8 &   0.0 &   0.2 &   0.4 &   0.2 &   0.2 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Ratios of raw prompt yields.}%
\label{par:ratios_of_raw_prompt_yields_}
As a post-unblinding check,
the prompt yield ratio of signal to control mode in data,
$N^{\mathrm{sig}}/N^{\mathrm{con}}$,
and the fit result,
$C_{\mathrm{year}} \times F \times M^{\mathrm{sig}} / M^{\mathrm{con}}$,
is show in Fig.~\ref{fig:nominal_decaytime_ratio_run2} for 2016-2018 data, and 
shown in Fig.~\ref{fig:nominal_decaytime_ratio} for each data-taking period.
Good agreement between the data and the fit can be observed.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/fit_data/Omegac_run2_simul_ratio.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/fit_data/Xic0_run2_simul_ratio.pdf}
  \caption{Prompt yield ratio of signal to control mode in 2016-2018 data
  for (left) \Omegac and (right) \Xicz mode,
  along with the fit result.}
  \label{fig:nominal_decaytime_ratio_run2}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_16_simul_ratio.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_17_simul_ratio.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Omegac_18_simul_ratio.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_16_simul_ratio.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_17_simul_ratio.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/Xic0_18_simul_ratio.pdf}
  \caption{Prompt yield ratio of signal to control mode in data
  for each data-taking period and for (top) \Omegac and (bottom) \Xicz mode,
  along with the fit result.}
  \label{fig:nominal_decaytime_ratio}
\end{figure}

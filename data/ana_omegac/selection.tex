\section{Event selection}%
\label{sec:event_selection}

The event reconstruction and selection are performed
in a data flow consisting several steps, 
as introduced in Sec.~\ref{sub:the_lhcb_data_flow}.
In this section, selection requirements at different steps are shown in detail.
The online trigger selection has to make a comprise due to limited
readout rate and storage bandwidth,
while the offline selection can be implemented with more involved algorithms.

\subsection{Trigger selection}%
\label{sub:turbo_selection}

Selection requirements in both the hardware and software level trigger
are applied in order to reduce the sample size and improve the signal purity.

\paragraph{L0 trigger.}%
\label{par:l0_trigger_}
To have a good estimation of L0 trigger efficiency,
we require \texttt{L0Hadron} TOS ($h$TOS) on the \Omegac, \Xicz and \Dz candidates
for corresponding decay modes.
According to simulation, $h$TOSed prompt signals constitute
around half of the total L0-triggered events.

\paragraph{HLT1 trigger.}%
\label{par:hlt1_trigger_}
The HLT1 TOS requirement of \texttt{Hlt1Track||Hlt1TwoTrack}
is applied to the \Omegac, \Xicz and \Dz candidates
for corresponding decay modes, which is embedded in the signal HLT2 line.
The HLT1 track lines and their performance
are discussed in Sec.~\ref{sub:performance_of_the_lhcb_detector}. 
According to simulation,
for \Dz prompt signals the ratio of \texttt{Hlt1Track} TOS only,
\texttt{Hlt1TwoTrack} TOS only,
and \texttt{Hlt1Track\&\&Hlt1TwoTrack} TOS is about 1:2:7.
The ratio obtained from the fit to data is about 1:3:6,
which is similar to the value from simulation.

\paragraph{HLT2 trigger.}%
\label{par:hlt2_trigger_}
In HLT2 trigger,
exclusive decays of \Omegac and \Xicz baryon are reconstructed in the $p\Km\Km\pip$
final state with a set of rectangular requirements.
The major selection requirements are summarised in Table~\ref{tab:turbo_signal}
for signal modes and in Table~\ref{tab:turbo_control} for the control mode.
For the \Omegac and \Xicz signal mode, the signal candidate is reconstructed in three steps:
\begin{enumerate}
  \item Selection of final-state tracks (DaughtersCuts).
    Each of the final-state particles is required to have a good track quality,
    a large transverse (\pt) and total ($p$) momentum,
    and particle-identification information (DLL)
    consistent with the corresponding $p$, $\Km$, or $\pip$ hypothesis.
    All final-state tracks are required to be inconsistent with 
    originating from any PV. 
    The PV associated to a single charged particle is defined to be 
    the PV with the smallest \chisqip.
    The \chisqip is defined as the difference 
    in the vertex-fit \chisq (\chisqvtx) of a given PV 
    reconstructed with and without the particle under consideration.
  \item Requirement of the combination of tracks (CombinationCuts).
    The scalar sum and the maximum of the \pt of final-state tracks is required to be large.
    The maximum and second maximum \chisqip of the final-state tracks is required to be large.
  \item Requirement on the signal candidate (MotherCuts).
    Charmed hadron candidates are required to have a decay vertex with good quality
    that is displaced from its associated PV.
    The decay time is required to be larger than 0.1\ps.
    The angle between the reconstructed momentum vector of a charmed baryon candidate 
    and the direction from its associated PV to its decay vertex, 
    the direction angle (acos(DIRA)),
    is required to be small to suppress combinatorial background.
\end{enumerate}
For the \Dz control mode,
similar requirements are applied in the above three steps.
In addition,
the distance of the closest approach (DOCA) between final-state tracks 
is required to be small for a good vertex quality. 
The \Omegac and \Xicz signal decays are filtered with the same selection,
except for the window of the reconstructed mass.
The requirements shown in red in Table~\ref{tab:turbo_signal}
are also applied offline to the control mode,
such that the effect of potential mismodelling of IP and decay-time resolution
can be further reduced in the efficiency ratio of signal to control modes.
To get an idea of the signal purity after the trigger selection,
the invariant mass distributions of 2018 data samples after the trigger selection
are shown in Fig.~\ref{fig:mass_turbo_output}.
\begin{table}[tb]
  \centering
  \caption{HLT2 selections for the \Omegac and \Xicz signal mode.
  Requirements shown in red are also applied to the control mode offline.}
  \label{tab:turbo_signal}
  \begin{tabular}{crl}
    \toprule
    Items & Variables & Requirements \\
    \midrule
    \multirow{7}{*}{DaughtersCuts} & $\pt$ & $>0.5\gev$ \\
    & $p$ & $>1\gev$ \\
    & \textcolor{red}{$\chisqip$} & $>4$ \\
    & Proton $\dllppi$ & $>10$ \\
    & Proton $\mathrm{DLL}_{\proton\kaon}$ & $>5$ \\
    & Kaon $\dllkpi$ & $>10$ \\
    & Pion $\dllkpi$ & $<0$  \\
    \midrule
    \multirow{4}{*}{CombinationCuts} & $\Sigma\pt$ & $>3\gev$ \\
    & Max(\pt) & $>1\gev$ \\
    & \textcolor{red}{Max(\chisqip)} & $>8$ \\
    & \textcolor{red}{Second max(\chisqip)} & $>6$ \\
    \midrule 
    \multirow{4}{*}{MotherCuts} & $\chisqvtxndf$ & $<10$ \\
    & Decay time & $>0.1\ps$ \\
    & $\chisq_{\mathrm{VD}}$ & $>10$ \\
    & \textcolor{red}{DIRA} & $>\cos(0.01)$ \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{HLT2 selections for the \Dz control mode.
  The index $i$ refers to any of the first three daughters in the \Dz decay.}
  \label{tab:turbo_control}
  \begin{tabular}{crl}
    \toprule
    Items & Variables & Requirements \\
    \midrule
    \multirow{5}{*}{DaughtersCuts} & $\pt$ & $>0.25\gev$ \\
    & $p$ & $>1\gev$ \\
    & $\chisqip$ & $>3$ \\
    & Kaon $\dllkpi$ & $>5$ \\
    & Pion $\dllkpi$ & $<5$ \\
    \midrule
    \multirow{4}{*}{CombinationCuts} & $\Sigma\pt$ & $>1.8\gev$ \\
    & $p$ & $>25\gev$ \\
    & $\mathrm{DOCA}(\mathrm{i},4)$ & $<100$ \\
    & $\chisq_{\mathrm{DOCA}}(\mathrm{i},4)$ & $<10$ \\
    \midrule
    \multirow{6}{*}{MotherCuts} & \pt & $>2\gev$ \\
    & $p$ & $>30\gev$ \\
    & $\chisqvtxndf$ & $<12$ \\
    & Decay time & $>0.1\ps$ \\
    & $\chisq_{\mathrm{VD}}$ & $>25$ \\
    & DIRA & $>\cos(0.02)$ \\
    \midrule
    \multirow{2}{*}{\Dstar Cuts} & $\chisqvtxndf$ & $<15$ \\
    & $M(\Dstarp) - M(\Dz) - M(\pip)$ & $\in[-5.0,30.43]\mev$ \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/turbo_output/OmegacM_turbo_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/turbo_output/Xic0M_turbo_18.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/turbo_output/D0M_turbo_18.pdf}
  \caption{Invariant mass distribution of (left) \Omegac, (middle) \Xicz, and (right) \Dz
  candidates in 2018 data samples after the trigger selection.}
  \label{fig:mass_turbo_output}
\end{figure}


\subsection{Offline selection}%
\label{sub:offline_selection}

The offline event selections are applied in several steps
in order to further suppress the background.

\paragraph{Preselection.}%
\label{ssub:pre_selection_and_mass_window}
After the trigger selection,
loose preselections, with very high signal efficiency,
are applied to both signal and control modes to reduce obvious backgrounds,
as shown in Table.~\ref{tab:preselection}.
These requirements reject candidates whose final-state tracks
are too far away from PV and whose vertex quality is too poor.
An additional mass window requirement of $|M(\Dstarp) - M(\Dz) - 146|<6\mev$
is applied to \Dz control mode to improve the signal purity.
\begin{table}[tb]
  \centering
  \caption{Offline preselection requirements,
  where $H_c$ denotes either \Omegac, \Xicz or \Dz hadron.}
  \label{tab:preselection}
  \begin{tabular}{rl}
    \toprule
    Variables & Requirements \\
    \midrule
    $\ln \chisq_{\mathrm{FD}}(H_c)$ & $<9$ \\
    $\ln \mathrm{Sum}(\mathrm{final\, state} \, \chisqip)$ & $<8$ \\
    $\ln \mathrm{Min}(\mathrm{final\, state} \, \chisqip)$ & $<5$ \\
    $\chisqvtx(H_c)$ & $<20$ \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{Multivariate analysis.}%
\label{ssub:bdt}
A multivariate classifier based on the Boosted Decision Tree (BDT) algorithm 
implemented in the TMVA toolkit~\inlinecite{Hocker:2007ht,TMVA4}
is trained to further suppress the combinatorial background.
Simulated prompt \Omegac decays with an input lifetime of $250\fs$
are used as the signal sample in the training.
The \Omegac data samples in the invariant mass sideband ($[2620,2670]\cup[2720,2770]\mev$)
are used as the background sample.
Eleven variables that show good discriminating power between signal and background candidates
are used as training variables, including
\begin{itemize}
  \item Topological variables: $\chisq_{\mathrm{vtx}}(\Omegac)$,
    DIRA of \Omegac, 
    natural log of the sum of \chisqip of four final-state tracks,
    and natural log of the minimum of \chisqip of four final-state tracks.
  \item Kinematic variables: $\pt(\Omegac)$, $\eta(\Omegac)$,
    \pt of the four final-state tracks,
    and minimum of the \pt of four final-state tracks.
\end{itemize}
Distributions of training variables for signal and background samples
are shown in Fig.~\ref{fig:bdt_training}.
Correlation matrices of signal and background samples 
are shown in Fig.~\ref{fig:bdt_correlation}.
BDT responses of training and test samples 
are shown in Fig.~\ref{fig:bdt_response}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.90\linewidth]{ana_omegac/TMVA/inputvar_c1.pdf}
  \includegraphics[width=0.90\linewidth]{ana_omegac/TMVA/inputvar_c2.pdf}
  \caption{Distributions of training variables in 2016 \Omegac simulation (Signal) and 
  \Omegac mass-sideband data (Background).}
  \label{fig:bdt_training}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/TMVA/CorrelationMatrixS.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/TMVA/CorrelationMatrixB.pdf}
  \caption{Correlation matrices of (left) signal and (right) background samples.}
  \label{fig:bdt_correlation}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_omegac/TMVA/overtrain_BDT.pdf}
  \caption{BDT responses of training and test samples.}
  \label{fig:bdt_response}
\end{figure}

Distributions of the BDT response for simulated \Omegac sample and
the background subtracted \Omegac data sample are shown in 
the left plot in Fig.~\ref{fig:data_response}.
Background subtraction is performed with the mass fit
to the \Omegac data sample before the BDT selection.
The discrepancy of the responses between data and simulation
is due to the discrepancy in some of the training variables.
To improve the signal purity while not biasing the decay-time distribution,
a loose BDT cut of $-0.2$ is applied to the \Omegac signal mode.
The same BDT selection is applied to the \Xicz signal mode.
No BDT selection is applied to the \Dz control mode.
The comparison of the BDT response for the \Xicz signal mode is
shown in the right plot in Fig.~\ref{fig:data_response}.
The background-like tail in the left of the data distribution
is due to the large secondary contributions in the \Xicz mode.
It should be noticed that we do not expect 
the distribution in simulation is the same as in data,
as the input lifetime in simulation is not the same as the true value.
%To understand the origin of the data-simulation disagreement,
%comparisons of distributions of the input variables and other variables of interest
%are performed in Appendix~\ref{sec:compare_data_mc_sweight}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/TMVA/data_BDT.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/TMVA/compare_BDT_Xic0_16.pdf}
  \caption{Distribution of BDT response 
  for 2016 (left) \Omegac simulated sample (with input lifetime of 250\fs)
  and the background subtracted data sample,
  and (right) \Xicz simulated sample (with input lifetime of 250\fs)
  and the background subtracted \Xicz data sample.}
  \label{fig:data_response}
\end{figure}


\paragraph{PID requirements.}%
\label{ssub:pid_requirements}
Besides the PID selections applied in the trigger,
further PID requirements are applied to both signal and control modes
after the BDT selection, including
\begin{itemize}
  \item $\dllppi>15$ and $\mathrm{DLL}_{\proton\kaon}>5$ for protons,
  \item $\dllkpi>13$ for kaons,
  \item $\dllkpi<0$ for pions.
\end{itemize}

\paragraph{Removal of track-clone candidates.}%
\label{ssub:removal_of_track_clone_candidate}
Track-clone candidates refer to those candidates
in which at least one pair of final-state tracks are clones of each other.
Two reconstructed tracks are clones of each other 
if they are reconstructed from a set of hits generated by one genuine track.
Fig.~\ref{fig:clones} shows the distributions of log-sized $\min(\theta_{i,j})$
for \Omegac and \Xicz 2018 data,
where $\min(\theta_{i,j})$ for a given candidate
is defined as the minimum of the angles between any pair of the four charged final-state tracks.
It is found that the contribution of clones is at 10\% level.
A requirement of $\min(\theta_{i,j})>0.5\mrad$ (indicated in red dashed line in the figure) 
is applied to remove track-clone candidates with a signal efficiency close to 100\%.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/selection/Omegac_min_theta.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/selection/Xic0_min_theta.pdf}
  \caption{Distributions of log-sized $\min(\theta_{i,j})$ in 2018 (left) \Omegac 
  and (right) \Xicz data.}
  \label{fig:clones}
\end{figure}

As an illustration of the signal purity,
the invariant mass distributions of 2016 signal and control samples
after all event selection criteria are shown in Fig.~\ref{fig:mass_total_16}.
The total signal yields (prompt and secondary signals combined)
in the decay time range of $[0.45,2.00]\ps$ are shown in Table~\ref{tab:total_yield}.
The time-integrated selection efficiency and
the figure of merit ($\frac{S}{\sqrt{S+B}}$ where $S$ and $B$ are calculated
in $2.5\sigma$ region around the \Omegac mass peak)
after different selection stages for 2016 \Omegac data
is shown in Table~\ref{tab:significance}.
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/mass_data_Omegac_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/mass_data_Xic0_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/fit_data/mass_data_D0_16.pdf}
  \caption{Invariant mass distributions of 2016 (left) \Omegac
  (middle) \Xicz and (right) \Dz data samples
  after all event selection criteria.}
  \label{fig:mass_total_16}
\end{figure}
\begin{table}[tb]
  \centering
  \caption{Total signal yields (prompt and secondary components combined)
  in the decay time range of $[0.45,2.00]\ps$
  after all event selection for signal and control modes.}
  \label{tab:total_yield}
  \begin{tabular}{c c c c}
    \toprule
    & \Omegac $[\times10^{3}]$ & \Xicz $[\times10^{3}]$ & \Dz $[\times10^{3}]$\\
    \midrule
    2016 & $4.3 \pm 0.2$ & $16.6 \pm 0.2$ & $126.9 \pm 0.5$  \\
    2017 & $5.6 \pm 0.2$ & $20.6 \pm 0.2$ & $170.3 \pm 0.5$  \\
    2018 & $5.9 \pm 0.2$ & $23.0 \pm 0.2$ & $182.2 \pm 0.6$  \\
    \bottomrule
  \end{tabular}
\end{table}
\begin{table}[tb]
  \centering
  \caption{Efficiency and figure of merit $\frac{S}{\sqrt{S+B}}$ 
  for 2016 \Omegac data after different selection stages.}
  \label{tab:significance}
  \begin{tabular}{lcc}
    \toprule
    Stages & Signal efficiency &$\frac{S}{\sqrt{S+B}}$ \\
    \midrule
    Pre-selection   & 94\% & 48.7 \\
    $+$ BDT$>-0.2$  & 93\% & 60.1 \\
    $+$ offline PID & 80\% & 64.5 \\
    \bottomrule
  \end{tabular}
\end{table}

\section{Systematic uncertainties}%
\label{sec:systematic_uncertainties}

Sources of systematic uncertainties on the measured lifetimes 
of the \Omegac and \Xicz baryon are considered, including:
\begin{itemize}
  \item Mismodelling of the prompt signal yield;
  \item Limited statistics of calibration sample;
  \item Precision of kinematic correction;
    %\item Binning scheme of the decay time;
  \item Input lifetime of the simulation samples;
  \item Decay time resolution;
    %\item Decay time acceptance;
    %\item ReFit PV issue;
  \item VELO hit error;
  \item Decay length scale;
  \item Uncertainty of \Dz lifetime;
  \item \Dz mixing.
\end{itemize}
In general,
systematic uncertainties are considered by
repeating the decay-time fit (and the two-dimensional $(m,\logip)$ fit)
with alternative configurations.
The numerical results are summarised in Table~\ref{tab:syst}
and are discussed in the following subsections in detail.
\begin{table}[b]
  \centering
  \caption{Summary of systematic uncertainties.}
  \label{tab:syst}
  \begin{tabular}{r  cc}
    \toprule
    Sources & \Omegac [\fs] & \Xicz [\fs] \\
    \midrule
    Fit model                   & 2.2 & 1.0 \\
    %Secondary fraction          & 0.9 & 0.4 \\
    Limited calibration sample  & 0.1 & 0.1 \\
    Kinematic correction         & 3.4 & 0.4 \\
    %L0 $h$TOS           & 2   & 2.7 \\
    %$\pt$ correction & 3   & 1.8 \\
    %$y$ correction   & 2   & 1.5 \\
    %MVA             & ?   & 2.0 \\
    %Binning scheme     & 1   & 2.5 \\
    %MC input lifetime  & & \\
    Decay time resolution       & 1.3  & 1.8 \\
    %Decay time acceptance       & 1.3  & 0.7 \\
    %ReFit PV issue              & 0.3  & 0.1 \\
    %ReFit PV issue              & 1.5  & 3.8 \\
    %VELO hit error              & 1.8  & 0.8 \\
    VELO hit error              & 1.1  & 0.5 \\
    Decay length scale          & 0.1  & 0.1 \\
    \Dz mixing                  & 0.8  & 0.6 \\
    \midrule
    Total syst.       & \omegacsys  & \xiczsys \\
    \midrule
    \Dz lifetime      & 0.7         & 0.2 \\ 
    \midrule
    Total stat.       & \omegacstat & \xiczstat \\
    \bottomrule
  \end{tabular}
\end{table}


\subsection{Modelling of prompt yield}%
\label{sub:the_prompt_yield_fit_model}

As mentioned in Sec.~\ref{sec:prompt_yield_extraction},
several parameters in the signal model are fixed to values obtained from simulation.
The effect on the fitted yield is studied
with the \Dz control mode by removing these constraints of parameters 
in the alternative fits by turns,
including $\sigma_p$, $\sigma_s$, $\xi_p$, $\xi_s$,
$\rho_{1p}$, $\rho_{1s}$, $\rho_{2p}$, and $\rho_{2s}$.

In the default fit model of prompt yield,
parameter $\Delta\mu_p$ and $\Delta\mu_s$ in the signal models 
are shared across decay-time bins.
This effect on the fitted yield is studied with the \Dz control mode 
by allowing them to be different in each decay-time bins in the alternative fits.

The background \logip distribution is modelled with data in the invariant-mass sideband.
An alternative background distribution obtained with
the background sWeights from the invariant-mass fit 
is used in the alternative fits to estimate the bias due to the background modelling.

Prompt yields obtained in these alternative fits
are shown in Fig.~\ref{fig:syst_yield} for the 2016 \Dz sample.
Taking half of the difference between the maximum and minimum yield 
in each bin as systematic uncertainty,
the relative uncertainty of the prompt yield in each bin is 
\begin{equation}
  [2.2, 6.1, 4.2, 2.3, 2.9, 2.1, 2.1, 4.0, 3.1] \%.
\end{equation}

The uncertainty due to the fit model 
is propagated to the measured decay time with pseudoexperiments.
In each pseudoexperiment,
the prompt data yields $N^{\mathrm{sig}}_i$ and $N^{\mathrm{con}}_i$ 
in each bin are sampled from Gaussian distributions
whose means are the nominal prompt yields and
widths are the nominal prompt yield times the relative systematic uncertainty.
Then the chi-squared fit to the decay time is repeated
with the sampled prompt data yields.
The resulting lifetime distribution from pseudoexperiments
is fitted with a Gaussian function whose width is taken as the systematic uncertainty.
The numerical result is shown in Table~\ref{tab:syst}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=.6\linewidth]{ana_omegac/syst/syst_D0_16_update.pdf}
  \caption{Prompt yields of the \Dz mode with alternative configurations in 2016 data.}
  \label{fig:syst_yield}
\end{figure}

%\subsection{Difference of secondary fraction between years}%
%\label{sub:difference_of_secondary_fraction_between_years}
%
%To estimate the effect due to different secondary fractions between data-taking periods,
%we perform the fit to the mass and \logip distribution 
%with secondary fraction $f_{s,i}$ in each decay time bin
%shared in 2016--2018 data-taking year.
%The resultant secondary fractions are shown in Fig.~\ref{fig:sfraction_share}.
%The relative difference of the \Dz prompt yield in each decay-time bin
%is shown in Table~\ref{tab:yield_diff}.
%
%This effect is propagated to measured signal lifetimes with pseudo-experiments.
%In each pseudo-experiment,
%the prompt data yield $N^{\mathrm{sig}}_i$ and $N^{\mathrm{con}}_i$
%in each decay-time bin are sampled from Gaussian distributions
%whose means are the nominal prompt yields and
%widths are the nominal prompt yield times the relative difference in Table~\ref{tab:yield_diff}.
%Then the chi-squared fit to the decay time is repeated
%with the sampled prompt data yields.
%The resulting lifetime distribution from pseudo-experiments
%is fitted with a Gaussian function whose width is taken as the systematic uncertainty.
%The resultant value is $0.9\fs$ for \Omegac lifetime and $0.4\fs$ for \Xicz lifetime.
%
%\begin{figure}[htb]
%  \centering
%  \includegraphics[width=0.5\linewidth]{ana_omegac/share_sfraction/sfraction_D0_run2_share.pdf}
%  \caption{The secondary fraction and its uncertainty with shared fractions in 2016--2018.}
%  \label{fig:sfraction_share}
%\end{figure}
%
%\begin{table}[htpb]
%  \centering
%  \caption{The relative difference (in \%) of the \Dz prompt yield
%  with different fit configuration.}
%  \label{tab:yield_diff}
%  \begin{tabular}{c | c c c c c c c c c}
%    Year & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\
%    \hline
%    16 &   3.3 &   1.1 &   2.8 &   2.2 &   1.9 &   0.8 &   0.7 &   0.9 &   1.0 \\
%    17 &   0.7 &   0.8 &   0.3 &   0.2 &   0.8 &   0.5 &   0.6 &   0.2 &   0.3 \\
%    18 &   0.7 &   1.1 &   0.9 &   0.8 &   0.0 &   0.2 &   0.4 &   0.2 &   0.2 \\
%  \end{tabular}
%\end{table}


\subsection{Limited statistics of calibration samples}%
\label{sub:limited_statistics_of_calibration_samples}

As discussed in Sec.~\ref{sec:efficiency_estimation},
calibration data samples are used to correct for some data-simulation discrepancy.
We consider the systematic uncertainties due to limited statistics of calibration data samples
with pseudo-experiments.
For this purpose, the calibration efficiency tables obtained from calibration data
are varied by changing the value in each bin 
according to its value and uncertainty assuming Gaussian distribution
in each pseudo-experiment.
Then the per-event weights are re-evaluated and
the chi-squared fit is performed with fluctuated weights to obtain the lifetime.
The width of the distribution of the measured lifetime is assigned as systematic uncertainty.

The procedure is performed for PID and L0 efficiency tables.
For PID efficiency, the width is found to be negligible for both \Omegac and \Xicz mode.
This is expected as the PID efficiency is only weakly time-dependent.
For L0 efficiency, the width is found to be 0.13\fs for \Omegac mode
and 0.06\fs for \Xicz mode.

%\subsection{L0 $h$TOS efficiency}%
%\label{sub:the_l0_htos_efficiency}
%
%The L0 $h$TOS efficiency is calculated with the L0 efficiency tables,
%as discussed in Sec.~\ref{sub:l0_efficiency_correction}.
%To estimate the systematic uncertainty due to the L0 efficiency modelling,
%an alternative method is used to calculate the L0 efficiency
%in which we require \texttt{L0\_HadronDecision\_TOS} of the charmed hadron in question
%is \texttt{True} for each candidate.
%The chi-squared fit to the decay time is repeated
%with the alternative prompt yield $M_i$ in simulation.
%The resulting difference in the decay-time fit is 
%assigned as systematic uncertainty,
%as shown in Table~\ref{tab:syst}.

\subsection{Kinematic corrections}%
\label{sub:kinematic_corrections}

As discussed in Sec.~\ref{sub:kinematic},
per-event weights are applied to simulation samples 
to correct for the data-simulation discrepancy in kinematic distributions
(\pt and $y$ of the charmed hadron and \pt of the final-state particles).
The binned prompt background subtracted data distribution is obtained
by performing two-dimensional mass and \logip fits in bins of the variable under consideration.
The precision of the kinematic correction is largely limited by the statistical uncertainty
of the data distribution obtained from the fit, especially for the \Omegac mode.

The effect of the uncertainty of the correction factors on the measured lifetime
is quantified with pseudo-experiments.
In each pseudoexperiment, 
the binned prompt background subtracted kinematic distributions in data are fluctuated, 
following Gaussian distribution, according to its value and uncertainty in each bin,
and the per-event weight of kinematic correction is re-calculated.
The decay time is then measured with re-calculated per-event weight.
A fit to the resulting distribution of measured decay time 
is performed with a Gaussian function,
whose width is taken as the systematic uncertainty 
due to the limited precision of the kinematic correction.
The numerical results for \Omegac and \Xicz mode are shown in Table~\ref{tab:syst}.

\subsection{Input lifetime of simulated samples}%
\label{sub:input_lifetime_of_simulated_samples}

Simulated signal samples used in the nominal fit
is generated with an input lifetime of $\tau_0=250\fs$
for both \Xicz and \Omegac signal modes.
To study the effect of different input lifetimes,
the simulated samples are re-weighted
w.r.t. the true decay time $t_{\mathrm{true}}$ by
\begin{equation}
  w(t_{\mathrm{true}}) = \frac{\frac{1}{\tau}\text{exp}(-\frac{t_{\mathrm{true}}}{\tau})}{\frac{1}{\tau_0}\text{exp}(-\frac{t_{\mathrm{true}}}{\tau_0})}
\end{equation}
to several alternative lifetimes $\tau$.
The alternative lifetimes are chosen to be values
within about seven times statistical uncertainty region 
of the measured central value.
The chi-squared fit to the decay time is repeated
with the alternative prompt simulation yield $M_i$.
The resulting difference in lifetime with different input lifetimes is negligible,
as shown in Fig.~\ref{fig:syst_input_lifetime}.
Note that the $x$-axis is blinded in Fig.~\ref{fig:syst_input_lifetime}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=.4\linewidth]{ana_omegac/syst/Omegac_tau_shift_witherr.png}
  \includegraphics[width=.4\linewidth]{ana_omegac/syst/Xic0_tau_shift_witherr.png}
  \caption{Measured lifetimes of (left) \Omegac and (right) \Xicz baryon
  with different input lifetimes of simulated samples. The $x$-axis is blinded.}
  \label{fig:syst_input_lifetime}
\end{figure}

\subsection{Decay-time resolution}%
\label{sub:time_resolution}

In the default fit, 
the decay-time resolution is taken as modeled by simulation.
The distributions of the difference of reconstructed and true decay time
in simulation are shown in Fig.~\ref{fig:decaytime_reso} for all modes.
The decay-time resolutions are estimated by fitting with a Gaussian function
and are found to be 57\fs, 65\fs, and $56\fs$
for \Omegac, \Xicz, and \Dz modes, respectively.

The control mode $\Dstarp \to \pip \Dz (\to \Km \Kp \pim \pip)$ is used to quantify
the level of difference of decay-time resolution between data and simulation.
The decay time is defined in Eq.~\ref{eq:decaytime_def} and
the most important contribution to the decay-time resolution is
the resolution of the decay vertex of the charmed hadron.
To be able to compare the resolution observed in data and in simulation,
we calculate the \Dz decay-time with the \Dz decay vertices reconstructed with 
both $\Km \pip$ and $\Kp \pim$ final-state tracks
%(with the \texttt{TupleTool} introduced here~\footnote{\tiny https://indico.cern.ch/event/897153/contributions/3783880/attachments/2004694/3347766/SVR\_Run12Perf\_WG\_Pres.pdf}),
denoted as $t_{\Km \pip}$ and $t_{\Kp \pim}$, respectively.
The difference of $t_{\Km \pip}$ and $t_{\Kp \pim}$ is taken as 
a measure of the decay-time resolution.

To minimize the difference introduced due to kinematic effects,
several corrections are applied to MC,
including the tracking efficiency, the PID efficiency,
the efficiency due to the $h$TOS requirement,
and phasespace distributions.
The momentum of \Dz between data and simulation are also re-weighted to be the same,
as shown in Fig.~\ref{fig:D0P_weight}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/D0_PX_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/D0_PY_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/D0_PZ_Pweight.pdf}
  \caption{Momentum distributions of \Dz in 2018 data and simulation for \Dz sample.}
  \label{fig:D0P_weight}
\end{figure}
We also check some other variables related to the decay time resolution,
including the angle between \Km \pip (\Kp \pim) from \Dz, \pt of \Dz daughters,
as shown in Fig.~\ref{fig:D0P_weight_other}.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Theta_Kp_Pim_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Kp_PT_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Pip_PT_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Theta_Km_Pip_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Km_PT_Pweight.pdf}
  \includegraphics[width=0.3\linewidth]{ana_omegac/decay_time_resolution/Pim_PT_Pweight.pdf}
  \caption{Distributions of (left) the angle between \Dz daughters and (middle and right) \pt of \Dz daughters in 2018 data and simulation.}
  \label{fig:D0P_weight_other}
\end{figure}

The distribution of $t_{\Km \pip} - t_{\Kp \pim}$ in data and MC
are shown in Fig.~\ref{fig:fit_resolution}.
A maximum likelihood fit is performed to extract the resolution.
The distribution of $t_{\Km \pip} - t_{\Kp \pim}$
can be well described by a double Gaussian function defined as
\begin{equation}
  \mathcal{G}(x; m_0, \sigma_t) = f\times\mathcal{G}_{\mathrm{Gaussian1}}(x) + (1-f)\times\mathcal{G}_{\mathrm{Gaussian2}}(x),
\end{equation}
and the effective resolution is
$\sigma_t=\sqrt{f \times \sigma_{t1}^2+(1-f) \times \sigma_{t2}^2}$.
The fit results show that the difference between data and MC is about 10\%.
In the pseudo-experiment below,
a conservative value of $20\fs$ is used as the difference of resolution
between data and simulation.
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/data_res_14vs23.pdf}
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/MC_res_14vs23_P_weight.pdf}
  \caption{The fit result of $t_{\Km \pip} - t_{\Kp \pim}$ in (left) data and (right) MC.}
  \label{fig:fit_resolution}
\end{figure}

The effect of different decay-time resolution in data and simulation
is estimated with pseudo-experiments for both \Omegac and \Xicz mode.
The pseudo-experiment is performed as follows:
\begin{enumerate}
  \item Generate pseudo-data and pseudo-mc samples according to 
    \begin{equation}
      \left( \exp ( -t/\tau ) \times Acc(t) \right) \ast Gaussian(\sigma_{t}),
    \end{equation}
    where $\tau$ is the input lifetime in simulation, $Acc(t)$ is
    the decay-time acceptance obtained from simulation,
    and $\sigma_{t}$ is the decay-time resolution.
    The decay-time resolution $\sigma_{t}$ is different by
    $20\fs$ in pseudo-data and pseudo-mc samples.
    Cases in which the resolution is either larger in data or in simulation are both considered.
    The total yield of the pseudo-data sample is normalised to that of the real data sample.
  \item The chi-squared fit to the decay time is performed with pseudo-data and pseudo-mc samples,
    following the same procedure as the nominal fit.
  \item Repeat the above two steps to get the distribution of difference between measured and input lifetimes.
  \item Fit the distribution of difference with a Gaussian distribution.
    The mean value of the Gaussian distribution is taken as the bias due to decay-time resolution.
\end{enumerate}

The distribution of difference between measured and input lifetimes
is shown in Fig.~\ref{fig:syst_time_resolution}.
Taking the maximum bias in both cases,
the systematic uncertainty due to decay-time resolution
is $1.31\fs$ and $1.81\fs$ for \Omegac and \Xicz modes, respectively.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.32\linewidth]{ana_omegac/syst/Omegac_reso_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/syst/Xic0_reso_16.pdf}
  \includegraphics[width=0.32\linewidth]{ana_omegac/syst/D0_reso_16.pdf}
  \caption{Distributions of the difference of reconstructed and true decay time
  in 2016 simulation for (left) \Omegac, (middle) \Xicz, and (right) \Dz modes.}
  \label{fig:decaytime_reso}
\end{figure}
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/Omegac_L20.pdf}
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/Xic0_L20.pdf}
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/Omegac_S20.pdf}
  \includegraphics[width=0.4\linewidth]{ana_omegac/decay_time_resolution/Xic0_S20.pdf}
  \caption{Distributions of difference between measured lifetimes and input lifetimes
  in pseudo-experiments.
  Cases in which the resolution is either $20\fs$ larger in data (top) or in simulation (bottom)
  are shown for both (left) \Omegac and (right) \Xicz modes.}
  \label{fig:syst_time_resolution}
\end{figure}

\subsection{Change of VELO hit error parametrization}%
\label{sub:change_of_velo_hit_error_parametrization}

As discussed in Sec.~\ref{sub:change_of_velo_parametrization_in_2017_2018_data},
the change of VELO hit error parametrization in 2017--2018 data can 
lead to a worse time-dependent agreement between data and simulation,
and this effect is corrected in the nominal fit
by applying a scaling factor to \chisqip variables of the final-state particles
for 2017--2018 simulation samples.

The uncertainty on the scaling factor is obtained
by making the \chisq test of the binned \chisqip distributions
in 2016 and 2018 data with different scaling factors.
A curve of scaling factor vs compatibility 
(in this case the \chisq from the \chisq test with the degrees of freedom of 50)
is shown in Fig.~\ref{fig:scale_factor_chisq}.
The range corresponding to one sigma internal is taken as the uncertainty
of the scaling factor, which is about 0.03.
The difference between the nominal lifetime and the alternative lifetime
obtained with a scaling factor of unit is 
$1.8\fs$ for \Omegac and $0.8\fs$ for \Xicz.
The systematic uncertainty due to the uncertainty of the scaling factor
is estimated by scaling the above difference with 0.03/0.05,
which leads to $1.1\fs$ for \Omegac and $0.5\fs$ for \Xicz.
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\linewidth]{ana_omegac/syst/scale_factor_chisq.pdf}
  \caption{The curve of scaling factor vs compatibility for (blue) Kaon and (red) Pion
  tracks from \Dz decay.}
  \label{fig:scale_factor_chisq}
\end{figure}


\subsection{Decay length scale}%
\label{sub:decay_length_scale}

The calculation of decay time requires information of the flight distance
between the original vertex (in this case PV) and the decay vertex.
The precision of this distance is dependent on the precision with which 
the relative position along the beam line ($z$-axis) of the LHCb modules.
There are two contributions: the  precision with which the VELO modules were assembled
and the track-based alignment.
%Following the discussion in LHCb-ANA-2012-049~\footnote{https://cds.cern.ch/record/1443886},
The overall relative uncertainty is assigned to be $\sigma_{z-scale} = 0.022\%$,
which corresponds to $0.06\fs$ for \Omegac and $0.03\fs$ for \Xicz.

\subsection{Hadronic interaction of final-state tracks}%
\label{sub:hadronic_interaction_of_final_state_tracks}

In tracking efficiency estimation,
the material effects for hadrons mainly come from the hadronic interactions with the material.
In most cases the interaction will be inelastic, thereby creating many secondary particles
while the original particle cannot be reconstructed anymore.
%Following the discussion in LHCb-PUB-2011-025~\footnote{https://cds.cern.ch/record/1402577}
%Sec.~3.4.1 and LHCb-ANA-2017-009~\footnote{https://cds.cern.ch/record/2242786} Sec.~6.4,
It is known from other LHCb analyses that
about 14\% of the pions, 11\% of the kaons, and 22\% of the protons
cannot be reconstructed due to hadronic interactions that occur before the
last tracking station. 
The MC simulation describes all the material effects discussed above. 
However, due to the uncertainty on the material budget and on the cross sections, 
the reconstruction efficiency obtained from simulation has an intrinsic uncertainty.
When assuming that the total material budget in the simulation has an uncertainty of 10\%
following LHCb-PUB-2011-025,
the systematic uncertainty due to material interactions is 
1.4\%, 1.1\%, and 2.2\% for pion, kaon, and proton, respectively.

Given the origin of this uncertainty,
the systematic uncertainty on hadronic interaction of tracks is assumed to be correlated
for different types, decay time bins and data-taking years
and propagated to the measured lifetime with pseudo-experiments.
In each pseudo-experiment, 
the efficiency in each decay time bin is varied according to the 
systematic uncertainty due to material interactions
and the width of the distribution of measured lifetime is assigned as systematic uncertainty.
The value is found to be $6.5\times10^{-3}\fs$ for \Omegac
and $1.1\times10^{-2}$ for \Xicz, which is negligible compared to other systematic uncertainties.


\subsection{Uncertainty of the \Dz lifetime}%
\label{sub:uncertainty_of_the_dz_lifetime}

The four-body \Dz decay is taken as the control channel of this measurement,
and the lifetime of \Dz used in the decay-time fit is the PDG value of $410.1\fs$.
The limited precision of \Dz lifetime, $1.5\fs$, is propagated to the measured lifetime of 
\Omegac and \Xicz with pseudo-experiments.
In each pseudo-experiment, the lifetime of \Dz, $\tau^{\mathrm{con}}$, in Eq.~\ref{eq:exp_ratio}
is randomized according to the Gaussian with the mean of $410.1\fs$ and the standard deviation of $1.5\fs$,
and the decay-time fit is repeated.
Then the spread in the distribution of the measured lifetime is taken as the systematic uncertainty,
as shown in Fig.~\ref{fig:syst_dz}.
The resultant uncertainty is $0.7\fs$ for \Omegac and $0.2\fs$ for \Xicz.

\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_dz_lifetime_omegac.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_dz_lifetime_xicz.pdf}
  \caption{Distribution of the measured lifetime of (left) \Omegac and (right) \Xicz
  with randomized $\tau^{\mathrm{con}}$ in the decay-time fit.}
  \label{fig:syst_dz}
\end{figure}

\subsection{\Dz mixing}%
\label{sub:charm_mixing}

The charm mixing of the \Dz meson is neglected in the nominal measurement.
As we do not distinguish $D^0$ and $\bar{D}^0$ in the analysis, the total time dependent decay rate is
\begin{equation}
  \label{eq:mixing}
  \frac{d\Gamma(D^0)}{dt} + \frac{d\Gamma(\bar{D}^0)}{dt} =N_f e^{-t/\tau} \{ (|A_f|^2 + |\bar{A}_f|^2)\cosh(yt/\tau) - 2Re(A_f^*\bar{A}_f)\sinh(yt/\tau) \}
\end{equation}
assuming $|q/p| = 1$ and arg$(q/p) = 0$. When $y$ (0.00645 in PDG20) is small, we have (up to the first order in Taylor expansion of $y$)
\begin{equation}
  \frac{d\Gamma(D^0)}{dt} + \frac{d\Gamma(\bar{D}^0)}{dt} =N_f e^{-t/\tau}(|A_f|^2 + |\bar{A}_f|^2) \{ 1 - \frac{2Re(A_f^*\bar{A}_f)}{(|A_f|^2 + |\bar{A}_f|^2)} yt/\tau \}.
\end{equation}
$F_+^{KK\pi\pi}$ is defined as
\begin{equation}
  F_+ \equiv \frac{\int |A_+|^2}{\int |A_+|^2 + |A_-|^2}, A_+ = A + \bar{A}, A_- = A - \bar{A},
\end{equation}
so we have
\begin{equation}
  F_+ = \frac{\int \{|A|^2 + |\bar{A}|^2 + 2Re(A\bar{A}^*)\}}{\int 2(|A|^2 + |A|^2)}.
\end{equation}
Thus,
\begin{equation}
  2 F_+ -1 = \frac{\int 2Re(A\bar{A}^*)}{\int (|A|^2 + |A|^2)}.
\end{equation}
$F_+$ is measured to be $(75.3 \pm 1.8 \pm 3.3 \pm 3.5)\%$~\inlinecite{dArgent:2017gzv}.

The impact of \Dz mixing is taken into account with pseudo-experiments.
In each pseudo-experiment,
the data distribution of \Dz mode is generated assuming a decay time distribution of \ref{eq:mixing},
with parameters of $y$ and $F_+$ taken its measured value.
Other components are generated according to the exponential distribution.
Then the nominal \chisq fit is performed to obtained the lifetime, i.e., 
no mixing is considered in the construction of \chisq.
An unbinned maximum likelihood fit with a Gaussian function is performed 
to the distribution of the obtained lifetime.
The difference of the mean of the Gaussian function w.r.t 
to the input lifetime in toy generation
is taken as the systematic uncertainty.
The resultant uncertainty is $0.81\fs$ for \Omegac and $0.56\fs$ for \Xicz.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_mixing_omegac.pdf}
  \includegraphics[width=0.45\linewidth]{ana_omegac/syst/toy_mixing_xicz.pdf}
  \caption{Distribution of the measured lifetime of (left) \Omegac and (right) \Xicz.}
  \label{fig:syst_dz_mixing}
\end{figure}
